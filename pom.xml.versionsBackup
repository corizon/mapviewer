<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>org.sonatype.oss</groupId>
        <artifactId>oss-parent</artifactId>
        <version>9</version>
    </parent>

    <artifactId>mapviewer</artifactId>
    <groupId>nl.bebr</groupId>
    <version>1.5-SNAPSHOT</version>
    <packaging>pom</packaging>
    <name>mapviewer</name>
    <description />

    <modules>
        <module>mapviewer-api</module>
        <module>mapviewer-data</module>
        <module>mapviewer-swing</module>
        <module>mapviewer-nb-swing</module>
        <module>mapviewer-javafx</module>
        <module>mapviewer-demo-swing</module>
        <module>mapviewer-demo-javafx</module>
        
        <module>mapviewer-weather</module>
    </modules>

    <repositories>
        
        <repository>
            <id>netbeans</id>
            <name>NetBeans</name>
            <url>http://netbeans.apidesign.org/maven2/</url>
        </repository>

        <repository>
            <id>bxd-snapshots</id>
            <name>bxd-snapshots</name>
            <url>https://nexus.admin.bebr.nl/repository/maven-snapshots/</url>
            <releases>
                <enabled>false</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>            
        </repository>
        
        <repository>
            <id>bxd-releases</id>
            <name>bxd-releases</name>
            <url>https://nexus.admin.bebr.nl/repository/maven-releases/</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>            
        </repository>

        <repository>
            <id>osgeo</id>
            <name>OSGeo Release Repository</name>
            <url>https://repo.osgeo.org/repository/release/</url>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
            <releases>
                <enabled>true</enabled>
            </releases>
        </repository>
        <repository>
            <id>osgeo-snapshot</id>
            <name>OSGeo Snapshot Repository</name>
            <url>https://repo.osgeo.org/repository/snapshot/</url>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
            <releases>
                <enabled>false</enabled>
            </releases>
        </repository>
        <!--repository>
            <id>maven2-repository.dev.java.net</id>
            <name>Java.net repository</name>
            <url>http://download.java.net/maven/2</url>
        </repository>
        <repository>
            <id>osgeo</id>
            <name>Open Source Geospatial Foundation Repository</name>
            <url>https://download.osgeo.org/webdav/geotools/</url>
        </repository>
        <repository>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
            <id>boundless</id>
            <name>Boundless Maven Repository</name>
            <url>https://repo.boundlessgeo.com/main/</url>
        </repository-->

    </repositories>

    <organization>
        <name>AgroSense</name>
        <url>http://www.agrosense.eu</url>
    </organization>

    <scm>
        <connection>scm:hg:ssh://hg@bitbucket.org/limetri/mapviewer</connection>
        <developerConnection>scm:hg:ssh://hg@bitbucket.org/limetri/mapviewer</developerConnection>
        <url>https://bitbucket.org/limetri/mapviewer</url>
        <tag>HEAD</tag>
    </scm>

    <licenses>
        <license>
            <name>gpl30</name>
            <url>http://www.gnu.org/licenses/gpl.txt</url>
        </license>
    </licenses>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <netbeans.version>RELEASE126</netbeans.version>
        <netbeans.hint.license>agrosense</netbeans.hint.license>
        
        <eventbus.version>1.0.3-NB80</eventbus.version>
        <lib-beansbinding.version>1.2.2-NB80</lib-beansbinding.version>
        <lib-common.version>1.2.2-NB80</lib-common.version>
        <lib-geotools.version>10.3-NB80</lib-geotools.version>
        <lib-miglayout.version>4.2.4-NB80</lib-miglayout.version>
        <lib-swingx.version>1.6.2-NB80-NO-WS</lib-swingx.version>
        <limetri.api.version>0.3.3-NB80</limetri.api.version>

        <commons-logging.version>1.1.1</commons-logging.version>
        <junit.version>4.11</junit.version>
        <mockito.version>1.9.5</mockito.version>
        <uispec4j.version>2.4</uispec4j.version>

    </properties>

    <dependencyManagement>
        <dependencies>

            <dependency>
                <groupId>nl.cloudfarming.eventbus</groupId>
                <artifactId>cloudfarming-eventbus</artifactId>
                <version>${eventbus.version}</version>
            </dependency>

            <dependency>
                <groupId>nl.cloudfarming.client</groupId>
                <artifactId>lib-swingx</artifactId>
                <version>${lib-swingx.version}</version>
            </dependency>

            <dependency>
                <groupId>junit</groupId>
                <artifactId>junit</artifactId>
                <scope>test</scope>
                <version>${junit.version}</version>
            </dependency>

            <dependency>
                <groupId>nl.cloudfarming.client</groupId>
                <artifactId>lib-beansbinding</artifactId>
                <version>${lib-beansbinding.version}</version>
            </dependency>

            <dependency>
                <groupId>org.mockito</groupId>
                <artifactId>mockito-all</artifactId>
                <version>${mockito.version}</version>
                <scope>test</scope>
            </dependency>

            <dependency>
                <groupId>nl.cloudfarming.client</groupId>
                <artifactId>lib-common</artifactId>
                <version>${lib-common.version}</version>
            </dependency>

            <dependency>
                <groupId>commons-logging</groupId>
                <artifactId>commons-logging</artifactId>
                <version>${commons-logging.version}</version>
            </dependency>

            <dependency>
                <artifactId>org-openide-filesystems</artifactId>
                <groupId>org.netbeans.api</groupId>
                <version>${netbeans.version}</version>
            </dependency>

            <dependency>
                <groupId>org.netbeans.modules</groupId>
                <artifactId>org-netbeans-modules-masterfs</artifactId>
                <version>${netbeans.version}</version>
            </dependency>

            <dependency>
                <artifactId>org-openide-util-lookup</artifactId>
                <groupId>org.netbeans.api</groupId>
                <version>${netbeans.version}</version>
            </dependency>

            <dependency>
                <groupId>org.netbeans.api</groupId>
                <artifactId>org-netbeans-modules-nbjunit</artifactId>
                <version>${netbeans.version}</version>
            </dependency>

            <dependency>
                <groupId>org.netbeans.api</groupId>
                <artifactId>org-openide-modules</artifactId>
                <version>${netbeans.version}</version>
            </dependency>

            <dependency>
                <groupId>org.netbeans.api</groupId>
                <artifactId>org-openide-util</artifactId>
                <version>${netbeans.version}</version>
            </dependency>
            
            <dependency>
                <groupId>org.netbeans.api</groupId>
                <artifactId>org-openide-util-ui</artifactId>
                <version>${netbeans.version}</version>
            </dependency>

            <dependency>
                <groupId>org.netbeans.api</groupId>
                <artifactId>org-netbeans-api-visual</artifactId>
                <version>${netbeans.version}</version>
            </dependency>

            <dependency>
                <groupId>org.netbeans.api</groupId>
                <artifactId>org-openide-nodes</artifactId>
                <version>${netbeans.version}</version>
            </dependency>

            <dependency>
                <artifactId>org-netbeans-api-progress</artifactId>
                <groupId>org.netbeans.api</groupId>
                <version>${netbeans.version}</version>
            </dependency>

            <dependency>
                <artifactId>org-openide-windows</artifactId>
                <groupId>org.netbeans.api</groupId>
                <version>${netbeans.version}</version>
            </dependency>

            <dependency>
                <artifactId>org-openide-awt</artifactId>
                <groupId>org.netbeans.api</groupId>
                <version>${netbeans.version}</version>
            </dependency>

            <dependency>
                <artifactId>org-openide-explorer</artifactId>
                <groupId>org.netbeans.api</groupId>
                <version>${netbeans.version}</version>
            </dependency>

            <dependency>
                <groupId>org.netbeans.api</groupId>
                <artifactId>org-netbeans-modules-settings</artifactId>
                <version>${netbeans.version}</version>
            </dependency>

            <dependency>
                <groupId>org.netbeans.api</groupId>
                <artifactId>org-netbeans-core-multiview</artifactId>
                <version>${netbeans.version}</version>
            </dependency>

            <dependency>
                <groupId>org.netbeans.api</groupId>
                <artifactId>org-netbeans-api-annotations-common</artifactId>
                <version>${netbeans.version}</version>
            </dependency>

            <dependency>
                <groupId>org.uispec4j</groupId>
                <artifactId>uispec4j</artifactId>
                <version>${uispec4j.version}</version>
            </dependency>

            <dependency>
                <artifactId>org-openide-dialogs</artifactId>
                <groupId>org.netbeans.api</groupId>
                <version>${netbeans.version}</version>
            </dependency>

            <dependency>
                <artifactId>org-openide-actions</artifactId>
                <groupId>org.netbeans.api</groupId>
                <version>${netbeans.version}</version>
            </dependency>

            <dependency>
                <groupId>org.netbeans.api</groupId>
                <artifactId>org-netbeans-modules-options-api</artifactId>
                <version>${netbeans.version}</version>
            </dependency>

            <dependency>
                <groupId>org.netbeans.api</groupId>
                <artifactId>org-netbeans-modules-javahelp</artifactId>
                <version>${netbeans.version}</version>
            </dependency>

            <dependency>
                <groupId>eu.agrosense.client</groupId>
                <artifactId>lib-geotools</artifactId>
                <version>${lib-geotools.version}</version>
            </dependency>
            <dependency>
                <groupId>eu.agrosense.client</groupId>
                <artifactId>lib-miglayout</artifactId>
                <version>${lib-miglayout.version}</version>
            </dependency>
            
            <dependency>
                <groupId>eu.limetri.api</groupId>
                <artifactId>geo</artifactId>
                <version>${limetri.api.version}</version>
            </dependency>
            <dependency>
                <groupId>eu.limetri.api</groupId>
                <artifactId>grid</artifactId>
                <version>${limetri.api.version}</version>
            </dependency>

        </dependencies>
    </dependencyManagement>

    <distributionManagement>
        <snapshotRepository>
            <id>bxd-snapshots</id>
            <name>bxd-snapshots</name>
            <url>https://nexus.admin.bebr.nl/repository/maven-snapshots/</url>
        </snapshotRepository>
        <repository>
            <id>bxd-releases</id>
            <name>bxd-releases</name>
            <url>https://nexus.admin.bebr.nl/repository/maven-releases/</url>
        </repository>
    </distributionManagement>

    <profiles>
        <profile>
            <id>deploy-snapshot</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.sonatype.plugins</groupId>
                        <artifactId>nexus-staging-maven-plugin</artifactId>
                        <version>1.6.8</version>
                        <executions>
                            <execution>
                                <id>default-deploy</id>
                                <phase>deploy</phase>
                                <goals>
                                    <goal>deploy</goal>
                                </goals>
                            </execution>
                        </executions>
                        <configuration>
                            <serverId>bxd-snapshots</serverId>
                            <nexusUrl>https://nexus.admin.bebr.nl/repository/maven-snapshots/</nexusUrl>
                            <skipStaging>true</skipStaging>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
        <profile>
            <id>deploy-release</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.sonatype.plugins</groupId>
                        <artifactId>nexus-staging-maven-plugin</artifactId>
                        <version>1.6.8</version>
                        <executions>
                            <execution>
                                <id>default-deploy</id>
                                <phase>deploy</phase>
                                <goals>
                                    <goal>deploy</goal>
                                </goals>
                            </execution>
                        </executions>
                        <configuration>
                            <serverId>bxd-releases</serverId>
                            <nexusUrl>https://nexus.admin.bebr.nl/repository/maven-releases/</nexusUrl>
                            <skipStaging>true</skipStaging>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
        <profile>
            <id>cluster-mode</id>
            <activation>
                <property>
                    <name>with-dependencies</name>
                    <value>true</value>
                </property>
            </activation>
        </profile>
    </profiles>



    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>3.1</version>
                    <configuration>
                        <source>1.8</source>
                        <target>1.8</target>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>nbm-maven-plugin</artifactId>
                    <version>3.14</version>
                    <extensions>true</extensions>
                </plugin>                
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-jar-plugin</artifactId>
                    <version>2.4</version>
                    <configuration>
                        <!-- to have the jar plugin pickup the nbm generated manifest -->
                        <useDefaultManifestFile>true</useDefaultManifestFile>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-javadoc-plugin</artifactId>
                    <configuration>
                        <additionalparam>-Xdoclint:none</additionalparam>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>
        <plugins>
            <plugin>
                <artifactId>maven-scm-plugin</artifactId>
                <version>1.8.1</version>
                <configuration>
                    <tag>${project.artifactId}-${project.version}</tag>
                </configuration>
            </plugin>
            
            <plugin>
                <inherited>false</inherited>
                <groupId>com.mycila.maven-license-plugin</groupId>
                <artifactId>maven-license-plugin</artifactId>
                <version>1.10.b1</version>
                <configuration>
                    <header>${basedir}/etc/agrosense_gplv3.txt</header>
                    <validHeaders>
                        <validHeader>${basedir}/etc/netbeans_cddl_gplv2.txt</validHeader>
                        <validHeader>${basedir}/etc/netbeans_cddl_gplv2.txt</validHeader>
                    </validHeaders>
                    <failIfMissing>true</failIfMissing>
                    <aggregate>true</aggregate>
                    <excludes>
                        <exclude>etc/license-agrosense.txt</exclude>
                        <exclude>**/src/test/resources/**</exclude>
                        <exclude>**/src/test/data/**</exclude>
                        <exclude>**/resources/**</exclude>
                        <exclude>**/site/**</exclude>
                        <exclude>**/*.properties</exclude>
                        <exclude>**/*.xml</exclude>
                        <exclude>**/*.mf</exclude>
                        <exclude>**/*.vm</exclude>
                        <exclude>**/*.log</exclude>
                        <exclude>**/*.form</exclude>
                        <exclude>**/.hgtags</exclude>
                        <exclude>**/.hgignore</exclude>
                        <exclude>**/*.orig</exclude>
                        <exclude>**/*.CSV</exclude>
                        <exclude>**/*.script</exclude>
                        <exclude>**/menu/**</exclude>
                        <exclude>**/toelatingen.xsd</exclude>
                        <exclude>**/generated/**</exclude>
                        <exclude>**/xmlschema/**</exclude>
                        <exclude>**/translations/omegat/**</exclude>
                        <exclude>**/*.sh</exclude>
                    </excludes>
                </configuration>
                <executions>
                    <execution>
                        <id>check-headers</id>
                        <phase>verify</phase>
                        <goals>
                            <goal>check</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-release-plugin</artifactId>
                <version>2.4.1</version>
                <configuration>
                    <pushChanges>false</pushChanges>
                </configuration>
            </plugin>
        </plugins>
    </build>

</project>
