/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.data;

/**
 * An immutable coordinate in the real (geographic) world, 
 * composed of a latitude and a longitude.
 * @author rbair
 */
public class GeoPosition {
    private double latitude;
    private double longitude;
    
    private static final int MAXITERS = 20;
    private static final double MAJOR_AXIS = 6378137.0; // WGS84 major axis
    private static final double SEMI_MAJOR_AXIS = 6356752.3142; // WGS84 semi-major axis
    private static final double F = (MAJOR_AXIS - SEMI_MAJOR_AXIS) / MAJOR_AXIS;
    private static final double aSqMinusBSqOverBSq = (MAJOR_AXIS * MAJOR_AXIS - SEMI_MAJOR_AXIS * SEMI_MAJOR_AXIS) / (SEMI_MAJOR_AXIS * SEMI_MAJOR_AXIS);
        
    /**
     * Creates a new instance of GeoPosition from the specified
     * latitude and longitude. These are double values in decimal degrees, not
     * degrees, minutes, and seconds.  Use the other constructor for those.
     * @param latitude a latitude value in decmial degrees
     * @param longitude a longitude value in decimal degrees
     */
    public GeoPosition(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
    // must be an array of length two containing lat then long in that order.
    /**
     * Creates a new instance of GeoPosition from the specified
     * latitude and longitude as an array of two doubles, with the
     * latitude first. These are double values in decimal degrees, not
     * degrees, minutes, and seconds.  Use the other constructor for those.
     * @param coords latitude and longitude as a double array of length two
     */
    public GeoPosition(double [] coords) {
        this.latitude = coords[0];
        this.longitude = coords[1];
    }
    
    /**
     * Creates a new instance of GeoPosition from the specified
     * latitude and longitude. 
     * Each are specified as degrees, minutes, and seconds; not
     * as decimal degrees. Use the other constructor for those.
     * @param latDegrees the degrees part of the current latitude
     * @param latMinutes the minutes part of the current latitude
     * @param latSeconds the seconds part of the current latitude
     * @param lonDegrees the degrees part of the current longitude
     * @param lonMinutes the minutes part of the current longitude
     * @param lonSeconds the seconds part of the current longitude
     */
    public GeoPosition(double latDegrees, double latMinutes, double latSeconds,
            double lonDegrees, double lonMinutes, double lonSeconds) {
        this(latDegrees + (latMinutes + latSeconds/60.0)/60.0,
             lonDegrees + (lonMinutes + lonSeconds/60.0)/60.0);
    }
    
    /**
     * Get the latitude as decimal degrees
     * @return the latitude as decimal degrees
     */
    public double getLatitude() {
        return latitude;
    }
    
    /**
     * Get the longitude as decimal degrees
     * @return the longitude as decimal degrees
     */
    public double getLongitude() {
        return longitude;
    }
    
    /**
     * Returns true the specified GeoPosition and this GeoPosition represent
     * the exact same latitude and longitude coordinates.
     * @param obj a GeoPosition to compare this GeoPosition to
     * @return returns true if the specified GeoPosition is equal to this one
     */
    public boolean equals(Object obj) {
        if (obj instanceof GeoPosition) {
            GeoPosition coord = (GeoPosition)obj;
            return coord.latitude == latitude && coord.longitude == longitude;
        }
        return false;
    }
    
    /**
     * {@inheritDoc}
     * @return 
     */
    public String toString() {
        return "[" + latitude + ", " + longitude + "]";
    }
    
    public float distanceTo(GeoPosition geoPosition) {
        return distanceTo(geoPosition.getLatitude(), geoPosition.getLongitude());
    }
    
    public float distanceTo(double latitudeTo, double longitudeTo) {
        // Based on android.location.Location.java
    	// Based on http://www.ngs.noaa.gov/PUBS_LIB/inverse.pdf
        // using the "Inverse Formula" (section 4)

        // Convert lat/long to radians
        double lat1 = Math.toRadians(latitude); 
        double lon1 = Math.toRadians(longitude);
        
        double lat2 = Math.toRadians(latitudeTo);
        double lon2 = Math.toRadians(longitudeTo);

        double L = lon2 - lon1;
        double A = 0.0;
        double U1 = Math.atan((1.0 - F) * Math.tan(lat1));
        double U2 = Math.atan((1.0 - F) * Math.tan(lat2));

        double cosU1 = Math.cos(U1);
        double cosU2 = Math.cos(U2);
        double sinU1 = Math.sin(U1);
        double sinU2 = Math.sin(U2);
        double cosU1cosU2 = cosU1 * cosU2;
        double sinU1sinU2 = sinU1 * sinU2;

        double sigma = 0.0;
        double deltaSigma = 0.0;
        double cosSqAlpha;
        double cos2SM;
        double cosSigma;
        double sinSigma;
        double cosLambda;
        double sinLambda;

        double lambda = L; // initial guess
        for (int iter = 0; iter < MAXITERS; iter++) {
            double lambdaOrig = lambda;
            cosLambda = Math.cos(lambda);
            sinLambda = Math.sin(lambda);
            double t1 = cosU2 * sinLambda;
            double t2 = cosU1 * sinU2 - sinU1 * cosU2 * cosLambda;
            double sinSqSigma = t1 * t1 + t2 * t2; // (14)
            sinSigma = Math.sqrt(sinSqSigma);
            cosSigma = sinU1sinU2 + cosU1cosU2 * cosLambda; // (15)
            sigma = Math.atan2(sinSigma, cosSigma); // (16)
            double sinAlpha = (sinSigma == 0) ? 0.0 :
                cosU1cosU2 * sinLambda / sinSigma; // (17)
            cosSqAlpha = 1.0 - sinAlpha * sinAlpha;
            cos2SM = (cosSqAlpha == 0) ? 0.0 :
                cosSigma - 2.0 * sinU1sinU2 / cosSqAlpha; // (18)

            double uSquared = cosSqAlpha * aSqMinusBSqOverBSq; // defn
            A = 1 + (uSquared / 16384.0) * // (3)
                (4096.0 + uSquared *
                 (-768 + uSquared * (320.0 - 175.0 * uSquared)));
            double B = (uSquared / 1024.0) * // (4)
                (256.0 + uSquared *
                 (-128.0 + uSquared * (74.0 - 47.0 * uSquared)));
            double C = (F / 16.0) *
                cosSqAlpha *
                (4.0 + F * (4.0 - 3.0 * cosSqAlpha)); // (10)
            double cos2SMSq = cos2SM * cos2SM;
            deltaSigma = B * sinSigma * // (6)
                (cos2SM + (B / 4.0) *
                 (cosSigma * (-1.0 + 2.0 * cos2SMSq) -
                  (B / 6.0) * cos2SM *
                  (-3.0 + 4.0 * sinSigma * sinSigma) *
                  (-3.0 + 4.0 * cos2SMSq)));

            lambda = L +
                (1.0 - C) * F * sinAlpha *
                (sigma + C * sinSigma *
                 (cos2SM + C * cosSigma *
                  (-1.0 + 2.0 * cos2SM * cos2SM))); // (11)

            double delta = (lambda - lambdaOrig) / lambda;
            if (Math.abs(delta) < 1.0e-12) {
                break;
            }
        }

        float distance = (float) (SEMI_MAJOR_AXIS * A * (sigma - deltaSigma));
        return distance;
    }
    
}