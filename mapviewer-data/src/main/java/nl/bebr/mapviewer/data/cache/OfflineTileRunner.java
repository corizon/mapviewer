/**
 * Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 * AgroSense is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the FLOSS License Exception
 * <http://www.agrosense.eu/foss-exception.html>.
 *
 * AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.data.cache;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.NoRouteToHostException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.SwingUtilities;

import nl.bebr.mapviewer.data.Tile;
import nl.bebr.mapviewer.data.TileCache;
import nl.bebr.mapviewer.data.cache.spi.CacheManager;
import nl.bebr.mapviewer.data.util.ConnectionChecker;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.HttpsURLConnection;

/**
 * Runner does the loading.
 *
 * <p>
 * First we check memory cache. If failed, disk cache is searched. <br> If also
 * unsuccessful and online, download tile from openstreetmap server <br>
 * Otherwise create offline Tile <br> </p>
 *
 * @author Frantisek Post
 */
public abstract class OfflineTileRunner<T> implements Runnable {

    private static final Logger LOG = Logger.getLogger(OfflineTileRunner.class.getName());
    private OfflineTileFactory<T> tileFactory;
    private TileCache<T> tileCache;
    private ConnectionChecker connectionChecker;
    private boolean running = true;
    private boolean persistent; //specifies whether tiles should be written to file

    /**
     * Constructor
     *
     * @param tileFactory
     */
    public OfflineTileRunner(OfflineTileFactory<T> tileFactory) {
        this.tileFactory = tileFactory;
        connectionChecker = ConnectionChecker.getInstance();
        tileCache = tileFactory.getTileCache();
        persistent = tileFactory.getInfo().isPersistent();
    }

    protected URI getURI(Tile<T> tile) throws URISyntaxException {
        if (tile.getURL() == null) {
            return null;
        }
        return new URI(tile.getURL());
    }

    protected abstract T readTileFromRepository(TileCacheInfo info);

    private void loadOneTile(final OfflineTile<T> tile) {
        /*
         * 3 strikes and you're out. Attempt to load the url. If it fails,
         * decrement the number of tries left and try again. Log failures.
         * If I run out of try s just get out. This way, if there is some
         * kind of serious failure, I can get out and let other tiles
         * try to load.
         */
        int trys = 3;
        while (!tile.isLoaded() && trys > 0) {
            try {
                T img;
                URI uri = getURI(tile);
                img = tileCache.get(uri);
                if (img == null) {
                    TileCacheInfo info = tileFactory.getInfo().getCacheInfo(uri);

                    if (info != null && CacheManager.getInstance().isCached(info)) {
//                    	img = TileRepository.getInstance().getTile(info);
                        img = readTileFromRepository(info);
                        // byte[] bimg = imageToByteArray(img);
                        tileCache.put(uri, img);
                        img = tileCache.get(uri);
                    } else if (connectionChecker.isOnline()) {
                        img = loadImage(uri);
                        if (img != null) {
                            tileCache.put(uri, img);
                            img = tileCache.get(uri);
                            if (persistent) {
                                getTileRepository().writeTile(info, img);
                            }
                        }
                    } else {
                        offlineTile(tile);
                    }

                }
                if (img == null) {
                    trys--;
                } else {
                    final T i = img;
                    SwingUtilities.invokeAndWait(new Runnable() {
                        @Override
                        public void run() {
                            tile.setImage(new SoftReference<>(i));
                            tile.setLoaded(true);
                        }
                    });
                }
            } catch (OutOfMemoryError memErr) {
                tileCache.needMoreMemory();
            } catch (UnknownHostException | NoRouteToHostException | ConnectException ex) {
                offlineTile(tile);
            } catch (Throwable e) {
                LOG.log(Level.SEVERE, "Failed to load a tile at url: " + tile.getURL() + ", retrying", e);

                Object oldError = tile.getError();
                tile.setError(e);
                tile.firePropertyChangeOnEDT("loadingError", oldError, e);
                if (trys == 0) {
                    tile.firePropertyChangeOnEDT("unrecoverableError", null, e);
                } else {
                    trys--;
                }
            }
        }
        tile.setLoading(false);
    }

    @Override
    public void run() {
        while (running) {
            try {
                BlockingQueue<Tile<T>> queue = tileFactory.getTileQueue();
                if (queue != null) {
                    final OfflineTile<T> tile = (OfflineTile<T>) queue.poll(1, TimeUnit.DAYS); //TODO how big the timeout
                    if (tile != null && running) {
                        loadOneTile(tile);
                    }
                } else {
                    // FIXME:
                    // Threads are started from the AbstractTileFactory constructor before a queue is initialized in subclass OfflineTileFactorySwing;
                    // this fixes itself within a few iterations when initialization completes.
                    // This issue does not (yet) explain spinning tile-pool threads, however, just the presence of the log line below does seem to fix it.
                    LOG.warning("Tile queue is null");
                }
            } catch (Throwable ie) {
                LOG.log(Level.WARNING, "Error during picking tile from queue ", ie);
            }
        }
    }

    private void offlineTile(OfflineTile<T> tile) {
        tile.createOfflineImage();
        tile.setLoaded(true);
        tile.setLoading(false);
    }

    protected byte[] cacheInputStream(URL url) throws IOException {
        //TODO FP catch java.io.IOException when Server returns HTTP response code: 50x	
        try {
            URLConnection conn = url.openConnection();
            conn.setRequestProperty("User-Agent", "MapViewer / 1.5.0");
            InputStream ins = conn.getInputStream();
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            byte[] buf = new byte[256];
            while (true) {
                int n = ins.read(buf);
                if (n == -1) {
                    break;
                }
                bout.write(buf, 0, n);
            }
            return bout.toByteArray();
        } catch (FileNotFoundException ex) {
            //TODO log
            return null;
        }
    }

    /**
     * Method to load image from given {@link URI}
     *
     * @param uri tile uri
     * @return image
     *
     * @throws MalformedURLException
     * @throws IOException
     */
    public abstract T loadImage(URI uri) throws MalformedURLException, IOException;

    /**
     * Method to get specific {@link TileRepository}
     *
     * @return
     */
    public abstract TileRepository<T> getTileRepository();

    public void stop() {
        running = false;
    }
}
