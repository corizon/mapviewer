/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.data;

import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.logging.Level;
import java.util.logging.Logger;

import nl.bebr.mapviewer.data.cache.OfflineTileRunner;
import nl.bebr.mapviewer.data.util.GeoUtil;

/**
 * The <code>AbstractTileFactory</code> provides a basic implementation for the TileFactory.
 * 
 * @author Frantisek Post
 */
public abstract class AbstractTileFactory<T, U extends Tile<T>> extends TileFactory<T, U> {

    private static final Logger LOG = Logger.getLogger(AbstractTileFactory.class.getName());
    private static final int POOL_SIZE = 5;

    private ExecutorService service;    
    private TileCache<T> cache;
    private Runnable[] tileRunner;

    /**
     * Creates a new instance of DefaultTileFactory using the spcified TileFactoryInfo
     * @param info a TileFactoryInfo to configure this TileFactory
     */
    public AbstractTileFactory(TileFactoryInfo info) {
        super(info);
        init();
    }
    
    private void init() {
        cache = createTileCache();
        tileRunner = new Runnable[POOL_SIZE];
        for (int i = 0; i < POOL_SIZE; i++) {
        	tileRunner[i] = createTileRunner();
        	getService().submit(tileRunner[i]);
        }
    }
    
    /**
     * Returns
     * @param pixelCoordinate
     * @return
     */
    //public TilePoint getTileCoordinate(Point2D pixelCoordinate) {
    //    return GeoUtil.getTileCoordinate(pixelCoordinate, getInfo());
    //}
    
    /**
     * Returns the tile that is located at the given tilePoint for this zoom. For example,
     * if getMapSize() returns 10x20 for this zoom, and the tilePoint is (3,5), then the
     * appropriate tile will be located and returned.
     * @param tilePoint
     * @param zoom
     * @return
     */
    @Override
    public U getTile(int x, int y, int zoom) {
        return getTile(x, y , zoom, true);
    }
    
    
    private U getTile(int tpx, int tpy, int zoom, boolean eagerLoad) {
        //wrap the tiles horizontally --> mod the X with the max width
        //and use that
        int tileX = tpx;//tilePoint.getX();
        int numTilesWide = (int)getMapSize(zoom).getWidth();
        if (tileX < 0) {
            tileX = numTilesWide - (Math.abs(tileX)  % numTilesWide);
        }
        
        tileX = tileX % numTilesWide;
        int tileY = tpy;
        //TilePoint tilePoint = new TilePoint(tileX, tpy);
        String url = getInfo().getTileUrl(tileX, tileY, zoom);//tilePoint);
        
        U tile;
        if (!getTileMap().containsKey(url)) {
            if (!GeoUtil.isValidTile(tileX, tileY, zoom, getInfo())) {
                tile = createTile(tileX, tileY, zoom, null, this);
            }  else {
                tile = createTile(tileX, tileY, zoom, url, this);
                startLoading(tile);
            }
            getTileMap().put(url,tile);
        }  else {
            tile = getTileMap().get(url);
            // if its in the map but is low and isn't loaded yet
            // but we are in high mode
            if (tile.getPriority()  == Tile.Priority.Low && eagerLoad && !tile.isLoaded()) {
                promote(tile);
            }
        }
        
        return tile;
    }
    
    public TileCache<T> getTileCache() {
        return cache;
    }
    
    public void setTileCache(TileCache<T> cache) {
        this.cache = cache;
    }
    
    /**
     * Subclasses may override this method to provide their own executor services. This 
     * method will be called each time a tile needs to be loaded. Implementations should 
     * cache the ExecutorService when possible.
     * @return ExecutorService to load tiles with
     */
    protected synchronized ExecutorService getService() {
        if(service == null) {
            service = Executors.newFixedThreadPool(POOL_SIZE, new ThreadFactory() {
                private int count = 0;
                
                @Override
                public Thread newThread(Runnable r) {
                    Thread t = new Thread(r, "tile-pool-" + count++);
                    t.setPriority(Thread.MIN_PRIORITY);
                    t.setDaemon(true);
                    return t;
                }
            });
        }
        return service;
    }
    
    
    
    @Override
    protected synchronized void startLoading(U tile) {
        if(tile.isLoading()) {
            return;
        }
        tile.setLoading(true);
        try {
            getTileQueue().put(tile);
        } catch (Exception ex) {
        	LOG.log(Level.FINE, "error during load start", ex);
        }
    }
    
    /**
     * Subclasses can override this if they need custom TileRunners for some reason
     * @return
     */
    protected abstract Runnable createTileRunner();
    
    
    /**
     * Increase the priority of this tile so it will be loaded sooner.
     */
    public synchronized void promote(U tile) {
        if(getTileQueue().contains(tile)) {
            try {
            	getTileQueue().remove(tile);
                tile.setPriority(Tile.Priority.High);
                getTileQueue().put(tile);
            } catch (Exception ex) {
            	LOG.log(Level.FINE, "error while promoting", ex);
            }
        }
    }
    
    /**
     * gets tileQueue which should be static.<br>
     * target implementation should provide static tileQueue
     */
    public abstract BlockingQueue<U> getTileQueue();
    
    public abstract TileCache<T> createTileCache();
    
    public abstract U createTile(int x, int y, int zoom, String url, AbstractTileFactory<T, U> tileFactory);
    
    /**
     * gets tileMap which should be static.<br>
     * target implementation should provide static tileMap
     */
    protected abstract Map<String, U> getTileMap();
    
    @Override
    public void clearQueueAndStopLoading() {
    	for (int i = 0; i < POOL_SIZE; i++) {
    		if (tileRunner[i] instanceof OfflineTileRunner) {
            	((OfflineTileRunner) tileRunner[i]).stop();
        	}
    	}
        getService().shutdown();
        getTileQueue().clear();
        cache.clear();
    }
}
