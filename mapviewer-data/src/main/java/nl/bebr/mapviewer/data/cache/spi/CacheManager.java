/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.data.cache.spi;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

import nl.bebr.mapviewer.data.cache.TileCacheInfo;

/**
 * CacheManager holds records for all cached (serialised) tiles.
 *
 * <p> During {@link #refresh()} scans cache directory and save tiles records by
 * zoom and coordinates. </p>
 *
 * @author Frantisek Post
 */

public class CacheManager {

    
    private static final String CACHE_ROOT_DIR = "tileCache";
    private static final Pattern DIR_PATTERN = Pattern.compile("\\d*");
    private static final Pattern CACHE_FILE_PATTERN = Pattern.compile("\\d*x\\d*");
    private static final CacheManager instance = new CacheManager();
    private static final Logger LOGGER = Logger.getLogger(CacheManager.class.getName()); 
    private final String userHome;
    
    private String mapTypeFolder = "OpenStreetMap";
    private int zoom = 7;
    
    //for faster searching - topmost key is map type, next key is zoom level, value is map. In that map, key is X coord. and value is list of Y coords.
    //TODO check memory cost
    private Map<Integer, Map<Integer, List<Integer>>> zoomMap;
    
    private long cacheSizeSum = 0;

    protected CacheManager() {
    	zoomMap = new HashMap<>();
        userHome = System.getProperty("user.home") + File.separatorChar + ".agrosense";
        refresh();
    }

    /**
     * Gets instance
     * @return
     */
    public static CacheManager getInstance() {
        return instance;
    }

    /**
     * Refreshes tile cache information
     */
    public void refresh() {
    	zoomMap.clear();
        cacheSizeSum = 0;
        FileObject cacheFolder = getCacheFolder();
        if (cacheFolder != null) {
            FileObject[] mapTypeFolders = cacheFolder.getChildren();
            
            for (FileObject child : mapTypeFolders) {
                if (child.isValid() && child.isFolder() && child.getName().equals(mapTypeFolder)) { 
                	processMapType(child);
                }
            }
        }
    }
    
    private void processMapType(FileObject mapTypeFolder) {
    	
    	FileObject[] zoomFolders = mapTypeFolder.getChildren();
        for (FileObject child : zoomFolders) {
        	boolean zoomIsRight = DIR_PATTERN.matcher(child.getName()).matches() && (Integer.parseInt(child.getName()) == zoom || Integer.parseInt(child.getName()) == zoom + 1); //TODO factory is preloading some tiles with zoom+1 level?
            if (child.isValid() && child.isFolder() &&  zoomIsRight) { 
                proccessFolder(child);
            }
        }
    }

    /**
     * Method to check, if image exists for tile defined by its coordinates and zoom level.
     * 
     * @param zoom zoom level
     * @param x x coordinate
     * @param y y coordinate
     * 
     * @return {@code true} if image is presented, {@code false} otherwise
     */
    public boolean isCached(TileCacheInfo tileCacheInfo) {
    	
    	int zoom = tileCacheInfo.getZoom();
    	int x = tileCacheInfo.getX();
    	int y = tileCacheInfo.getY();
    	
			Map<Integer, List<Integer>> coordMap = zoomMap.get(zoom);
			if (coordMap != null) {
				List<Integer> yCoordList = coordMap.get(x);
				if (yCoordList != null) {
					return yCoordList.contains(y);
				} else {
					return false;
				}
			} else {
				return false;
			}
		
    }

    protected void proccessFolder(FileObject folder) {
        int zoom = Integer.parseInt(folder.getName());
        FileObject[] children = folder.getChildren();
        for (FileObject child : children) {
            if (child.isData() && CACHE_FILE_PATTERN.matcher(child.getName()).matches()) { 
                proccessFile(zoom, child);
            }
        }
    }

    protected void proccessFile(int zoom, FileObject file) {
        String name = file.getName();
        String[] coords = name.split("x");
        if (coords != null && coords.length == 2) {
            int x = Integer.parseInt(coords[0]);
            int y = Integer.parseInt(coords[1]);
            addRecord(zoom, x, y);
        }
    }

    protected void addFileSize(FileObject file) {
        cacheSizeSum += file.getSize();
    }

    protected void addRecord(int zoom, int x, int y) {
        Map<Integer, List<Integer>> map = zoomMap.get(zoom);
        if (map == null) {
        	map = new HashMap<>();
            zoomMap.put(zoom, map);
        }
        List<Integer> yCoords = map.get(x);
        if (yCoords == null) {
            yCoords = new ArrayList<>();
            map.put(x, yCoords);
        }
        yCoords.add(y);
    }

    /**
     * Gets maximum cache size on files
     * @return
     */
    public long getCacheSize() {
        return cacheSizeSum;
    }
    
    public FileObject getCacheFolder() {
    	FileObject configFolder = FileUtil.getConfigRoot(); //init value
    	FileObject configRoot = null;
		try {
			configRoot = FileUtil.createFolder(new File(userHome));
		} catch (IOException e) {
			LOGGER.log(Level.FINE, "Exception while getting root folder.", e);
		}
        
		if (configRoot != null) {
			configFolder = configRoot.getFileObject(CACHE_ROOT_DIR);
			if (null == configFolder) {
				try {
					configFolder = configRoot.createFolder(CACHE_ROOT_DIR);
				} catch (IOException ex) {
					LOGGER.log(Level.FINE,
							"Exception while getting tile chache folder.", ex);
				}
			}
		}
        return configFolder;
    }
    
    public void setZoomLevel(int zoom) {
    	int oldZoom = this.zoom;
    	
    	this.zoom = zoom;
    	if (zoom != oldZoom) {
    		refresh();
    	}
    }
    
    public void setMapTypeFolder(String mapTypeFolder) {
    	String oldMapTypeFolder = this.mapTypeFolder;
    	
    	this.mapTypeFolder = mapTypeFolder;
    	if (!oldMapTypeFolder.equals(mapTypeFolder)) {
    		refresh();
    	}
    }
    
}
