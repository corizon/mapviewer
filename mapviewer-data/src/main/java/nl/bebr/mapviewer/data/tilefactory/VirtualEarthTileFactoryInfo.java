/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.data.tilefactory;

import java.net.URI;


import nl.bebr.mapviewer.data.TileFactoryInfo;
import nl.bebr.mapviewer.data.cache.TileCacheInfo;


/*******************************************************************************
* http://www.viavirtualearth.com/vve/Articles/RollYourOwnTileServer.ashx
* @author Fabrizio Giudici
* @version $Id: MicrosoftVirtualEarthProvider.java 115 2007-11-08 22:04:36Z
* fabriziogiudici $
********************************************************************************/
public class VirtualEarthTileFactoryInfo extends TileFactoryInfo
{
	/**
	 * Use road map
	 */
	public final static MVEMode MAP = new MVEMode("Map", "map", "r", ".png");

	/**
	 * Use satellite map
	 */
	public final static MVEMode SATELLITE = new MVEMode("Satellite", "satellite", "a", ".jpeg");

	/**
	 * Use hybrid map
	 */
	public final static MVEMode HYBRID = new MVEMode("Hybrid", "hybrid", "h", ".jpeg");

	/**
	 * The map mode
	 */
	public static class MVEMode
	{
		private String type;
		private String ext;
		private String name;
		private String label;

		private MVEMode(final String name, final String label, final String type, final String ext)
		{
			this.type = type;
			this.ext = ext;
			this.name = name;
			this.label = label;
		}
	}

	private final static int TOP_ZOOM_LEVEL = 19;

	private final static int MAX_ZOOM_LEVEL = 17;

	private final static int MIN_ZOOM_LEVEL = 1;

	private final static int TILE_SIZE = 256;

	private MVEMode mode;

	/**
	 * @param mode the mode
	 */
	public VirtualEarthTileFactoryInfo(MVEMode mode)
	{
		super("VirtualEarth"+mode.name, MIN_ZOOM_LEVEL, MAX_ZOOM_LEVEL, TOP_ZOOM_LEVEL, TILE_SIZE, false, false, "", "", "", "");
		
		this.mode = mode;
	}

	/**
	 * @return the name of the selected mode
	 */
	public String getModeName()
	{
		return mode.name;
	}
	
	/**
	 * @return the label of the selected mode
	 */
	public String getModeLabel()
	{
		return mode.label;
	}
	
	@Override
	public String getTileUrl(final int x, final int y, final int zoom)
	{
		final String quad = tileToQuadKey(x, y, recalculateZoom(zoom));
		return "http://" + mode.type + quad.charAt(quad.length() - 1) + 
				".ortho.tiles.virtualearth.net/tiles/"
				+ mode.type + quad + mode.ext + "?g=1";
	}

	private String tileToQuadKey(final int tx, final int ty, final int zl)
	{
		String quad = "";

		for (int i = zl; i > 0; i--)
		{
			int mask = 1 << (i - 1);
			int cell = 0;

			if ((tx & mask) != 0)
			{
				cell++;
			}

			if ((ty & mask) != 0)
			{
				cell += 2;
			}

			quad += cell;
		}

		return quad;
	}
	
	@Override
	public synchronized TileCacheInfo getCacheInfo(URI uri) {
		String text = uri.getPath();
		String tileUri = text.substring(8, text.indexOf("."));
		
		int shift = 0;
		int x = 0;
		int y = 0;
		int zoom = tileUri.length();
		for (int i = tileUri.length()-1; i >= 0; i--) {
			char ch = tileUri.charAt(i);
			int mask = 1 << shift;
			if ('1' == ch) {
				x = x | mask;
			} else if ('2' == ch) {
				y = y | mask;
			} else if ('3' == ch) {
				x = x | mask;
				y = y | mask;
			}
			
			shift++;
		}
		zoom = recalculateZoom(zoom);
		TileCacheInfo tileCacheInfo = new TileCacheInfo(zoom, x, y);
		tileCacheInfo.setExtension(mode.ext.substring(1));
		tileCacheInfo.setType(getName());
		return tileCacheInfo;
	}
	
	@Override
	protected int recalculateZoom(int zoom) {
		return TOP_ZOOM_LEVEL - zoom;
	}
	
}

