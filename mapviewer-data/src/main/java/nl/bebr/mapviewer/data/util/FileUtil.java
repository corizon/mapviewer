/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.data.util;

import java.io.File;
import java.io.IOException;
import org.openide.filesystems.FileObject;

/**
 *
 * @author Frantisek Post
 */
public class FileUtil {
    
     /**
     * Touching file changes its lastModified date record.
     * <p>
     * Works only for existing files (not directories)
     * </p>
     * @param file
     * @return {@code true} if success, {@false} is touch failed.
     * 
     * 
     */
	//TODO optimization and solving problem when sorting - put files to set and with some size of this set, touch all files at once.
	//in separate thread.
    public static boolean touch(File file) throws IOException {
        if (file != null && file.exists() && file.isFile()) {
            return file.setLastModified(System.currentTimeMillis());
        } else {
            return false;
        }
    }
    
    /**
     * Touch the fileObject
     * 
     * @param fileObject
     * @return
     * @throws IOException
     */
    public static boolean touch(FileObject fileObject) throws IOException {
        return touch(org.openide.filesystems.FileUtil.toFile(fileObject));
    } 
    
}
