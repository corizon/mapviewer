/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.data.cache;

import java.lang.ref.SoftReference;

import nl.bebr.mapviewer.data.Tile;

/**
 * Offline version of {@link Tile}.
 * <br>
 * Call of method {{@link #createOfflineImage()} creates default offline image and sets the Tile as offline
 *
 * @author Frantisek Post
 */
public abstract class OfflineTile<T> extends Tile<T> {

    protected static final int TILE_SIZE = 256;
    
    private boolean offline = false;

    /**
     * Constructor
     * 
     * @param x x coordinate
     * @param y z coordinate 
     * @param zoom zoom level
     * @param url url of the tile
     * @param tileFactory tilefactory
     */
    public OfflineTile(int x, int y, int zoom, String url, OfflineTileFactory<T> tileFactory) {
        super(x, y, zoom, url, tileFactory);
        setLoading(false);
    }

    /**
     * Sets image to Tile
     * @param image
     */
    public void setImage(SoftReference<T> image) {
        this.image = image;
    }

    /**
     * Creates offline image and marks Tile as offline
     */
    public void createOfflineImage() {
        setImage(new SoftReference<T>(createOfflineImageImpl()));
        offline = true;
    }

    /**
     * Gets value of the {@link #offline} flag
     * @return
     */
    public boolean isOffline() {
        return offline;
    }

    protected abstract T createOfflineImageImpl();
    
}
