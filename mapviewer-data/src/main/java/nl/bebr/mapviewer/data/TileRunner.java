/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.data;

import java.io.IOException;
import java.lang.ref.SoftReference;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.SwingUtilities;

import nl.bebr.mapviewer.data.cache.OfflineTileRunner;

/**
 * Original TileRunner, replaced by {@link OfflineTileRunner}
 * 
 * @author Frantisek Post
 *
 * @param <T>
 * @param <U>
 */
public abstract class TileRunner<T, U extends Tile<T>> implements Runnable {
	
	private static final Logger LOG = Logger.getLogger(TileRunner.class.getName()); 
	
	private AbstractTileFactory<T, U> tileFactory;
	private TileCache<T> tileCache;
    
    public TileRunner(AbstractTileFactory<T, U> tileFactory) {
    	this.tileFactory = tileFactory;
    	this.tileCache = tileFactory.getTileCache();
    }
    
    /**
     * Gets the full URI of a tile.
     * @param tile
     * @throws java.net.URISyntaxException
     * @return
     */
    protected URI getURI(U tile) throws URISyntaxException {
        if(tile.getURL() == null) {
            return null;
        }
        return new URI(tile.getURL());
    }
    /**
     * implementation of the Runnable interface.
     */
    public void run() {
        /*
         * 3 strikes and you're out. Attempt to load the url. If it fails,
         * decrement the number of tries left and try again. Log failures.
         * If I run out of try s just get out. This way, if there is some
         * kind of serious failure, I can get out and let other tiles
         * try to load.
         */
        final U tile = tileFactory.getTileQueue().remove();
        
        int trys = 3;
        while (!tile.isLoaded() && trys > 0) {
            try {
                T img = null;
                URI uri = getURI(tile);
                img = tileCache.get(uri);
                if(img == null) {
                    //byte[] bimg = cacheInputStream(uri.toURL());
                    //img = GraphicsUtilities.loadCompatibleImage(new ByteArrayInputStream(bimg));//ImageIO.read(new URL(tile.url));
                    img = loadImage(uri);
                    tileCache.put(uri, img);
                    img = tileCache.get(uri);
                }
                if(img == null) {
                    LOG.log(Level.INFO, "Failed to load: " + uri);
                    trys--;
                } else {
                    final T i = img;
                    SwingUtilities.invokeAndWait(new Runnable() {
                        public void run() {
                            tile.image = new SoftReference<T>(i);
                            tile.setLoaded(true);
                        }
                    });
                }
            } catch (OutOfMemoryError memErr) {
            	tileCache.needMoreMemory();
            } catch (Throwable e) {
                LOG.log(Level.SEVERE,
                        "Failed to load a tile at url: " + tile.getURL() + ", retrying", e);
                //temp
                System.err.println("Failed to load a tile at url: " + tile.getURL());
                e.printStackTrace();
                ///temp
                Object oldError = tile.getError();
                tile.setError(e);
                tile.firePropertyChangeOnEDT("loadingError", oldError, e);
                if (trys == 0) {
                    tile.firePropertyChangeOnEDT("unrecoverableError", null, e);
                } else {
                    trys--;
                }
            }
        }
        tile.setLoading(false);
    }
    
    public abstract T loadImage(URI uri) throws MalformedURLException, IOException;
    
}