/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.data.bmng;

import nl.bebr.mapviewer.data.AbstractTileFactory;
import nl.bebr.mapviewer.data.Tile;
import nl.bebr.mapviewer.data.TileCache;
import nl.bebr.mapviewer.data.GeoPosition;
import nl.bebr.mapviewer.data.DefaultTileFactory;
import java.awt.Dimension;
import java.awt.geom.Point2D;
import java.util.Map;
import java.util.concurrent.BlockingQueue;


public class CylindricalProjectionTileFactory<T, U extends Tile<T>> extends DefaultTileFactory<T, U> {

    public CylindricalProjectionTileFactory() {
        this(new SLMapServerInfo());
    }
    public CylindricalProjectionTileFactory(SLMapServerInfo info) {
        super(info);
    }

    public Dimension getMapSize(int zoom) {
        int midpoint = ((SLMapServerInfo)getInfo()).getMidpoint();
        if(zoom < midpoint) {
            int w = (int)Math.pow(2,midpoint-zoom);
            return new Dimension(w,w/2);
            //return super.getMapSize(zoom);
        }
        return new Dimension(2,1);
    }
    
    public Point2D geoToPixel(GeoPosition c, int zoom) {
        // calc the pixels per degree
        Dimension mapSizeInTiles = getMapSize(zoom);
        //double size_in_tiles = (double)getInfo().getMapWidthInTilesAtZoom(zoom);
        //double size_in_tiles = Math.pow(2, getInfo().getTotalMapZoom() - zoom);
        double size_in_pixels = mapSizeInTiles.getWidth()*getInfo().getTileSize(zoom);
        double ppd = size_in_pixels / 360;
        
        // the center of the world
        double centerX = this.getTileSize(zoom)*mapSizeInTiles.getWidth()/2;
        double centerY = this.getTileSize(zoom)*mapSizeInTiles.getHeight()/2;
        
        double x = c.getLongitude() * ppd + centerX;
        double y = -c.getLatitude() * ppd + centerY;
        
        return new Point2D.Double(x, y);
    }

    public GeoPosition pixelToGeo(Point2D pix, int zoom) {
        // calc the pixels per degree
        Dimension mapSizeInTiles = getMapSize(zoom);
        double size_in_pixels = mapSizeInTiles.getWidth()*getInfo().getTileSize(zoom);
        double ppd = size_in_pixels / 360;

        // the center of the world
        double centerX = this.getTileSize(zoom)*mapSizeInTiles.getWidth()/2;
        double centerY = this.getTileSize(zoom)*mapSizeInTiles.getHeight()/2;
        
        double lon = (pix.getX() - centerX)/ppd;
        double lat = -(pix.getY() - centerY)/ppd;

        return new GeoPosition(lat,lon);
    }
    
	@Override
	protected Runnable createTileRunner() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public TileCache<T> createTileCache() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public U createTile(int x, int y, int zoom, String url,
			AbstractTileFactory<T, U> tileFactory) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public BlockingQueue<U> getTileQueue() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	protected Map<String, U> getTileMap() {
		// TODO Auto-generated method stub
		return null;
	}
    
    /*
    x = lat * ppd + fact
    x - fact = lat * ppd
    (x - fact)/ppd = lat
    y = -lat*ppd + fact
    -(y-fact)/ppd = lat
    */
}