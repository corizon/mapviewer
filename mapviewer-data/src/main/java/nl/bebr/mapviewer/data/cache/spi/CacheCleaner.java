/**
 * Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 * AgroSense is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the FLOSS License Exception
 * <http://www.agrosense.eu/foss-exception.html>.
 *
 * AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.data.cache.spi;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.filesystems.FileObject;

/**
 * Class responsible for keeping file cache under specified size.
 *
 * @author Frantisek Post
 */
public class CacheCleaner {

    private static final Logger LOG = Logger.getLogger(CacheCleaner.class.getName());
    private static CacheCleaner instance = new CacheCleaner();

    public static final long DEFAULT_SIZE = 2000 * 1024 * 1024; // 2 GB initial size
    public static final long UNLIMITED_SIZE = -1;

    private boolean running = false; //to avoid calling twice or more - tiles are loaded from different threads
    private Comparator<FileObjectWrapper> comparator;
    protected long maximumCacheSize = DEFAULT_SIZE; //TODO settings

    protected CacheCleaner() {
        comparator = (FileObjectWrapper file1, FileObjectWrapper file2) -> (int) (file2.getLastModifiedTime() - file1.getLastModifiedTime());
    }

    private List<FileObject> findCacheFiles(FileObject root) {
        List<FileObject> files = new ArrayList<>();
        findCacheFiles(root, files);
        return files;
    }

    private void findCacheFiles(FileObject file, List<FileObject> files) {
        if (file != null) {
            if (file.isFolder()) {
                FileObject[] children = file.getChildren();
                for (FileObject child : children) {
                    findCacheFiles(child, files);
                }
            } else {
                files.add(file);
            }
        }
    }

    private List<FileObject> getFilesToDelete(List<FileObjectWrapper> files) {
        long sizeSummary = 0;
        List<FileObject> filesToDelete = new ArrayList<>();
        for (FileObjectWrapper file : files) {
            FileObject fileObject = file.getFileObject();
            sizeSummary += fileObject.getSize();
            if (sizeSummary > maximumCacheSize) {
                filesToDelete.add(fileObject);
            }
        }
        return filesToDelete;
    }

    private void deleteFiles(List<FileObjectWrapper> files) {
        List<FileObject> filesToDelete = getFilesToDelete(files);
        filesToDelete.stream().forEach((file) -> {
            safelyDelete(file);
        });
    }

    private void safelyDelete(FileObject file) {
        try {
            file.delete();
        } catch (Throwable t) {
            LOG.log(Level.FINER, "unable to delete cache file {0}, exception {1}", new Object[]{file.getPath(), t.getMessage()});
        }
    }

    protected void runInternal() {
        running = true;
        try {
            List<FileObject> files = findCacheFiles(getRootDir());

            List<FileObjectWrapper> fixDateFiles = new ArrayList<>();
            for (FileObject fileObject : files) {
                FileObjectWrapper wrapper = new FileObjectWrapper(fileObject.lastModified().getTime(), fileObject);
                fixDateFiles.add(wrapper);
            }
            Collections.sort(fixDateFiles, comparator);

            deleteFiles(fixDateFiles);
        } catch (Throwable t) {
        	//noop
            //no exception should be thrown, but just in case :)
        } finally {
            running = false;
        }

    }

    protected FileObject getRootDir() {
        return CacheManager.getInstance().getCacheFolder();
    }

    /// API ///
    /**
     * Starts the CacheCleaner
     */
    public void run() {
        if (running || isUnlimited()) {
            return;
        }

        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                runInternal();
            }

        });
        thread.setName("Cache deleting thread");
        thread.start();
    }

    /**
     * Gets instance
     *
     * @return instance
     */
    public static CacheCleaner getInstance() {
        return instance;
    }

    /**
     * Set the maximum file cache size
     *
     * @param maximumCacheSize maximum file cache size
     */
    public void setMaximumCacheSize(long maximumCacheSize) {
        long oldSize = this.maximumCacheSize;
        this.maximumCacheSize = maximumCacheSize;
        //in case, the newly set maximum size is smaller than actual, check the files and delete necessary
        //or in case the cache was unlimited and I am setting limit
        if ((maximumCacheSize < oldSize && !isUnlimited()) || (oldSize == UNLIMITED_SIZE && !isUnlimited())) {
            run();
        }
    }

    /**
     * Returns {@code true} if size is unlimited, otherwise {
     *
     * @false}
     * @return
     */
    public boolean isUnlimited() {
        return maximumCacheSize == UNLIMITED_SIZE;
    }

    /**
     * Created to wrap FileObject, because some file can be touched during the
     * sort, thus changing its lastModified date and breaking comparator's
     * general contract.
     *
     * @author post
     *
     */
    private static class FileObjectWrapper {

        long lastModifiedTime;
        FileObject fileObject;

        public FileObjectWrapper(long lastModifiedTime, FileObject fileObject) {
            super();
            this.lastModifiedTime = lastModifiedTime;
            this.fileObject = fileObject;
        }

        public long getLastModifiedTime() {
            return lastModifiedTime;
        }

        public FileObject getFileObject() {
            return fileObject;
        }

    }

}
