/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.data.esri;

import java.awt.geom.Point2D;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;
import java.util.concurrent.BlockingQueue;


import nl.bebr.mapviewer.data.AbstractTileFactory;
import nl.bebr.mapviewer.data.DefaultTileFactory;
import nl.bebr.mapviewer.data.GeoPosition;
import nl.bebr.mapviewer.data.Tile;
import nl.bebr.mapviewer.data.TileCache;
import nl.bebr.mapviewer.data.TileFactoryInfo;
import nl.bebr.mapviewer.data.util.GeoUtil;

/**
 *
 * @author rbair
 */
public class ESRITileFactory<T, U extends Tile<T>> extends DefaultTileFactory<T, U> {
    private static final String projection = "8"; //mercator projection
    private static final String format = "png"; //get pngs back
    private String userId;
    private String datasource; //should be enum
    
    /** Creates a new instance of ESRITileFactory */
    public ESRITileFactory() {
        super(new ESRITileProviderInfo());
        ((ESRITileProviderInfo)super.getInfo()).factory = this;
        datasource = "ArcWeb:TA.Streets.NA";
    }
    
    public void setUserID(String id) {
        //temp hack
        this.userId = id;
    }
    
    private static final class ESRITileProviderInfo extends TileFactoryInfo {
        private ESRITileFactory<?,?> factory;
        
        private ESRITileProviderInfo() {
            super(0, 17, 18, 256, false, true, "http://www.arcwebservices.com/services/v2006/restmap?actn=getMap", "", "", "");
        }
        
        public String getTileUrl(int x, int y, int zoom) {
            //provide the center point of the tile, in lat/long coords
            int tileY = y;
            int tileX = x;
            int pixelX = tileX * factory.getTileSize(zoom) + (factory.getTileSize(zoom) / 2);
            int pixelY = tileY * factory.getTileSize(zoom) + (factory.getTileSize(zoom) / 2);
            
            GeoPosition latlong = GeoUtil.getPosition(new Point2D.Double(pixelX, pixelY), zoom, this);
            
            //Chris is going to hate me for this (relying on 72dpi!), but:
            //72 pixels per inch. The earth is 24,859.82 miles in circumference, at the equator.
            //Thus, the earth is 24,859.82 * 5280 * 12 * 72 pixels in circumference.
            
            double numFeetPerDegreeLong = 24859.82 * 5280 / 360; //the number of feet per degree longitude at the equator
            double numPixelsPerDegreeLong = getLongitudeDegreeWidthInPixels(zoom);
            double numPixelsPerFoot = 96 * 12;
            int sf = (int)(numFeetPerDegreeLong / (numPixelsPerDegreeLong / numPixelsPerFoot));
            
            //round lat and long to 5 decimal places
            BigDecimal lat = new BigDecimal(latlong.getLatitude());
            BigDecimal lon = new BigDecimal(latlong.getLongitude());
            lat = lat.setScale(5, RoundingMode.DOWN);
            lon = lon.setScale(5, RoundingMode.DOWN);
            
            String url = baseURL +
                    "&usrid=" + factory.userId +
                    "&ds=" + factory.datasource +
                    "&c=" + lon.doubleValue() + "%7C" + lat.doubleValue() +
                    "&sf=" + sf + //52500" +
                    "&fmt=" + format +
                    "&ocs=" + projection;
            return url;
        }
    }

	@Override
	protected Runnable createTileRunner() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TileCache<T> createTileCache() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public U createTile(int x, int y, int zoom, String url,
			AbstractTileFactory<T, U> tileFactory) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BlockingQueue<U> getTileQueue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Map<String, U> getTileMap() {
		// TODO Auto-generated method stub
		return null;
	}
}
