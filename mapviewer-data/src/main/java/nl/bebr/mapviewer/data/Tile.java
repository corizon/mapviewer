/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.data;

import java.awt.EventQueue;
import java.beans.PropertyChangeListener;
import java.lang.ref.SoftReference;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.SwingUtilities;

import nl.bebr.mapviewer.data.util.AbstractBean;

/**
 * The Tile class represents a particular square image
 * piece of the world bitmap at a particular zoom level.
 * @author joshy, Frantisek Post
 */

public class Tile<T> extends AbstractBean {
    
    private static final Logger LOG = Logger.getLogger(Tile.class.getName());
	public static final String LOADED = "loaded";

    public enum Priority { High, Low }
    
    private Priority priority = Priority.High;
    protected TileFactory<T, Tile<T>> tileFactory;
    private boolean isLoading = false;

    static {
        LOG.setLevel(Level.OFF);
    }
    
    
    /**
     * If an error occurs while loading a tile, store the exception
     * here.
     */
    private Throwable error;
    
    /**
     * The url of the image to load for this tile
     */
    private String url;
    
    /**
     * Indicates that loading has succeeded. A PropertyChangeEvent will be fired
     * when the loading is completed
     */
    
    private boolean loaded = false;
    /**
     * The zoom level this tile is for
     */
    private int zoom, x, y;
    
    /**
     * The image loaded for this Tile
     */
    protected SoftReference<T> image = new SoftReference<T>(null);
    
    /**
     * Create a new Tile at the specified tile point and zoom level
     * @param location
     * @param zoom
     */
    public Tile(int x, int y, int zoom) {
        loaded = false;
        this.zoom = zoom;
        this.x = x;
        this.y = y;
    }
    
    public Tile(int x, int y, int zoom, String url, TileFactory<T, Tile<T>> tileFactory) {
        this.url = url;
        loaded = false;
        this.zoom = zoom;
        this.x = x;
        this.y = y;
        this.tileFactory = tileFactory;
    }
    
    /**
     * Create a new Tile that loads its data from the given URL. The URL must
     * resolve to an image
     */
    public Tile(int x, int y, int zoom, String url, Priority priority, TileFactory<T, Tile<T>> tileFactory) {
        this.url = url;
        loaded = false;
        this.zoom = zoom;
        this.x = x;
        this.y = y;
        this.priority = priority;
        this.tileFactory = tileFactory;
    }
    
    /**
     *
     * Indicates if this tile's underlying image has been successfully loaded yet.
     * @returns true if the Tile has been loaded
     * @return
     */
    public synchronized boolean isLoaded() {
        return loaded;
    }
    
    /**
     * Toggles the loaded state, and fires the appropriate property change notification
     */
    public synchronized void setLoaded(boolean loaded) {
        boolean old = isLoaded();
        this.loaded = loaded;
        firePropertyChange(LOADED, old, isLoaded());
    }
    
    /**
     * Returns the last error in a possible chain of errors that occured during
     * the loading of the tile
     */
    public Throwable getUnrecoverableError() {
        return error;
    }
    
    /**
     * Returns the Throwable tied to any error that may have ocurred while
     * loading the tile. This error may change several times if multiple
     * errors occur
     * @return
     */
    public Throwable getLoadingError() {
        return error;
    }
    
    /**
     * Returns the Image associated with this Tile. This is a read only property
     *          This may return null at any time, however if this returns null,
     *          a load operation will automatically be started for it.
     */
    public T getImage() {
        T img = image.get();
        if (img == null) {
            setLoaded(false);
            tileFactory.startLoading(this);
        }
        
        return img;
    }
    
    /**
     * @return the zoom level that this tile belongs in
     */
    public int getZoom() {
        return zoom;
    }
    
    //////////////////JavaOne Hack///////////////////
    private PropertyChangeListener uniqueListener = null;
    /**
     * Adds a single property change listener. If a listener has been previously
     * added then it will be replaced by the new one.
     * @param propertyName
     * @param listener
     */
    public void addUniquePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if (uniqueListener != null && uniqueListener != listener) {
            removePropertyChangeListener(propertyName, uniqueListener);
        }
        if (uniqueListener != listener) {
            uniqueListener = listener;
            addPropertyChangeListener(propertyName, uniqueListener);
        }
    }
    
    /////////////////End JavaOne Hack/////////////////
    
    /**
     */
    public void firePropertyChangeOnEDT(final String propertyName, final Object oldValue, final Object newValue) {
        if (!EventQueue.isDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    firePropertyChange(propertyName, oldValue, newValue);
                }
            });
        }
    }
    
	/**
	 * @return the error
	 */
	public Throwable getError() {
		return error;
	}

	/**
	 * @param error the error to set
	 */
	public void setError(Throwable error) {
		this.error = error;
	}
	
	/**
	 * @return the isLoading
	 */
	public boolean isLoading() {
		return isLoading;
	}

	/**
	 * @param isLoading the isLoading to set
	 */
	public void setLoading(boolean isLoading) {
		this.isLoading = isLoading;
	}
	
    /**
     * Gets the loading priority of this tile.
     * @return
     */
    public Priority getPriority() {
        return priority;
    }
    
	/**
	 * Set the loading priority of this tile.
	 * @param priority the priority to set
	 */
    public void setPriority(Priority priority) {
		this.priority = priority;
	}
    
    /**
     * Gets the URL of this tile.
     * @return
     */
    public String getURL() {
        return url;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
    

}

