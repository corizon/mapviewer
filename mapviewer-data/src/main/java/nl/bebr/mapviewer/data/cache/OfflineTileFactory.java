/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.data.cache;


import nl.bebr.mapviewer.data.DefaultTileFactory;
import nl.bebr.mapviewer.data.Tile;
import nl.bebr.mapviewer.data.TileFactoryInfo;
import nl.bebr.mapviewer.data.util.ConnectionChecker;
import nl.bebr.mapviewer.data.util.GeoUtil;

/**
 * Copied some code from AbstractTileFactory, so I can use own TileRunner, which does all the
 * reading and writing to the cache
 * 
 * @author Frantisek Post
 */
public abstract class OfflineTileFactory<T> extends DefaultTileFactory<T, Tile<T>> {

    private ConnectionChecker connectionChecker;
    
    /**
     * Constructor
     * 
     * @param tileFactoryInfo
     */
    public OfflineTileFactory(TileFactoryInfo tileFactoryInfo) {
        super(tileFactoryInfo);
        connectionChecker = ConnectionChecker.getInstance();
    }

    @Override
    public Tile<T> getTile(int x, int y, int zoom) {
        return getTileImpl(x, y, zoom, true);
    }

    private Tile<T> getTileImpl(int tpx, int tpy, int zoom, boolean eagerLoad) {
        //wrap the tiles horizontally --> mod the X with the max width
        //and use that
        int tileX = tpx;//tilePoint.getX();
        int numTilesWide = (int) getMapSize(zoom).getWidth();
        if (tileX < 0) {
            tileX = numTilesWide - (Math.abs(tileX) % numTilesWide);
        }

        tileX = tileX % numTilesWide;
        int tileY = tpy < 0 ? 0 : tpy;
        String url = getInfo().getTileUrl(tileX, tileY, zoom);

        Tile<T> tile;
        if (!getTileMap().containsKey(url) || needRefresh(url)) { 
        	tile = createOfflineTile(tileX, tileY, zoom, url);
            if (GeoUtil.isValidTile(tileX, tileY, zoom, getInfo())) {
                startLoading(tile);
            }
            
            getTileMap().put(url, tile);
        } else {
            tile = getTileMap().get(url);
            // if its in the map but is low and isn't loaded yet
            // but we are in high mode
            if (tile.getPriority() == Tile.Priority.Low && eagerLoad && !tile.isLoaded()) {
                promote(tile);
            }
        }

        return tile;
    }

    //we are online, but cached tile contains offline image
    private boolean needRefresh(String url) {
        OfflineTile<T> tile = (OfflineTile<T>) getTileMap().get(url);
        return connectionChecker.isOnline() && tile.isOffline();
    }

    /**
     * Method for creating offline Tile
     * 
     * @param tileX x coordinates
     * @param tileY y coordinates
     * @param zoom zoom level
     * @param url tile url
     * 
     * @return created offline tile
     */
    protected abstract Tile<T> createOfflineTile(int tileX, int tileY, int zoom, String url);
    
}
