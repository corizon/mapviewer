/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.data.cache.spi;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openide.filesystems.FileObject;

import nl.bebr.mapviewer.data.cache.TileCacheInfo;
import nl.bebr.mapviewer.data.cache.TileService;
import nl.bebr.mapviewer.data.cache.WritableTileService;
import nl.bebr.mapviewer.data.util.FileUtil;

/**
 * Abstract implementation of {@link TileService}
 *
 * @author Frantisek Post
 */
public abstract class AbstractTileService<T> implements WritableTileService<T>, TileService<T> {

    private static final Logger LOG = Logger.getLogger(AbstractTileService.class.getName());
    private static final int HITS_TO_CLEAN_CACHE = 30; //every 30th write to disk trigger clean of cache
    
    private int hitCounter = 0;

    protected FileObject getTileFile(TileCacheInfo tileCacheInfo, boolean createMissing) {
        FileObject zoomFolder = getZoomFolder(tileCacheInfo, createMissing);
        FileObject tileFile = null;
        if (zoomFolder != null) {
            tileFile = zoomFolder.getFileObject(tileCacheInfo.getPath());
            if (tileFile == null && createMissing) {
                try {
                    tileFile = zoomFolder.createData(tileCacheInfo.getPath());
                } catch (IOException ex) {
                    LOG.log(Level.FINE, "Error creating tile file " + tileCacheInfo.getPath(), ex);
                }
            }
        }
        return tileFile;
    }

    protected FileObject getZoomFolder(TileCacheInfo tileCacheInfo, boolean createMissing) {
        FileObject zoomFolder = null;
        String mapType = tileCacheInfo.getType();
        String zoomFolderName = String.format("%d", tileCacheInfo.getZoom());
        FileObject rootFolder = CacheManager.getInstance().getCacheFolder();
        
        if (rootFolder != null) {
        	FileObject typeFolder = rootFolder.getFileObject(mapType);
            if (typeFolder == null) {
            	try {
    				typeFolder = rootFolder.createFolder(mapType);
    			} catch (IOException ex) {
    				LOG.log(Level.FINE, "Error creating folder " + typeFolder, ex);
    			}
            }
        	
			if (typeFolder != null) {
				zoomFolder = typeFolder.getFileObject(zoomFolderName);
				if (zoomFolder == null && createMissing) {
					try {
						zoomFolder = typeFolder.createFolder(zoomFolderName);
					} catch (IOException ex) {
						LOG.log(Level.FINE, "Error creating folder "
								+ zoomFolderName, ex);
					}
				}
			}
        }
        return zoomFolder;
    }

    protected void readCallback(FileObject tileFile) {
        try {
            FileUtil.touch(tileFile); 
        } catch (IOException ex) {
            LOG.log(Level.FINE, "Error while touching file", ex);
        }
    }

    protected void writeCallback() {
        if (!CacheCleaner.getInstance().isUnlimited()) {
            hitCounter++;
            if (hitCounter >= HITS_TO_CLEAN_CACHE) {
                hitCounter = 0;
                CacheCleaner.getInstance().run();
            }
        }
    }
    
    //API
    @Override
    public boolean contains(TileCacheInfo tileCacheInfo) {
        return CacheManager.getInstance().isCached(tileCacheInfo);
    }

    @Override
    public T getTile(TileCacheInfo tileCacheInfo) {
        return readImage(tileCacheInfo);
    }
    
    @Override
    public void writeTile(TileCacheInfo tileCacheInfo, T image) {
        writeImage(tileCacheInfo, image);
    }
    
    /**
     * Reads image for given {@link TileCacheInfo}
     * 
     * @param tileCacheInfo
     * @return image
     */
    public abstract T readImage(TileCacheInfo tileCacheInfo);
    
    /**
     * Stores image for given {@link TileCacheInfo}
     * 
     * @param tileCacheInfo
     * @param image
     */
    public abstract void writeImage(TileCacheInfo tileCacheInfo, T image);
}

