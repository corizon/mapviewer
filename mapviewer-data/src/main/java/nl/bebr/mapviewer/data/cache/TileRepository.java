/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.data.cache;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.openide.util.Lookup;


/**
 * Uses lookup to find all implementations of {@link TileService} interface.
 * 
 * <p>
 * {@link DefaultTileService} is added at the top of the list.
 * </p>
 * 
 * @author Frantisek Post
 */
public abstract class TileRepository<T> {

    @SuppressWarnings("rawtypes")
	private List<TileService> services;
    private WritableTileService<T> writableService;
    
    /**
     * Constructor
     */
    public TileRepository() {
        services = new ArrayList<>();
        services.addAll(findImplementations());
        writableService = findWritableImplementation();
    }
    
    @SuppressWarnings("rawtypes")
	protected List<TileService> findImplementations() {
        Collection<? extends TileService> caches = Lookup.getDefault().lookupAll(TileService.class);
        return new ArrayList<>(caches);
    }
    
    @SuppressWarnings("unchecked")
	protected WritableTileService<T> findWritableImplementation() {
    	return Lookup.getDefault().lookup(WritableTileService.class);
    }
    
    private TileService<T> getCacheThatContainsTile(TileCacheInfo tileCacheInfo) {
        TileService<T> tileService = null;
        for (TileService<T> service : services) {
            if (service.contains(tileCacheInfo)) {
                tileService = service;
                break;
            }
        }
        return tileService;
    }
    
    //API
    
    /**
     * Gets image for given tile defined by its {@link TileCacheInfo}
     * 
     * @param tileCacheInfo tileCacheInfo of tile to load
     * @return image
     */
    public T getTile(TileCacheInfo tileCacheInfo) {
        TileService<T> tileService = getCacheThatContainsTile(tileCacheInfo);
        return (tileService != null) ? tileService.getTile(tileCacheInfo) : null;
    } 
    
    /**
     * Persists image for tile defined by its {@link TileCacheInfo}
     * 
     * @param tileCacheInfo tileCacheInfo of tile to save
     * @param image image
     */
    public void writeTile(TileCacheInfo tileCacheInfo, T image) {
    	if (writableService != null) {
    		writableService.writeTile(tileCacheInfo, image);
    	}
    }
    
}
