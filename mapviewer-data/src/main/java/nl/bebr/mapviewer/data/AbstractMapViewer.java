/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.data;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import nl.bebr.mapviewer.data.cache.spi.CacheManager;
import nl.bebr.mapviewer.data.util.ScaleUtil;


/**
 * AbstractMapViewer was created to provide common functionality for both Swing and JavaFX implementation. 
 * 
 * @author Frantisek Post
 *
 * @param <T> image type (BuffredIamge for swing and Image for JavaFX) 
 * @param <U> Tile type
 */
public abstract class AbstractMapViewer<T, U extends Tile<T>> {

	private int zoom = 1;

	private Point2D center = new Point2D.Double(0, 0);

	private TileFactory<T, U> tileFactory;
	
	/**
     * The position in latitude/longitude of the "address" being mapped. This
     * is a special coordinate that, when moved, will cause the map to be moved
     * as well. It is separate from "center" in that "center" tracks the current
     * center (in pixels) of the viewport whereas this will not change when panning
     * or zooming. Whenever the addressLocation is changed, however, the map will
     * be repositioned.
     */
    private GeoPosition addressLocation;

    /**
     * Specifies whether panning is enabled. Panning is being able to click and
     * drag the map around to cause it to move
     */
    private boolean panEnabled = true;
    
    /**
     * Specifies whether zooming is enabled (the mouse wheel, for example, zooms)
     */
    private boolean zoomEnabled = true;
    
    private boolean restrictOutsidePanning = false;
    
    private boolean horizontalWrapped = true;

    private List<RepaintCallback> repaintCallbacks = new ArrayList<>();
    
    private static final int MINIMUM_ZOOM = 1;
    
    protected long scaleValue;
    
    protected float pixelToMeter;
    
    private static final int MAXIMUM_SCALE_WIDTH = 150;
    
	/**
	 * Gets {@link TileFactory}
	 * 
	 * @return
	 */
	public TileFactory<T, U> getTileFactory() {
		return tileFactory;
	}

	/**
	 * Set zoom level
	 * 
	 * @param zoom
	 */
	public void setZoom(int zoom) {
		if (zoom == this.zoom) {
			return;
		}

		CacheManager.getInstance().setZoomLevel(zoom);
		
		TileFactoryInfo info = getTileFactory().getInfo();
		// don't repaint if we are out of the valid zoom levels
		if (info != null
				&& (zoom < info.getMinimumZoomLevel() || zoom > info
						.getMaximumZoomLevel())) {
			return;
		}

		// if(zoom >= 0 && zoom <= 15 && zoom != this.zoom) {
		int oldzoom = this.zoom;
		Point2D oldCenter = getCenter();
		Dimension oldMapSize = getTileFactory().getMapSize(oldzoom);
		this.zoom = zoom;
		this.firePropertyChange("zoom", oldzoom, zoom);

		Dimension mapSize = getTileFactory().getMapSize(zoom);

		setCenter(new Point2D.Double(oldCenter.getX()
				* (mapSize.getWidth() / oldMapSize.getWidth()),
				oldCenter.getY()
						* (mapSize.getHeight() / oldMapSize.getHeight())));
        calculateScale();
		repaint();
	}

	/**
	 * Gets the current zoom level
	 * 
	 * @return the current zoom level
	 */
	public int getZoom() {
		return this.zoom;
	}

	/**
	 * Gets center of the map in pixel coordinates
	 * 
	 * @return center of the map in pixel coordinates
	 */
	public Point2D getCenter() {
		return center;
	}

	/**
	 * Sets the new center of the map in pixel coordinates.
	 * 
	 * @param center  the new center of the map in pixel coordinates
	 */
	public void setCenter(Point2D center) {
		Point2D old = this.getCenter();

		if (isRestrictOutsidePanning()) {
			Insets insets = getInsets();
			int viewportHeight = getHeight() - insets.top - insets.bottom;
			int viewportWidth = getWidth() - insets.left - insets.right;

			// don't let the user pan over the top edge
			Rectangle newVP = calculateViewportBounds(center);
			if (newVP.getY() < 0) {
				double centerY = viewportHeight / 2;
				center = new Point2D.Double(center.getX(), centerY);
			}

			// don't let the user pan over the left edge
			if (!isHorizontalWrapped() && newVP.getX() < 0) {
				double centerX = viewportWidth / 2;
				center = new Point2D.Double(centerX, center.getY());
			}

			// don't let the user pan over the bottom edge
			Dimension mapSize = getTileFactory().getMapSize(getZoom());
			int mapHeight = (int) mapSize.getHeight()
					* getTileFactory().getTileSize(getZoom());
			if (newVP.getY() + newVP.getHeight() > mapHeight) {
				double centerY = mapHeight - viewportHeight / 2;
				center = new Point2D.Double(center.getX(), centerY);
			}

			// don't let the user pan over the right edge
			int mapWidth = (int) mapSize.getWidth()
					* getTileFactory().getTileSize(getZoom());
			if (!isHorizontalWrapped()
					&& (newVP.getX() + newVP.getWidth() > mapWidth)) {
				double centerX = mapWidth - viewportWidth / 2;
				center = new Point2D.Double(centerX, center.getY());
			}

			// if map is to small then just center it vert
			if (mapHeight < newVP.getHeight()) {
				double centerY = mapHeight / 2;// viewportHeight/2;// -
												// mapHeight/2;
				center = new Point2D.Double(center.getX(), centerY);
			}

			// if map is too small then just center it horiz
			if (!isHorizontalWrapped() && mapWidth < newVP.getWidth()) {
				double centerX = mapWidth / 2;
				center = new Point2D.Double(centerX, center.getY());
			}
		}

		// joshy: this is an evil hack to force a property change event
		// i don't know why it doesn't work normally
		old = new Point(5, 6);

		GeoPosition oldGP = this.getCenterPosition();
		this.center = center;
		firePropertyChange("center", old, this.center);// .getCenter());
		firePropertyChange("centerPosition", oldGP, this.getCenterPosition());
		calculateScale();
		
		repaint();
	}

	/**
	 * Gets bounds of component
	 * 
	 * @return bounds of component
	 */
	public Rectangle getViewportBounds() {
		return calculateViewportBounds(getCenter());
	}

	private Rectangle calculateViewportBounds(Point2D center) {
		Insets insets = getInsets();
		// calculate the "visible" viewport area in pixels
		int viewportWidth = getWidth() - insets.left - insets.right;
		int viewportHeight = getHeight() - insets.top - insets.bottom;
		double viewportX = (center.getX() - viewportWidth / 2);
		double viewportY = (center.getY() - viewportHeight / 2);
		return new Rectangle((int) viewportX, (int) viewportY, viewportWidth,
				viewportHeight);
	}

	/**
	 * Sets center position of the map
	 * 
	 * @param geoPosition center position of the map
	 */
	public void setCenterPosition(GeoPosition geoPosition) {
        GeoPosition oldVal = getCenterPosition();
        setCenter(getTileFactory().geoToPixel(geoPosition, zoom));
        repaint();
        GeoPosition newVal = getCenterPosition();
        firePropertyChange("centerPosition", oldVal, newVal);
    }
    
    /**
     * A property indicating the center position of the map
     * @return the current center position
     */
    public GeoPosition getCenterPosition() {
        return getTileFactory().pixelToGeo(getCenter(), zoom);
    }
    
    /**
     * Set the current tile factory
     * @param factory the new property value
     */
    public void setTileFactory(TileFactory<T, U> factory) {
        this.tileFactory = factory;
        this.setZoom(zoom);
        CacheManager.getInstance().setMapTypeFolder(factory.getInfo().getName());
    }
    
    /**
     * Gets the current address location of the map. This property does not change when the user
     * pans the map. This property is bound.
     * @return the current map location (address)
     */
    public GeoPosition getAddressLocation() {
        return addressLocation;
    }
    
    /**
     * Gets the current address location of the map
     * @param addressLocation the new address location
     * @see getAddressLocation()
     */
    public void setAddressLocation(GeoPosition addressLocation) {
        GeoPosition old = getAddressLocation();
        this.addressLocation = addressLocation;
        setCenter(getTileFactory().geoToPixel(addressLocation, getZoom()));
        
        firePropertyChange("addressLocation", old, getAddressLocation());
        repaint();
    }
    
    /**
     * Re-centers the map to have the current address location
     * be at the center of the map, accounting for the map's width
     * and height.
     * @see getAddressLocation
     */
    public void recenterToAddressLocation() {
        setCenter(getTileFactory().geoToPixel(getAddressLocation(),getZoom()));
        repaint();
    }
	
    /**
     * Calculates a zoom level so that all points in the specified set will be
     * visible on screen. This is useful if you have a bunch of points in an
     * area like a city and you want to zoom out so that the entire city and it's
     * points are visible without panning.
     * @param positions A set of GeoPositions to calculate the new zoom from
     */
    public void calculateZoomFrom(Set<GeoPosition> positions) {
        //u.p("calculating a zoom based on: ");
        //u.p(positions);
        if(positions.size() < 2) {
            return;
        }
        
        int zoom = getZoom();
        Rectangle2D rect = generateBoundingRect(positions, zoom);
        //Rectangle2D viewport = map.getViewportBounds();
        int count = 0;
        while(!getViewportBounds().contains(rect)) {
            //u.p("not contained");
            Point2D center = new Point2D.Double(
                    rect.getX() + rect.getWidth()/2,
                    rect.getY() + rect.getHeight()/2);
            GeoPosition px = getTileFactory().pixelToGeo(center,zoom);
            //u.p("new geo = " + px);
            setCenterPosition(px);
            count++;
            if(count > 30) break;
            
            if(getViewportBounds().contains(rect)) {
                //u.p("did it finally");
                break;
            }
            zoom = zoom + 1;
            if(zoom > 15) {
                break;
            }
            setZoom(zoom);
            rect = generateBoundingRect(positions, zoom);
        }
    }
    
    private Rectangle2D generateBoundingRect(final Set<GeoPosition> positions, int zoom) {
        Point2D point1 = getTileFactory().geoToPixel(positions.iterator().next(), zoom);
        Rectangle2D rect = new Rectangle2D.Double(point1.getX(), point1.getY(),0,0);
        
        for(GeoPosition pos : positions) {
            Point2D point = getTileFactory().geoToPixel(pos, zoom);
            rect.add(point);
        }
        return rect;
    }
    
    /**
     * A property indicating if the map should be pannable by the user using
     * the mouse.
     * @return property value
     */
    public boolean isPanEnabled() {
        return panEnabled;
    }
    
    /**
     * A property indicating if the map should be pannable by the user using
     * the mouse.
     * @param panEnabled new property value
     */
    public void setPanEnabled(boolean panEnabled) {
        boolean old = isPanEnabled();
        this.panEnabled = panEnabled;
        firePropertyChange("panEnabled", old, isPanEnabled());
    }
    
    /**
     * A property indicating if the map should be zoomable by the user using
     * the mouse wheel.
     * @return the current property value
     */
    public boolean isZoomEnabled() {
        return zoomEnabled;
    }
    
    /**
     * A property indicating if the map should be zoomable by the user using
     * the mouse wheel.
     * @param zoomEnabled the new value of the property
     */
    public void setZoomEnabled(boolean zoomEnabled) {
        boolean old = isZoomEnabled();
        this.zoomEnabled = zoomEnabled;
        firePropertyChange("zoomEnabled", old, isZoomEnabled());
    }
    
    public boolean isRestrictOutsidePanning() {
        return restrictOutsidePanning;
    }
    
    public void setRestrictOutsidePanning(boolean restrictOutsidePanning) {
        this.restrictOutsidePanning = restrictOutsidePanning;
    }
    
    public boolean isHorizontalWrapped() {
        return horizontalWrapped;
    }
    
    public void setHorizontalWrapped(boolean horizontalWrapped) {
        this.horizontalWrapped = horizontalWrapped;
    }
    
    /**
	 * Converts the specified GeoPosition to a point in the JXMapViewer's local
	 * coordinate space. This method is especially useful when drawing lat/long
	 * positions on the map.
	 * 
	 * @param pos
	 *            a GeoPosition on the map
	 * @return the point in the local coordinate space of the map
	 */
	public Point2D convertGeoPositionToPoint(GeoPosition pos) {
		// convert from geo to world bitmap
		Point2D pt = getTileFactory().geoToPixel(pos, getZoom());
		// convert from world bitmap to local
		Rectangle bounds = getViewportBounds();
		return new Point2D.Double(pt.getX() - bounds.getX(), pt.getY()
				- bounds.getY());
	}

	/**
	 * Converts the specified Point2D in the JXMapViewer's local coordinate
	 * space to a GeoPosition on the map. This method is especially useful for
	 * determining the GeoPosition under the mouse cursor.
	 * 
	 * @param pt
	 *            a point in the local coordinate space of the map
	 * @return the point converted to a GeoPosition
	 */
	public GeoPosition convertPointToGeoPosition(Point2D pt) {
		// convert from local to world bitmap
		Rectangle bounds = getViewportBounds();
		Point2D pt2 = new Point2D.Double(pt.getX() + bounds.getX(), pt.getY()
				+ bounds.getY());

		// convert from world bitmap to geo
		GeoPosition pos = getTileFactory().pixelToGeo(pt2, getZoom());
		return pos;
	}
    
	/**
	 * Calculates zoom level from selected areain the map
	 * 
	 * @param x width of the selected area
	 * @param y height of the selected area
	 * 
	 * @return zoom level
	 */
	public int calculateZoom(double x, double y) {
		if (x != 0 && y != 0) {
			double weightX = getWidth() / x;
			double weightY = getHeight() / y;
			double scale = Math.min(weightX, weightY);
			scale = Math.log(scale) / Math.log(2);

                        if (scale < 0) {
                            scale = -1;
                        }
                        
			return getZoom() - (int) scale;
		} else {
			return getZoom();
		}
	}
	
	/**
	 * Zoom the map to show selected area in the map
	 * 
	 * @param startX x coordinate of area start
	 * @param startY y coordinate of area start
	 * @param endX x coordinate of area end
	 * @param endY x coordinate of area end
	 */
	public void zoomToCoordinates(double startX, double startY, double endX, double endY) {
		double oldCenterX = getWidth() / 2;
		double oldCenterY = getHeight() / 2;
		
		double centerX = Math.min(startX, endX) + Math.abs(startX - endX)/2;
		double centerY = Math.min(startY, endY) + Math.abs(startY - endY)/2;

		double x = getCenter().getX() + (centerX - oldCenterX);
		double y = getCenter().getY() + (centerY - oldCenterY);

		int maxHeight = (int) (getTileFactory().getMapSize(getZoom()).getHeight() * getTileFactory().getTileSize(getZoom()));
		if (y > maxHeight) {
			y = maxHeight;
		}

		setCenter(new Point2D.Double(x, y));
                        
		setZoomFromPixelPosition(new Point2D.Double(startX, startY), new Point2D.Double(endX, endY));
	}
        
    protected abstract void firePropertyChange(String name, Object oldValue, Object newValue);
    
	/**
	 * Gets width of mapViewer component
	 * 
	 * @return
	 */
	protected abstract int getWidth();
	
	/**
	 * Gets height of mapViewer component
	 * 
	 * @return
	 */
	protected abstract int getHeight();
	
	/**
	 * Gets insets of mapViewer component
	 * 
	 * @return
	 */
	protected abstract Insets getInsets();
	
	/**
	 * Callback method to repaint mapViewer component
	 */
	protected abstract void repaint();
	
	/**
	 * Method to fire all {@link RepaintCallback}s.   
	 */
	public void fireRepaintCallbacks() {
		
		RepaintCallback[] callbacks = null;
		synchronized (this) {
			callbacks = repaintCallbacks.toArray(new RepaintCallback[repaintCallbacks.size()]);
		}
		
		for (RepaintCallback callback : callbacks) {
			callback.repaint();
		}
		
	}
	
	/**
	 * Add {@link RepaintCallback} to a list of callbacks, fired when repaint of a component and its subcomponents is needed
	 * 
	 * @param repaintCallback
	 */
	public synchronized void addRepaintCallback(RepaintCallback repaintCallback) {
		repaintCallbacks.add(repaintCallback);
	}
	
	/**
	 * Remove {@link RepaintCallback} from a list of callbacks
	 * 
	 * @param repaintCallback
	 */
	public synchronized void removeRepaintCallback(RepaintCallback repaintCallback) {
		repaintCallbacks.remove(repaintCallback);
	}
        
       public void setZoomFromGeoPosition(GeoPosition startPosition, GeoPosition endPosition) {
            setZoomFromPixelPosition(convertGeoPositionToPoint(startPosition), convertGeoPositionToPoint(endPosition));
        }
       
       private void setZoomFromPixelPosition(Point2D startPoint, Point2D endPoint) {
            int calculatedZoom = calculateZoom(Math.abs(startPoint.getX() - endPoint.getX()), Math.abs(startPoint.getY() - endPoint.getY()));
            if (calculatedZoom < MINIMUM_ZOOM) {
                calculatedZoom = MINIMUM_ZOOM;
            }

            if (calculatedZoom != getZoom()) {
                setZoom(calculatedZoom);
            } else {
                repaint();
            }
       }
       
       private void calculateScale() {
    	   	Point2D centerPointOnScreen = new Point(getWidth()/2, getHeight()/2);
            GeoPosition startPoint = convertPointToGeoPosition(new Point2D.Double(centerPointOnScreen.getX(), centerPointOnScreen.getY()));
            GeoPosition endPoint = convertPointToGeoPosition(new Point2D.Double(centerPointOnScreen.getX() + 100, centerPointOnScreen.getY()));
            
            float distance = startPoint.distanceTo(endPoint);
            pixelToMeter = 100f / distance;

            double unitToMetersRatio = ScaleUtil.getUnitToMetersRatio();
            int zoom2 = zoom;
            scaleValue = ScaleUtil.getScaleValue(zoom);
            double width = pixelToMeter * scaleValue * unitToMetersRatio;
            
            while (width > MAXIMUM_SCALE_WIDTH && zoom2 > 1) {
            	zoom2--;
            	scaleValue = ScaleUtil.getScaleValue(zoom2);
            	width = pixelToMeter * scaleValue * unitToMetersRatio;
            }
            
       }
	
       public long getScaleValue() {
    	   return scaleValue;
       }
       
       public float getPixelToMeter() {
    	   return pixelToMeter;
       }
}
