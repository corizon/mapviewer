/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.data.util;

import java.util.Locale;
import nl.bebr.mapviewer.data.util.Bundle;
import org.openide.util.NbBundle;

@NbBundle.Messages({
            "scale_meter=m",
            "scale_kilometer=km",
            "scale_mile=mi",
            "scale_feet=ft",
            "scale_hectar=ha",
            "scale_acres=acres",
            "scale_square_meter=\u33A1",
            "scale_square_feet=square feet"})
public class ScaleUtil {

        private static final int HECTAR_TO_METER = 10000;
        private static final int KM_TRESHOLD = 2000;
	private static final double FEET = 0.3048; //in meters
        private static final double SQUARE_M_TO_FOOT = 10.7643;
        private static final double HA_TO_ACRES = 2.471;
        private static final double MILE_TO_M = 1609.3;
	
	private static final long[] SCALE_VALUES_METRIC = new long[] { 50, 100, 250,
			500, 1000, 2000, 4000, 10000, 15000, 30000, 50000, 100000, 250000,
			500000, 1000000, 2000000, 4000000 };

	private static final long[] SCALE_VALUES_IMPERIAL = new long[] { 200, 500, 1000,
		2000, 4000, 5280, 10560, 26400, 52800, 105600, 158400, 264000, 792000,
		1584000, 2640000, 5280000, 10560000 };
	
	public static long getScaleValue(int zoom) {
		UnitSystem unitSystem = getUnitSystem();
		
		switch (unitSystem) {
		case METRIC:
			return getScaleValueMetric(zoom);
		case IMPERIAL:
			return getScaleValueImperial(zoom);
		default:
			return getScaleValueMetric(zoom);
		}
	}
	
	public static UnitSystem getUnitSystem() {
		Locale defaultLocale = Locale.getDefault();
		String cc = defaultLocale.getCountry();
		
		//TODO FP: since there is no exact method to find out coorrect unit system from Locale, it si done this way for now.
		//There should be possibility of some settings later.
		
		if ("US".equals(cc) || "GB".equals(cc) || "CA".equals(cc)) {
			return UnitSystem.IMPERIAL;
		} else {
			return UnitSystem.METRIC;
		}
	}
        
        public static String formatLength(double length) {
            UnitSystem unitSystem = getUnitSystem();
            String out = null;
            switch (unitSystem) {
                case IMPERIAL: 
                    out = formatLengthImperial(length);
                    break;
                case METRIC:
                    out = formatLengthMetric(length);
                    break;
                default:
            }
            return out;
        }
	
        public static String formatLengthMetric(double length) {
            if (length > KM_TRESHOLD) {
                return String.format("%.3f %s", length / 1000, Bundle.scale_kilometer());
            } else {
                return String.format("%.1f ", length, Bundle.scale_meter());
            }
        }
        
        public static String formatLengthImperial(double length) {
            if (length > MILE_TO_M) {
                return String.format("%.3f %s", length / MILE_TO_M, Bundle.scale_mile());
            } else {
                return String.format("%.1f %s", length / FEET, Bundle.scale_feet());
            }
        }
        
        public static String formatArea(double area) {
            UnitSystem unitSystem = getUnitSystem();
            String out = null;
            switch (unitSystem) {
                case IMPERIAL: 
                    out = formatAreaImperial(area);
                    break;
                case METRIC:
                    out = formatAreaMetric(area);
                    break;
                default:
            }
            return out;
        }
        
        public static String formatAreaMetric(double area) {
            if (area > 1) {
                return String.format("%.3f %s", area, Bundle.scale_hectar());
            } else {
                 return String.format("%.2f %s", area * HECTAR_TO_METER, Bundle.scale_square_meter() );
            }
        }
        
        public static String formatAreaImperial(double area) {
            if (area > (1/HA_TO_ACRES)) {
                return String.format("%.3f %s", area * HA_TO_ACRES, Bundle.scale_acres());
            } else {
                return String.format("%.2f %s", area * HECTAR_TO_METER * SQUARE_M_TO_FOOT, Bundle.scale_square_feet());
            }
        }
        
	private static long getScaleValueMetric(int zoom) { // km/m
		if (zoom < 0) {
			throw new IllegalArgumentException("zoom must be positive");
		}
		if (zoom > SCALE_VALUES_METRIC.length) {
			throw new IllegalAccessError(String.format("maximum zoom value can be %d", SCALE_VALUES_METRIC.length));
		}
		
		return SCALE_VALUES_METRIC[zoom-1];
	}
	
	private static long getScaleValueImperial(int zoom) { // mi/ft
		if (zoom < 0) {
			throw new IllegalArgumentException("zoom must be positive");
		}
		if (zoom > SCALE_VALUES_IMPERIAL.length) {
			throw new IllegalAccessError(String.format("maximum zoom value can be %d", SCALE_VALUES_IMPERIAL.length));
		}
		
		return SCALE_VALUES_IMPERIAL[zoom-1];
	}
	
	public static double getUnitToMetersRatio() {
		UnitSystem systemUnit = getUnitSystem();
		
		switch (systemUnit) {
		case METRIC:
			return 1d;
		case IMPERIAL:
			return FEET;
		default:
			return 1d;
		}

	}
	
	public enum UnitSystem {
		
		METRIC,
		IMPERIAL;
	
	}
	
}
