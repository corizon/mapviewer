/**
 * Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 * AgroSense is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the FLOSS License Exception
 * <http://www.agrosense.eu/foss-exception.html>.
 *
 * AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.data.util;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.SwingUtilities;
import org.openide.util.Exceptions;

/**
 * Checks connection availability.
 *
 * <p>
 * It uses {@link java.net.InetAddress#getByName(java.lang.String) to identify if machine is connected to internet, and
 * <br>
 * {@link java.net.Socket} to check, if address www.openstreetmap.org is accessible.
 * </p>
 *
 * <p>
 * Check is running periodically in a {@link java.util.Timer} with period 5000 ms.
 * </p>
 *
 * <p>
 * When status changes (from online to offline and vice versa) {@link java.beans.PropertyChangeListener} is fired.
 * </p>
 *
 * @author Frantisek Post
 */
public class ConnectionChecker {

	//TODO connection checker should check address of map server. but we have two+ map servers
    private static final int INITIAL_DELAY = 0;
    private static final long CHECK_PERIOD_DEFAULT = 5000; //5s period of checking connection to openstreetmap.org
    private static final String HOST_NAME_DEFAULT = "www.openstreetmap.org";
    private static final int PORT_NUMBER_DEFAULT = 80;
    private static final String PROPERTY_NAME = "status";
    private static final ConnectionChecker instance = new ConnectionChecker();

    private PropertyChangeSupport propertyChangeSupport;
    private Timer timer;
    private boolean started = false;
    private boolean online = false;
    private long checkPeriod;
    private String hostName;
    private int portNumber;

    private ConnectionChecker() {
        propertyChangeSupport = new PropertyChangeSupport(this);
        checkPeriod = CHECK_PERIOD_DEFAULT;
        hostName = HOST_NAME_DEFAULT;
        portNumber = PORT_NUMBER_DEFAULT;
        start();
    }

    private TimerTask createTimerTask() {
        return new TimerTask() {

            @Override
            public void run() {
                doCheck();
            }

        };
    }

    private void doCheck() {
        boolean oldOnline = online;

        if (isOnlineFastCheck()) {
            try (Socket socket = new Socket(hostName, portNumber)) {
                online = (socket.getInputStream() != null);
            } catch (Throwable e) {
                online = false;
            }
        } else {
            online = false;
        }
        if (oldOnline != online) {
            propertyChangeSupport.firePropertyChange(PROPERTY_NAME, oldOnline, online);
        }

    }

    /**
     * Fast check, if we have internet connection <br>
     * Doesn't say, if map server is accessible
     * 
     * WARNING: Not always fast, it uses dns, so if dns is really slow, this method
     * is very slow as well.
     *
     * @return
     */
    private boolean isOnlineFastCheck() {
        boolean check = false;
        try {
            InetAddress.getByName(hostName);
            check = true;
        } catch (UnknownHostException e) {
            //noop
        }
        return check;
    }

    // API //
    /**
     * Returns singleton instance
     *
     * @return
     */
    public static ConnectionChecker getInstance() {
        return instance;
    }

    /**
     * Returns checker state
     * <p>
     * Checker is started automatically when util module is loaded </p.
     *
     * @return checker state
     */
    public boolean isStarted() {
        return started;
    }

    /**
     * Starting checker.
     * <p>
     * Checking timer starts immediately, with default period 2000 ms.
     * </p>
     */
    public void start() {
        if (!started) {
            timer = new Timer("onlineChecker", true);
            timer.schedule(createTimerTask(), INITIAL_DELAY, checkPeriod);
            started = true;
        }
    }

    /**
     * Stopping checker
     */
    //TODO test starting and stopping
    public void stop() {
        if (started) {
            timer.cancel();
            timer = null;
            started = false;
        }
    }

    /**
     * Returns online status
     * <p>
     * {@link Boolean} type is used to be able to return answer "I don't know"
     * in case the checker is not running
     * <p>
     *
     * <p>
     * First testing if there is a connection. This should be quite fast. If
     * connection is available, then return result of test in timer.
     * </p>
     * NOTE: Displable fast check since this could be blocking in case of slow dns server
     * method just returns online state now.
     *
     * @return online status
     */
    public boolean isOnline() {
        
//        if (!isOnlineFastCheck()) {
//            return false;
//        } else {
            return online;
//        }
    }

    /**
     * Add property change listener
     * <p>
     * Listener is fired with property name {@code status} in case status
     * changes (online->offline, offline->online)
     * <p>
     * @param listener
     */
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    /**
     * Remove property change listener
     *
     * @param listener
     */
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    /**
     * Return time in milliseconds between two checks
     *
     * @return
     */
    public long getCheckPeriod() {
        return checkPeriod;
    }

    /**
     * Set time in milliseconds between two checks
     * <p>
     * To apply new settings call {@link #restart()}
     * </p>
     *
     * @param checkPeriod
     */
    public void setCheckPeriod(long checkPeriod) {
        this.checkPeriod = checkPeriod;
    }

    /**
     * Return hostname, that is used to checked online status
     * @return 
     */
    public String getHostName() {
        return hostName;
    }

    /**
     * Set hostname to check online status
     * <p>
     * To apply new settings call {@link #restart()}
     * </p>
     *
     * @param hostName
     */
    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    /**
     * Return checked port number
     *
     * @return
     */
    public int getPortNumber() {
        return portNumber;
    }

    /**
     * Set port number
     * <p>
     * To apply new settings call {@link #restart()}
     * </p>
     *
     * @param portNumber
     */
    public void setPortNumber(int portNumber) {
        this.portNumber = portNumber;
    }

    /**
     * Restart checker to apply new settings.
     * <p>
     * Actually calling {@link #stop()} and {@link start}
     * </p>
     */
    public void restart() {
        stop();
        start();
    }
}
