/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.data.cache;

import nl.bebr.mapviewer.data.Tile;


/**
 * Transfer object with information about {@link Tile}
 * 
 * @author Frantisek Post
 */
public class TileCacheInfo {
    
	public static final String FILE_NAME_FORMAT = "%dx%d.%s";
	private static final String DEFAULT_EXTENSION = "png";
	
    private int zoom;
    private int x;
    private int y;
    private String path;
    private String extension = DEFAULT_EXTENSION;
    private String type = null;
    
    /**
     * Constructor
     * 
     * @param zoom zoom level
     * @param x x coordinates
     * @param y y coordinates
     */
    public TileCacheInfo(int zoom, int x, int y) {
        this(zoom, x, y, null);
    }
    
    /**
     * Constructor
     * 
     * @param zoom
     * @param x
     * @param y
     * @param type
     */
    public TileCacheInfo(int zoom, int x, int y, String type) {
    	this(zoom, x, y, type, DEFAULT_EXTENSION);
    }
    
    /**
     * Constructor
     * 
     * @param zoom
     * @param x
     * @param y
     * @param type
     * @param extension
     */
    public TileCacheInfo(int zoom, int x, int y, String type, String extension) {
    	this.zoom = zoom;
    	this.x = x;
    	this.y = y;
    	this.type = type;
    	this.extension = extension;
    	path = getFileName(x, y);
    }
    
    private String getFileName(int x, int y) {
        return String.format(FILE_NAME_FORMAT, x, y, extension);
    }

    /**
     * Gets x coordinate
     * @return x coordinate
     */
    public int getX() {
        return x;
    }

    /**
     * Gets y coordinate
     * @return y coordinate
     */
    public int getY() {
        return y;
    }

    /**
     * Gets zoom level
     * @return zoom level
     */
    public int getZoom() {
        return zoom;
    }

    /**
     * returns only filename, without zoom directory.
     * <p>
     * This is because I need to create file with this name and don't want to parse it again to separate directory and filename
     * </p>
     * @return 
     */
    public String getPath() {
        return path;
    }

    public void setExtension(String extension) {
		this.extension = extension;
		path = getFileName(x, y);
	}
    
    /**
     * Type of the map - e.g. satellite, map, hybrid etc.
     * @param type
     */
    public void setType(String type) {
		this.type = type;
	}
    
    public String getType() {
		return type;
	}

	@Override
	public String toString() {
		return "TileCacheInfo [zoom=" + zoom + ", x=" + x + ", y=" + y
				+ ", path=" + path + ", extension=" + extension + ", type="
				+ type + "]";
	}
    
}
