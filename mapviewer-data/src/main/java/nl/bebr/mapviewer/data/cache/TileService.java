/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.data.cache;


/**
 * Interface of Tile service.
 * <p>
 * Defines methods for check if service contain some {@link Tile} - {@link #contains(TileCacheInfo)}<br><br>
 * and method to load the {@link Tile} from cache - {@link #getTile(TileCacheInfo)}. 
 * <p>
 *
 * @author Frantisek Post
 */
public interface TileService<T> {

    /**
     * Is tile, defined by param, provided by this service?
     * 
     * @param tileCacheInfo
     * @return 
     */
    boolean contains(TileCacheInfo tileCacheInfo);
    
    /**
     * Get tile image
     * 
     * @param tileCacheInfo
     * @return 
     */
    T getTile(TileCacheInfo tileCacheInfo); 
    
}
