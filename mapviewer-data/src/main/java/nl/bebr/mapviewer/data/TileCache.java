/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.data;

import java.io.IOException;
import java.net.URI;
import java.util.LinkedList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
* An implementation only class for now. For internal use only.
*
* @author joshua.marinacci@sun.com, Frantisek Post
*/
public abstract class TileCache<T> {
  
  private static final Logger LOG = Logger.getLogger(TileCache.class.getName());
  private static final long CACHE_SIZE = 1024 * 1024 * 50; //50MB maximum cache size
  private LinkedList<URI> imgmapAccessQueue = new LinkedList<>();
  private int imagesize = 0;
    
  /**
   * Put a tile image into the cache. This puts both a buffered image and array of bytes that make up the compressed image.
   *
   * @param uri
   *            URI of image that is being stored in the cache
   * @param img
   *            image to store in the cache
   */
  public void put(URI uri, T img) {
      addToImageCache(uri, img);
  }

  /**
   * Returns a buffered image for the requested URI from the cache. This method must return null if the image is not in the cache. If the image is unavailable
   * but it's compressed version *is* available, then the compressed version will be expanded and returned.
   *
   * @param uri URI of the image previously put in the cache
   * @return the image matching the requested URI, or null if not available
   * @throws java.io.IOException
   */
  public T get(URI uri) throws IOException {
      synchronized (getImgMap()) {
          if (getImgMap().containsKey(uri)) {
              imgmapAccessQueue.remove(uri);
              imgmapAccessQueue.addLast(uri);
              return getImgMap().get(uri);
          }
      }
      return null;
  }

  protected abstract int getImageSizeInBytes(T image);
  
  /**
   * target implementation should return static imageMap
 * @return
 */
  protected abstract Map<URI, T> getImgMap();
  
  /**
   * Request that the cache free up some memory. How this happens or how much memory is freed is up to the TileCache implementation. Subclasses can implement
   * their own strategy. The default strategy is to clear out all buffered images but retain the compressed versions.
   */
  public void needMoreMemory() {
	  synchronized (getImgMap()) {
		  getImgMap().clear();
	  }
  }

    private void cleanTheCache() {
        int retryCounter = 10; //10 attempts and stop
        while (imagesize > CACHE_SIZE && retryCounter >= 0) {
            URI olduri = imgmapAccessQueue.removeFirst();
            T oldimg = getImgMap().remove(olduri);
            if (oldimg != null) {
                imagesize -= getImageSizeInBytes(oldimg);
            }
            retryCounter--;
        }
    }
    
  private void addToImageCache(final URI uri, final T img) {
      synchronized (getImgMap()) {
          if (imagesize > CACHE_SIZE) {
              LOG.log(Level.FINEST, "Cache needs to be cleared, actual size is: {0} MB", imagesize / (1024*1024));
              cleanTheCache();
          }

          getImgMap().put(uri, img);
          imagesize += getImageSizeInBytes(img);
          imgmapAccessQueue.addLast(uri);
      }
  }
  
  public void clear() {
      getImgMap().clear();
      imgmapAccessQueue.clear();
      imagesize = 0;
  }

}
