/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
 package nl.bebr.mapviewer.data;

import nl.bebr.mapviewer.data.util.AbstractBean;


/**
 * A Waypoint is a GeoPosition that can be drawn on a may using 
 * a WaypointPainter.
 * @author joshy
 */
public class Waypoint extends AbstractBean {
    private GeoPosition position;

    /**
     * Creates a new instance of Waypoint at lat/long 0,0
     */
    public Waypoint() {
        this(new GeoPosition(0, 0));
    }
    
    /**
     * Creates a new instance of Waypoint at the specified
     * latitude and longitude
     * @param latitude new latitude
     * @param longitude new longitude
     */
    public Waypoint(double latitude, double longitude) {
        this(new GeoPosition(latitude,longitude));
    }
    
    /**
     * Creates a new instance of Waypoint at the specified
     * GeoPosition
     * @param coord a GeoPosition to initialize the new Waypoint
     */
    public Waypoint(GeoPosition coord) {
        this.position = coord;
    }
    
    /**
     * Get the current GeoPosition of this Waypoint
     * @return the current position
     */
    public GeoPosition getPosition() {
        return position;
    }

    /**
     * Set a new GeoPosition for this Waypoint
     * @param coordinate a new position
     */
    public void setPosition(GeoPosition coordinate) {
        GeoPosition old = getPosition();
        this.position = coordinate;
        firePropertyChange("position", old, getPosition());
    }
    
    
}
