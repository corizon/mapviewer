/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.data.cache.spi;

import nl.bebr.mapviewer.data.cache.spi.CacheCleaner;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.reflect.Field;
import java.util.Date;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;


/**
 * 
 * @author Frantisek Post
 */
public class CacheCleanerTest {

    private MockCacheCleaner cacheCleaner;
    private FileObject[] files;
    
    private static final char[] data;
    
    static {
        data = new char[200];
        for (int i = 0; i < 200; i++) {
            data[i] = '0';
        }
    }
    
    @Before
    public void setUp() throws IOException {
        FileSystem memoryFileSystem = FileUtil.createMemoryFileSystem();
        FileObject cacheRoot = memoryFileSystem.getRoot();
        files = new FileObject[5];
        FileObject zoomFolder = cacheRoot.createFolder("8");
        long timeBase = System.currentTimeMillis() - 10000;
        
        for (int i = 0; i < 5; i++) {
            files[i] = zoomFolder.createData(String.format("%dx5.png", i));
            writeDataToFile(files[i]);
            setLastModified(files[i], timeBase + i * 1000);
        }
        cacheCleaner = new MockCacheCleaner(cacheRoot);
    }
    
     private void writeDataToFile(FileObject file) throws IOException {
        OutputStreamWriter writer = new OutputStreamWriter(file.getOutputStream());
        writer.write(data);
        writer.close();
    }
    
    @Test
    public void testCleaner_on_demand() throws IOException, InterruptedException {   
        cacheCleaner.runInternal(); //calling internal method, because it is called from new Thread when invoking run method
        
        //first 3 files should be deleted
        Assert.assertFalse(files[0].isValid());
        Assert.assertFalse(files[1].isValid());
        Assert.assertFalse(files[2].isValid());
        Assert.assertTrue(files[3].isValid());
        Assert.assertTrue(files[4].isValid());
    }
    
    @Test
    public void testCleaner_on_demand_with_touch() throws IOException, InterruptedException {
        //touch files, to mark them as "used recently" and those won't be deleted
        setLastModified(files[0], System.currentTimeMillis());
        setLastModified(files[1], System.currentTimeMillis());
        cacheCleaner.runInternal();

        Assert.assertTrue(files[0].isValid());
        Assert.assertTrue(files[1].isValid());
        Assert.assertFalse(files[2].isValid());
        Assert.assertFalse(files[3].isValid());
        Assert.assertFalse(files[4].isValid());
    }

    @Test
    public void testCleaner_on_maximum_size_change() throws IOException, InterruptedException {
        cacheCleaner.maximumCacheSize = 1000; //some hight value, so setting it back to 700 will cause invocation of method run 
        cacheCleaner.setMaximumCacheSize(700);
        
        Assert.assertEquals(1, cacheCleaner.getRunCallsCount()); // run() should be called
    }
    
    public static class MockCacheCleaner extends CacheCleaner {

        private int runCallsCount = 0;
        private FileObject rootDir;
        
        public MockCacheCleaner(FileObject rootDir) {
            this.rootDir = rootDir;
            maximumCacheSize = 500;
        }

        @Override
        public FileObject getRootDir() {
            return rootDir;
        }
        
        @Override
        public void run() {
            runCallsCount++;
        }

        public int getRunCallsCount() {
            return runCallsCount;
        }
    }
       
    //ugly, but only way how to set last modified date
    private void setLastModified(FileObject fileObject, long modified) {
        try {
            Field field = fileObject.getClass().getDeclaredField("lastModified");
            field.setAccessible(true);
            field.set(fileObject, new Date(modified));
        } catch (IllegalAccessException ex) {
            //noop
        } catch (NoSuchFieldException ex) {
            //noop
        }
    }
    
}
