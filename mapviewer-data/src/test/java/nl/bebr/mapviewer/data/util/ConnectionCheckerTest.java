/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.data.util;

import nl.bebr.mapviewer.data.util.ConnectionChecker;
import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


/**
 *
 * @author Frantisek Post
 */
public class ConnectionCheckerTest {

    private static final int MAX_DELAY = 100; //10 ms maximum delay to check online status //TODO FP tune the value
    
    @Before
    public void prepare() {
        ConnectionChecker.getInstance().start();
    }
    
    @After
    public void cleanUp() {
        ConnectionChecker.getInstance().stop();
    }
    
    @Test
    public void testStartAndStop() {
        ConnectionChecker checker = ConnectionChecker.getInstance();
        
        Assert.assertTrue(checker.isStarted());
        
        checker.stop();
        Assert.assertFalse(checker.isStarted());
        
        checker.start();
        Assert.assertTrue(checker.isStarted());
    }
    
    @Test
    public void testCheckDelay() {
        ConnectionChecker checker = ConnectionChecker.getInstance();
        int attepts = 10;
        long summaryDelay = 0;
        for (int i = 0; i < attepts; i++) {
            long start = System.nanoTime();
            checker.isOnline();
            long finish = System.nanoTime();
            long delay = (finish - start) / 1000000; //to miliseconds
            summaryDelay += delay;
        }
        summaryDelay /= attepts; //average of 5 measurements to fix peak value causing test failure 
        Assert.assertTrue("Maximum delay of "+MAX_DELAY+" ms exceeded",summaryDelay < MAX_DELAY);
    }
    
 }
