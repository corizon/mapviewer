/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.data.cache.spi;

import nl.bebr.mapviewer.data.cache.spi.CacheManager;
import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;

import nl.bebr.mapviewer.data.cache.TileCacheInfo;

/**
 *
 * @author Frantisek Post
 */
public class CacheManagerTest {

    private static final String EXTENSION = "png";
    private CacheManager cacheManager;
    
    @Before
    public void prepare() throws IOException {
        FileSystem memoryFileSystem = FileUtil.createMemoryFileSystem();
        FileObject cacheRoot = memoryFileSystem.getRoot();
        
        cacheManager = new MockCacheManager(cacheRoot);
        cacheManager.setZoomLevel(8);
        cacheManager.setMapTypeFolder("map");
        
        FileObject typeRoot = cacheRoot.createFolder("map");
        
        FileObject zoom8Dir = typeRoot.createFolder("8");
        FileObject zoom5Dir = typeRoot.createFolder("9");
        typeRoot.createFolder("DIR");
        
        zoom8Dir.createData("image.png");
        zoom8Dir.createData("1x5", EXTENSION);
        zoom8Dir.createData("167x56", EXTENSION);
        zoom5Dir.createData("21x15", EXTENSION);
        zoom5Dir.createData("17x63", EXTENSION);
    }
    
    @Test
    public void testContains() {
        cacheManager.refresh();
        
        Assert.assertTrue(cacheManager.isCached(new TileCacheInfo(8, 1, 5, "map")));
        Assert.assertTrue(cacheManager.isCached(new TileCacheInfo(8, 167, 56,"map")));
        Assert.assertTrue(cacheManager.isCached(new TileCacheInfo(9, 21, 15,"map")));
        Assert.assertTrue(cacheManager.isCached(new TileCacheInfo(9, 17, 63,"map")));
        
        Assert.assertFalse(cacheManager.isCached(new TileCacheInfo(1, 1, 5,"map")));
        Assert.assertFalse(cacheManager.isCached(new TileCacheInfo(3, 7, 3,"map")));
    }
    
    private static class MockCacheManager extends CacheManager {
     
        private FileObject cacheRoot;
        
        public MockCacheManager(FileObject cacheRoot) {
            this.cacheRoot = cacheRoot;
        }

        @Override
        public FileObject getCacheFolder() {
            return cacheRoot;
        }
        
    }
}
