/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.data.tilefactory;

import nl.bebr.mapviewer.data.tilefactory.VirtualEarthTileFactoryInfo;
import java.net.URI;
import java.net.URISyntaxException;

import junit.framework.Assert;

import org.junit.Test;

import nl.bebr.mapviewer.data.cache.TileCacheInfo;
import nl.bebr.mapviewer.data.tilefactory.VirtualEarthTileFactoryInfo.MVEMode;

public class VirtualEarthTileFactoryInfoTest {

	@Test
	public void testURLTranslation() throws URISyntaxException {
		VirtualEarthTileFactoryInfo info = new VirtualEarthTileFactoryInfo(VirtualEarthTileFactoryInfo.MAP);
		
		int x = 1;
		int y = 2;
		int zoom = 3;
		
		String tileUrl = info.getTileUrl(x, y, zoom);
		
		TileCacheInfo cacheInfo = info.getCacheInfo(new URI(tileUrl));
		
		Assert.assertEquals(x, cacheInfo.getX());
		Assert.assertEquals(y, cacheInfo.getY());
		Assert.assertEquals(zoom, cacheInfo.getZoom());
	}
	
}
