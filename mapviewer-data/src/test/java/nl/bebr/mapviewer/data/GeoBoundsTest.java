/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.data;

import nl.bebr.mapviewer.data.GeoPosition;
import nl.bebr.mapviewer.data.GeoBounds;
import java.util.HashSet;
import java.util.Set;

import junit.framework.Assert;

import org.junit.Test;


public class GeoBoundsTest {

	@Test
	public void testIntersect() {
		GeoBounds tested = new GeoBounds(10, 10, 50, 30);
		GeoBounds outside = new GeoBounds(0, 0, 20, 9);
		GeoBounds inside = new GeoBounds(20, 20, 40, 25);
		GeoBounds partiallyInside = new GeoBounds(0, 0, 40, 25);
		
		Assert.assertFalse(tested.intersects(outside));
		Assert.assertTrue(tested.intersects(inside));
		Assert.assertTrue(tested.intersects(partiallyInside));
	}
	
	@Test
	public void testNorthWestSouthEast() {
		GeoBounds tested = new GeoBounds(10, 12, 50, 30);
		GeoPosition northWest = tested.getNorthWest();
		GeoPosition southEast = tested.getSouthEast();
		
		Assert.assertEquals(12d, northWest.getLatitude());
		Assert.assertEquals(50d, northWest.getLongitude());
		
		Assert.assertEquals(30d, southEast.getLatitude());
		Assert.assertEquals(10d, southEast.getLongitude());
	}
	
	@Test
	public void testMultiContructor() {
		Set<GeoPosition> boundSet = new HashSet<>(); 
		boundSet.add(new GeoPosition(10, 10));
		boundSet.add(new GeoPosition(20, 20));
		boundSet.add(new GeoPosition(40, 0));
		boundSet.add(new GeoPosition(200, 5));
		boundSet.add(new GeoPosition(30, 30));
		boundSet.add(new GeoPosition(50, 60));
		boundSet.add(new GeoPosition(7, 90));
		boundSet.add(new GeoPosition(0, 30));
		boundSet.add(new GeoPosition(60, 60));
		boundSet.add(new GeoPosition(5, 5));
		
		GeoBounds tested = new GeoBounds(boundSet);
		GeoPosition northWest = tested.getNorthWest();
		GeoPosition southEast = tested.getSouthEast();
		
		Assert.assertEquals(0d, northWest.getLatitude());
		Assert.assertEquals(200d, northWest.getLongitude());
		
		Assert.assertEquals(90d, southEast.getLatitude());
		Assert.assertEquals(0d, southEast.getLongitude());
	}
	
}
