/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.data.util;

import nl.bebr.mapviewer.data.util.ConnectionChecker;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Scanner;
import org.junit.Ignore;


/**
 *
 * @author Frantisek Post
 */
@Ignore
public class DemoConnectionChecker {
    
    public static void main(String[] args) {
        final ConnectionChecker checker = ConnectionChecker.getInstance();
        checker.start();
        checker.addPropertyChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                System.out.println("STATUS CHANGED, now I am " + (checker.isOnline()? "online" : "offline"));
            }
            
        });
        System.out.println("ONLINE " + checker.isOnline());
        Scanner scanner = new Scanner(System.in);
        scanner.next();
        scanner.close();
    }
    
}
