[![Maven Central](https://maven-badges.herokuapp.com/maven-central/nl.bebr/mapviewer/badge.png)](https://maven-badges.herokuapp.com/maven-central/nl.bebr/mapviewer)
[![Jenkins](http://img.shields.io/jenkins/s/http/build.agrosense.eu:8080/mapviewer~core-ci.png)](http://build.agrosense.eu:8080/job/mapviewer~core-ci)

#MapViewer

The mapviewer is a modular, feature rich map for Java applications.

Available for:

* NetBeans platform , as module
* Java Swing
* JavaFX

Main Features:

* Map Background switch (build in support for openstreetmap and bing satalite maps)
* Display current weather and forecast on the map (source: openweatherdata)
* Map cache, parts of the map the user visited will be available without internet connection as well
* Draw and split geometries
* Reorder layers, show/hide layers
* Drag and drop geographical objects onto the map to visualize them


## NetBeans module 

```
#!xml
        <dependency>
            <groupId>nl.bebr</groupId>
            <artifactId>mapviewer-nb-swing</artifactId>
            <version>1.5.0</version>
        </dependency>
```

Note:

* For NetBeans version RELEASE12.6 and higher use 1.5.0
* For NetBeans version RELEASE80 and higher use 1.4
* For NetBeans version RELEASE73 and lower use 1.3

## Swing

```
#!xml
        <dependency>
            <groupId>nl.bebr</groupId>
            <artifactId>mapviewer-swing</artifactId>
            <version>1.5.0</version>
        </dependency>
```

## JavaFX (experimental)

```
#!xml
        <dependency>
            <groupId>nl.bebr</groupId>
            <artifactId>mapviewer-javafx</artifactId>
            <version>1.5.0</version>
        </dependency>
```