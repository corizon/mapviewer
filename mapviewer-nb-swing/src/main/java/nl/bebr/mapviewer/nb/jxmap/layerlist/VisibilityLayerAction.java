/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.nb.jxmap.layerlist;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;


import org.openide.util.NbBundle;

import nl.bebr.mapviewer.api.LayerAction;
import nl.bebr.mapviewer.api.LayerInfo;

/**
 * Action for toggling the visibility of a layer.
 * 
 * @author Timon Veenstra
 */
@NbBundle.Messages({"visibility layer hide=Hide",
                    "visibilityv layer show=Show"})
public class VisibilityLayerAction extends LayerAction {
    private final LayerInfo layerInfo;

    private static final long serialVersionUID = -1222090203615272257L;

    public VisibilityLayerAction(final LayerInfo layerInfo) {
        this.layerInfo = layerInfo;
        
        setName(layerInfo);

        layerInfo.addPropertyChangeListener(LayerInfo.PROPERTY_VISIBLE, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                setName(layerInfo);
            }
        });
    }

    private void setName(LayerInfo layerInfo){
        if (layerInfo.isVisible()) {
            putValue(NAME, Bundle.visibility_layer_hide());
        } else {
            putValue(NAME, Bundle.visibilityv_layer_show());
        }
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        layerInfo.setVisible(!layerInfo.isVisible());
    }

    public LayerInfo getLayerInfo() {
        return layerInfo;
    }
    
    
}
