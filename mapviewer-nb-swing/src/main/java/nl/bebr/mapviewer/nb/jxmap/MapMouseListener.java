/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.nb.jxmap;

import nl.bebr.mapviewer.swing.jxmap.map.MapMouseEvent;

/**
 *
 * @author Timon Veenstra
 */
//TODO every listener should (javadoc says must) extend EventListener
public interface MapMouseListener {

    RootMapPanel.LayerGroup getGroup();

    /**
     * unique layer id within a group
     * 
     * @return 
     */
    int getId();

    /**
     * gets triggered for all mouse events.
     * 
     * @param mapMouseEvent 
     */
    void onMapMouseEvent(MapMouseEvent mapMouseEvent);

    /**
     * 
     * @param mapMouseEvent 
     */
    void mapMousePressed(MapMouseEvent mapMouseEvent);

    /**
     * 
     * @param mapMouseEvent 
     */
    void mapMouseReleased(MapMouseEvent mapMouseEvent);

    /**
     * 
     * @param mapMouseEvent 
     */
    void mapMouseDragged(MapMouseEvent mapMouseEvent);
}
