/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.nb.jxmap.layerlist;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.Action;


import org.openide.actions.ToolsAction;
import org.openide.nodes.Node;
import org.openide.util.ImageUtilities;
import org.openide.util.actions.SystemAction;

//import eu.limetri.api.geo.DynamicGeometrical;
import eu.limetri.api.geo.Geographical;
import nl.bebr.mapviewer.api.HasMapActions;
import nl.bebr.mapviewer.api.Layer;
import nl.bebr.mapviewer.api.LayerInfo;
import nl.bebr.mapviewer.api.LayerListRemovableNode;
import nl.bebr.mapviewer.api.SingleObjectLayer;
import nl.bebr.mapviewer.api.SplitGeometryHandler;
import nl.bebr.mapviewer.nb.util.SelectFieldInMapAction;
import nl.bebr.mapviewer.swing.jxmap.MapProperties;
import nl.bebr.mapviewer.swing.jxmap.layerlist.AbstractLayerListNode;
import nl.bebr.mapviewer.swing.jxmap.map.CreateGeometryAction;
import nl.bebr.mapviewer.swing.jxmap.map.SplitGeometryAction;
import nl.bebr.mapviewer.swing.jxmap.layerlist.FilterLayerListChildren;
import javax.swing.AbstractAction;
import org.openide.util.NbBundle;
import nl.bebr.mapviewer.swing.jxmap.map.RemoveLayerAction;

/**
 *
 * @author Timon Veenstra
 */
@NbBundle.Messages({
    "LayerAction_top=Move to the top",
    "LayerAction_up=Move up",
    "LayerAction_down=Move down",
    "LayerAction_bottom=Move to the bottom"
})
class LayerListNode extends AbstractLayerListNode implements LayerListRemovableNode {

    private final SettingsForLayerAction settingsAction;
    private boolean visible = true;

    @Override
    public Image getIcon(int type) {
        Image icon = getOriginal().getIcon(type);
        if (!visible) {
            icon = ImageUtilities.createDisabledImage(icon);
        }
        return icon;
    }

    LayerListNode(LayerListController.LayerWrapper layerWrapper) { 
        super(layerWrapper.getLayerNode(), new FilterLayerListChildren(layerWrapper.getLayerNode()), layerWrapper.getLayerInfo());
        Layer layer = getLookup().lookup(Layer.class);
        settingsAction = new SettingsForLayerAction(info, layer);
        visible = info.isVisible();
        initListener();
    }

    private void initListener() {
        info.addPropertyChangeListener(LayerInfo.PROPERTY_VISIBLE, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                visible = (Boolean) evt.getNewValue();
            }
        });
    }

    @Override
    public Action[] getActions(boolean context) {
        final Layer layer = getLookup().lookup(Layer.class);

        List<Action> superActions = new ArrayList();
        //Remove the ToolsAction from the list
        if (super.getActions(context) != null && super.getActions(context).length > 0) {
            //New ArrayList because of abstractList return
            List<Action> actions = new ArrayList(Arrays.asList(super.getActions(context)));
            for (int i = 0; i < actions.size(); i++) {
                if (actions.get(i) instanceof ToolsAction) {
                    actions.remove(i);
                }
            }

            superActions.addAll(actions);
        }
        
        //FIXME: DelegateAction can be anything, delete just happens to be the only implementation in the current (rather limited) set of super-actions;
        //considering flipping the behavior: i.e. do not include the super actions and filter, but let the original node explicitly specify which ones to include (usually only the 'open' action)
        //requires separating getMapActions from DynamicGeometrical
        Action deleteAction = null;
        for (Action action: superActions) {
            if ("DelegateAction".equals(action.getClass().getSimpleName())) {
                deleteAction = action;
                break;
            }
        }
        
        if (deleteAction != null) {
            superActions.remove(deleteAction);
        }
        
        superActions.addAll(getMapActions());
        superActions.addAll(getGeometryActions(layer));
        superActions.addAll(getLayerActions());
        return superActions.toArray(new Action[superActions.size()]);
    }

    private List<Action> getMapActions() {
        List<Action> actions = new ArrayList<>();
        HasMapActions object = getLookup().lookup(HasMapActions.class);
        if (object != null) {
            actions.addAll(Arrays.asList(object.getMapActions()));
        }
        return actions;
    }

    private List<Action> getGeometryActions(Layer layer) {
        List<Action> actions = new ArrayList<>();
        if (layer instanceof SingleObjectLayer) {
            actions.add(new CreateGeometryAction((SingleObjectLayer) layer));

            if (getLookup().lookup(SplitGeometryHandler.class) != null) {
                actions.add(new SplitGeometryAction(this));
            }
        }
        Geographical geographical = getLookup().lookup(Geographical.class);
        if (geographical != null) {
            actions.add(new SelectFieldInMapAction(geographical));
        }
        //TODO: add LocationPicker for DynamicPoint
        return actions;
    }

    private List<Action> getLayerActions() {
        List<Action> actions = new ArrayList<>();

        if (MapProperties.getLayerNodeRemoveActionEnabledValue()) {
            actions.add(SystemAction.get(RemoveLayerAction.class));
        }

        if (MapProperties.getLayerNodeSettingsActionEnabledValue()) {
            actions.add(settingsAction);
        }
        if (MapProperties.getLayerNodevisibilityActionEnabledValue()) {
            actions.add(new VisibilityLayerAction(info) {

                @Override
                public void actionPerformed(ActionEvent ae) {
                    super.actionPerformed(ae);
                    fireIconChange();
                }
            });
        }

        actions.add(null);
        actions.add(new MoveToTopAction(info));
        actions.add(new MoveUpAction(info));
        actions.add(new MoveDownAction(info, getParentNode()));
        actions.add(new MoveToBottomAction(info, getParentNode()));
        
        return actions;
    }

    @Override
    public Action getPreferredAction() {
        Geographical geographical = getLookup().lookup(Geographical.class);
        if (geographical != null) {
            return new SelectFieldInMapAction(geographical);
        } else {
            return settingsAction;
        }
    }

    @Override
    public Node getRemovableNode() {
        return this;
    }
    
    public static class MoveToTopAction extends AbstractAction {

        private LayerInfo info;
        
        public MoveToTopAction(LayerInfo info) {
            super(NbBundle.getMessage(LayerListNode.class, "LayerAction_top"));
            this.info = info;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            info.setOrder(-1);
        }

        @Override
        public boolean isEnabled() {
            return info.getOrder() > 0;
        }
        
    }
    
    public static class MoveUpAction extends AbstractAction {

        private LayerInfo info;
        
        public MoveUpAction(LayerInfo info) {
            super(NbBundle.getMessage(LayerListNode.class, "LayerAction_up"));
            this.info = info;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            info.setOrder(info.getOrder() - 3);
        }

        @Override
        public boolean isEnabled() {
            return info.getOrder() > 0;
        }
        
    }
    
    public static class MoveDownAction extends AbstractAction {

        private LayerInfo info;
        private Node parentNode;
        
        public MoveDownAction(LayerInfo info, Node parentNode) {
            super(NbBundle.getMessage(LayerListNode.class, "LayerAction_down"));
            this.info = info;
            this.parentNode = parentNode;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            info.setOrder(info.getOrder() + 3);
        }

        @Override
        public boolean isEnabled() {
            int nodesCount = parentNode.getChildren().getNodesCount();
            int maxOrder = (nodesCount - 1) * 2;
            return info.getOrder() < maxOrder;
        }
        
    }
    
    public static class MoveToBottomAction extends AbstractAction {

        private LayerInfo info;
        private Node parentNode;
        
        public MoveToBottomAction(LayerInfo info, Node parentNode) {
            super(NbBundle.getMessage(LayerListNode.class, "LayerAction_bottom"));
            this.info = info;
            this.parentNode = parentNode;
        }

        private int getMaxOrder() {
            int nodesCount = parentNode.getChildren().getNodesCount();
            int maxOrder = (nodesCount - 1) * 2;
            return maxOrder;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            int maxOrder = getMaxOrder();
            info.setOrder(maxOrder + 1);
        }

        @Override
        public boolean isEnabled() {
            int maxOrder = getMaxOrder();
            return info.getOrder() < maxOrder;
        }
        
    }
}
