/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.nb.jxmap.layerlist;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.ActionMap;
import javax.swing.JComponent;


import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

import nl.bebr.mapviewer.api.Layer;
import nl.bebr.mapviewer.api.LayerInfo;
import nl.bebr.mapviewer.api.util.ExplorerManagerUtil;
import nl.bebr.mapviewer.swing.jxmap.MapProperties;
import nl.bebr.mapviewer.swing.jxmap.layerlist.LayerListPanel;

/**
 *
 * @author Timon Veenstra
 */
public class LayerListController implements ExplorerManager.Provider, Lookup.Provider {

    private final ExplorerManager manager = new ExplorerManager();
    private final List<LayerWrapper> layerNodes = Collections.synchronizedList(new ArrayList<LayerWrapper>());
    private final LayerListPanel layerListPanel = new LayerListPanel(manager);
    private final Lookup lookup = ExplorerUtils.createLookup(manager, new ActionMap());
    //
    // selection change listener on the explorer manager of the layerlist
    // will search for the topComponent explorer manager ( which is the explorer
    //  manager of the map) and propagate selection changes
    //
    private final PropertyChangeListener selectionChangeListener = new PropertyChangeListener() {
        @Override
        public void propertyChange(final PropertyChangeEvent evt) {
            if (ExplorerManager.PROP_SELECTED_NODES.equals(evt.getPropertyName())) {
                assert evt.getNewValue() instanceof Node[] : "selecte node events should progatate Node[] as new value";
                List<Layer> selectedLayers = new ArrayList<>();
                for (Node node : (Node[]) evt.getNewValue()) {
                    Layer layer = node.getLookup().lookup(Layer.class);
                    if (layer != null) {
                        selectedLayers.add(layer);
                    }
                }
                ExplorerManager em = ExplorerManager.find(layerListPanel);
                ExplorerManagerUtil.setSelectedObjects(selectedLayers.toArray(new Layer[selectedLayers.size()]), em);
            }
        }
    };

    public LayerListController() {
        manager.setRootContext(new LayerListRootNode(new LayerListChildFactory(this)));
        manager.addPropertyChangeListener(selectionChangeListener);

        if (MapProperties.getLayerListExpandedValue()) {
            layerListPanel.expand();
        } else {
            layerListPanel.collapse();
        }
    }

    public void addLayerNode(Node layerNode, LayerInfo layerInfo) {
        LayerWrapper layerWrapper = new LayerWrapper(layerNode, layerInfo);
        if (!layerNodes.contains(layerWrapper)) {
            layerNodes.add(layerWrapper);
            ((LayerListRootNode) manager.getRootContext()).refresh();
        }
    }

    public void removeLayerNode(Node layerNode) {
        // can remove based on layer wrapper with null info since info is not in equals or hashcode
        layerNodes.remove(new LayerWrapper(layerNode, null));
        ((LayerListRootNode) manager.getRootContext()).refresh();
    }

    LayerWrapper[] getLayerNodes() {
        return layerNodes.toArray(new LayerWrapper[layerNodes.size()]);
    }

    public JComponent getLayerList() {
        return layerListPanel;
    }

    @Override
    public ExplorerManager getExplorerManager() {
        return manager;
    }

    @Override
    public Lookup getLookup() {
        return lookup;
    }

    static class LayerWrapper {

        private final Node layerNode;
        private final LayerInfo layerInfo;

        public LayerWrapper(Node layerNode, LayerInfo layerInfo) {
            this.layerNode = layerNode;
            this.layerInfo = layerInfo;
        }

        public Node getLayerNode() {
            return layerNode;
        }

        public LayerInfo getLayerInfo() {
            return layerInfo;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final LayerWrapper other = (LayerWrapper) obj;
            if (this.layerNode != other.layerNode && (this.layerNode == null || !this.layerNode.equals(other.layerNode))) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 5;
            hash = 17 * hash + (this.layerNode != null ? this.layerNode.hashCode() : 0);
            return hash;
        }
    }
}
