/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.nb.util;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;


import org.openide.awt.ActionID;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle;

import eu.limetri.api.geo.Geographical;
import nl.bebr.mapviewer.api.event.GeoEvent;

/**
 * Action to select a field on the map
 *
 * @author Timon Veenstra
 */
@ActionRegistration(displayName = "#action.select_plot_in_map.name", iconBase = "nl/bebr/mapviewer/swing/jxmap/icons/map-go-icon.png")
@ActionID(category = "TaskBar/Area", id = "nl.bebr.mapviewer.swing.util.SelectFieldInMapAction")
@NbBundle.Messages("action.select_plot_in_map.name=Go to")
public class SelectFieldInMapAction extends AbstractAction {

    private final Geographical geographical;

    public SelectFieldInMapAction(Geographical geographical) {
        super(Bundle.action_select_plot_in_map_name());
        this.geographical = geographical;
    }

    /**
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        GeoEvent.getProducer().triggerEvent(GeoEvent.REQUEST_FOCUS, geographical);
    }
}
