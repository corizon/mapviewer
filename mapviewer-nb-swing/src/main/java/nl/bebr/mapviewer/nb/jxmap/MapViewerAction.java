/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.nb.jxmap;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

/**
 * Action which shows MapViewer component.
 *
 * @author Timon Veenstra
 */
public class MapViewerAction extends AbstractAction {

    public MapViewerAction() {
        super(NbBundle.getMessage(MapViewerAction.class, "CTL_MapViewerAction"));
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        TopComponent win = MapViewerTopComponent.findInstance();
        win.open();
        win.requestActive();
    }
}
