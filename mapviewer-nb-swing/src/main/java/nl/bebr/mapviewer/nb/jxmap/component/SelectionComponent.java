/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.nb.jxmap.component;

import java.awt.Component;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 * Combo showing always one fixed value, allows select same item simultaneously.<br>
 * This means, change listeners are fired with every selection.<br><br>
 *
 * Acts like toolbar popup button.
 * 
 * @author Frantisek Post
 */
public class SelectionComponent<T> extends JComboBox<T> {
    
    public SelectionComponent(T fixedValue) {
        super();
        setEditable(false);
        setRenderer(new Renderer(getRenderer(), fixedValue));
    }

    @Override
    public void firePopupMenuWillBecomeInvisible() {
        super.firePopupMenuWillBecomeInvisible(); 
        setSelectedIndex(-1); //reset the selection
    }

    @Override
    public void firePopupMenuCanceled() {
        super.firePopupMenuCanceled(); 
        setSelectedIndex(-1); //reset the selection
    }
    
    private class Renderer implements ListCellRenderer<T> {

        ListCellRenderer<? super T> originalRenderer;
        T fixedValue;
        
        private Renderer(ListCellRenderer<? super T> originalRenderer, T value) {
            this.originalRenderer = originalRenderer;
            this.fixedValue = value;
        }

        @Override
        public Component getListCellRendererComponent(JList<? extends T> list, T value, int index, boolean isSelected, boolean cellHasFocus) {
            Component component;
            
            if (index == -1) {
                component = originalRenderer.getListCellRendererComponent(list, fixedValue, index, isSelected, cellHasFocus);
            } else {
                component = originalRenderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            }
            
            return component;
        }
        
    }
    
}
