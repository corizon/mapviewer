/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.nb.jxmap;

import nl.bebr.mapviewer.swing.jxmap.MapDataManager;
import java.awt.BorderLayout;
import java.util.logging.Logger;

import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

/**
 * Top component which displays the map.
 */
@ConvertAsProperties(dtd = "-//nl.bebr.mapviewer.swing.jxmap//MapViewerTopComponent//EN", autostore = false)
@TopComponent.Description(preferredID = "MapViewerTopComponent",
iconBase="nl/cloudfarming/client/icon/map-icon.png", 
persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = "map", openAtStartup = true)
@ActionID(category = "Window", id = "nl.bebr.mapviewer.swing.jxmap.MapViewerTopComponent")
@ActionReference(path = "Menu/Window" /*
 * , position = 333
 */)
@TopComponent.OpenActionRegistration(displayName = "#CTL_MapViewerAction",
preferredID = "MapViewerTopComponent")
@NbBundle.Messages({
    "CTL_MapViewerAction=Map",
    "CTL_MapViewerTopComponent=Map Window",
    "HINT_MapViewerTopComponent=This is a Map window"
})
public final class MapViewerTopComponent extends TopComponent implements ExplorerManager.Provider {

    private static MapViewerTopComponent instance;
    /** path to the icon used by the component and its open action */
    static final String ICON_PATH = "nl/cloudfarming/client/icon/map-icon24.png";
    private static final String PREFERRED_ID = "MapViewerTopComponent";
    private final MapDataManager mapDataManager = new MapDataManager();
    private final MapView mapView = new MapView();

    public MapViewerTopComponent() {
        initComponents();
        setName(NbBundle.getMessage(MapViewerTopComponent.class, "CTL_MapViewerTopComponent"));
        setToolTipText(NbBundle.getMessage(MapViewerTopComponent.class, "HINT_MapViewerTopComponent"));

        setIcon(ImageUtilities.loadImage(ICON_PATH, true));
        associateLookup(mapView.getLookup());
    }


    /** This method is called from within the constructor to
     * initialize the form.
     */
    private void initComponents() {
        setLayout(new BorderLayout());
        //
        // add mapDataManager as LayerDropTarget to the mapView so dragged layers can be added
        //
        mapView.addLayerDropTarget(mapDataManager);
        add(mapView, BorderLayout.CENTER);

    }

    public MapDataManager getMapDataManager() {
        return mapDataManager;
    }

    /**
     * Gets default instance. Do not use directly: reserved for *.settings files only,
     * i.e. deserialization routines; otherwise you could get a non-deserialized instance.
     * To obtain the singleton instance, use {@link #findInstance}.
     * @return 
     */
    public static synchronized MapViewerTopComponent getDefault() {
        if (instance == null) {
            instance = new MapViewerTopComponent();
        }
        return instance;
    }

    /**
     * Obtain the TestWindowTopComponent instance. Never call {@link #getDefault} directly!
     * @return 
     */
    public static synchronized MapViewerTopComponent findInstance() {
        TopComponent win = WindowManager.getDefault().findTopComponent(PREFERRED_ID);
        if (win == null) {
            Logger.getLogger(MapViewerTopComponent.class.getName()).warning(
                    "Cannot find " + PREFERRED_ID + " component. It will not be located properly in the window system.");
            return getDefault();
        }
        if (win instanceof MapViewerTopComponent) {
            return (MapViewerTopComponent) win;
        }
        Logger.getLogger(MapViewerTopComponent.class.getName()).warning(
                "There seem to be multiple components with the '" + PREFERRED_ID
                + "' ID. That is a potential source of errors and unexpected behavior.");
        return getDefault();
    }

    @Override
    public int getPersistenceType() {
        return TopComponent.PERSISTENCE_ALWAYS;
    }

    void writeProperties(java.util.Properties p) {
        // better to version settings since initial version as advocated at
        // http://wiki.apidesign.org/wiki/PropertyFiles
        p.setProperty("version", "1.0");
    }

    Object readProperties(java.util.Properties p) {
        if (instance == null) {
            instance = this;
        }
        instance.readPropertiesImpl(p);
        return instance;
    }

    private void readPropertiesImpl(java.util.Properties p) {
        String version = p.getProperty("version");
    }

    @Override
    protected String preferredID() {
        return PREFERRED_ID;
    }

    @Override
    public ExplorerManager getExplorerManager() {
        return mapDataManager.getExplorerManager();
    }
    // It is good idea to switch all listeners on and off when the
    // component is shown or hidden. In the case of TopComponent use:

    @Override
    protected void componentOpened() {
        super.componentOpened();
    }

    
    
    @Override
    protected void componentActivated() {
        ExplorerUtils.activateActions(mapDataManager.getExplorerManager(), true);
//        mapDataManager.setActive(true);
    }

    @Override
    protected void componentDeactivated() {
        ExplorerUtils.activateActions(mapDataManager.getExplorerManager(), false);
//        mapDataManager.setActive(false);
    }
}
