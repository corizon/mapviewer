/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.nb.jxmap;

import nl.bebr.mapviewer.swing.jxmap.map.MapMouseEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Timon Veenstra
 */
public class MapMouseHandler implements MouseListener, MouseMotionListener, MouseWheelListener {

    private static final Logger LOGGER = Logger.getAnonymousLogger();
    private boolean trace = false;
//    static{
//        LOGGER.setLevel(Level.ALL);
//        LOGGER.addHandler(SystemLogController.getInstance());
//    }
    private final Comparator<MapMouseListener> comparator = new MapMouseListenerComparator();
    private final MapMouseListener root;
    private final SortedSet<MapMouseListener> listeners = new TreeSet<>(comparator);

    public MapMouseHandler(MapMouseListener root) {
        this.root = root;
    }

    public void addMapMouseListener(MapMouseListener mapMouseListener) {
        listeners.add(mapMouseListener);
    }

    public void removeMapMouseListener(MapMouseListener mapMouseListener) {
        listeners.remove(mapMouseListener);
    }

    /**
     * dispatch the event to the listeners and the map panel
     *
     * @param e
     */
    private void dispatch(MouseEvent e) {
        MapMouseEvent event = new MapMouseEvent(e);
        LISTENERS:
        for (MapMouseListener listener : listeners) {
            trace("Dispatching to:");
            trace("id:" + listener.getId() + "");
            trace("group:" + listener.getGroup().name());
            listener.onMapMouseEvent(event);
            if (event.isConsumed()) {
                trace("is consumed");
                break LISTENERS;
            }
        }
        if (!event.isConsumed()) {
            trace("dispatching to the root panel");
            root.onMapMouseEvent(event);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        trace = true;
        trace("==============================================================");
        trace("mouseClicked");
        dispatch(e);
        trace = false;
    }

    @Override
    public void mousePressed(MouseEvent e) {
        trace = true;
        trace("==============================================================");
        trace("mousePressed");
        dispatch(e);
        trace = false;
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        trace = true;
        trace("==============================================================");
        trace("mouseReleased");
        dispatch(e);
        trace = false;
    }

    @Override
    public void mouseEntered(MouseEvent e) {
//        trace("mouseEntered");
        dispatch(e);
    }

    @Override
    public void mouseExited(MouseEvent e) {
//        trace("mouseExited");
        dispatch(e);
    }

    @Override
    public void mouseDragged(MouseEvent e) {
//        trace("mouseDragged");
        dispatch(e);
    }

    @Override
    public void mouseMoved(MouseEvent e) {
//        trace("mouseMoved");
        dispatch(e);
    }

    private void trace(String msg) {
        if (trace && LOGGER.isLoggable(Level.FINEST)) {
            LOGGER.finest(msg);
        }
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
//        trace("mouseWheelMoved");
        dispatch(e);
    }

    public final class MapMouseListenerComparator implements Comparator<MapMouseListener> {

        @Override
        public int compare(MapMouseListener o1, MapMouseListener o2) {
            if (o1 == null || o2 == null) {
                return -1;
            }
            if (o1.getGroup().equals(o2.getGroup())) {
                return (o1.getId() == o2.getId()) ? 0 : (o1.getId() < o2.getId()) ? -1 : 1;
            } else {
                return (o1.getGroup().getValue() == o2.getGroup().getValue()) ? 0 : (o1.getGroup().getValue() > o2.getGroup().getValue()) ? - 1 : 1;
            }
        }
    }
}
