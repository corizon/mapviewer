/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.nb.jxmap.map;

import nl.bebr.mapviewer.api.RangedLegendPalette;
import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import nl.bebr.mapviewer.nb.jxmap.map.Bundle;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.util.NbBundle;

/**
 * 
 * @author johan
 */
@NbBundle.Messages({
    "range_settings_action.name=Range settings",
    "range_settings_action.dialog.title=Range Settings",
    "range_settings_action.dialog.ok=Ok",
    "range_settings_action.dialog.cancel=Cancel",
})
public class CustomRangeAction extends AbstractAction {
    private Dialog dlg;
    private CustomRangePanel panel;
    private final List<RangedLegendPalette<Double>> palettes;
    
    public CustomRangeAction(List<RangedLegendPalette<Double>> palettes) {
        this.palettes = palettes;
        putValue(NAME, Bundle.range_settings_action_name());
    }
    
    public CustomRangeAction(RangedLegendPalette<Double> palette) {
        this(Collections.singletonList(palette));
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        //open modal dialog:
        //toggle between a (managed) data range and a configurable custom range
        
        panel = new CustomRangePanel(palettes.iterator().next());
        
        DialogDescriptor dd = new DialogDescriptor(panel, Bundle.range_settings_action_dialog_title());
        dd.setOptions(new Object[]{getOkButton(), getCancelButton()});
        dlg = DialogDisplayer.getDefault().createDialog(dd);
        dlg.setVisible(true);
    }
    
    private JButton getOkButton() {
        JButton jButtonOK = new JButton();
        jButtonOK.setText(Bundle.range_settings_action_dialog_ok());
        jButtonOK.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                panel.apply(palettes);
                dlg.setVisible(false);
            }
        });
        return jButtonOK;
    }

    private JButton getCancelButton() {
        JButton jButtonCancel = new JButton();
        jButtonCancel.setText(Bundle.range_settings_action_dialog_cancel());
        jButtonCancel.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                dlg.setVisible(false);
            }
        });
        return jButtonCancel;
    }    
}
