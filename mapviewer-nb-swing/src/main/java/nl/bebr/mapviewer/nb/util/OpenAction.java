/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.nb.util;

import java.awt.event.ActionEvent;
import java.io.Serializable;

import javax.swing.AbstractAction;

import org.netbeans.core.api.multiview.MultiViews;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.TopComponent;

/**
 * <p>Open action for multi views</p>
 * 
 * <p>On action performed will open a MultiView {@link TopComponent} with all the editors for the given mime type</p>
 * 
 * <p> for nodes </p>
 * <p> * make the node serializable</p>
 * <p> * add new OpenAction("application/x-mime-for-node",this) to returned actions in {@link Node#getActions(boolean) }</p>
 * 
 * <p>For more information see {@see <a href="http://netbeans.dzone.com/nb-multiview-editor-for-nodes">How to Create MultiView Editors for Nodes}</a>
 * 
 * 
 * 
 * @author Merijn Zengers
 */
//TODO move to mapviewer-nb
@Messages({"OpenAction context menu display name=Open"})
public class OpenAction<T extends Serializable & Lookup.Provider> extends AbstractAction {

    private String mimeType;
    private T context;

    public OpenAction(String mimeType, T context) {
        super(Bundle.OpenAction_context_menu_display_name());
        this.mimeType = mimeType;
        this.context = context;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        TopComponent tc = MultiViews.createMultiView(mimeType, context);
        tc.open();
        tc.requestActive();
    }
}
