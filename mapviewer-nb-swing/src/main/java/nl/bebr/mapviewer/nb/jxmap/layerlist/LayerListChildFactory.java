/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.nb.jxmap.layerlist;

import java.util.Arrays;
import java.util.List;

import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;

/**
 * Fasctory for the creation of layer nodes in the layerlist
 *
 * @author Timon Veenstra
 */
class LayerListChildFactory extends ChildFactory<LayerListController.LayerWrapper> {

    private final LayerListController controller;

    public LayerListChildFactory(LayerListController layerController) {
        this.controller = layerController;
    }

    public void refresh() {
        refresh(true);
    }

    @Override
    protected boolean createKeys(List<LayerListController.LayerWrapper> toPopulate) {
        toPopulate.addAll(Arrays.asList(this.controller.getLayerNodes()));
        return true;
    }

    @Override
    protected Node createNodeForKey(final LayerListController.LayerWrapper key) {
        return new LayerListNode(key);
    }
}
