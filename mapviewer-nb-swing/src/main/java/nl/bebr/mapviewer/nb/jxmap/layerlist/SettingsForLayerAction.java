/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.nb.jxmap.layerlist;

import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;


import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.util.NbBundle;

import nl.bebr.mapviewer.api.Layer;
import nl.bebr.mapviewer.api.LayerAction;
import nl.bebr.mapviewer.api.LayerInfo;

/**
 * Action to open and close the settings panel for a layer.
 * 
 * @author Timon Veenstra
 */
@NbBundle.Messages({"settings_for_layer.name=Layer settings",
                    "settings_for_layer.dialog.title=Layer Settings",
                    "settings_for_layer.dialog.ok=Ok",
                    "settings_for_layer.dialog.cancel=Cancel"})
public class SettingsForLayerAction extends LayerAction {

    private LayerSettingsPanel panel;
    private Dialog dlg;
    private final LayerInfo layerInfo;
    private final Layer layer;

    public SettingsForLayerAction(LayerInfo layerInfo, Layer layer) {
        putValue(NAME, Bundle.settings_for_layer_name());
        this.layerInfo = layerInfo;
        this.layer = layer;
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {

        panel = new LayerSettingsPanel(layer, layerInfo);
        DialogDescriptor dd = new DialogDescriptor(panel, Bundle.settings_for_layer_dialog_title());
        dd.setOptions(new Object[]{getOkButton(), getCancelButton()});
        dlg = DialogDisplayer.getDefault().createDialog(dd);
        dlg.setVisible(true);
    }

    private JButton getOkButton() {
        JButton jButtonOK = new JButton();
        jButtonOK.setText(Bundle.settings_for_layer_dialog_ok());
        jButtonOK.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
//                LayerListTopComponent.findInstance().refresh();
                dlg.setVisible(false);
            }
        });
        return jButtonOK;
    }

    private JButton getCancelButton() {
        JButton jButtonCancel = new JButton();
        jButtonCancel.setText(Bundle.settings_for_layer_dialog_cancel());
        jButtonCancel.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                panel.cancelSettings();
//                LayerListTopComponent.findInstance().refresh();
                dlg.setVisible(false);
            }
        });
        return jButtonCancel;
    }
}
