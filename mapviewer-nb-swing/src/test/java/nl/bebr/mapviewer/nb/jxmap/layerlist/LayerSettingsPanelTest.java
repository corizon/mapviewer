/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.nb.jxmap.layerlist;

import nl.bebr.mapviewer.nb.jxmap.layerlist.LayerSettingsPanel;
import nl.bebr.mapviewer.nb.jxmap.layerlist.LayerListController;
import javax.swing.JCheckBox;
import javax.swing.JSlider;
import static org.junit.Assert.*;
import org.junit.Test;
import static org.mockito.Mockito.*;
import org.openide.util.lookup.Lookups;
import org.uispec4j.Panel;

import nl.bebr.mapviewer.api.Layer;
import nl.bebr.mapviewer.api.LayerInfo;
import org.junit.After;
import org.junit.Before;

/**
 *
 * @author Timon Veenstra
 */
public class LayerSettingsPanelTest {

    LayerListController controllerMock;
    
    @Test
    public void transparencyRecalculationTest() {
        assertEquals(100, LayerSettingsPanel.recalcTrans(1.0f));
        assertEquals(99, LayerSettingsPanel.recalcTrans(0.99f));
        assertEquals(50, LayerSettingsPanel.recalcTrans(0.5f));
        assertEquals(0, LayerSettingsPanel.recalcTrans(0.0f));

        assertEquals(1.0f, LayerSettingsPanel.recalcTrans(100), 0.000001f);
        assertEquals(0.99f, LayerSettingsPanel.recalcTrans(99), 0.000001f);
        assertEquals(0.50f, LayerSettingsPanel.recalcTrans(50), 0.000001f);
        assertEquals(0.0f, LayerSettingsPanel.recalcTrans(0), 0.000001f);

    }

    @Test
    public void testPanel() {
        LayerInfo layerInfo = new LayerInfo();
        Layer layer = mock(Layer.class);
        when(layer.getName()).thenReturn("layername");
        when(controllerMock.getLookup()).thenReturn(Lookups.fixed(layer));
        Panel settingsPanel = new Panel(new LayerSettingsPanel(layer, layerInfo));
        settingsPanel.containsLabel("layername").check();
        settingsPanel.containsSwingComponent(JSlider.class).check();
        JSlider visibilitySlider = settingsPanel.findSwingComponent(JSlider.class);
        assertNotNull(visibilitySlider);
        assertEquals(layerInfo.getTransparency(), LayerSettingsPanel.recalcTrans(visibilitySlider.getValue()), 0.000001);
    }

    @Test
    public void testCancel() {
        LayerInfo layerInfo = new LayerInfo(true, true, 0.0f);
        Layer layer = mock(Layer.class);
        when(layer.getName()).thenReturn("layername");
        when(controllerMock.getLookup()).thenReturn(Lookups.fixed(layer));

        LayerSettingsPanel wrappedPanel = new LayerSettingsPanel(layer, layerInfo);


        Panel settingsPanel = new Panel(wrappedPanel);
        settingsPanel.containsLabel("layername").check();
        settingsPanel.containsSwingComponent(JSlider.class).check();
        JSlider transparencySlider = settingsPanel.findSwingComponent(JSlider.class);
        assertNotNull(transparencySlider);
        assertEquals(layerInfo.getTransparency(), LayerSettingsPanel.recalcTrans(transparencySlider.getValue()), 0.000001);
        transparencySlider.setValue(50);
        wrappedPanel.cancelSettings();


        JCheckBox visibleBox = settingsPanel.findSwingComponent(JCheckBox.class, LayerSettingsPanel.VISIBILITY_CHECKBOX_NAME);
        assertNotNull(visibleBox);
        assertTrue(visibleBox.getModel().isSelected());
        assertTrue(layerInfo.isVisible());
        visibleBox.getModel().setSelected(false);
        assertFalse(visibleBox.getModel().isSelected());
        assertFalse(layerInfo.isVisible());


        JCheckBox iconsShowBox = settingsPanel.findSwingComponent(JCheckBox.class, LayerSettingsPanel.ICONS_DISPLAYED_CHECKBOX_NAME);
        assertNotNull(iconsShowBox);
        assertTrue(iconsShowBox.getModel().isSelected());
        assertTrue(layerInfo.showIcons());

        iconsShowBox.getModel().setSelected(false);
        assertFalse(iconsShowBox.getModel().isSelected());
        assertFalse(layerInfo.showIcons());

        wrappedPanel.cancelSettings();
        assertTrue(layerInfo.isVisible());
        assertFalse(layerInfo.showIcons());
    }
    
    @Before
    public void setUp() {
        controllerMock = mock(LayerListController.class);
    }
    
    @After 
    public void tearDown() {
        reset(controllerMock);
    }
    
}
