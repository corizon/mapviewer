/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.nb.jxmap;

import nl.bebr.mapviewer.nb.jxmap.MapViewerTopComponent;
import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

import nl.bebr.mapviewer.data.util.ConnectionChecker;

/**
 *
 * @author post
 */
public class DemoFrame extends JFrame {
    
    public DemoFrame() {
        super();
        ConnectionChecker.getInstance().start();
        
        JPanel cp = new JPanel();
        cp.setLayout(new BorderLayout());
        setContentPane(cp);
        cp.setSize(1024, 1024);
        MapViewerTopComponent mapPanel = new MapViewerTopComponent();
        cp.add(mapPanel, BorderLayout.CENTER);
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(1024, 1024);
        setVisible(true);
    }
    
    public static void main(String[] args) {
        new DemoFrame();
    }
}
