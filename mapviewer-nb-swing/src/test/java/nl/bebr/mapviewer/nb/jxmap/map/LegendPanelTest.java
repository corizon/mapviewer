/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.nb.jxmap.map;

import nl.bebr.mapviewer.nb.jxmap.map.LegendPanel;
import java.awt.Color;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;
import static org.mockito.Mockito.*;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

import nl.bebr.mapviewer.api.Layer;
import nl.bebr.mapviewer.api.LegendPalette;
import nl.bebr.mapviewer.api.SimpleLayer;
import nl.bebr.mapviewer.swing.jxmap.MapDataManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;

/**
 *
 * @author Merijn Zengers <Merijn.Zengers@gmail.nl>
 */
@Ignore
public class LegendPanelTest {

    LegendPanel legendPanelMock;
    LegendPalette paletteMock;
    
    @Test
    public void testDeactivateLegend() {
        System.out.println("deactivateLegend");

        //first add a node to activate currentlyShowing
        Layer layer = new SimpleLayer();
        List<LegendPalette> palettes = new ArrayList<>();
        palettes.add(paletteMock);
        layer.setPalette(paletteMock);
        InstanceContent ic = new InstanceContent();
        ic.add(layer);
        AbstractLookup lookup = new AbstractLookup(ic);
        Node node1 = new AbstractNode(Children.LEAF, lookup);

        doCallRealMethod().when(legendPanelMock).activateLegend(palettes);
        doCallRealMethod().when(legendPanelMock).deactivateLegend(palettes);
        when(legendPanelMock.getLegendPaletteFromNode(any(Node.class))).thenReturn(paletteMock);

        MapDataManager dataManager = new MapDataManager();
        when(legendPanelMock.getExplorerManager()).thenReturn(dataManager.getExplorerManager());
        //Nothing currently showing add one node should activate
        dataManager.addLayerNode(node1);
        legendPanelMock.palettesChanged();

        legendPanelMock.deactivateLegend(palettes);

        verify(paletteMock, times(1)).removePropertyChangeListener(any(PropertyChangeListener.class));

    }

    @Test
    public void testActivateLegend() {
        System.out.println("activateLegend");
        List<LegendPalette> palettes = new ArrayList<>();
        palettes.add(paletteMock);
        doCallRealMethod().when(legendPanelMock).activateLegend(palettes);
        legendPanelMock.activateLegend(palettes);
        verify(paletteMock, times(1)).addPropertyChangeListener(any(PropertyChangeListener.class));
    }

    /**
     * Test of nodesAdded method, of class LegendPanel.
     */
    @Test
    public void testNodesAdded() {
        System.out.println("nodesAdded");

        Layer layer = new SimpleLayer();
        LegendPalette palette = new MockLegendPalette(Color.BLACK, "description");
        layer.setPalette(palette);
        InstanceContent ic = new InstanceContent();
        ic.add(layer);
        AbstractLookup lookup = new AbstractLookup(ic);
        Node node1 = new AbstractNode(Children.LEAF, lookup);

        doCallRealMethod().when(legendPanelMock).getLegendPaletteFromNode(any(Node.class));
        doCallRealMethod().when(legendPanelMock).activateLegend(anyList());
        doCallRealMethod().when(legendPanelMock).deactivateLegend(anyList());

        MapDataManager dataManager = new MapDataManager();
        when(legendPanelMock.getExplorerManager()).thenReturn(dataManager.getExplorerManager());

        //Nothing currently showing add one node should activate
        dataManager.addLayerNode(node1);
        legendPanelMock.palettesChanged();

        verify(legendPanelMock, times(1)).deactivateLegend(anyList());
        verify(legendPanelMock, times(1)).activateLegend(anyList());

        //Now add one additional node with the same palette or palette that is equal. should result in still showing
        Layer layer2 = new SimpleLayer();
        LegendPalette palette2 = new MockLegendPalette(Color.BLACK, "description");
        layer2.setPalette(palette2);
        InstanceContent ic2 = new InstanceContent();
        ic2.add(layer2);
        AbstractLookup lookup2 = new AbstractLookup(ic2);
        Node node2 = new AbstractNode(Children.LEAF, lookup2);
        dataManager.addLayerNode(node2);
        legendPanelMock.palettesChanged();

        verify(legendPanelMock, times(2)).deactivateLegend(anyList());
        verify(legendPanelMock, times(2)).activateLegend(anyList());

        //Now create new palette and add. should result in deactivate
        Layer layer3 = new SimpleLayer();
        LegendPalette palette3 = new MockLegendPalette(Color.BLUE, "description");
        layer3.setPalette(palette3);
        InstanceContent ic3 = new InstanceContent();
        ic3.add(layer3);
        AbstractLookup lookup3 = new AbstractLookup(ic3);
        Node node3 = new AbstractNode(Children.LEAF, lookup3);

        dataManager.addLayerNode(node3);
        legendPanelMock.palettesChanged();

        verify(legendPanelMock, times(3)).deactivateLegend(anyList());
        verify(legendPanelMock, times(2)).activateLegend(anyList());
    }

    /**
     * Test of nodesRemoved method, of class LegendPanel.
     */
    @Test
    public void testNodesRemoved() {
        System.out.println("nodesRemoved");
        Layer layer = new SimpleLayer();
        LegendPalette palette = new MockLegendPalette(Color.BLACK, "description");
        layer.setPalette(palette);
        InstanceContent ic = new InstanceContent();
        ic.add(layer);
        AbstractLookup lookup = new AbstractLookup(ic);
        Node node1 = new AbstractNode(Children.LEAF, lookup);


        doCallRealMethod().when(legendPanelMock).addNotify();
        doCallRealMethod().when(legendPanelMock).activateLegend(anyList());
        doCallRealMethod().when(legendPanelMock).deactivateLegend(anyList());
        doCallRealMethod().when(legendPanelMock).getLegendPaletteFromNode(any(Node.class));

        MapDataManager dataManager = new MapDataManager();
        when(legendPanelMock.getExplorerManager()).thenReturn(dataManager.getExplorerManager());


        //Add one node and remove it
        dataManager.addLayerNode(node1);
        legendPanelMock.palettesChanged();
        verify(legendPanelMock, times(1)).deactivateLegend(anyList());
        verify(legendPanelMock, times(1)).activateLegend(anyList());

        dataManager.removeLayerNode(node1);

        legendPanelMock.palettesChanged();
        //verify deactivate
        verify(legendPanelMock, times(2)).deactivateLegend(anyList());
        verify(legendPanelMock, times(1)).activateLegend(anyList());

        //Now add two nodes with same palette and remove one
        Node node2 = new AbstractNode(Children.LEAF, lookup);

        dataManager.addLayerNode(node1);
        dataManager.addLayerNode(node2);

        legendPanelMock.palettesChanged();
        verify(legendPanelMock, times(2)).activateLegend(anyList());

        //verify no deactivate so still only one invocation
        verify(legendPanelMock, times(3)).deactivateLegend(anyList());
        verify(legendPanelMock, times(2)).activateLegend(anyList());

        //Now add onother node with no palette
        Node node3 = new AbstractNode(Children.LEAF);
        dataManager.addLayerNode(node3);
        legendPanelMock.palettesChanged();

        //verify deactivate
        verify(legendPanelMock, times(4)).deactivateLegend(anyList());
        verify(legendPanelMock, times(2)).activateLegend(anyList());

        //Now remove last node and verify activate
        dataManager.removeLayerNode(node3);
        legendPanelMock.palettesChanged();

        verify(legendPanelMock, times(5)).deactivateLegend(anyList());
        verify(legendPanelMock, times(3)).activateLegend(anyList());
    }

    @Test
    public void testGetLegendPaletteFromNode() {
        System.out.println("getLegendPaletteFromNode");
        Layer layer = new SimpleLayer();
        LegendPalette palette = new MockLegendPalette(Color.BLACK, "description");
        layer.setPalette(palette);
        InstanceContent ic = new InstanceContent();
        ic.add(layer);
        AbstractLookup lookup = new AbstractLookup(ic);
        Node node1 = new AbstractNode(Children.LEAF, lookup);

        doCallRealMethod().when(legendPanelMock).getLegendPaletteFromNode(node1);

        LegendPalette found = legendPanelMock.getLegendPaletteFromNode(node1);

        assertNotNull(found);

        assertEquals(palette, found);

    }

    public class MockLegendPalette extends LegendPalette<Object> {

        private String description;

        public MockLegendPalette() {
            this(Color.BLACK, "");
        }

        public MockLegendPalette(Color color, String description) {
            super(color, "km");
            this.description = description;
        }

        @Override
        public Entry[] getEntries() {
            return new MockLegendColors[]{new MockLegendColors()};
        }

        private class MockLegendColors extends Entry {

            @Override
            public Color getColor() {
                return getBaseColor();
            }

            @Override
            public String getDescription() {
                return description;
            }
        }
    }
    
    @Before
    public void setUp() {
        paletteMock = mock(MockLegendPalette.class);
        legendPanelMock = mock(LegendPanel.class);
    }
    
    @After
    public void tearDown() {
        reset(paletteMock);
        reset(legendPanelMock);
    }
}
