/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.nb.util;

import nl.bebr.mapviewer.nb.util.SelectFieldInMapAction;
import java.awt.event.ActionEvent;
import nl.cloudfarming.eventbus.GuiEvent;
import nl.cloudfarming.eventbus.GuiEventBus;
import nl.cloudfarming.eventbus.GuiEventListener;
import org.mockito.ArgumentMatcher;

import eu.limetri.api.geo.Geographical;
import nl.bebr.mapviewer.api.event.GeoEvent;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

/**
 *
 * @author Merijn Zengers
 */
public class SelectFieldInMapActionTest {

    GuiEventListener<Geographical> eventListenerMock;
    Geographical geographicalMock;
    
    /**
     * Test of actionPerformed method, of class SelectFieldInMapAction.
     */
    @Test
    public void testActionPerformed() {
        System.out.println("actionPerformed");

        GuiEventBus.addListener(eventListenerMock);
        when(eventListenerMock.listensTo(GeoEvent.REQUEST_FOCUS)).thenReturn(true);
        when(eventListenerMock.getModuleName()).thenReturn("nb-mapviewer-swing");

        ActionEvent e = null;
        SelectFieldInMapAction instance = new SelectFieldInMapAction(geographicalMock);
        instance.actionPerformed(e);
        verify(eventListenerMock).onEvent(argThat(new ArgumentMatcher<GuiEvent<Geographical>>() {

            @Override
            public boolean matches(Object argument) {
                return ((GuiEvent<Geographical>) argument).getContent().equals(geographicalMock);
            }
        }));
        
        GuiEventBus.removeListener(eventListenerMock);
    }
    
    @Before
    public void setUp() {
        eventListenerMock = mock(GuiEventListener.class);
        geographicalMock = mock(Geographical.class);
    }
    
    @After
    public void tearDown() {
        reset(geographicalMock);
        reset(eventListenerMock);
    }
}
