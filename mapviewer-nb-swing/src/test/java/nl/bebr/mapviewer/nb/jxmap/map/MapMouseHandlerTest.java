/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.nb.jxmap.map;

import static org.junit.Assert.*;
import org.junit.Test;

import nl.bebr.mapviewer.nb.jxmap.LayerPanel;
import nl.bebr.mapviewer.nb.jxmap.MapMouseHandler;
import nl.bebr.mapviewer.nb.jxmap.MapMouseListener;
import nl.bebr.mapviewer.nb.jxmap.RootMapPanel;
import org.junit.After;
import org.junit.Before;
import static org.mockito.Mockito.*;

/**
 *
 * @author merijn
 */
public class MapMouseHandlerTest {

    MapMouseListener mapMouseListenerMock1;
    MapMouseListener mapMouseListenerMock2;
    
    /**
     * Test of removeMapMouseListener method, of class MapMouseHandler.
     */
    @Test
    public void testMapMouseComaparotor() {
        System.out.println("testMapMouseComaparotor");

        mapMouseListenerMock1 = mock(LayerPanel.class);
        mapMouseListenerMock2 = mock(LayerPanel.class);
        when(mapMouseListenerMock1.getGroup()).thenReturn(RootMapPanel.LayerGroup.LAYER_ACTIVE);
        when(mapMouseListenerMock2.getGroup()).thenReturn(RootMapPanel.LayerGroup.LAYER_ACTIVE);
        when(mapMouseListenerMock1.getId()).thenReturn(0);
        when(mapMouseListenerMock2.getId()).thenReturn(1);
        MapMouseHandler mapMouseHandler = new MapMouseHandler(mapMouseListenerMock1);
        MapMouseHandler.MapMouseListenerComparator comparator = mapMouseHandler.new MapMouseListenerComparator();

        assertEquals(-1, comparator.compare(mapMouseListenerMock1, mapMouseListenerMock2));
        assertEquals(-1, comparator.compare(null, mapMouseListenerMock2));
    }
    
    @Before 
    public void setUp() {
        mapMouseListenerMock1 = mock(LayerPanel.class);
        mapMouseListenerMock2 = mock(LayerPanel.class);
    }
    
    @After
    public void tearDown() {
        reset(mapMouseListenerMock1);
        reset(mapMouseListenerMock2);
    }
}
