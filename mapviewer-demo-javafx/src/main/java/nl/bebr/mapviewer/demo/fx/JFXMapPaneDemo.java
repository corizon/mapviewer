/**
 * Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 * AgroSense is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the FLOSS License Exception
 * <http://www.agrosense.eu/foss-exception.html>.
 *
 * AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.demo.fx;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import nl.bebr.mapviewer.data.GeoPosition;
import nl.bebr.mapviewer.data.TileFactoryInfo;
import nl.bebr.mapviewer.data.tilefactory.OSMTileFactoryInfo;
import nl.bebr.mapviewer.data.tilefactory.VirtualEarthTileFactoryInfo;
import nl.bebr.mapviewer.data.util.ConnectionChecker;
import nl.bebr.mapviewer.fx.JFXMapPane;
import java.net.URL;

/**
 * @author Frantisek Post
 */
public class JFXMapPaneDemo extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        ConnectionChecker.getInstance().start();

        primaryStage.setTitle("JFXMapPane Demo");
        StackPane root = new StackPane();
        final JFXMapPane map = new JFXMapPane();

        GeoPosition greenwich = new GeoPosition(51.4788, 0.0106);
        map.setZoom(7);
        map.setAddressLocation(greenwich);

        TileFactoryInfo osmInfo = new OSMTileFactoryInfo();
        osmInfo.setIconUrl(JFXMapPaneDemo.class.getClassLoader().getResource("osm.png"));

        VirtualEarthTileFactoryInfo vetfi1 = new VirtualEarthTileFactoryInfo(VirtualEarthTileFactoryInfo.MAP);
        VirtualEarthTileFactoryInfo vetfi2 = new VirtualEarthTileFactoryInfo(VirtualEarthTileFactoryInfo.SATELLITE);
        VirtualEarthTileFactoryInfo vetfi3 = new VirtualEarthTileFactoryInfo(VirtualEarthTileFactoryInfo.HYBRID);

        vetfi1.setIconUrl(JFXMapPaneDemo.class.getClassLoader().getResource("vem.png"));
        vetfi2.setIconUrl(JFXMapPaneDemo.class.getClassLoader().getResource("ves.png"));
        vetfi3.setIconUrl(JFXMapPaneDemo.class.getClassLoader().getResource("veh.png"));
        root.getChildren().add(map);

        TileFactoryInfo[] infos = new TileFactoryInfo[]{osmInfo, vetfi1, vetfi2, vetfi3};
        map.setTypeSelectionEnabled(true, infos);

        Scene scene = new Scene(root, 800, 600);

        URL cssUrl = this.getClass().getClassLoader().getResource("JFXMapPaneDemo.css");
        if (cssUrl != null) {
            scene.getStylesheets().add(cssUrl.toExternalForm());
        }else{
            System.err.println("WARNING:stylesheet not found!");
        }

        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
