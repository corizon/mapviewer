/*
 * Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 * AgroSense is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the FLOSS License Exception
 * <http://www.agrosense.eu/foss-exception.html>.
 *
 * AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * AgroSense. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *    Timon Veenstra <tveentra@bebr.nl> - initial API and implementation and/or initial documentation
 */
package nl.bebr.mapviewer.demo.swing;

import nl.bebr.mapviewer.api.Palette;
import java.awt.Color;

/**
 *
 * @author Timon Veenstra <tveentra@bebr.nl>
 */
class SomePalette extends Palette<Double> {

    SomePalette() {
    }

    @Override
    public Color getColorForValue(Double value) {
        if (0 <= value && value <= 0.5) {
            return Color.green;
        } else {
            return Color.RED;
        }
    }
    
}
