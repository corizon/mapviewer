/*
 * Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 * AgroSense is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the FLOSS License Exception
 * <http://www.agrosense.eu/foss-exception.html>.
 *
 * AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * AgroSense. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *    Timon Veenstra <tveentra@bebr.nl> - initial API and implementation and/or initial documentation
 */
package nl.bebr.mapviewer.demo.swing;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.Polygon;
import eu.limetri.api.geo.DataRecord;
import eu.limetri.api.geo.DataSet;
import eu.limetri.api.geo.DataSetGeographical;
import eu.limetri.api.geo.PolygonSupport;
import nl.bebr.mapviewer.api.Palette;
import nl.bebr.mapviewer.api.SimpleLayer;
import nl.bebr.mapviewer.api.SingleObjectLayer;
import nl.bebr.mapviewer.nb.jxmap.MapView;
import nl.bebr.mapviewer.swing.jxmap.MapDataManager;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author Timon Veenstra <tveentra@bebr.nl>
 */
public class DemoWithListFrame extends JFrame implements ExplorerManager.Provider {

    private static final long serialVersionUID = 1L;

    private final MapDataManager mapDataManager = new MapDataManager();
    private final MapView mapView = new MapView();

    public DemoWithListFrame() {
        super();
        init();
    }

    private void init() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(1024, 768);

        JPanel p = new JPanel(new BorderLayout());
        p.add(mapView, BorderLayout.CENTER);
        setContentPane(p);

        mapView.addLayerDropTarget(mapDataManager);

        DataSet dataSet = new DataSet();
        
        dataSet.add(new DataRecord(0.0106,51.4798, 0.4));
        dataSet.add(new DataRecord(0.0116,51.4798, 1.0));
        dataSet.add(new DataRecord(0.0126,51.4798, 0.4));
        dataSet.add(new DataRecord(0.0136,51.4798, 1.0));
        dataSet.add(new DataRecord(0.0146,51.4798, 0.4));
        dataSet.add(new DataRecord(0.0156,51.4798, 0.4));
        dataSet.add(new DataRecord(0.0166,51.4798, 1.0));
        dataSet.add(new DataRecord(0.0176,51.4798, 0.4));
        dataSet.add(new DataRecord(0.0186,51.4798, 1.0));
        dataSet.add(new DataRecord(0.0196,51.4798, 0.4));
        dataSet.add(new DataRecord(0.0206,51.4798, 1.0));        
        
        dataSet.add(new DataRecord(0.0106,51.4818, 1.0));
        dataSet.add(new DataRecord(0.0116,51.4818, 0.4));
        dataSet.add(new DataRecord(0.0126,51.4818, 1.0));
        dataSet.add(new DataRecord(0.0136,51.4818, 0.4));
        dataSet.add(new DataRecord(0.0146,51.4818, 1.0));
        dataSet.add(new DataRecord(0.0156,51.4818, 0.4));
        dataSet.add(new DataRecord(0.0166,51.4818, 1.0));
        dataSet.add(new DataRecord(0.0176,51.4818, 0.4));
        dataSet.add(new DataRecord(0.0186,51.4818, 1.0));
        dataSet.add(new DataRecord(0.0196,51.4818, 0.4));
        dataSet.add(new DataRecord(0.0206,51.4818, 1.0));            


        Coordinate[] coordinates = new Coordinate[]{
            new Coordinate(0.0106,51.4788),
            new Coordinate(0.0206,51.4788),
            new Coordinate(0.0206,51.4888),
            new Coordinate(0.0106,51.4888),
            new Coordinate(0.0106,51.4788)};

        GeometryFactory geometryFactory = new GeometryFactory();
        Polygon boundingBox = geometryFactory.createPolygon(coordinates);

        JButton button = new JButton("add sample layers");
        button.setPreferredSize(new Dimension(100, 100));
        button.addActionListener((ActionEvent e) -> {
            mapDataManager.dropNode(new DataNode(dataSet, boundingBox));
            mapDataManager.dropNode(new GeoNode(new PolygonSupport(boundingBox){

                @Override
                public Class<Polygon> getType() {
                    return Polygon.class;
                }
            }
            ));
        });
        
        p.add(button, BorderLayout.NORTH);
    }

    public static void main(String[] args) {
        DemoWithListFrame frame = new DemoWithListFrame();
        frame.setVisible(true);
    }

    @Override
    public ExplorerManager getExplorerManager() {
        return mapDataManager.getExplorerManager();
    }
    

}
