/**
 * Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 * AgroSense is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * There are special exceptions to the terms and conditions of the GPLv3 as it
 * is applied to this software, see the FLOSS License Exception
 * <http://www.agrosense.eu/foss-exception.html>.
 *
 * AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.demo.swing;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

import nl.bebr.mapviewer.data.GeoPosition;
import nl.bebr.mapviewer.data.TileFactoryInfo;
import nl.bebr.mapviewer.data.tilefactory.OSMTileFactoryInfo;
import nl.bebr.mapviewer.data.tilefactory.VirtualEarthTileFactoryInfo;
import nl.bebr.mapviewer.data.util.ConnectionChecker;
import nl.bebr.mapviewer.swing.impl.OfflineTileFactorySwing;
import nl.bebr.mapviewer.swing.jxmap.map.JXMapPanel;
import nl.bebr.mapviewer.swing.jxmap.map.component.LayerSelectionComponent;
import nl.bebr.mapviewer.swing.overlay.CompoundOverlayPainter;

/**
 * @author Frantisek Post
 *
 */
public class DemoFrame extends JFrame {

    private static final long serialVersionUID = 1L;

    public DemoFrame() {
        super();
        init();
    }

    private void init() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(1024, 768);

        ConnectionChecker.getInstance().start();
        TileFactoryInfo osmInfo = new OSMTileFactoryInfo();
        osmInfo.setIconUrl(DemoFrame.class.getResource("osm.png"));

        VirtualEarthTileFactoryInfo vetfi1 = new VirtualEarthTileFactoryInfo(VirtualEarthTileFactoryInfo.MAP);
        VirtualEarthTileFactoryInfo vetfi2 = new VirtualEarthTileFactoryInfo(VirtualEarthTileFactoryInfo.SATELLITE);
        VirtualEarthTileFactoryInfo vetfi3 = new VirtualEarthTileFactoryInfo(VirtualEarthTileFactoryInfo.HYBRID);

        vetfi1.setIconUrl(DemoFrame.class.getResource("vem.png"));
        vetfi2.setIconUrl(DemoFrame.class.getResource("ves.png"));
        vetfi3.setIconUrl(DemoFrame.class.getResource("veh.png"));

        // Setup JXMapViewer
        final JXMapPanel mapViewer = new JXMapPanel();
        mapViewer.setTileFactory(new OfflineTileFactorySwing(osmInfo));

        GeoPosition greenwich = new GeoPosition(51.4788, 0.0106);

        // Set the focus
        mapViewer.setZoom(15);
        mapViewer.setAddressLocation(greenwich);

        JPanel p = new JPanel(new BorderLayout());
        p.add(mapViewer, BorderLayout.CENTER);

        mapViewer.setTypeSelectionEnabled(true, osmInfo, vetfi1, vetfi2, vetfi3);
        setContentPane(p);

        mapViewer.setOverlayPainter(new CompoundOverlayPainter());

        JPanel panel = new JPanel();
        panel.setOpaque(false);
        LayerSelectionComponent layerSelectionComponent = new LayerSelectionComponent();
        mapViewer.add(panel);
        panel.add(layerSelectionComponent);

    }

    public static void main(String[] args) {
        DemoFrame frame = new DemoFrame();
        frame.setVisible(true);
    }

}
