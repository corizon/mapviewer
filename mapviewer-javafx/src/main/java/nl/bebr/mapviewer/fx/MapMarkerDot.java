/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 * <http://www.agrosense.eu/foss-exception.html>.
 *
 * AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.fx;

import nl.bebr.mapviewer.fx.api.MapMarker;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.Group;
import javafx.scene.effect.DropShadow;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Circle;

/**
 * A simple implementation of the {@link MapMarker} interface. Each map marker
 * is painted as a circle with a black border line and filled with a specified
 * color.
 *
 * @author Jan Peter Stotz
 *
 */
public class MapMarkerDot implements MapMarker, Marker {

    double lat;
    double lon;
    Color color;
    Circle sphere;
    int radius;

    public MapMarkerDot(double lat, double lon) {
        this(10, Color.YELLOW, lat, lon);
    }

    public MapMarkerDot(int radius, Color color, double lat, double lon) {
        super();
        this.color = color;
        this.lat = lat;
        this.lon = lon;
        this.radius = radius;

        this.sphere = new Circle();
        this.sphere.setCenterX(radius);
        this.sphere.setCenterY(radius);
        this.sphere.setRadius(radius);
        this.sphere.setCache(true);

        List<Stop> stops = new ArrayList<>();
        stops.add(new Stop(0.0, color));
        stops.add(new Stop(1.0, Color.BLACK));
        RadialGradient rgrad = new RadialGradient(0.0, 0.0, sphere.getCenterX() - sphere.getRadius() / 3, sphere.getCenterY() - sphere.getRadius() / 3, sphere.getRadius(), false, CycleMethod.REPEAT, stops);

        this.sphere.setFill(rgrad);

        DropShadow dropShadow = new DropShadow();
        dropShadow.setOffsetX(10);
        dropShadow.setOffsetY(10);
        dropShadow.setColor(Color.rgb(50, 50, 50, 0.7));
        this.sphere.setEffect(dropShadow);

    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public void Render(Group g, Point position) {
        this.sphere.setTranslateX(position.x - this.radius);
        this.sphere.setTranslateY(position.y - this.radius);
        g.getChildren().add(this.sphere);
    }

    @Override
    public String toString() {
        return "MapMarker at " + lat + " " + lon;
    }

}
