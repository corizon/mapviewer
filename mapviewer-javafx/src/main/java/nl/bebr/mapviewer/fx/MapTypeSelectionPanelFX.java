/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.fx;

import java.net.URL;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Side;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;


import nl.bebr.mapviewer.data.TileFactoryInfo;
import nl.bebr.mapviewer.data.common.TileFactoryInfoSelectionEventHandler;

public class MapTypeSelectionPanelFX extends BorderPane {

	private Label label;
	private Button button;
	private TileFactoryInfo[] tileFactoryInfos;
	private TileFactoryInfo activeTileFactoryInfo;
	private TileFactoryInfoSelectionEventHandler eventHandler;
	private static final ImageView imageDown = new ImageView(new Image(MapTypeSelectionPanelFX.class.getResource("down14.png").toString()));
	
	public MapTypeSelectionPanelFX() {
		super();
		init();
	}
	
	private void init() {
		getStyleClass().add("MapTypeSelectionPanelFX");
		button = new Button();
		button.setGraphic(imageDown);
		label = new Label();
		
		setLeft(label);
		setRight(button);
		
		BorderPane.setMargin(label, new Insets(2, 10, 0, 10));
		
		button.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent arg0) {
				showPopup();
			}
			
		});
	}
	
	private void showPopup() {
		ContextMenu menu = new ContextMenu();
		
		for (final TileFactoryInfo tileFactoryInfo : tileFactoryInfos) {
			MenuItem menuItem = new MenuItem();
			menuItem.setText(tileFactoryInfo.getName());
			if (tileFactoryInfo.getIconUrl() != null) {
				menuItem.setGraphic(createIcon(tileFactoryInfo.getIconUrl()));
			}
			menu.getItems().add(menuItem);
			
			boolean active = tileFactoryInfo.equals(activeTileFactoryInfo);
			if (!active) {
				menuItem.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent event) {
						selectTileInfo(tileFactoryInfo);
						setActiveTileFactoryInfo(tileFactoryInfo);
					}

				});
			}
		}
		
		menu.show(this, Side.TOP, 0, 0);
	}
	
	private ImageView createIcon(URL iconUrl) {
		return new ImageView(new Image(iconUrl.toString()));
	}

	protected void selectTileInfo(TileFactoryInfo info) {
		if (eventHandler != null) {
			eventHandler.itemSelected(info);
		}
	}
	
	public void setOnSelectionChange(TileFactoryInfoSelectionEventHandler eventHandler) {
		this.eventHandler = eventHandler;
	}
	
	public void setItems(TileFactoryInfo[] tileFactoryInfos) {
		this.tileFactoryInfos = tileFactoryInfos;
	}
	
	
	public void setActiveTileFactoryInfo(TileFactoryInfo activeTileFactoryInfo) {
		this.activeTileFactoryInfo = activeTileFactoryInfo;
		label.setText(activeTileFactoryInfo.getName());
	}
}
