/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.fx.impl;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;

import nl.bebr.mapviewer.data.cache.OfflineTile;
import nl.bebr.mapviewer.data.cache.OfflineTileFactory;

/**
 * JavaFX implementation of {@link OfflineTile}
 * 
 * @author Frantisek Post
 */
public class OfflineTileFX extends OfflineTile<Image> {

	private static WritableImage image;
	
	static {
		image = new WritableImage(256, 256);

		// TODO ugly and slow, but its done only once and for now I didn't find any other way
		BufferedImage offlineImage = new BufferedImage(TILE_SIZE, TILE_SIZE, BufferedImage.TYPE_INT_RGB);
		Graphics2D graphics = offlineImage.createGraphics();
		graphics.setColor(new Color(240, 240, 240));
		graphics.fillRect(0, 0, TILE_SIZE, TILE_SIZE);
		graphics.setColor(Color.BLACK);
		String message = "no data"; //TODO i18n
		int textWidth = graphics.getFontMetrics().stringWidth(message);
		int textHeight = graphics.getFontMetrics().getHeight();
		graphics.drawString(message, (TILE_SIZE - textWidth) / 2, (TILE_SIZE - textHeight) / 2);

		image = SwingFXUtils.toFXImage(offlineImage, image);
	}
	
	/**
	 * Constructor
	 * 
	 * @param x - x coordinate
	 * @param y - y coordinate
	 * @param zoom - zoom level
	 * @param url - url of tile
	 * @param tileFactory - tileFactory
	 */
	public OfflineTileFX(int x, int y, int zoom, String url, OfflineTileFactory<Image> tileFactory) {
		super(x, y, zoom, url, tileFactory);
	}

	@Override
	protected Image createOfflineImageImpl() {
		return image;
	}

	
}
