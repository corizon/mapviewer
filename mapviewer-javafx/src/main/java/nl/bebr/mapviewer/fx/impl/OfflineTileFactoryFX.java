/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.fx.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

import javafx.scene.image.Image;


import nl.bebr.mapviewer.data.AbstractTileFactory;
import nl.bebr.mapviewer.data.Tile;
import nl.bebr.mapviewer.data.TileCache;
import nl.bebr.mapviewer.data.TileFactoryInfo;
import nl.bebr.mapviewer.data.cache.OfflineTileFactory;
import nl.bebr.mapviewer.data.cache.spi.TileComparator;

/**
 * JavaFX implementation of {@link OfflineTileFactory}
 * 
 * @author Frantisek Post
 */
public class OfflineTileFactoryFX extends OfflineTileFactory<Image> {

	private final BlockingQueue<Tile<Image>> tileQueue = new PriorityBlockingQueue<Tile<Image>>(5, new TileComparator<Image>());
	private final Map<String, Tile<Image>> tileMap = new HashMap<>();
	
	public OfflineTileFactoryFX(TileFactoryInfo tileFactoryInfo) {
		super(tileFactoryInfo);
	}

	@Override
	protected Tile<Image> createOfflineTile(int tileX, int tileY, int zoom,
			String url) {
		return new OfflineTileFX(tileX, tileY, zoom, url, this);
	}

	@Override
	protected Runnable createTileRunner() {
		return new OfflineTileRunnerFX(this);
	}

	@Override
	public TileCache<Image> createTileCache() {
		return new TileCacheFX();
	}

	@Override
	public Tile<Image> createTile(int x, int y, int zoom, String url,
			AbstractTileFactory<Image, Tile<Image>> tileFactory) {
		return new OfflineTileFX(x, y, zoom, url, (OfflineTileFactory<Image>) tileFactory);
	}

	@Override
	public BlockingQueue<Tile<Image>> getTileQueue() {
		return tileQueue;
	}

	@Override
	protected Map<String, Tile<Image>> getTileMap() {
		return tileMap;
	}

}
