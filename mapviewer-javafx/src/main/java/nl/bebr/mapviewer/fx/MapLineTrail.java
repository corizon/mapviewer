/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.fx;

import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.effect.DropShadow;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.shape.StrokeLineJoin;
import javafx.scene.shape.StrokeType;

import com.vividsolutions.jts.geom.Coordinate;

import nl.bebr.mapviewer.fx.api.MapLine;
/**
 *
 * @author smithjel
 */
public class MapLineTrail implements MapLine {
    
    String Name;
    Color color;
    double dashOffset;
    double width;
    ObservableList<Coordinate> Coordinates;
    

    public MapLineTrail(String n, Color c, double width, double dashOffset, List<Coordinate> coords) {
        super();
        this.Name = n;
        this.color = c;
        this.dashOffset = dashOffset;
        this.width = width;
        this.Coordinates = FXCollections.observableList(coords);
    }
 
    

    @Override
    public void addCoordinate(Coordinate coordinate) {
        this.Coordinates.add(coordinate);
    }


    public void Render(JFXMapPane viewer, Group g) {
        Polyline polyline = new Polyline();
        
        polyline.setStrokeType(StrokeType.CENTERED);
        polyline.setStrokeDashOffset(this.dashOffset);
        polyline.setStroke( this.color );
        polyline.setStrokeWidth( this.width );
        polyline.setStrokeLineCap(StrokeLineCap.ROUND);        
        polyline.setStrokeLineJoin(StrokeLineJoin.ROUND);
        
        
        DropShadow dropShadow = new DropShadow();
        dropShadow.setOffsetX(10);
        dropShadow.setOffsetY(10);
        dropShadow.setColor(Color.rgb(50, 50, 50, 0.7));
        polyline.setEffect(dropShadow);
        
//        for (Coordinate coordinate : this.Coordinates) {
//            Point p = viewer.getMapPosition(coordinate.y, coordinate.x,false);
//            polyline.getPoints().add((double)p.x);
//            polyline.getPoints().add((double)p.y);
//        }
        g.getChildren().add(polyline);
    }


    @Override
    public String toString() {
        return "MapLine" + Name;
    }

    
    
}
