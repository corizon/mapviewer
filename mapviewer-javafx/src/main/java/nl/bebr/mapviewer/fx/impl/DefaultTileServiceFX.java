/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.fx.impl;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;


import javafx.scene.image.Image;

import javax.imageio.ImageIO;

import org.openide.filesystems.FileObject;

import nl.bebr.mapviewer.data.cache.TileCacheInfo;
import nl.bebr.mapviewer.data.cache.spi.AbstractTileService;
import javafx.embed.swing.SwingFXUtils;

/**
 * JavaFX implementation of {@link AbstractTileService}
 * 
 * @author Frantisek Post
 */
public class DefaultTileServiceFX extends AbstractTileService<Image> implements TileServiceFX, WritableTileServiceFX {

	private static final Logger LOG = Logger.getLogger(DefaultTileServiceFX.class.getName());
	
	@Override
	public Image readImage(TileCacheInfo tileCacheInfo) {
		 Image image = null;
	      FileObject tileFile = getTileFile(tileCacheInfo, false);
	      if (tileFile != null) {
	          try (InputStream stream = tileFile.getInputStream()) {
	              image = new Image(stream);
	              readCallback(tileFile);
	          } catch (IOException ex) {
	        	  ex.printStackTrace();
	              LOG.log(Level.FINE, "Error reading tile image from file ", ex);
	          }
	      }
	      return image;
	}

	@Override
	public void writeImage(TileCacheInfo tileCacheInfo, Image image) {
		FileObject tileFile = getTileFile(tileCacheInfo, true);
		BufferedImage bufferedImage = SwingFXUtils.fromFXImage(image, null);
		
	      if (tileFile != null) {
	          try (OutputStream stream = tileFile.getOutputStream()) {
	        	  ImageIO.write(bufferedImage, "png", stream);
	              writeCallback();
	          } catch (IOException ex) {
	          	ex.printStackTrace();
	              LOG.log(Level.FINE, "Problem writing tile image to file", ex);
	          }
	      } else {
	          LOG.fine("Failed to write image for tile");
	      }
	}

}
