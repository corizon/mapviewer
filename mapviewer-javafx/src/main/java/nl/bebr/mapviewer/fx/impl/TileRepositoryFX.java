/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.fx.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javafx.scene.image.Image;

import org.openide.util.Lookup;

import nl.bebr.mapviewer.data.cache.TileRepository;
import nl.bebr.mapviewer.data.cache.TileService;
import nl.bebr.mapviewer.data.cache.WritableTileService;

/**
 * JavaFX implementation of {@link TileRepository}
 * 
 * @author Frantisek Post
 */
public class TileRepositoryFX extends TileRepository<Image> {

	private static final TileRepositoryFX instance = new TileRepositoryFX();

	private static DefaultTileServiceFX service;
	
	private TileRepositoryFX() {
	}
	
	/**
	 * Returns instance
	 * @return
	 */
	public static TileRepository<Image> getInstance() {
		return instance;
	}

	
	@SuppressWarnings("rawtypes")
	protected List<TileService> findImplementations() {
		Collection<? extends TileService> caches = Lookup.getDefault().lookupAll(TileServiceFX.class);
		List<TileService> list = new ArrayList<TileService>();
		list.add(getService()); //default first
		list.addAll(caches);
		return list;
	}

	protected WritableTileService<Image> findWritableImplementation() {
		WritableTileServiceFX customWritableTileService = Lookup.getDefault().lookup(WritableTileServiceFX.class);
		if (customWritableTileService != null) {
			return customWritableTileService;
		} else {
			return getService();
		}
	}

	private DefaultTileServiceFX getService() {
		if (service == null) {
			service = new DefaultTileServiceFX();
		}
		return service;
	}
	
}
