/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.api;


import nl.bebr.mapviewer.api.BaseLayer;
import nl.bebr.mapviewer.api.Palette;
import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import static org.junit.Assert.*;
import org.junit.Test;

import nl.bebr.mapviewer.api.Layer;
/**
 *
 * @author Timon Veenstra
 */
public class BaseLayerTest {
    
    @Test
    public void testPaletteColorChange() throws InterruptedException{
        final Palette palette = new Palette(Color.YELLOW);
        
        BaseLayer layer = new BaseLayer(palette, "layerName"){
        };
        
        final CountDownLatch latch =  new CountDownLatch(1);
        
        layer.addPropertyChangeListener(Layer.PROP_PALETTE,new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                assertEquals(Layer.PROP_PALETTE,evt.getPropertyName());
                latch.countDown();
            }
        });
        //
        // test PropertyChangeEvent  layer base color change
        //
        layer.propertyChange(new PropertyChangeEvent(this, Palette.PROP_BASE_COLOR, palette, palette));
//        palette.setBaseColor(Color.RED);
        assertTrue("No PropertyChangeEvent occurred within 100 ms!",latch.await(100, TimeUnit.MILLISECONDS));
        assertNotNull(layer.getPalette());
        assertEquals(palette, layer.getPalette());
        
    }
    
    @Test
    public void testPaletteChange() throws InterruptedException{
        final Palette palette1 = new Palette(Color.YELLOW);
        final Palette palette2 = new Palette(Color.MAGENTA);
        
        BaseLayer layer = new BaseLayer(palette1,  "layerName"){

        };
        
        final CountDownLatch latch =  new CountDownLatch(1);
        
        layer.addPropertyChangeListener(Layer.PROP_PALETTE,new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                assertEquals(Layer.PROP_PALETTE,evt.getPropertyName());
                latch.countDown();
            }
        });
        //
        // test PropertyChangeEvent  layer base color change
        //
        assertEquals(palette1, layer.getPalette());
        layer.setPalette(palette2);
//        palette.setBaseColor(Color.RED);
        assertTrue("No PropertyChangeEvent occurred within 100 ms!",latch.await(100, TimeUnit.MILLISECONDS));
        assertNotNull(layer.getPalette());
        assertEquals(palette2, layer.getPalette());
        
    }    
    
}
