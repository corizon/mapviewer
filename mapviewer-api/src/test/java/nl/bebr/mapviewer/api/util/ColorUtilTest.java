/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.api.util;

import nl.bebr.mapviewer.api.util.ColorUtil;
import java.awt.Color;
import static org.junit.Assert.assertEquals;
import org.junit.Test;


/**
 *
 * @author Timon Veenstra
 */
public class ColorUtilTest {
        private static final int RED_50_PERCENT = new Color(255,0,0,127).getRGB();
    private static final int RED_10_PERCENT = new Color(255,0,0,229).getRGB();
    private static final int RED_100_PERCENT = new Color(255,0,0,0).getRGB();
    
    
    @Test
    public void testMakeMoreTransparent(){
        assertEquals(Color.RED.getRGB(), ColorUtil.makeTransparant(Color.RED.getRGB(), 0));
        assertEquals(RED_50_PERCENT, ColorUtil.makeTransparant(Color.RED.getRGB(), 50));
        assertEquals(RED_10_PERCENT, ColorUtil.makeTransparant(Color.RED.getRGB(), 10));
        assertEquals(RED_100_PERCENT, ColorUtil.makeTransparant(Color.RED.getRGB(), 100));
    }
}
