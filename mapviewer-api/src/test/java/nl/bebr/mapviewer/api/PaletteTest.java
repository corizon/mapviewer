/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.api;

import nl.bebr.mapviewer.api.Palette;
import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import org.jdesktop.swingx.color.ColorUtil;
import static org.junit.Assert.*;
import org.junit.Test;
import org.netbeans.api.visual.model.ObjectState;

import junit.framework.Assert;

/**
 *
 * @author Timon Veenstra
 */
public class PaletteTest {
    
    @Test
    public void testColorForValue(){
        final Color baseColor = Color.CYAN;
        Palette palette = new Palette(baseColor);
        //
        // standard implementation always returns black for any value
        //
        compareColors(Color.BLACK,palette.getColorForValue(null));
    }      
    
    @Test
    public void testName(){
        final Color baseColor = Color.CYAN;
        Palette palette = new Palette(baseColor);
        //
        // standard implementation has name of the color
        //
        assertEquals(baseColor.toString(),palette.getName());
    }    
    
    @Test
    public void testSupports(){
        final Color baseColor = Color.CYAN;
        Palette palette = new Palette(baseColor);
        //
        // supports should always return true for the default implementation
        //
        
        assertTrue(palette.supports(this.getClass()));
    }
    
    @Test
    public void testInterchangeability(){
        final Color baseColor = Color.CYAN;
        Palette palette = new Palette(baseColor);
        //
        // default implementation does not support interchangeability
        //
        
        assertFalse(palette.isInterchangeable());
        assertNull(palette.getInterchangeablePalettes());
    }    
    
    @Test
    public void testBaseColor() throws InterruptedException{
        final Color baseColor = Color.CYAN;
        Palette palette = new Palette(baseColor);
        //
        // test if basecolor is same as constructed
        //
        assertEquals(baseColor,palette.getBaseColor());
        //
        // test property change event on change of base color
        //
        final CountDownLatch latch = new CountDownLatch(1);
        final Color newColor = Color.MAGENTA;
        palette.addPropertyChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                compareColors(baseColor, (Color)evt.getOldValue());
                compareColors(newColor, (Color)evt.getNewValue());
                assertEquals(Palette.PROP_BASE_COLOR, evt.getPropertyName());
                latch.countDown();
            }
        });
        palette.setBaseColor(newColor);
        assertTrue("No PropertyChangeEvent occurred within 100 milliseconds!",latch.await(100, TimeUnit.MILLISECONDS));
        
    }    
    
    @Test
    public void testGetColorForState(){
        final Color baseColor = Color.CYAN;
        final Color normalColor = ColorUtil.setAlpha(baseColor, Palette.ALPHA_NORMAL);
        final Color selectedColor = ColorUtil.setAlpha(baseColor, Palette.ALPHA_SELECTED);
        final Color hoverColor = ColorUtil.setAlpha(baseColor, Palette.ALPHA_HOVER);
        Palette palette = new Palette(baseColor);
        //
        // happy flow
        //
        compareColors(normalColor, palette.getColorForState(ObjectState.createNormal()));
        compareColors(selectedColor, palette.getColorForState(ObjectState.createNormal().deriveSelected(true)));
        compareColors(hoverColor, palette.getColorForState(ObjectState.createNormal().deriveObjectHovered(true)));
        compareColors(hoverColor, palette.getColorForState(ObjectState.createNormal().deriveWidgetHovered(true)));
        //
        // other states should return normal color
        //
        compareColors(normalColor, palette.getColorForState(ObjectState.createNormal().deriveHighlighted(true)));
        compareColors(normalColor, palette.getColorForState(ObjectState.createNormal().deriveObjectFocused(true)));        
        compareColors(normalColor, palette.getColorForState(ObjectState.createNormal().deriveWidgetAimed(true)));        
        //
        // testing state combinations
        // hover should take precedence over selected
        //
        compareColors(hoverColor, palette.getColorForState(ObjectState.createNormal().deriveSelected(true).deriveObjectHovered(true)));
        compareColors(hoverColor, palette.getColorForState(ObjectState.createNormal().deriveSelected(true).deriveWidgetHovered(true)));
    }
    
    private void compareColors(Color color1, Color color2) {
        if (color1 == null && color2 == null) {
            //noop
        } else if (color1 == null || color2 == null) {
            Assert.fail("colors are not equal (one is null)");
        } else {
            if (color1.getRed() != color2.getRed() || color1.getGreen() != color2.getGreen() || color1.getBlue() != color2.getBlue()) {
                Assert.fail("colors are not equal: expected " + color1 + ", got " + color2);
            } else if (color1.getAlpha()!= color2.getAlpha()) {
                Assert.fail("colors are not equal: expected " + color1 + " with alpha " + color1.getAlpha() + ", got " + color2 + " with alpha " + color2.getAlpha());
            }
        }
    }
}
