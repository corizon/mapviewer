/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.api;

import nl.bebr.mapviewer.api.LayerInfo;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import static org.junit.Assert.*;
import org.junit.Test;


/**
 *
 * @author Timon Veenstra
 */
public class LayerInfoTest {
    
    @Test
    public void testSelected() throws InterruptedException{
        LayerInfo layerInfo = new LayerInfo(false, false, 0.0f);
        
        final CountDownLatch latch = new CountDownLatch(1);
        layerInfo.addPropertyChangeListener(LayerInfo.PROPERTY_SELECTED, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                assertEquals(LayerInfo.PROPERTY_SELECTED, evt.getPropertyName());
                assertEquals(Boolean.FALSE,evt.getOldValue());
                assertEquals(Boolean.TRUE,evt.getNewValue());
                latch.countDown();
            }
        });
        //
        // selected should be false to start with and then turn true
        //
        assertFalse(layerInfo.isSelected());
        layerInfo.setSelected(true);
        assertTrue("No PropertyChangeEvent for selected occurred in 100 ms!",latch.await(100, TimeUnit.MILLISECONDS));
        assertTrue(layerInfo.isSelected());
    }
    
    @Test
    public void testVisible() throws InterruptedException{
        LayerInfo layerInfo = new LayerInfo(false, false, 0.0f);
        
        final CountDownLatch latch = new CountDownLatch(1);
        layerInfo.addPropertyChangeListener(LayerInfo.PROPERTY_VISIBLE, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                assertEquals(LayerInfo.PROPERTY_VISIBLE, evt.getPropertyName());
                assertEquals(Boolean.FALSE,evt.getOldValue());
                assertEquals(Boolean.TRUE,evt.getNewValue());
                latch.countDown();
            }
        });
        //
        // selected should be false to start with and then turn true
        //
        assertFalse(layerInfo.isVisible());
        layerInfo.setVisible(true);
        assertTrue("No PropertyChangeEvent for visible occurred in 100 ms!",latch.await(100, TimeUnit.MILLISECONDS));
        assertTrue(layerInfo.isVisible());
    }    
}
