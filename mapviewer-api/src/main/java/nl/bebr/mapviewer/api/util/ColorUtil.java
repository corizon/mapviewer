/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.api.util;

import java.awt.Color;

/**
 *
 * @author Timon Veenstra
 * @author johan
 */
public class ColorUtil {

    /**
     * make a color more transparent
     * 
     * 
     * @param rgb
     * @param alphaPercent
     * @return
     * @deprecated use org.jdesktop.swingx.color.ColorUtil#setAlpha(Color c, int alpha);
     * 
     * TODO: NOT USED ANYWHERE, CAN BE REMOVED?
     */
    @Deprecated
    public static int makeTransparant(int rgb, int alphaPercent) {
        int a = (rgb >> 24) & 0xff;
        a *= ((double) (100 - alphaPercent) / (double) 100);
        return ((rgb & 0x00ffffff) | (a << 24));
    }
    
    public static Color interpolateRGBA(Color c1, Color c2, double fraction) {
        assert fraction >= 0.0 && fraction <= 1.0;
        
        final double complement = 1.0 - fraction;
        
        return new Color(
                (int)(complement * c1.getRed()   + fraction * c2.getRed()),
                (int)(complement * c1.getGreen() + fraction * c2.getGreen()),
                (int)(complement * c1.getBlue()  + fraction * c2.getBlue()),
                (int)(complement * c1.getAlpha() + fraction * c2.getAlpha()));
    }
}
