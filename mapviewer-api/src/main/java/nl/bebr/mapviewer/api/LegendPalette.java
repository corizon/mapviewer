/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.api;

import java.awt.Color;
import java.util.Arrays;
import java.util.Objects;

/**
 * Palette for Legends
 *
 * @author Merijn Zengers <Merijn.Zengers@gmail.nl>
 * @param <E>
 */
public abstract class LegendPalette<E> extends Palette<E> {

    public static final String PROP_LEGEND_COLORS = "legend_colors";
    
    // TODO: make updatable, listen for unit changes
    // move unit to subclass (measurement values will use it but e.g. categories won't)
    // delegate presentation to MeasurementPresenter (prefer FixedUnit version).
    // Do the unit and entry labels even have to be inside a palette or can we create
    // a separate class used by the legend panel besides the palette
    
    private final String unit;

    public LegendPalette(String unit) {
        this(Color.BLACK, unit);
    }
    
    public LegendPalette(Color baseColor, String unit) {
        super(baseColor);
        this.unit = unit;
    }

    public String getUnit() {
        return unit;
    }

    public abstract Entry[] getEntries();

    @Override
    public final boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LegendPalette other = (LegendPalette) obj;
        if (!Objects.equals(this.unit, other.unit)) {
            return false;
        }
        if (!Arrays.equals(this.getEntries(), other.getEntries())) {
            return false;
        }
        return true;
    }

    @Override
    public final int hashCode() {
        int hash = 5;
        hash = 17 * hash + Objects.hashCode(unit);
        hash = 17 * hash + Arrays.hashCode(this.getEntries());
        return hash;
    }
    
    public static abstract class Entry {

        public abstract Color getColor();

        public abstract String getDescription();

        @Override
        public final boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Entry other = (Entry) obj;
            if (!Objects.equals(this.getColor(), other.getColor())) {
                return false;
            }
            if (!Objects.equals(this.getDescription(), other.getDescription())) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 5;
            hash = 17 * hash + Objects.hashCode(this.getColor());
            hash = 17 * hash + Objects.hashCode(this.getDescription());
            return hash;
        }
    }
    
    protected static class DefaultEntry extends Entry {

        private final Color color;
        private final String description;

        protected DefaultEntry(Color color, String description) {
            this.color = color;
            this.description = description;
        }

        @Override
        public Color getColor() {
            return color;
        }

        @Override
        public String getDescription() {
            return description;
        }        
    }

}
