/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.api;

import java.io.IOException;


import org.netbeans.api.actions.Savable;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

import com.vividsolutions.jts.geom.Geometry;
import eu.limetri.api.geo.DynamicGeometrical;
import eu.limetri.api.geo.Geometrical;


/**
 * Layer containing a single geometrical object which can be edited.
 * 
 * @author johan
 */
public class SingleObjectLayer extends SimpleLayer {
    public static final String PROP_GEOMETRY = Geometrical.PROP_GEOMETRY;
    private final DynamicGeometrical object;
    private final Lookup lookup;

    /**
     * 
     * @param name
     * @param geometrical the "single object" which provides the geometry for this layer
     * @param lookup optional lookup for autosave feature; this might need to come from a
     * different node than the one that provides the geometrical (e.g. for a treatment zone
     * the field would need to be saved)
     */
    public SingleObjectLayer(String name, DynamicGeometrical geometrical, Lookup lookup) {
        super(name);
        this.object = geometrical;
        this.lookup = lookup;
    }
    
    public SingleObjectLayer(String name, DynamicGeometrical geometrical, Lookup lookup, Palette palette) {
        this(name, geometrical, lookup);
        setPalette(palette);
    }
    
    public boolean hasGeometry() {
        return object.getGeometry() != null  && !object.getGeometry().isEmpty();
    }

    public Geometrical getObject() {
        return object;
    }
    
    public void setGeometry(Geometry newGeometry) {
        Geometry oldGeometry = object.getGeometry();
        object.setGeometry(newGeometry);
        // notify listeners, e.g. a layerpanel so it can create/update widgets
        getChangeSupport().firePropertyChange(PROP_GEOMETRY, oldGeometry, newGeometry);
        
        // autosave:
        if (lookup != null) {
            Savable savable = lookup.lookup(Savable.class);
            if (savable != null) {
                try {
                    savable.save();
                } catch (IOException ex) {
                    Exceptions.printStackTrace(ex);
                }
            }
        }
    }
    
}
