/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.api;

import java.awt.Image;
import java.awt.Rectangle;

import com.vividsolutions.jts.geom.Geometry;

/**
 * Describes capabilities to generate images with map information
 * 
 * 
 * @author Timon Veenstra <monezz@gmail.com>
 */
public interface MapImageProvider {
    
    /**
     * Retrieve an image for a bouding box using the provided dimensions
     * 
     * @param dimensions width and hight will be used for returned image
     * @param boundingBox geometrical bounding box for the map image
     * @return map image
     */
    Image imageForBoundingBox(Rectangle dimensions, Geometry boundingBox);
}
