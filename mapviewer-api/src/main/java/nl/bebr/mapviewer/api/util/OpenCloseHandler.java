/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.api.util;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.openide.nodes.Node;
import org.openide.util.Lookup;

/**
 * Interface describing the capability to respond to something opened and or
 * closed. <p> Used in the generic {@link OpenAction} to enable all
 * implementations to react on a data object being openend. {@link OpenCloseHandler#close(org.openide.util.Lookup.Provider)
 * }
 * should be called by the data objects editor component. </p>
 * <p>
 *  Typical implementation for a MultiViewElement would be
 * <pre>
 *   public void componentOpened() {
 *       OpenCloseHandler.Event.triggerOpen(obj.getNodeDelegate());
 *   }
 *
 *   public void componentClosed() {
 *       OpenCloseHandler.Event.triggerClose(obj.getNodeDelegate());
 *   }
 * </pre>
 * </>
 *
 * @author Timon Veenstra <monezz@gmail.com>
 */
public interface OpenCloseHandler {
  
    /**
     * Handle object with provided context being opened.
     *
     * @param context
     */
    void open(Node context);

    /**
     * Handle object with provided context being closed.
     *
     * @param context
     */
    void close(Node context);

    public static class Event {
        private static final Logger LOGGER = Logger.getLogger(OpenCloseHandler.Event.class.getName());

        /**
         * Trigger an open event and signal all {@link OpenCloseHandler#open(org.openide.util.Lookup.Provider) }
         * implementations
         * 
         * @param context 
         */
        public static void triggerOpen(Node context) {
            for (OpenCloseHandler openCloseHandler : Lookup.getDefault().lookupAll(OpenCloseHandler.class)) {
                try {
                    openCloseHandler.open(context);
                } catch (Exception ex) {
                    LOGGER.log(Level.WARNING, "FIXME:Ignored exception thrown from OpenCloseHandler.open {0}", ex.getMessage());
                }
            }
        }
        
        /**
         * Trigger an open event and signal all {@link OpenCloseHandler#close(org.openide.util.Lookup.Provider) }
         * implementations
         * 
         * @param context 
         */        
        public static void triggerClose(Node context) {
            for (OpenCloseHandler openCloseHandler : Lookup.getDefault().lookupAll(OpenCloseHandler.class)) {
                try {
                    openCloseHandler.close(context);
                } catch (Exception ex) {
                    LOGGER.log(Level.WARNING, "FIXME:Ignored exception thrown from OpenCloseHandler.close {0}", ex.getMessage());
                }
            }
        }        
    }
}
