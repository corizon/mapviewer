/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.api;

import java.awt.Color;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.List;

import org.jdesktop.swingx.color.ColorUtil;
import org.netbeans.api.visual.model.ObjectState;

/**
 * Palette Palettes are used to provide a source for coloring objects shown on
 * the map. There are basically two types of coloring a palette can provide: 
 * <UL>
 * <LI>state based coloring</LI>
 * <LI>value based coloring</LI>
 * </UL>
 *
 * <h3>State based coloring</h3>
 * <p>
 * Each palette has a base color. This base color a mandatory attribute and needs
 * to be passed to the constructor. It can be changed afterwards by calling the 
 * {@link #setBaseColor(java.awt.Color) } method.
 * Main method for retrieving state based coloring is 
 * {@link #getColorForState(org.netbeans.api.visual.model.ObjectState)}.
 * It will take an ObjectState and derive a color from the base color depending 
 * on the state. Basic implementation will only take in account the states
 * normal, selected and hover. The returned colors are all the base color
 * with a different alpha value, resp {@link #ALPHA_NORMAL}, {@link #ALPHA_SELECTED}
 * and {@link #ALPHA_HOVER}.
 * </p>
 *
 * <h3>Value based coloring</h3>
 * <p>
 * Value based coloring is usually based on ranges of values. Each range has
 * its own designated color. The method {@link #getColorForValue(java.lang.Object)}
 * will determine in which range the provided value belongs and return the 
 * appropriate color.
 * </p>
 * <p>
 * WARNING, this implementation does not support color ranges and will always
 * return the base color.
 * </p>
 * 
 * @param Generic V The value object type, Palette&lt;Number&gt; will take Number 
 * arguments for determining the value based color.
 * @author Timon Veenstra
 */
public class Palette<V extends Object> implements Serializable {

    private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    public static final String PROP_BASE_COLOR = "PROP_BASE_COLOR";
    public static final int ALPHA_NORMAL = 229; // 90%
    public static final int ALPHA_SELECTED = 229;//90% //TODO FP: we don't change transparency now
    public static final int ALPHA_HOVER = 229;//90%
    private Color baseColor;
    
    public Palette() {
        this(Color.BLACK);
    }

    public Palette(Color baseColor) {
        this.baseColor = baseColor;
    }

    /**
     * Gets the color for the state of the object.
     * @param state the state the Object is in.
     * @return The Color representing the state of the LayerObject
     */
    public Color getColorForState(final ObjectState state) {
        if (state.isHovered() || state.isObjectHovered()) {
            return ColorUtil.setAlpha(baseColor, ALPHA_HOVER);
        }        
        if (state.isSelected()) {
            return ColorUtil.setAlpha(baseColor, ALPHA_SELECTED);
        }
        return ColorUtil.setAlpha(baseColor, ALPHA_NORMAL);
    }

    /**
     * Method to determine if the palette is suited for the class
     * @return returns true if the palette is suitable for the provided class
     */
    public boolean supports(Class clazz) {
        return true;
    }

    /**
     * Some palettes contain a list with interchangeable palettes
     * These palettes can at any time be changed for any of the palettes in that
     * list. Implementations returning a list should return the same
     * interchangeable palette list for any of the interchangeable palettes.
     * Palettes which support interchangeablitity will have to override
     * @{@link Palette#isInterchangeable()} to return true.
     * 
     * Palettes which do not support interchangeablitity will return null;
     * In that case @{@link Palette#isInterchangeable()} will return false.
     * 
     * @return List of pallete options 
     */
    public List<Palette<V>> getInterchangeablePalettes() {
        return null;
    }

    /**
     * Returns true if a palette supports interchangeablitity
     * 
     * @see Palette#getInterchangeablePalettes() 
     * @return 
     */
    public boolean isInterchangeable() {
        return false;
    }

    /**
     * Gets the base color for the palette.
     * The base color can be used by implementation to derive state based
     * colors. Value based colors will normally not be derived from a base color.
     * 
     * @return 
     */
    public Color getBaseColor() {
        return baseColor;
    }

    /**
     * Sets the base color for a palette.
     * The base color can be used by implementation to derive state based
     * colors. Value based colors will normally not be derived from a base color.
     * Setting the base color will most likely trigger a repaint for all 
     * components which based their color from this palette.
     * 
     * @param color 
     */
    public void setBaseColor(Color color) {
        Color oldColor = this.baseColor;
        this.baseColor = color;
        pcs.firePropertyChange(PROP_BASE_COLOR, oldColor, color);
    }

    /**
     * returns the name of the palette.
     * 
     * @return 
     */
    public String getName() {
        return this.baseColor.toString();
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        pcs.addPropertyChangeListener(propertyChangeListener);
    }
    
    public void addPropertyChangeListener(String prop, PropertyChangeListener propertyChangeListener) {
        pcs.addPropertyChangeListener(prop, propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        pcs.removePropertyChangeListener(propertyChangeListener);
    }
    
    public void removePropertyChangeListener(String prop, PropertyChangeListener propertyChangeListener) {
        pcs.removePropertyChangeListener(prop, propertyChangeListener);
    }
    
    protected PropertyChangeSupport getPropertyChangeSupport(){
        return pcs;
    } 

    /**
     * Gets the color for the provided value
     * 
     * Implementations with a value palette need to override this method
     * 
     * @param value the value for which the color should be returned
     * @return the color suited for the value
     */
    public Color getColorForValue(V value) {
        return Color.BLACK;
    }
    
    @Override
    public Palette clone() throws CloneNotSupportedException {
        return (Palette)super.clone();
    } 
}
