/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.api;

import javax.swing.Action;

/**
 *
 * @author johan
 */
public interface HasMapActions {
    //interface split off DynamicGeometrical so we can use custom actions with any Geographical
    
    /**
     * Creates and returns an array of actions that should be added to the
     * context menu of this DynamicGeometrical on the map, but not to the
     * default node actions.
     *
     * @return Map specific actions for this DynamicGeometrical
     */
    public Action[] getMapActions();

    /**
     * Adds the given map action to this DynamicGeometrical
     *
     * @param action The action to add
     */
    //FIXME: remove method? prefer to add to widget or layerlistnode instead of original
    //currently only used to add PickLicationAction to a DynamicPoint,
    //see if we can move (postpone) that to where the `getMapActions()` result is processed.
    //(requires JXMapViewer reference)
    public void addMapAction(Action action);
}
