/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.api;


import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import org.openide.explorer.ExplorerManager;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;

import eu.limetri.api.geo.Geographical;
import nl.bebr.mapviewer.api.util.ExplorerManagerUtil;

/**
 * <h1>Layer</h1> A layer is a a grouped set of one or more items that posses
 * the capability to be displayed on a map.
 *
 * <h3>Data hierarchy</h3> Layers and layer data are usually organized as nodes
 * in a tree. The layer can be considered the parent node, and the
 * <code>Geographical</code>s the children. Because of this, the layer and its
 * data will often be displayed in an explorer like component like a
 * <code>BeanTreeView</code>.
 *
 *
 * <h3>Selection synchronization</h3> To synchronize selection between the map
 * and views based on the
 * <code>ExplorerManager</code> managing the layer and its data, the issueing
 * ExplorerManager needs to add propertyChangeListeners to the layer. Any change
 * in selection on the map will be published as propertyChange event with as
 * key:
 * {@link ExplorerManager#PROP_SELECTED_NODES}
 *
 *
 *
 *
 *
 * @author Timon Veenstra
 */
public interface Layer {

    public static final String PROP_PALETTE = "palette";
    public static final String PROP_CATEGORY = "category";
    public static final String PROP_NAME = "name";
    public static String LAYER_NODE_ICON_BASE = "nl/cloudfarming/client/icon/layers-map-icon.png";

    public static enum SelectionChangeDirection {

        BASE_TO_MAP, MAP_TO_BASE;
    }

    /**
     * The name of this layer.
     *
     * @return String name
     */
    public String getName();
    
    /**
     * A palette contains information about how the layer should be displayed.
     *
     * @return Palette
     */
    public Palette getPalette();

    /**
     * A palette contains information about how the layer should be displayed.
     *
     * @Param The palette to set
     */
    void setPalette(Palette palette);

    /**
     * Get an array with actions for this layer.
     *
     * @return
     */
    public LayerAction[] getActions();

    /**
     * Add property change listener to this class.
     *
     * @param listener The listener object
     */
    public void addPropertyChangeListener(PropertyChangeListener listener);

    /**
     * Add property change listener to this class.
     *
     * @param propertyName Property to listen to for changes
     * @param listener The Listener object
     */
    void addPropertyChangeListener(String propertyName, PropertyChangeListener listener);

    /**
     * Remove the property change listener from this class.
     *
     * @param propertyNamen The property the listener is currently listening to
     * @param listener The Listener object
     */
    void removePropertyChangeListener(String propertyName, PropertyChangeListener listener);

    /**
     * Remove the property change listener from this class.
     *
     * @param listener The Listener object
     */
    void removePropertyChangeListener(PropertyChangeListener listener);

//    ExplorerManager getExplorerManager();
    void fireNodeSelectionChange(PropertyChangeEvent event, SelectionChangeDirection direction);

    SelectionChangeDirection getActiveChangeDirection();

    static class Sync {

        private static final Set<PropertyChangeListener> listeners = Collections.synchronizedSet(new HashSet<PropertyChangeListener>());

        public static void SyncBaseExplorer(final Layer layer, final ExplorerManager baseExplorer) {
            PropertyChangeListener baseExplorerListener = new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    if (ExplorerManager.PROP_SELECTED_NODES.equals(evt.getPropertyName())) {
                        layer.fireNodeSelectionChange(evt, SelectionChangeDirection.BASE_TO_MAP);
                    }
                }
            };
            listeners.add(baseExplorerListener);
            baseExplorer.addPropertyChangeListener(baseExplorerListener);

            PropertyChangeListener baseLayerListener = new PropertyChangeListener() {

                @Override
                public void propertyChange(final PropertyChangeEvent evt) {
                    if (ExplorerManager.PROP_SELECTED_NODES.equals(evt.getPropertyName())) {
                        if (layer.getActiveChangeDirection() != SelectionChangeDirection.BASE_TO_MAP) {
                            try {
                                baseExplorer.setSelectedNodes(new Node[0]);
                            } catch (PropertyVetoException ex) {
                                Exceptions.printStackTrace(ex);
                            }
                            for (Node node : (Node[]) evt.getNewValue()) {
                                Geographical object = node.getLookup().lookup(Geographical.class);
                                ExplorerManagerUtil.addObjectToSelection(object, baseExplorer);
                            }

                        }
                    }
                }
            };

            listeners.add(baseLayerListener);
            layer.addPropertyChangeListener(baseLayerListener);
        }

        public static void SyncMapExplorer(final Layer layer, final ExplorerManager mapExplorer) {
            PropertyChangeListener mapListener = new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    if (ExplorerManager.PROP_SELECTED_NODES.equals(evt.getPropertyName())) {
                        layer.fireNodeSelectionChange(evt, SelectionChangeDirection.MAP_TO_BASE);
                    }
                }
            };
            listeners.add(mapListener);
            mapExplorer.addPropertyChangeListener(mapListener);

            PropertyChangeListener layerListener = new PropertyChangeListener() {

                @Override
                public void propertyChange(final PropertyChangeEvent evt) {
                    if (ExplorerManager.PROP_SELECTED_NODES.equals(evt.getPropertyName())) {
                        if (layer.getActiveChangeDirection() != SelectionChangeDirection.MAP_TO_BASE) {
                            try {
                                mapExplorer.setSelectedNodes(new Node[0]);
                            } catch (PropertyVetoException ex) {
                                Exceptions.printStackTrace(ex);
                            }
                            for (Node node : (Node[]) evt.getNewValue()) {
                                Geographical object = node.getLookup().lookup(Geographical.class);
                                ExplorerManagerUtil.addObjectToSelection(object, mapExplorer);
                            }

                        }

                    }
                }
            };

            listeners.add(layerListener);

            layer.addPropertyChangeListener(layerListener);
        }
    }

    static class Finder {

        /**
         * Utility method to find a layer object in a node lookup somewhere in
         * the node hierarchy. Only traverses upwards from the provided node
         * (parent)
         *
         * @param node
         * @return
         */
        public static Layer findLayer(Node node) {
            Layer layer = node.getLookup().lookup(Layer.class);
            if (layer == null && node.getParentNode() != null) {
                layer = findLayer(node.getParentNode());
            }
            return layer;
        }
        
        /**
         * Utility method to find the first node upwards from provided node
         * with a layer object in its lookup;
         * 
         * @param node
         * @return 
         */
        public static Node findNodeWithLayer(Node node){
            Layer layer = node.getLookup().lookup(Layer.class);
            if (layer != null){
                return node;
            }
            if (layer == null && node.getParentNode() != null) {
                return findNodeWithLayer(node.getParentNode());
            }
            return null;            
        }
    }
}
