/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.api;

/**
 * Defines a value range
 *
 * For example a range with all whole numbers between 0 and 10 is defined as:
 * 
 * min value = 0
 * max value = 10
 * steps = 10
 *
 * Ranges can have all numeric datatypes.
 *
 * @author Timon Veenstra
 */
public interface ValueRange<D extends Number> {

    /**
     * Minimum range value
     * @return minimum range value
     */
    D getMinValue();

    /**
     * Maximum range value
     * @return maximum range value
     */
    D getMaxValue();

    /**
     * Amount of steps the range is divided in
     * @return Int with amount of steps
     */
    int getSteps();


}
