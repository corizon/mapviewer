/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.api;


import org.openide.nodes.Node;

import com.vividsolutions.jts.geom.Geometry;

import nl.bebr.mapviewer.api.event.GeoEvent;

/**
 * Abstraction for providing the input and handling the output of 
 * splitting a geometry.
 * 
 * @author johan
 */
public abstract class SplitGeometryHandler {
    
    public abstract Geometry getSource();
    public abstract boolean handle(Geometry[] geometries, Node sourceNode);
    
    protected void addToMap(Node node) {
        if (node != null) {
            GeoEvent.getProducer().triggerEvent(GeoEvent.NEW_LAYER, node);
        }
    }
    
    protected void removeFromMap(Node node) {
        if (node != null) {
            GeoEvent.getProducer().triggerEvent(GeoEvent.DISCARD_LAYER, node);
        }
    }
}
