/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.api;

import java.awt.Color;
import java.text.DecimalFormat;

import nl.bebr.mapviewer.api.util.Callback;
import nl.bebr.mapviewer.api.util.Gradient;
import java.util.Objects;
import java.util.function.Consumer;


/**
 * LegendPalette based on a numeric range.
 * 
 * <p>
 * Extracted and refactored from FieldTaskPalette.
 * Removed integer restriction from palette (LegendPalette now uses same generic type as ValueRange);
 * this means small ranges (range size smaller than number of colors) can now use the full color range.
 * TODO: might add multiplication factor for scaled values in integer rasters (see SensorPalette)
 * </p>
 * 
 * <h3>Cooperation with RasterRenderers</h3>
 * <p>
 * Values passed into getColorForValue might be truncated even though they are of the correct type.
 * This is caused by the use of lower precision types in intermediate data structures.
 * E.g. a grid coverage for Double data might be backed by a raster using an int databuffer.
 * </p>
 * 
 * @param <N> numeric type used by the data values represented in this legend
 * 
 * @author johan
 */
public class RangedLegendPalette<N extends Number> extends LegendPalette<Number> implements ValueRange<N> {

    private static final Color[] COLORS = new Color[] {
        new Color(0, 96, 255),
        new Color(0, 159, 255),
        new Color(0, 223, 255),
        new Color(32, 255, 223),
        new Color(96, 255, 159),
        new Color(159, 255, 96),
        new Color(223, 255, 32),
        new Color(255, 223, 0),
        new Color(255, 159, 0),
        new Color(255, 96, 0),
    };
    
    private static final Gradient GRADIENT = Gradient.create(COLORS);
    
    // The data the legend applies to might be scaled:
    // e.g. sensor data is scaled up by a factor 1000 so the 0.0-1.0 range can be represented by integers,
    // and a resolution factor has to be applied to raw isoxml values.
    // The values in the labels of the legend entries need to be scaled back by this factor.
    private double scale = 1.0;
    
    private final Range<N> dataRange; // keep original data range instance while a custom range is applied.
    private Range<N> currentRange;

    public RangedLegendPalette(N minValue, N maxValue, String unit) {
        this(Color.BLACK, minValue, maxValue, unit, 1.0);
    }
    
    public RangedLegendPalette(N minValue, N maxValue, String unit, double scale) {
        this(Color.BLACK, minValue, maxValue, unit, scale);
    }
    
    public RangedLegendPalette(Color baseColor, N minValue, N maxValue, String unit) {
        this(baseColor, minValue, maxValue, unit, 1.0);
    }
    
    public RangedLegendPalette(Color baseColor, N minValue, N maxValue, String unit, double scale) {
        super(baseColor, unit);
        currentRange = dataRange = new Range<>(minValue, maxValue);
        this.scale = scale;
    }

    @Override
    public N getMaxValue() {
        return currentRange.getMax();
    }
    
    @Override
    public N getMinValue() {
        return currentRange.getMin();
    }
    
    private double getMin(){
        return toDouble(currentRange.getMin());
    }
    
    private double getMax(){
        return toDouble(currentRange.getMax());
    }
    
    //null-safe conversion to primitive
    private static double toDouble(Number n){
        return n == null ? 0.0d : n.doubleValue();
    }

    public Range<N> getDataRange() {
        //todo: use defensive copy or allow mutations?
        return dataRange;
    }
    
    public void setDataRange(Range<N> r) {
        setDataRange(r.min, r.max);
    }
    
    public void setDataRange(N minValue, N maxValue) {
        dataRange.setMin(minValue);
        dataRange.setMax(maxValue);
        // Fire a single event on range updates; min and max can be changed 
        // simultaneously, e.g. when changing the value of a single valued task.
        // When doing separate updates (and firing separate events) the 
        // intermediate range can be invalid (min > max), causing errors in both
        // the legend and the grid coverage renderer.
        if (!usesCustomRange()) fireRangeChange();
    }
    
    public void setRange(Range<N> range) {
        currentRange = range;
        fireRangeChange();
    }
    
    public boolean usesCustomRange() {
        return currentRange != dataRange;
    }
    
    public void applyDataRange() {
        if (usesCustomRange()) {
            currentRange = dataRange;
            fireRangeChange();
        }
    }
    
    private void fireRangeChange() {
        getPropertyChangeSupport().firePropertyChange(LegendPalette.PROP_LEGEND_COLORS, null, null);
    }
    
    protected Color[] getColors() {
        return COLORS;
    }
    
    protected Gradient getGradient() {
        return GRADIENT;
    }

    /**
     * Get the number of colors used in a legend for the current value range.
     * 
     * The inherited name is somewhat misleading when using a gradient:
     * we still need to return the number of colors (colorstops),
     * the actual number of steps used in calculations is one lower however.
     * 
     * @return 
     */
    @Override
    public int getSteps() {
        return getColors().length;
    }

    /**
     * Gets the color for the provided value
     *
     * @param value the value for which the color should be returned
     * @return the color suited for the value
     */
    @Override
    public Color getColorForValue(Number value) {
        //FIXME: testcases still expect discrete colorstop entries (i.e. exact Colors from array above) for intermediate values
//        return getGradientColorForValue(value);
        double stepSize = getStepSize();

        for (int i = 0; i < getSteps(); i++) {
            if (value.doubleValue() <= stepSize * i + getMin()) {
                return getColors()[i];
            }
        }
        return getBaseColor();
    }
    
    public Color getGradientColorForValue(Number value) {
        return getGradient().getColor(
                getMax() - getMin() == 0 ? 0.0 :
                (value.doubleValue() - getMin()) / (getMax() - getMin()));
    }

    // gradient step size: (max-min)/(n-1)
    private double getStepSize() {
        return getMax() - getMin() > 0 ? (getMax() - getMin()) / (getSteps() -1) : 0;
    }

    @Override
    public LegendPalette.Entry[] getEntries() {
        double stepSize = getStepSize();
        int nColors = getSteps();
        Color[] colors = getColors();
        assert nColors <= colors.length;
        
        DecimalFormat format = getFormat();
        
        if (getMax() == getMin()) {
            return new DefaultEntry[] {new DefaultEntry(colors[0], format.format(getMin() / scale))};
        }
        
        DefaultEntry[] entries = new DefaultEntry[nColors];

        double stepValue = getMin();
        for (int i = 0; i < nColors; i++) {
            entries[i] = new DefaultEntry(colors[i], format.format(stepValue / scale));
            stepValue += stepSize;
        }
        return entries;
    }
    
    protected DecimalFormat getFormat() {
        double displayRange = (getMax() - getMin()) / scale;
        int maxFractionDigits = 0;
        // adapt rounding to size of displayed range:
        if (displayRange > 0 && displayRange < getColors().length) {
            maxFractionDigits = 2;
        }
        DecimalFormat format = new DecimalFormat();
        format.setMaximumFractionDigits(maxFractionDigits);
        return format;
    }
    
    public double getScale() {
        return scale;
    }
    
    @Deprecated
    public static Callback<double[]> createDoubleRangeCallback(final RangedLegendPalette<Double> palette) {
        return (double[] ds) -> {
            if (ds != null && ds.length == 2) palette.setDataRange(ds[0], ds[1]);
        };        
    }
    
    //TODO: replace array with Range
    public static Consumer<double[]> createDoubleRangeConsumer(final RangedLegendPalette<Double> palette) {
        return (double[] ds) -> {
            if (ds != null && ds.length == 2) palette.setDataRange(ds[0], ds[1]);
        };        
    }
    
    // TODO (maybe later): support both gradient (current impl) and interval based legends.
    // compare org.geotools.styling.ColorMap: TYPE_RAMP and TYPE_INTERVAL
    
    
    //TODO: extract to api/util module, add contains & overlap checks etc.
    public static class Range<T> {
        private T min;
        private T max;

        public Range(T min, T max) {
            this.min = min;
            this.max = max;
        }

        public Range(Range<T> other) {
            this(other.min, other.max);
        }

        public T getMin() {
            return min;
        }

        public void setMin(T min) {
            this.min = min;
        }

        public T getMax() {
            return max;
        }

        public void setMax(T max) {
            this.max = max;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 47 * hash + Objects.hashCode(this.min);
            hash = 47 * hash + Objects.hashCode(this.max);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Range<?> other = (Range<?>) obj;
            if (!Objects.equals(this.min, other.min)) {
                return false;
            }
            if (!Objects.equals(this.max, other.max)) {
                return false;
            }
            return true;
        }
        
    }
}