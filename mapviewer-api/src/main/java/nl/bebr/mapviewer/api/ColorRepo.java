/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.api;

import java.awt.Color;

/**
 * Basic colors for layer objects
 *
 * @author Frantisek Post
 */
public class ColorRepo {

    private static int counter = 0;
    private static final int TRANSPARENCY = 178; //70%
    private static Color[] colors = {
        new Color(255, 128, 128, TRANSPARENCY),
        new Color(255, 255, 128, TRANSPARENCY),
        new Color(128, 255, 128, TRANSPARENCY),
        new Color(0, 255, 128, TRANSPARENCY),
        new Color(128, 255, 255, TRANSPARENCY),
        new Color(0, 128, 255, TRANSPARENCY),
        new Color(255, 128, 192, TRANSPARENCY),
        new Color(255, 128, 255, TRANSPARENCY),
        new Color(255, 0, 0, TRANSPARENCY),
        new Color(255, 255, 0, TRANSPARENCY),
        new Color(128, 255, 0, TRANSPARENCY),
        new Color(0, 255, 64, TRANSPARENCY),
        new Color(0, 255, 255, TRANSPARENCY),
        new Color(0, 128, 196, TRANSPARENCY),
        new Color(128, 128, 192, TRANSPARENCY),
        new Color(255, 0, 255, TRANSPARENCY),
        new Color(128, 64, 64, TRANSPARENCY),
        new Color(255, 128, 64, TRANSPARENCY),
        new Color(0, 255, 0, TRANSPARENCY),
        new Color(0, 128, 128, TRANSPARENCY),
        new Color(0, 64, 128, TRANSPARENCY),
        new Color(128, 128, 255, TRANSPARENCY),
        new Color(128, 0, 64, TRANSPARENCY),
        new Color(255, 0, 128, TRANSPARENCY),};

    public static Color getNextColor() {
        Color color = colors[counter];
        counter++;
        if (counter >= colors.length) {
            counter = 0;
        }
        return color;
    }
}
