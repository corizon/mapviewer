/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.api;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * Holds the extra info about a layer, for example the transparency value and 
 * the indication if a layer is visible or not.
 * 
 * @author Maarten Hollenga
 * @author Timon Veenstra
 */
public class LayerInfo {

    private boolean selected = false;
    private boolean visible = true;
    private float transparency = 0.6f;
    private boolean locked = false;
    private boolean showLabels = true;
    private boolean showIcons = true;    
    public static final String PROPERTY_SELECTED = "selected";
    public static final String PROPERTY_VISIBLE = "visible";
    public static final String PROPERTY_TRANSPARENCY = "transparency";
    public static final String PROPERTY_LOCKED = "locked";
    public static final String PROPERTY_SHOW_ICONS = "showIcons";
    public static final String PROPERTY_SHOW_LABELS = "showLabels";    
    public static final String PROPERTY_ORDER = "order";    
    private final PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private int order = 0;

    public LayerInfo() {
    }

    public LayerInfo(boolean selected, boolean visible, float transparency) {
        this.selected = selected;
        this.visible = visible;
        this.transparency = transparency;
    }

    /**
     * Indicates when the layer is selected or not.
     * 
     * @return Is selected or not
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * Sets a new indication of layer selected.
     * 
     * @param selected New indication of selected layer
     */
    public void setSelected(boolean selected) {
    	//I didn't find any case, where it would be useful, so I suppose it was a bug
    	//setting value of the internal field should be done, before firing property change
    	boolean oldSelected = this.selected;
    	this.selected = selected;
        changeSupport.firePropertyChange(PROPERTY_SELECTED, oldSelected, selected);
    }

    /**
     * Gets the transparancy of the layer.
     * 1.0 = fully transparent
     * 
     * @return Transparancy
     */
    public float getTransparency() {
        return transparency;
    }

    /**
     * Sets the transparancy of the layer.
     * value between 0 and 1.0
     * 
     * @param transparency New transparancy
     */
    public void setTransparency(float transparency) {
    	//setting value of the internal field should be done, before firing property change
        assert transparency >= 0.0 && transparency <= 1.0;
        float oldTransparency = this.transparency;
        this.transparency = transparency;
        changeSupport.firePropertyChange(PROPERTY_TRANSPARENCY, oldTransparency, transparency);
    }

    /**
     * Indicates when the layer is visible or not.
     * 
     * @return Is visible or not
     */
    public boolean isVisible() {
        return visible;
    }

    /**
     * Sets a new indication of layer visible.
     * 
     * @param visible New indication of visible layer
     */
    public void setVisible(boolean visible) {
        //setting value of the internal field should be done, before firing property change
    	boolean oldVisible = this.visible;
    	this.visible = visible;
        changeSupport.firePropertyChange(PROPERTY_VISIBLE, oldVisible, visible);
    }

    /**
     * Locked layers should not be editable
     *
     * @return true if the layer is locked
     */
    public boolean isLocked() {
        return locked;
    }

    /**
     * Lock or unlock a layer.
     * 
     * @param locked
     */
    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public boolean showLabels() {
        return showLabels;
    }

    public boolean showIcons() {
        return showIcons;
    }

    public void setShowLabels(boolean showLabels) {
        boolean old = this.showLabels;
        this.showLabels = showLabels;
        changeSupport.firePropertyChange(PROPERTY_SHOW_LABELS, old, showLabels);
    }

    public void setShowIcons(boolean showIcons) {
        boolean old = this.showIcons;
        this.showIcons = showIcons;
        changeSupport.firePropertyChange(PROPERTY_SHOW_ICONS, old, showIcons);
    }    
    
    /**
     * Add property change listener to this class.
     * @param listener Listener to add to this BaseLayer
     */
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    /**
     * Add property change listener to this class.
     * @param propertyName PropertyName to listen to
     * @param listener Listener to add to this BaseLayer
     */
    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(propertyName, listener);
    }

    /**
     * Remove the property change listener from this class.
     * @param listener Listener to remove
     */
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }

    /**
     * Remove the property change listener from this class.
     * @param propertyName PropertyName to stop listening to
     * @param listener Listener to remove
     */
    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(propertyName, listener);
    }

    /**
     * Get the order of the layer
     * @return 
     */
    public int getOrder() {
        return order;
    }

    /**
     * Sets the order of the layer
     * @param order 
     */
    public void setOrder(int order) {
        int oldOrder = this.order;
        this.order = order;
        changeSupport.firePropertyChange(PROPERTY_ORDER, oldOrder, order);
    }
    
}