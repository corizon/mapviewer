/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.api;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import org.openide.explorer.ExplorerManager;

/**
 * Basic implementation of the Layer-interface.
 *
 * The basis for a Layer on a map.
 *
 * @author Timon Veenstra
 */
public class BaseLayer implements Layer, PropertyChangeListener {

    /**
     * Palette identifier String
     */
    /**
     * Property Identifier used when setting the ObjectLayer Interactive
     * (PropertyChange will be fired, using this id)
     */
    private Palette palette;
    private final String name;
    private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    private SelectionChangeDirection selectionChangeDirection;


    /**
     * Constructor can only be called from extending classes since this class is
     * abstract.
     *
     * @param palette Color Palette
     * @param category Category this Layer belongs to
     * @param name Name of the Layer
     */
    public BaseLayer(Palette palette,  String name) {
        this.palette = palette;
        this.name = name;

        if (palette != null) {
            palette.addPropertyChangeListener(this);
        }
    }


    /**
     * @see Layer#getName Layer
     * @return The name of this BaseLayer
     */
    @Override
    public String getName() {
        return this.name;
    }
    
    /**
     * @see Layer#setPalette(Palette palette) Layer
     * @param palette The palette to set for this BaseLayer
     */
    @Override
    public final void setPalette(final Palette palette) {
        assert palette != null;
        if (this.palette != null) {
            this.palette.removePropertyChangeListener(this);
        }
        Palette old = this.palette;
        this.palette = palette;
        pcs.firePropertyChange(PROP_PALETTE, old, palette);
        palette.addPropertyChangeListener(this);
    }

    /**
     * @see Layer#getPalette Layer
     * @return The palette The palette for this BaseLayer
     */
    @Override
    public Palette getPalette() {
        return this.palette;
    }

    /**
     * Add property change listener to this class.
     * {@link nl.cloudfarming.client.model.PropertyChangeEmitter#addPropertyChangeListener(PropertyChangeListener listener)}}
     *
     * @param listener Listener to add to this BaseLayer
     */
    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    /**
     * Add property change listener to this class.
     * {@link nl.cloudfarming.client.model.PropertyChangeEmitter#addPropertyChangeListener(String propertyName, PropertyChangeListener listener)}
     *
     * @param propertyName PropertyName to listen to
     * @param listener Listener to add to this BaseLayer
     */
    @Override
    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(propertyName, listener);
    }

    /**
     * Remove the property change listener from this class.
     * {@link nl.cloudfarming.client.model.PropertyChangeEmitter#removePropertyChangeListener(String propertyName, PropertyChangeListener listener)}
     *
     * @param propertyName PropertyName to stop listening to
     * @param listener Listener to remove
     */
    @Override
    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(propertyName, listener);
    }

    /**
     * Remove the property change listener from this class.
     * {@link nl.cloudfarming.client.model.PropertyChangeEmitter#removePropertyChangeListener(PropertyChangeListener listener)}
     *
     * @param listener Listener to remove
     */
    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    /**
     * Returns the PropertyChangeSupport object bound to this class.
     *
     * @return The PropertyChangeSupport object bound to this class.
     */
    protected PropertyChangeSupport getChangeSupport() {
        return pcs;
    }

    /**
     * @see Layer#getActions
     * @return Array of actions for this BaseLayer
     */
    @Override
    public LayerAction[] getActions() {
        return null;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (Palette.PROP_BASE_COLOR.equals(evt.getPropertyName())) {
            pcs.firePropertyChange(PROP_PALETTE, null, palette);
        }
    }

    @Override
    public void fireNodeSelectionChange(PropertyChangeEvent event, SelectionChangeDirection direction) {
        assert ExplorerManager.PROP_SELECTED_NODES.equals(event.getPropertyName()):"Selection change event should always use propertyname ExplorerManager.PROP_SELECTED_NODES";
        if (selectionChangeDirection == null) {
            selectionChangeDirection = direction;
            pcs.firePropertyChange(event);
            selectionChangeDirection = null;
        }
    }

    @Override
    public SelectionChangeDirection getActiveChangeDirection() {
        return selectionChangeDirection;
    }

//    @Override
//    public boolean equals(Object obj) {
//        if (obj == null) {
//            return false;
//        }
//        if (getClass() != obj.getClass()) {
//            return false;
//        }
//        final BaseLayer other = (BaseLayer) obj;
//        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
//            return false;
//        }
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//        int hash = 5;
//        hash = 97 * hash + (this.name != null ? this.name.hashCode() : 0);
//        return hash;
//    }

    
}
