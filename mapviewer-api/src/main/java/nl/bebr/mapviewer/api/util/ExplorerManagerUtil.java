/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.api.util;

import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openide.explorer.ExplorerManager;
import org.openide.nodes.Node;

/**
 *
 * @author Timon Veenstra
 */
public class ExplorerManagerUtil {

    private static final Logger LOG = Logger.getLogger(ExplorerManagerUtil.class.getName());

    /**
     * Set an array of objects as selected in an Explorer Manager Will attempt
     * to find the nodes for the objects
     *
     * @param objects
     * @param explorerManager
     */
    public static void setSelectedObjects(Object[] objects, ExplorerManager explorerManager) {
        try {
            List<Node> nodes = new ArrayList<>();
            for (Object object : objects) {
                Node node = findNodeForObject(object, explorerManager.getRootContext());
                if (node == null) {
                    LOG.log(Level.WARNING, "No node found for object {0} in explorer manager {1}", new Object[]{object, explorerManager});
                } else {
                    nodes.add(node);
                }
            }
            explorerManager.setSelectedNodes(nodes.toArray(new Node[nodes.size()]));
        } catch (PropertyVetoException ex) {
            LOG.warning("setSelectedObject vetoed by the explorer manager, a view might not be able to display the selected nodes");
        }
    }

    /**
     * Set an object as the currently selected selected
     *
     * silently fails when object is not managed by the explorer manager
     *
     * @param object Bean to set selected
     */
    public static void setSelectedObject(Object object, ExplorerManager explorerManager) {
        try {
            Node node = findNodeForObject(object, explorerManager.getRootContext());
            if (node == null) {
                LOG.log(Level.WARNING, "No node found for object {0} in explorer manager {1}", new Object[]{object, explorerManager});
                return;
            }
            explorerManager.setSelectedNodes(new Node[]{node});
        } catch (PropertyVetoException ex) {
            LOG.warning("setSelectedObject vetoed by the explorer manager, a view might not be able to display the selected nodes");
        }
    }

    /**
     * add a bean to the node selection
     *
     * silently fails when object is not managed by the explorer manager
     *
     * @param object Bean to add to the selection
     */
    public static void addObjectToSelection(Object object, ExplorerManager explorerManager) {
        try {
            List<Node> selected = new ArrayList<>(Arrays.asList(explorerManager.getSelectedNodes()));
            Node node = findNodeForObject(object, explorerManager.getRootContext());
            if (node == null) {
                LOG.log(Level.WARNING, "No node found for object {0} in explorer manager {1}", new Object[]{object, explorerManager});
                return;
            }
            // avoid duplicate selection
            if (!selected.contains(node)) {
                selected.add(node);
            }
            explorerManager.setSelectedNodes(selected.toArray(new Node[]{}));
        } catch (PropertyVetoException ex) {
            LOG.warning("addObjectToSelection vetoed by the explorer manager, a view might not be able to display the selected nodes");
        }
    }

    /**
     * remove a node from the selection silently fails when object is not
     * managed by the explorer manager
     *
     * @param object Bean to remove from the selection
     */
    public static void removeObjectFromSelection(Object object, ExplorerManager explorerManager) {
        try {
            List<Node> selected = new ArrayList<>(Arrays.asList(explorerManager.getSelectedNodes()));

            Node node = findNodeForObject(object, explorerManager.getRootContext());
            if (node == null) {
                LOG.log(Level.WARNING, "No node found for object {0} in explorer manager {1}", new Object[]{object, explorerManager});
                return;
            }

            if (selected.contains(node)) {
                selected.remove(node);
                explorerManager.setSelectedNodes(selected.toArray(new Node[]{}));
            }
        } catch (PropertyVetoException ex) {
            LOG.warning("addObjectToSelection vetoed by the explorer manager, a view might not be able to display the selected nodes");
        }
    }

    /**
     * Adds a node to the selection of an explorer manager. When the node is
     * already selected, nothing will happen. silently fails when node is not
     * managed by the explorer manager
     *
     * @param node
     * @param explorerManager
     */
    public static void addNodeToSelection(Node node, ExplorerManager explorerManager) {
        List<Node> selected = new ArrayList<>(Arrays.asList(explorerManager.getSelectedNodes()));
        if (!selected.contains(node)) {
            try {
                selected.add(node);
                explorerManager.setSelectedNodes(selected.toArray(new Node[]{}));
            } catch (PropertyVetoException ex) {
                LOG.warning("addObjectToSelection vetoed by the explorer manager, a view might not be able to display the selected nodes");
            }
        }
    }

    /**
     * removes a node from the selection of an explorer manager. silently fails
     * when node is not managed by the explorer manager
     *
     * @param node
     * @param explorerManager
     */
    public static void removeNodeFromSelection(Node node, ExplorerManager explorerManager) {
        List<Node> selected = new ArrayList<>(Arrays.asList(explorerManager.getSelectedNodes()));
        if (selected.contains(node)) {
            try {
                selected.remove(node);
                explorerManager.setSelectedNodes(selected.toArray(new Node[]{}));
            } catch (PropertyVetoException ex) {
                LOG.warning("addObjectToSelection vetoed by the explorer manager, a view might not be able to display the selected nodes");
            }
        }
    }

    /**
     * Find an instance of an object in a node hierarchy.
     *
     *
     * @param <T> Type of object to find
     * @param clazz class of the object to find
     * @param node node to start search from downwards
     * @return
     */
    public static <T> T findObjectForNodeDown(Class<T> clazz, Node node) {
        if (clazz == null || node == null) {
            return null;
        }
        T wrappedObject = node.getLookup().lookup(clazz);
        if (wrappedObject != null) {
            return wrappedObject;
        }
        if (node.getChildren() != null) {
            for (Node child : node.getChildren().getNodes()) {
                T found = findObjectForNodeDown(clazz, child);
                if (found != null) {
                    return found;
                }
            }
        }
        return null;
    }

    /**
     * Find an instance of an object in a node hierarchy.
     *
     *
     * @param <T> Type of object to find
     * @param clazz class of the object to find
     * @param node node to start search from downwards
     * @return
     */
    public static <T> T findObjectForNodeUp(Class<T> clazz, Node node) {
        if (clazz == null || node == null) {
            return null;
        }
        T wrappedObject = node.getLookup().lookup(clazz);
        if (wrappedObject != null) {
            return wrappedObject;
        }
        if (node.getChildren() != null) {
            Node parent = node.getParentNode();
            T found = findObjectForNodeUp(clazz, parent);
            if (found != null) {
                return found;
            }
        }
        return null;
    }

    /**
     * Find the first node with the provided object in its lookup.
     *
     * @param object
     * @param node root node to start searching from
     * @return found node with object in its lookup
     */
    public static Node findNodeForObject(Object object, Node node) {
        if (object == null || node == null) {
            return null;
        }
        Object wrappedObject = node.getLookup().lookup(object.getClass());
        if (object.equals(wrappedObject)) {
            return node;
        }
        if (node.getChildren() != null) {
            for (Node child : node.getChildren().getNodes()) {
                Node found = findNodeForObject(object, child);
                if (found != null) {
                    return found;
                }
            }
        }
        return null;
    }
}
