/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.api;

import java.awt.Color;
import java.util.Random;

import org.netbeans.api.visual.model.ObjectState;

/**
 *
 * Quick implementation to create a layer with random color.
 * 
 * @author Timon Veenstra <monezz@gmail.com>
 */
public class RandomColorPalette extends Palette<Object> {

    public RandomColorPalette() {
        super(Color.BLACK);
        Random rand = new Random();
        float r = rand.nextFloat();
        float g = rand.nextFloat();
        float b = rand.nextFloat();
        Color randomColor = new Color(r, g, b);
        setBaseColor(randomColor);        
    }

    // trying variant for indicating geometry validity (shapes) TODO:move to own class
    public RandomColorPalette(boolean isValid, boolean isCorrected) {
        super(Color.BLACK);
        Random rand = new Random();
        float f = .3f;
        float r = rand.nextFloat() * f;
        float g = rand.nextFloat() * f;
        float b = rand.nextFloat() * f;
        Color randomColor = !isValid ? new Color(1-r, g, b) : (isCorrected ? new Color(1-r, 1-g, b) : new Color(r, 1-g, 1-b));
        setBaseColor(randomColor);        
    }
        
    @Override
    public Color getColorForState(ObjectState state) {
        return super.getColorForState(state);
    }    
}
