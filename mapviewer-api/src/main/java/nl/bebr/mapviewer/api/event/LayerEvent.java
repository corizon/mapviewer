/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.api.event;

import nl.bebr.mapviewer.api.Layer;
import nl.cloudfarming.eventbus.GuiEvent;

/**
 * GuiEvent specific to Layers. Specialized Layer Event classes should extend this abstract class.
 * @author Timon Veenstra
 */
public abstract class LayerEvent implements GuiEvent<Layer>{


    private final Layer layer;

    public LayerEvent(Layer layer) {
        this.layer = layer;
    }

    /**
     * @see GuiEvent#getContent() 
     * @return The Layer for which this LayerEvent was fired.
     */
    @Override
    public Layer getContent() {
        return this.layer;
    }
}
