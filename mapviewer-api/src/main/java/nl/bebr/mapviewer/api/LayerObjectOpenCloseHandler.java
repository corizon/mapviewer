/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.api;

import nl.cloudfarming.eventbus.GuiEvent;
import nl.cloudfarming.eventbus.GuiEventKey;

import org.openide.nodes.Node;
import org.openide.util.lookup.ServiceProvider;

import nl.bebr.mapviewer.api.event.GeoEvent;
import nl.bebr.mapviewer.api.util.OpenCloseHandler;

/**
 * 
 *
 * @author Timon Veenstra <monezz@gmail.com>
 */
@ServiceProvider(service = OpenCloseHandler.class)
public class LayerObjectOpenCloseHandler implements OpenCloseHandler {

    @Override
    public void open(Node node) {
        if (node.getLookup().lookup(Layer.class) != null) {
            AddToMapEvent event = new AddToMapEvent(node);
            new GeoEvent.GeoEventProducer().triggerEvent(event);
        }
    }

    @Override
    public void close(Node node) {
        if (node.getLookup().lookup(Layer.class) != null) {
            RemoveFromMapEvent event = new RemoveFromMapEvent(node);
            new GeoEvent.GeoEventProducer().triggerEvent(event);            
        }
    }

    private class AddToMapEvent implements GuiEvent<Node> {

        private Node node;

        public AddToMapEvent(Node node) {
            this.node = node;
        }

        @Override
        public GuiEventKey getKey() {
            return GeoEvent.NEW_LAYER;
        }

        @Override
        public Node getContent() {
            return node;
        }
    }

    private class RemoveFromMapEvent implements GuiEvent<Node> {

        private Node node;

        public RemoveFromMapEvent(Node node) {
            this.node = node;
        }

        @Override
        public GuiEventKey getKey() {
            return GeoEvent.DISCARD_LAYER;
        }

        @Override
        public Node getContent() {
            return node;
        }
    }
}
