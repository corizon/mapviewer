/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.api.event;

import eu.limetri.api.geo.Geographical;
import nl.cloudfarming.eventbus.BaseGuiEventProducer;
import nl.cloudfarming.eventbus.GuiEventKey;
import nl.cloudfarming.eventbus.GuiEventProducer;

/**
 *
 * @author Timon Veenstra
 */
public enum GeoEvent implements GuiEventKey{

    /**
     * Requests focus for a {@link Geographical} object
     */
    REQUEST_FOCUS,
    /**
     * Sends a hint that a layer is no longer valid and should be discarded
     */
    DISCARD_LAYER,
    /**
     * Data of a layer have been changed
     */
    DATA_CHANGED_EVENT,
    /**
     * A new layer have been added
     */
    NEW_LAYER,
    /**
     * A layer wants to be edited
     */
    EDIT_LAYER,   
    /**
     * A layer wants to be split
     */
    SPLIT_LAYER,
    
    /**
     * Tools panel is shown/hidden
     */
    TOOLS_PANEL,
    
    MEASURE_LENGTH,
    
    MEASURE_AREA;
    
    @Override
    public String getKey() {
        return toString();
    }
    
    /**
     * Static method to get a producer for this kind of events.
     * 
     * @return 
     */
    public static GuiEventProducer getProducer(){
        return new GeoEventProducer();
    }

    /**
     * The producer for this kind of events
     */
    public static class GeoEventProducer extends BaseGuiEventProducer {

        public GeoEventProducer() {
            super(GeoEvent.class);
        }
    }    
    
}
