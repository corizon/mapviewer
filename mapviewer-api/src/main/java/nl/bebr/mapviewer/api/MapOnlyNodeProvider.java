/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.api;


import org.openide.nodes.Node;


/**
 * This interface describes the property of an object to return an array of
 * nodes that are not used in any explorer, but can be displayed on the map.
 *
 * @author Wytse Visser
 * @author Maciek Dulkiewicz
 */
public interface MapOnlyNodeProvider {

    /**
     * This method returns an array of nodes that can be displayed on the map
     * but shouldn't be used in any other tree structure.
     *
     * @return Array of nodes (with {@link Geographical} objects in their
     * lookups)
     */
    public Node[] getMapOnlyNodes();
}
