/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.api;

import eu.limetri.api.geo.Geometrical;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;



/**
 *
 * A layer represents a set of data to be shown on the
 * map in a layer.
 *
 * @deprecated Layers provide an explorer manager which keeps track of layer objects and selection
 * @author Timon Veenstra
 */
@Deprecated
public abstract class ObjectLayer<O extends Geometrical> extends BaseLayer {

    private final List<O> objects = new ArrayList<>();
    private final List<O> selectedObjects = new ArrayList<>();
    
    public ObjectLayer(Palette palette, String name) {
        super(palette, name);
    }

    /**
     * get a list of objects in the layer
     * Objects extend the LayerObject interface which contains
     * a geometry.
     *
     * @return
     */
    public List<O> getObjects() {
        return Collections.unmodifiableList(this.objects);
    }

    /**
     * Add a layerObject to the internal objects list
     *
     * @param object
     */
    public void addObject(O object) {
        this.objects.add(object);
    }

    /**
     * remove a layerObject from the internal objects list
     *
     * @param object
     */
    public void removeObject(O object) {
        if (this.objects.contains(object)) {
            this.objects.remove(object);
        }
    }

    /**
     * clear the objects list
     */
    public void clearObjects() {
        this.objects.clear();
    }

    /**
     * 
     * @param objectsToAdd
     */
    public void addObjects(List<O> objectsToAdd) {
        this.objects.addAll(objectsToAdd);
    }


    public List<O> getSelectedObjects() {
        return selectedObjects;
    }
}
