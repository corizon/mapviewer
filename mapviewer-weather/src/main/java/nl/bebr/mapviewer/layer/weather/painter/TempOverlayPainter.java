/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.layer.weather.painter;

import nl.bebr.mapviewer.layer.weather.component.legend.AbstractLegendPanel;
import nl.bebr.mapviewer.layer.weather.component.legend.TemperatureLegendPanel;
import nl.bebr.mapviewer.layer.weather.tilefactory.WeatherTileFactoryInfo;
import nl.bebr.mapviewer.swing.overlay.AbstractOverlayPainter;
import nl.bebr.mapviewer.swing.overlay.LegendProvider;
import javax.swing.JComponent;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author Frantisek Post
 */

@ServiceProvider(service = AbstractOverlayPainter.class)
public class TempOverlayPainter extends WeatherOverlayPainter implements LegendProvider{
 
    AbstractLegendPanel legendPanel;
    
    public TempOverlayPainter() {
        super(WeatherTileFactoryInfo.TEMP, "Temperature");
        legendPanel = new TemperatureLegendPanel();
    }

    @Override
    public JComponent getLegend() {
        return legendPanel;
    }
    
}
