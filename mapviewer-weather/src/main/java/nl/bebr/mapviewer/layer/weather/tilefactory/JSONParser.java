/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.layer.weather.tilefactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.List;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Frantisek Post
 */
public class JSONParser {

    public List<CurrentWeatherData> parseCurrentWeatherData(String jsonUrl) throws JSONException, IOException {
        return parseCurrentWeatherData(jsonUrl, false);
    }

    public List<ForecastWeatherData> parseForecast(String jsonUrl, boolean translateFromKelvin) throws JSONException, IOException {
        JSONObject json = readJsonFromUrl(jsonUrl);
        List<ForecastWeatherData> data = new ArrayList<>();
        if (json.has("list")) {
            JSONArray dayArray = json.getJSONArray("list");
            for (int i = 0; i < dayArray.length(); i++) {
                JSONObject day = dayArray.getJSONObject(i);
                ForecastWeatherData weatherData = getForecastData(day, translateFromKelvin);
                data.add(weatherData);
            }
        } else {
            ForecastWeatherData weatherData = getForecastData(json, translateFromKelvin);
            data.add(weatherData);
        }
        return data;
    }

    public List<ForecastWeatherData> parseDetailForecast(String jsonUrl, boolean translateFromKelvin) throws JSONException, IOException {
        JSONObject json = readJsonFromUrl(jsonUrl);
        List<ForecastWeatherData> data = new ArrayList<>();
        if (json.has("list")) {
            JSONArray dayArray = json.getJSONArray("list");
            for (int i = 0; i < dayArray.length(); i++) {
                JSONObject day = dayArray.getJSONObject(i);
                ForecastWeatherData weatherData = getDetailForecastData(day, translateFromKelvin);
                data.add(weatherData);
            }
        } else {
            ForecastWeatherData weatherData = getDetailForecastData(json, translateFromKelvin);
            data.add(weatherData);
        }
        return data;
    }

    public List<CurrentWeatherData> parseCurrentWeatherData(String jsonUrl, boolean translateFromKelvin) throws JSONException, IOException {
        JSONObject json = readJsonFromUrl(jsonUrl);
        List<CurrentWeatherData> data = new ArrayList<>();
        if (json.has("list")) {
            JSONArray cityArray = json.getJSONArray("list");
            for (int i = 0; i < cityArray.length(); i++) {
                JSONObject city = cityArray.getJSONObject(i);
                CurrentWeatherData weatherData = getWeatherData(city, translateFromKelvin);
                data.add(weatherData);
            }
        } else {
            CurrentWeatherData weatherData = getWeatherData(json, translateFromKelvin);
            data.add(weatherData);
        }
        return data;
    }

    private ForecastWeatherData getForecastData(JSONObject jsonObject, boolean translateFromKelvin) throws JSONException {
        ForecastWeatherData weatherData = new ForecastWeatherData();
        weatherData.setDateTime(jsonObject.getLong("dt"));
        weatherData.setClouds(jsonObject.getInt("clouds"));
        weatherData.setHumidity(jsonObject.getDouble("humidity"));
        weatherData.setPressure(jsonObject.getDouble("pressure"));
        weatherData.setWind(jsonObject.getDouble("speed"));
        weatherData.setDescription(jsonObject.getJSONArray("weather").getJSONObject(0).getString("main"));
        JSONObject tempObject = jsonObject.getJSONObject("temp");
        weatherData.setTempMin(translateTemperature((float) tempObject.getDouble("min"), translateFromKelvin));
        weatherData.setTempMax(translateTemperature((float) tempObject.getDouble("max"), translateFromKelvin));
        weatherData.setTempNight(translateTemperature((float) tempObject.getDouble("night"), translateFromKelvin));
        weatherData.setTempDay(translateTemperature((float) tempObject.getDouble("day"), translateFromKelvin));
        weatherData.setTempEvening(translateTemperature((float) tempObject.getDouble("eve"), translateFromKelvin));
        weatherData.setTempMorning(translateTemperature((float) tempObject.getDouble("morn"), translateFromKelvin));
        if (jsonObject.has("snow")) {
            weatherData.setSnow(jsonObject.getDouble("snow"));
        }
        if (jsonObject.has("rain")) {
            weatherData.setRain(jsonObject.getDouble("rain"));
        }
        return weatherData;
    }

    private ForecastWeatherData getDetailForecastData(JSONObject jsonObject, boolean translateFromKelvin) throws JSONException {
        ForecastWeatherData weatherData = new ForecastWeatherData();
        weatherData.setDateTime(jsonObject.getLong("dt"));
        weatherData.setWind(jsonObject.getJSONObject("wind").getDouble("speed"));
        JSONObject mainObject = jsonObject.getJSONObject("main");
        weatherData.setHumidity(mainObject.getDouble("humidity"));
        weatherData.setPressure(mainObject.getDouble("pressure"));
        weatherData.setTempDay(translateTemperature((float) mainObject.getDouble("temp"), translateFromKelvin));
        weatherData.setTempMin(translateTemperature((float) mainObject.getDouble("temp_min"), translateFromKelvin));
        weatherData.setTempMax(translateTemperature((float) mainObject.getDouble("temp_max"), translateFromKelvin));
        weatherData.setIcon(jsonObject.getJSONArray("weather").getJSONObject(0).getString("icon"));

        if (jsonObject.has("rain")) {
            weatherData.setRain(jsonObject.getJSONObject("rain").getDouble("3h"));
        }

        if (jsonObject.has("snow")) {
            weatherData.setSnow(jsonObject.getJSONObject("snow").getDouble("3h"));
        }

        return weatherData;
    }

    private CurrentWeatherData getWeatherData(JSONObject jsonObject, boolean translateFromKelvin) throws JSONException {
        CurrentWeatherData weatherData = new CurrentWeatherData();
        weatherData.setCityName(jsonObject.getString("name"));
        weatherData.setCityId(jsonObject.getLong("id"));
        JSONObject coord = jsonObject.getJSONObject("coord");
        weatherData.setLongitude(coord.getDouble("lon"));
        weatherData.setLatitude(coord.getDouble("lat"));
        JSONObject main = jsonObject.getJSONObject("main");
        weatherData.setTemp(translateTemperature((float) main.getDouble("temp"), translateFromKelvin));
        weatherData.setTemp_min(translateTemperature((float) main.getDouble("temp_min"), translateFromKelvin));
        weatherData.setTemp_max(translateTemperature((float) main.getDouble("temp_max"), translateFromKelvin));

        if (main.has("humidity")) {
            weatherData.setHumidity(main.getInt("humidity"));
        }
        if (main.has("pressure")) {
            weatherData.setPressure(main.getInt("pressure"));
        }

        JSONArray weather = jsonObject.getJSONArray("weather");
        if (weather.length() > 0) {
            JSONObject weatherObject = weather.getJSONObject(0);
            weatherData.setWeatherIcon(weatherObject.getString("icon"));
        }
        return weatherData;
    }

    private static float translateTemperature(float temp, boolean translate) {
        return translate ? temp - 274.15f : temp;
    }

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        url = url.concat("&APPID=" + WeatherFactory.API_CODE);
        try (InputStream is = new URL(url).openStream()) {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONObject json = new JSONObject(jsonText);
            return json;
        }
    }
}
