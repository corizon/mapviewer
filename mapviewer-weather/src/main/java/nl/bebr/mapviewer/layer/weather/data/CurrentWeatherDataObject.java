/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.layer.weather.data;

import nl.bebr.mapviewer.layer.weather.tilefactory.CurrentWeatherData;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.List;

/**
 *
 * @Frantisek Post
 */
public class CurrentWeatherDataObject {
    
    public static final long OUTDATED_TRESHOLD = 1800000l; //30 minutes old are outdated
    
    private List<CurrentWeatherData> data;
    PropertyChangeSupport propertyChangeSupport;
    public static final String LOADED = "loaded";
    private boolean loaded = false;
    private long time;
    
    public CurrentWeatherDataObject() {
        propertyChangeSupport = new PropertyChangeSupport(this);
    }
    
    public void setData(List<CurrentWeatherData> data) {
        this.data = data;
        setLoaded();
        propertyChangeSupport.firePropertyChange(LOADED, null, data);
    }
    
    public List<CurrentWeatherData> getData() {
        return data;
    }
    
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }
    
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    public void setLoaded() {
        this.loaded = true;
        time = System.currentTimeMillis();
    }
    
    public boolean isLoaded() {
        return loaded;
    }
    
    public boolean isOutdated() {
        long timeDelay = System.currentTimeMillis() - time;
        return timeDelay >= OUTDATED_TRESHOLD;
    }
}
