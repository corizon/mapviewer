/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.layer.weather.painter;

import nl.bebr.mapviewer.data.GeoPosition;
import nl.bebr.mapviewer.layer.weather.component.CurrentWeatherPanel;
import nl.bebr.mapviewer.layer.weather.data.CurrentWeatherDataObject;
import nl.bebr.mapviewer.layer.weather.data.WeatherDataFactory;
import nl.bebr.mapviewer.layer.weather.painter.icon.WeatherIconRepository;
import nl.bebr.mapviewer.layer.weather.tilefactory.CurrentWeatherData;
import nl.bebr.mapviewer.layer.weather.tilefactory.WeatherFactory;
import nl.bebr.mapviewer.swing.JXMapViewer;
import nl.bebr.mapviewer.swing.overlay.AbstractOverlayPainter;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.JPopupMenu;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author Frantisek Post
 */
@ServiceProvider(service = AbstractOverlayPainter.class)
public class CurrentOverlayPainter extends AbstractOverlayPainter {

    private WeatherFactory weatherFactory;
    private WeatherIconRepository iconRepository;
    private static final String temp = "%.1f\u00b0";
    private Map<Point2D, CurrentWeatherData> iconsOnMap;
    private JXMapViewer mapViewer;

    public CurrentOverlayPainter() {
        weatherFactory = new WeatherFactory();
        iconRepository = new WeatherIconRepository();
        setName("Current weather");
        iconsOnMap = new HashMap<>();
    }

    @Override
    public void paint(Graphics2D g, final JXMapViewer mapviewer, int width, int height) {

        if (!visible) {
            return;
        }

        if (this.mapViewer == null) {
            this.mapViewer = mapviewer;
        }

        iconsOnMap.clear();

        final Graphics2D g2 = (Graphics2D) g.create();

        Rectangle viewportBounds = mapviewer.getViewportBounds();

        GeoPosition start = mapviewer.getTileFactory().pixelToGeo(new Point(viewportBounds.x, viewportBounds.y), mapviewer.getZoom());
        GeoPosition end = mapviewer.getTileFactory().pixelToGeo(new Point(viewportBounds.x + viewportBounds.width, viewportBounds.y + viewportBounds.height), mapviewer.getZoom());

        final CurrentWeatherDataObject weatherDataObject = WeatherDataFactory.getInstance().getCurrentWeatherDataForArea(start.getLatitude(), start.getLongitude(), end.getLatitude(), end.getLongitude(), mapviewer.getZoom());
        
        if (weatherDataObject.isLoaded()) {
            List<CurrentWeatherData> cities = weatherDataObject.getData();

            if (cities != null) {
                cities.stream().forEach((d) -> {
                    GeoPosition geoposition = new GeoPosition(d.getLatitude(), d.getLongitude());
                    Point2D point = mapviewer.convertGeoPositionToPoint(geoposition);
                    Icon icon = iconRepository.loadIcon(d.getWeatherIcon());
                    if (icon != null) {
                        icon.paintIcon(mapviewer, g2, (int) point.getX(), (int) point.getY());
                        printBorderred(g2, String.format(temp, d.getTemp()), (int) point.getX() + icon.getIconWidth() / 4, (int) point.getY() + icon.getIconHeight());
                        iconsOnMap.put(point, d);
                    }
                });
            }
        }
        
        weatherDataObject.addPropertyChangeListener((PropertyChangeEvent evt) -> {
            mapviewer.repaint();
        });

        g2.dispose();
    }

    private void printBorderred(Graphics2D g, String text, int x, int y) {
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setColor(Color.WHITE);
        g.drawString(text, x - 1, y - 1);
        g.drawString(text, x - 1, y + 1);
        g.drawString(text, x + 1, y - 1);
        g.drawString(text, x + 1, y + 1);
        g.setColor(Color.BLACK);
        g.drawString(text, x, y);
    }

    @Override
    protected void processMapEvent(MouseEvent event) {

        Point2D mousePoint = new Point2D.Double(event.getX(), event.getY());
        CurrentWeatherData weatherData = null;

        if (event.getButton() == MouseEvent.BUTTON1) {
            for (Map.Entry<Point2D, CurrentWeatherData> entry : iconsOnMap.entrySet()) {
                if (isClicked(entry.getKey(), mousePoint)) {
                    weatherData = entry.getValue();
                    break;
                }
            }
        } else if (event.getButton() == MouseEvent.BUTTON3) {
            GeoPosition geoposition = mapViewer.convertPointToGeoPosition(mousePoint);
            List<CurrentWeatherData> weatherDataList = weatherFactory.getCurrentWeatherData(geoposition.getLongitude(), geoposition.getLatitude());
            if (!weatherDataList.isEmpty()) {
                weatherData = weatherDataList.get(0);
            }
        }

        if (weatherData != null) {
            CurrentWeatherPanel panel = new CurrentWeatherPanel();
            panel.setWeatherData(weatherData);
            JPopupMenu menu = new JPopupMenu();
            menu.add(panel);
            menu.show(event.getComponent(), (int) mousePoint.getX(), (int) mousePoint.getY());
            event.consume();
        }
    }

    private boolean isClicked(Point2D point, Point2D clicked) {
        int size = 50;
        return point.getX() < clicked.getX() && point.getX() + size > clicked.getX()
                && point.getY() < clicked.getY() && point.getY() + size > clicked.getY();
    }
    
    @Override
    public void setVisible(boolean visible) {
        boolean oldVisible = isVisible();
        super.setVisible(visible); 
        if (oldVisible != visible && mapViewer != null) {
            mapViewer.repaint();
        }
    }
    
}
