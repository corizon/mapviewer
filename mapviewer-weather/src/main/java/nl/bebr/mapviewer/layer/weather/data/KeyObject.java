/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.layer.weather.data;

/**
 *
 * @author Frantisek Post
 */
public class KeyObject {
    
    int zoom;
    double startLongitude;
    double startLatitude;
    double endLongitude;
    double endLatitude;

    public KeyObject(int zoom, double startLongitude, double startLatitude, double endLongitude, double endLatitude) {
        this.zoom = zoom;
        this.startLongitude = startLongitude;
        this.startLatitude = startLatitude;
        this.endLongitude = endLongitude;
        this.endLatitude = endLatitude;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + this.zoom;
        hash = 41 * hash + (int) (Double.doubleToLongBits(this.startLongitude) ^ (Double.doubleToLongBits(this.startLongitude) >>> 32));
        hash = 41 * hash + (int) (Double.doubleToLongBits(this.startLatitude) ^ (Double.doubleToLongBits(this.startLatitude) >>> 32));
        hash = 41 * hash + (int) (Double.doubleToLongBits(this.endLongitude) ^ (Double.doubleToLongBits(this.endLongitude) >>> 32));
        hash = 41 * hash + (int) (Double.doubleToLongBits(this.endLatitude) ^ (Double.doubleToLongBits(this.endLatitude) >>> 32));
        return hash;
    }

    public int getZoom() {
        return zoom;
    }

    public double getStartLongitude() {
        return startLongitude;
    }

    public double getStartLatitude() {
        return startLatitude;
    }

    public double getEndLongitude() {
        return endLongitude;
    }

    public double getEndLatitude() {
        return endLatitude;
    }

    
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final KeyObject other = (KeyObject) obj;
        if (this.zoom != other.zoom) {
            return false;
        }
        if (Double.doubleToLongBits(this.startLongitude) != Double.doubleToLongBits(other.startLongitude)) {
            return false;
        }
        if (Double.doubleToLongBits(this.startLatitude) != Double.doubleToLongBits(other.startLatitude)) {
            return false;
        }
        if (Double.doubleToLongBits(this.endLongitude) != Double.doubleToLongBits(other.endLongitude)) {
            return false;
        }
        if (Double.doubleToLongBits(this.endLatitude) != Double.doubleToLongBits(other.endLatitude)) {
            return false;
        }
        return true;
    }
    
}
