/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.layer.weather.tilefactory;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import org.json.JSONException;

/**
 *
 * @author Frantisek Post
 */
public class WeatherFactory {

    public static final String API_CODE = "0d54f33f521cfd4acf7aa3ec4ed1ab3b";
    
    private static final int NUMBER_OF_ATTEMPTS = 3;
    private static final String serviceUrlForLocation = "http://api.openweathermap.org/data/2.5/weather?lat=%.10f&lon=%.10f";
    private static final String serviceUrlForArea = "http://api.openweathermap.org/data/2.5/box/city?bbox=%.10f,%.10f,%.10f,%.10f,%d&cluster=yes";
    private static final String serviceUrlForId = "http://api.openweathermap.org/data/2.5/weather?id=%d";
    private static final String serviceUrlForGroupId = "http://api.openweathermap.org/data/2.5/group?id=%s1&unitsq=metric";
    private static final String serviceUrlForecastDetail = "http://api.openweathermap.org/data/2.5/forecast?id=%d";
    
    private JSONParser jsonParser;

    public WeatherFactory() {
        jsonParser = new JSONParser();
    }

    public List<CurrentWeatherData> getCurrentWeatherData(double startLongitude, double startLatitude) {
        return readCitiesData(startLongitude, startLatitude);
    }

    public List<CurrentWeatherData> getCurrentWeatherDataForArea(double startLongitude, double startLatitude, double endLongitude, double endLatitude, int zoom) {
        return readCitiesData(startLongitude, startLatitude, endLongitude, endLatitude, 19 - zoom);
    }

    public List<CurrentWeatherData> readCitiesData(double startLongitude, double startLatitude) {
        String url = String.format(Locale.ENGLISH, serviceUrlForLocation, startLatitude, startLongitude);
        List<CurrentWeatherData> data = null;

        for (int i = 0; i < NUMBER_OF_ATTEMPTS; i++) {
            try {
                data = jsonParser.parseCurrentWeatherData(url, true);
                break;
            } catch (JSONException | IOException ex) {
                //another attempt
            }
        }

        return data;
    }

    public List<CurrentWeatherData> readCitiesData(double startLongitude, double startLatitude, double endLongitude, double endLatitude, int zoom) {
        String url = String.format(Locale.ENGLISH, serviceUrlForArea, startLatitude, startLongitude, endLatitude, endLongitude, zoom);
        List<CurrentWeatherData> data = null;

        for (int i = 0; i < NUMBER_OF_ATTEMPTS; i++) {
            try {
                data = jsonParser.parseCurrentWeatherData(url);
                break;
            } catch (JSONException | IOException ex) {
                //another try
            }
        }

        return data;
    }

    public CurrentWeatherData getCurrentWeatherData(long id) {
        String url = String.format(Locale.ENGLISH, serviceUrlForId, id);
        List<CurrentWeatherData> data = null;

        for (int i = 0; i < NUMBER_OF_ATTEMPTS; i++) {
            try {
                data = jsonParser.parseCurrentWeatherData(url, true);
                break;
            } catch (JSONException | IOException ex) {
                //another try
            }
        }

        return data != null && !data.isEmpty() ? data.get(0) : null;
    }

    public List<CurrentWeatherData> getCurrentWeatherDataForCities(long... ids) {

        String text = "";
        for (long id : ids) {
            text += id + ",";
        }

        String url = String.format(Locale.ENGLISH, serviceUrlForGroupId, text);
        List<CurrentWeatherData> data = null;

        for (int i = 0; i < NUMBER_OF_ATTEMPTS; i++) {
            try {
                data = jsonParser.parseCurrentWeatherData(url, true);
                break;
            } catch (JSONException | IOException ex) {
                //another try
            }
        }

        return data;
    }

    public List<ForecastWeatherData> getDetailForecast(long cityId) {
        String url = String.format(Locale.ENGLISH, serviceUrlForecastDetail, cityId);
        List<ForecastWeatherData> data = null;

        for (int i = 0; i < NUMBER_OF_ATTEMPTS; i++) {
            try {
                data = jsonParser.parseDetailForecast(url, true);
                break;
            } catch (JSONException | IOException ex) {
                //another try
            }
        }

        return data;
    }
}
