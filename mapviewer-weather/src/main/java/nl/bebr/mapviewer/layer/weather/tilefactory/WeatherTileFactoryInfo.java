/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 * <http://www.agrosense.eu/foss-exception.html>.
 *
 * AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.layer.weather.tilefactory;

import nl.bebr.mapviewer.data.TileFactoryInfo;

public class WeatherTileFactoryInfo extends TileFactoryInfo {

    private static final int max = 19;

    public static final String PRECIPITATION = "precipitation"; //Quantity of precipitation
    public static final String PRECIPITATION_CLS = "precipitation_cls"; //Precipitation classic style
    public static final String RAIN = "rain"; //Rain precipitation
    public static final String RAIN_CLS = "rain_cls"; //Rain classic style
    public static final String SNOW = "snow"; //Snow precipitation
    public static final String CLOUDS = "clouds"; //Cloud Cover
    public static final String CLOUDS_CLS = "clouds_cls"; //clouds classic style
    public static final String PRESSURE = "pressure"; //Sea Level Pressure
    public static final String PRESSURE_CNTR = "pressure_cntr"; //Sea Level Pressure contour
    public static final String TEMP = "temp"; //Temperature
    public static final String WIND = "wind"; //Wind speed

    /**
     * Default constructor
     */
    public WeatherTileFactoryInfo(String type) {
        super("OpenWeatherMap",
                1, max - 2, max,
                256, true, true, // tile size is 256 and x/y orientation is normal
                "https://tile.openweathermap.org/map/" + type,
                "x", "y", "z");						// 5/15/10.png

        setPersistent(false);
    }

    @Override
    public String getTileUrl(int x, int y, int zoom) {

        String openWeatherMapApiKey = System.getenv("OPENWEATHERMAP_API_KEY");
        String url = this.baseURL + "/" + recalculateZoom(zoom) + "/" + x + "/" + y + ".png?APPID=" + openWeatherMapApiKey;
        return url;
    }

    @Override
    protected int recalculateZoom(int zoom) {
        return max - zoom;
    }

}
