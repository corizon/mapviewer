/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.layer.weather.painter.icon;

import nl.bebr.mapviewer.data.util.ConnectionChecker;
import nl.bebr.mapviewer.data.util.GraphicsUtilities;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.openide.util.Exceptions;

/**
 * Repository for openweather.org icons
 * 
 * @author Frantisek Post
 */
public class WeatherIconRepository {
    
    private static final String URL = "http://openweathermap.org/img/w/%s.png";
    
    private Map<String, Icon> iconMap;
    
    private ConnectionChecker connChecker = ConnectionChecker.getInstance();
    
    public WeatherIconRepository() {
        iconMap = new HashMap<>();
    }
    
    public Icon loadIcon(String iconCode) {
        Icon icon = iconMap.get(iconCode);
        if (icon == null && connChecker.isOnline()) {
            try {
                icon = loadIconFromUrl(String.format(URL, iconCode));
                iconMap.put(iconCode, icon);
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
        return icon;
    }
    
    private Icon loadIconFromUrl(String url) throws IOException {
        byte[] bimg = cacheInputStream(new URL(url));
        if (bimg != null) {
            BufferedImage img = GraphicsUtilities.loadCompatibleImage(new ByteArrayInputStream(bimg));
            return new ImageIcon(img);
        } else {
            return null;
        }
    }

    protected byte[] cacheInputStream(URL url) throws IOException {
        //TODO FP catch java.io.IOException when Server returns HTTP response code: 50x	
        try {
            InputStream ins = url.openStream();
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            byte[] buf = new byte[256];
            while (true) {
                int n = ins.read(buf);
                if (n == -1) {
                    break;
                }
                bout.write(buf, 0, n);
            }
            return bout.toByteArray();
        } catch (FileNotFoundException ex) {
            //TODO log
            return null;
        }
    }
    
}
