/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.layer.weather.component.legend;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.beans.Transient;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import javax.swing.JPanel;

/**
 * 
 * @author Frantisek Post
 */
public class AbstractLegendPanel extends JPanel {

	Map<Float, Color> legendMap;

	private static final int LEGEND_HEIGHT = 200;
	private static final int LEGEND_WIDTH = 60;

	private double elementHeight;

	public AbstractLegendPanel() {

	}

	public void setMap(Map<Float, Color> legendMap) {
		this.legendMap = legendMap;
		elementHeight = LEGEND_HEIGHT / legendMap.size();
	}

	@Override
	@Transient
	public Dimension getPreferredSize() {
		return new Dimension(LEGEND_WIDTH, LEGEND_HEIGHT);
	}
	
	@Override
	public void paint(Graphics g) {
		Graphics2D g2d = (Graphics2D) g.create();

		int textHeight = g2d.getFontMetrics().getAscent()*3/4; //number height could be 75% of the ascent
		double textOffset = (elementHeight - textHeight) / 2;
		
		g2d.setColor(Color.WHITE);
		g2d.fillRect(0, 0, getWidth(), getHeight());

		int width = LEGEND_WIDTH / 4;
		int index = 0;
		for (Entry<Float, Color> entry : legendMap.entrySet()) {
			g2d.setColor(entry.getValue());
			g2d.fillRect(LEGEND_WIDTH - width, (int) (index * elementHeight),
					width, (int) elementHeight);
			g2d.setColor(Color.BLACK);
			g2d.drawString(
					String.format("%.1f", entry.getKey()),
					10,
					(int) ((index + 1) * elementHeight - textOffset));
			index++;
		}

		g2d.dispose();
	}

}
