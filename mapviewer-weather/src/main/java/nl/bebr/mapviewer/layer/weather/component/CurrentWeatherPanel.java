/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.layer.weather.component;

import nl.bebr.mapviewer.layer.weather.painter.icon.WeatherIconRepository;
import nl.bebr.mapviewer.layer.weather.tilefactory.CurrentWeatherData;
import nl.bebr.mapviewer.layer.weather.tilefactory.ForecastWeatherData;
import nl.bebr.mapviewer.layer.weather.tilefactory.WeatherFactory;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import net.miginfocom.swing.MigLayout;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.util.NbBundle;

/**
 *
 * @author Frantisek Post
 */
@NbBundle.Messages({
    "forecast_button_title=5 days forecast (3 hours)",
    "actual_temp=Actual temp: %.1f\u00b0C min: %.1f\u00b0C max: %.1f\u00b0C",
    "sea_level_pressure=Sea level pressure: %d hPa",
    "humidity=Humidity: %d ",
    "graph_temperature=Temperature",
    "graph_temperature_minimum=Temperature minimum",
    "graph_temperature_maximum=Temperature maximum",
    "graph_humidity=Humidity",
    "graph_pressure=Pressure",
    "graph_precipitation=Precipitation",
    "graph_rain=Rain",
    "graph_snow=Snow",
    "graph_wind_speed=Wind speed",
    "title_axis_temperature_minimum=Temperature minimum",
    "title_axis_temperature_maximum=Temperature maximum",
    "title_axis_rain=Rain",
    "title_axis_snow=Snow",
    "axis_date=Date",
    "axis_title_temperature=Temperature [\u00b0C]",
    "axis_title_humidity=Humidity [%]",
    "axis_title_pressure=Pressure [hPa]",
    "axis_title_precipitation=Precipitation [mm]",
    "axis_title_wind_speed=Wind speed [m/s]",
    "weather_forecast_window_title=Weather forecast window",
    "close_button_text=Close"
})
public class CurrentWeatherPanel extends JPanel {

    private CurrentWeatherData weatherData;
    private WeatherIconRepository iconRepository;
    private JLabel cityLabel;
    private JLabel iconLabel;
    private JLabel tempLabel;
    private JLabel pressureLabel;
    private JLabel humidityLabel;
    private JLabel windLabel;
    private JTextArea descriptionTextArea;
    private JButton forecast5Button;
    private WeatherFactory weatherfactory;
    private static final SimpleDateFormat AXIS_DATE_FORMAT = new SimpleDateFormat("EEE. d kk:00");

    public CurrentWeatherPanel() {
        super();
        initComponents();
        iconRepository = new WeatherIconRepository();
        weatherfactory = new WeatherFactory();
    }

    private void initComponents() {
        cityLabel = new JLabel();
        iconLabel = new JLabel();
        tempLabel = new JLabel();
        pressureLabel = new JLabel();
        humidityLabel = new JLabel();
        windLabel = new JLabel();
        descriptionTextArea = new JTextArea();
        forecast5Button = new JButton(NbBundle.getMessage(CurrentWeatherPanel.class, "forecast_button_title"));

        setLayout(new MigLayout());

        add(cityLabel, "wrap");
        add(iconLabel, "wrap");
        add(tempLabel, "wrap");
        add(pressureLabel, "wrap");
        add(humidityLabel, "wrap");
        add(windLabel, "wrap");
        add(descriptionTextArea, "wrap");
        add(forecast5Button, "wrap");

        forecast5Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showDetailForecast(getCityId());
            }
        });
    }

    private long getCityId() {
        return weatherData.getCityId();
    }

    private Icon getIconForState(String state) {
        return iconRepository.loadIcon(state);
    }

    public void setWeatherData(CurrentWeatherData weatherData) {
        this.weatherData = weatherData;

        cityLabel.setText(weatherData.getCityName());
        iconLabel.setIcon(getIconForState(weatherData.getWeatherIcon()));
        String text = String.format(NbBundle.getMessage(CurrentWeatherPanel.class, "actual_temp"), weatherData.getTemp(), weatherData.getTemp_min(), weatherData.getTemp_max());
        tempLabel.setText(text);
        text = String.format(NbBundle.getMessage(CurrentWeatherPanel.class, "sea_level_pressure"), weatherData.getPressure());
        pressureLabel.setText(text);
        text = String.format(NbBundle.getMessage(CurrentWeatherPanel.class, "humidity"), weatherData.getHumidity());
        humidityLabel.setText(text);
        descriptionTextArea.setText(weatherData.getWeatherDescription());
    }

    public List<ForecastWeatherData> loadForecastData(long cityId) {
        return weatherfactory.getDetailForecast(cityId);
    }

    private void showDetailForecast(long cityId) {
        List<ForecastWeatherData> data = loadForecastData(cityId);

        if (data == null) {
            return;
        }

        XYSeries seriesTemp = new XYSeries(NbBundle.getMessage(CurrentWeatherPanel.class, "graph_temperature"));
        XYSeries seriesTempMax = new XYSeries(NbBundle.getMessage(CurrentWeatherPanel.class, "graph_temperature_maximum"));
        XYSeries seriesTempMin = new XYSeries(NbBundle.getMessage(CurrentWeatherPanel.class, "graph_temperature_minimum"));
        XYSeries seriesHumidity = new XYSeries(NbBundle.getMessage(CurrentWeatherPanel.class, "graph_humidity"));
        XYSeries seriesRain = new XYSeries(NbBundle.getMessage(CurrentWeatherPanel.class, "graph_rain"));
        XYSeries seriesSnow = new XYSeries(NbBundle.getMessage(CurrentWeatherPanel.class, "graph_snow"));
        XYSeries seriesPressure = new XYSeries(NbBundle.getMessage(CurrentWeatherPanel.class, "graph_pressure"));
        XYSeries seriesWind = new XYSeries(NbBundle.getMessage(CurrentWeatherPanel.class, "graph_wind_speed"));

        for (ForecastWeatherData forecast : data) {
            seriesTemp.add(forecast.getDateTime() * 1000, forecast.getTempDay());
            seriesTempMax.add(forecast.getDateTime() * 1000, forecast.getTempMax());
            seriesTempMin.add(forecast.getDateTime() * 1000, forecast.getTempMin());
            seriesHumidity.add(forecast.getDateTime() * 1000, forecast.getHumidity());
            seriesRain.add(forecast.getDateTime() * 1000, forecast.getRain());
            seriesSnow.add(forecast.getDateTime() * 1000, forecast.getSnow());
            seriesPressure.add(forecast.getDateTime() * 1000, forecast.getPressure());
            seriesWind.add(forecast.getDateTime() * 1000, forecast.getWind());
        }

        XYSeriesCollection temperatureDataset = new XYSeriesCollection();
        temperatureDataset.addSeries(seriesTemp);
        temperatureDataset.addSeries(seriesTempMax);
        temperatureDataset.addSeries(seriesTempMin);

        Dimension graphSize = new java.awt.Dimension(500, 270);
        
        String dateText = NbBundle.getMessage(CurrentWeatherPanel.class, "axis_date");
        JFreeChart temperatureChart = createXYLineChart(NbBundle.getMessage(CurrentWeatherPanel.class, "graph_temperature"), dateText, 
                NbBundle.getMessage(CurrentWeatherPanel.class, "axis_title_temperature"), temperatureDataset, true, true);
        ChartPanel temperatureChartPanel = new ChartPanel(temperatureChart);
        temperatureChartPanel.setPreferredSize(graphSize);

        XYSeriesCollection humidityDataset = new XYSeriesCollection();
        humidityDataset.addSeries(seriesHumidity);

        JFreeChart humidityChart = createXYLineChart(NbBundle.getMessage(CurrentWeatherPanel.class, "graph_humidity"), dateText, 
                NbBundle.getMessage(CurrentWeatherPanel.class, "axis_title_humidity"), humidityDataset, false, false);
        ChartPanel humidityChartPanel = new ChartPanel(humidityChart);
        humidityChartPanel.setPreferredSize(graphSize);

        XYSeriesCollection precipitationDataset = new XYSeriesCollection();
        precipitationDataset.addSeries(seriesRain);
        precipitationDataset.addSeries(seriesSnow);

        JFreeChart precipitationChart = createBarChart(NbBundle.getMessage(CurrentWeatherPanel.class, "graph_precipitation"), dateText, 
                NbBundle.getMessage(CurrentWeatherPanel.class, "axis_title_precipitation"), precipitationDataset, true);
        ChartPanel precipitationChartPanel = new ChartPanel(precipitationChart);
        humidityChartPanel.setPreferredSize(graphSize);

        XYSeriesCollection pressureDataset = new XYSeriesCollection();
        pressureDataset.addSeries(seriesPressure);

        JFreeChart pressureChart = createXYLineChart(NbBundle.getMessage(CurrentWeatherPanel.class, "graph_pressure"), dateText, 
                NbBundle.getMessage(CurrentWeatherPanel.class, "axis_title_pressure"), pressureDataset, false, false);
        ChartPanel pressureChartPanel = new ChartPanel(pressureChart);
        humidityChartPanel.setPreferredSize(graphSize);

        XYSeriesCollection windDataset = new XYSeriesCollection();
        windDataset.addSeries(seriesWind);

        JFreeChart windChart = createXYLineChart(NbBundle.getMessage(CurrentWeatherPanel.class, "graph_wind_speed"), dateText, 
                NbBundle.getMessage(CurrentWeatherPanel.class, "axis_title_wind_speed"), windDataset, false, true);
        ChartPanel windChartPanel = new ChartPanel(windChart);
        humidityChartPanel.setPreferredSize(graphSize);

        JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.addTab(NbBundle.getMessage(CurrentWeatherPanel.class, "graph_temperature"), temperatureChartPanel);
        tabbedPane.addTab(NbBundle.getMessage(CurrentWeatherPanel.class, "graph_humidity"), humidityChartPanel);
        tabbedPane.addTab(NbBundle.getMessage(CurrentWeatherPanel.class, "graph_precipitation"), precipitationChartPanel);
        tabbedPane.addTab(NbBundle.getMessage(CurrentWeatherPanel.class, "graph_pressure"), pressureChartPanel);
        tabbedPane.addTab(NbBundle.getMessage(CurrentWeatherPanel.class, "graph_wind_speed"), windChartPanel);

        JPanel p = new JPanel();
        p.setLayout(new BorderLayout());
        p.add(tabbedPane, BorderLayout.CENTER);

        DialogDescriptor dd = new DialogDescriptor(p, NbBundle.getMessage(CurrentWeatherPanel.class, "weather_forecast_window_title"));
        dd.setOptions(new Object[]{NbBundle.getMessage(CurrentWeatherPanel.class, "close_button_text")});
        dd.setClosingOptions(new Object[]{NbBundle.getMessage(CurrentWeatherPanel.class, "close_button_text")});
        DialogDisplayer.getDefault().createDialog(dd).setVisible(true);
    }

    private JFreeChart createXYLineChart(String title, String xAxisLabel,
            String yAxisLabel, XYDataset dataset, boolean legend, boolean includeZero) {

        DateAxis xAxis = new DateAxis(xAxisLabel);
        xAxis.setDateFormatOverride(AXIS_DATE_FORMAT);
        NumberAxis yAxis = new NumberAxis(yAxisLabel);
        yAxis.setAutoRangeIncludesZero(includeZero);
        XYItemRenderer renderer = new XYLineAndShapeRenderer(true, false);
        XYPlot plot = new XYPlot(dataset, xAxis, yAxis, renderer);
        plot.setOrientation(PlotOrientation.VERTICAL);
        return new JFreeChart(title, JFreeChart.DEFAULT_TITLE_FONT, plot, legend);
    }

    private JFreeChart createBarChart(String title, String xAxisLabel,
            String yAxisLabel, XYDataset dataset, boolean legend) {

        DateAxis xAxis = new DateAxis(xAxisLabel);
        xAxis.setDateFormatOverride(AXIS_DATE_FORMAT);
        NumberAxis yAxis = new NumberAxis(yAxisLabel);

        XYBarRenderer renderer = new XYBarRenderer();
        renderer.setShadowVisible(false);
        XYPlot plot = new XYPlot(dataset, xAxis, yAxis, renderer);
        plot.setOrientation(PlotOrientation.VERTICAL);
        return new JFreeChart(title, JFreeChart.DEFAULT_TITLE_FONT, plot, legend);
    }
}
