/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.impl;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.swing.UIManager;

import nl.bebr.mapviewer.data.cache.OfflineTile;
import nl.bebr.mapviewer.data.cache.OfflineTileFactory;

/**
* Default implementation of {@link OfflineTile} for Swing
* 
* @author Frantisek Post
*
*/
public class OfflineTileSwing extends OfflineTile<BufferedImage> {

  private static Color offlineTileBackground = loadColor("OfflineTile.background", new Color(240, 240, 240));
  private static Color offlineTileForeground = loadColor("OfflineTile.foreground", Color.BLACK);
	
	/**
	 * Constructor 
	 * 
	 * @param x x coordinate
	 * @param y y coordinate
	 * @param zoom zoom level
	 * @param url url of tile
	 * @param tileFactory tileFactory
	 */
	public OfflineTileSwing(int x, int y, int zoom, String url,	OfflineTileFactory<BufferedImage> tileFactory) {
		super(x, y, zoom, url, tileFactory);
	}

	@Override
	protected BufferedImage createOfflineImageImpl() {
      BufferedImage offlineImage = new BufferedImage(TILE_SIZE, TILE_SIZE, BufferedImage.TYPE_INT_RGB);
      Graphics2D graphics = offlineImage.createGraphics();
      graphics.setColor(offlineTileBackground);
      graphics.fillRect(0, 0, TILE_SIZE, TILE_SIZE);
      graphics.setColor(offlineTileForeground);
      String message = "no data";
      int textWidth = graphics.getFontMetrics().stringWidth(message);
      int textHeight = graphics.getFontMetrics().getHeight();

      graphics.drawString(message, (TILE_SIZE - textWidth) / 2, (TILE_SIZE - textHeight) / 2);
	  return offlineImage;
	}
	
	private static Color loadColor(String colorName, Color defaultColor) {
		Color color = UIManager.getColor(colorName);
		if (color == null) {
			color = defaultColor;
		}
		return color;
	}

}
