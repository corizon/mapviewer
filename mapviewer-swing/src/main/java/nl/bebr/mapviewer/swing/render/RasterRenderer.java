/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.render;

import java.awt.Graphics2D;
import java.awt.Rectangle;


import org.netbeans.api.visual.model.ObjectState;
import org.opengis.coverage.Coverage;

import nl.bebr.mapviewer.api.Palette;

/**
 *
 * @author Timon Veenstra
 * @param <R>
 */
public interface RasterRenderer<R extends Coverage> {

    /**
     * paint the raster
     *
     * @param raster
     * @param viewport
     * @param palette
     * @param graphics
     * @param state
     * @param translator
     */
    void paint(R raster, Rectangle viewport, Graphics2D graphics, Palette palette, ObjectState state, GeoTranslator translator);
}
