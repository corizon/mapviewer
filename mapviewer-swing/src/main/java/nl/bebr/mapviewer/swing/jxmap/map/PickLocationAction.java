/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.jxmap.map;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;


import org.openide.nodes.Node;
import org.openide.util.NbBundle;

import eu.limetri.api.geo.DynamicPoint;
import nl.bebr.mapviewer.data.GeoPosition;
import nl.bebr.mapviewer.swing.JXMapViewer;
import nl.bebr.mapviewer.swing.jxmap.map.Bundle;
import nl.bebr.mapviewer.swing.jxmap.map.Bundle;

/**
 * The action for picking a location for a DynamicGeographical.
 * 
 * @author Wytse Visser
 */
@NbBundle.Messages("pick_location_action_name=Pick location")
public class PickLocationAction extends AbstractAction {

    private final DynamicPoint point;
    private final JXMapViewer mapViewer;

    private PickLocationAction(DynamicPoint point, JXMapViewer mapViewer) {
        putValue(NAME, Bundle.pick_location_action_name());
        this.point = point;
        this.mapViewer = mapViewer;
    }

    /**
     * Checks if the node's lookup contains a DynamicGeometrical. If it does,
     * create and return a PickLocationAction for the scene.
     *
     * @param node The node with (or without) a DynamicGeometrical in its lookup
     * @param scene The scene to create the PickLocationAction for
     * @return The created action (or null of the node's lookup doesn't contain
     * a DynamicGeometrical)
     */
    public static Action create(Node node, MapScene scene) {
        DynamicPoint geometrical = node.getLookup().lookup(DynamicPoint.class);
        if (geometrical != null) {
            return new PickLocationAction(geometrical, scene.getMapViewer());
        }
        return null;
    }

    /**
     * Creates a new PickLocationAction and adds it to the mapViewer
     *
     * @param e The ActionEvent that caused this method to be invoked.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        new LocationPicker(mapViewer).start();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PickLocationAction other = (PickLocationAction) obj;
        if (this.point != other.point && (this.point == null || !this.point.equals(other.point))) {
            return false;
        }
        if (this.mapViewer != other.mapViewer && (this.mapViewer == null || !this.mapViewer.equals(other.mapViewer))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    /**
     * The class can be used for picking a new location for a
     * DynamicGeographical. It should be added to the given JXMapViewer. If the
     * user clicks on the map, it sets the position of the DynamicGeometrical to
     * the selected position. Then (or if the user presses Escape) it removes
     * itself from the JXMapViewer.
     */
    class LocationPicker extends AbstractLocationPicker {

        public LocationPicker(JXMapViewer mapViewer) {
            super(mapViewer);
        }

        @Override
        public boolean select(GeoPosition position) {
            PickLocationAction.this.point.setPosition(position.getLongitude(), position.getLatitude());
            return true;
        }

    }
}
