/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.jxmap.map;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;



import nl.bebr.mapviewer.data.GeoPosition;
import nl.bebr.mapviewer.swing.JXMapViewer;
import nl.bebr.mapviewer.swing.render.JXMapGeoTranslator;
import nl.bebr.mapviewer.swing.render.GeoTranslator;

/**
 * Pick a location on the map.
 * Clicking on the map selects a location, pressing the escape key cancels the action.
 * 
 * @author johan
 */
//TODO FP: I need to consume the event in tme mouseListener. Without that, layer gets selected when I measure something inside.
//but this listener gets fired later than listeners on widgets. so consuming has no efect, because the layer is already
//selected.
public abstract class AbstractLocationPicker implements MouseListener, KeyListener {
    public static final String PROP_ACTIVE = "active";
    private final JXMapViewer mapViewer;
    private final boolean multiple;
    private boolean active = false;
    private PropertyChangeSupport propertyChangeSupport;
    private MouseListener[] mouseListeners;

    public AbstractLocationPicker(JXMapViewer mapViewer) {
        this(mapViewer, false);
    }

    public AbstractLocationPicker(JXMapViewer mapViewer, boolean multiple) {
        this.mapViewer = mapViewer;
        this.multiple = multiple;
    }

    public void start() {
        // TODO: set cursor on topPanel instead of container
        mapViewer.getParent().setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
        
        mouseListeners = mapViewer.getMouseListeners();
        for (MouseListener ml: mouseListeners) {
            mapViewer.removeMouseListener(ml);
        }
        
        mapViewer.addMouseListener(this);
        mapViewer.addKeyListener(this);
        setActive(true);
    }

    /**
        * Removes itself from the map viewer and resets the map viewer's
        * cursor to the default one.
        */
    public void stop() {
        
        for (MouseListener ml: mouseListeners) {
            mapViewer.addMouseListener(ml);
        }
        
        mapViewer.removeMouseListener(this);
        mapViewer.removeKeyListener(this);
        mapViewer.getParent().setCursor(Cursor.getDefaultCursor());
        setActive(false);
    }

    public boolean isActive() {
        return active;
    }
    
    private void setActive(boolean active) {
        boolean oldActive = this.active;
        this.active = active;
        getPropertyChangeSupport().firePropertyChange(PROP_ACTIVE, oldActive, active);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        Point point = new Point(e.getX(), e.getY());
        GeoTranslator translator = new JXMapGeoTranslator(mapViewer);
        GeoPosition position = translator.pixelToGeo(null, null, point);
        e.consume();
        if (select(position) && !multiple) {
            stop();   
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
            cancel();
            stop();
        }
    }

    /**
        * Handle the selected location
        * 
        * @param position
        * @return true if the selected location was accepted
        */
    public abstract boolean select(GeoPosition position);

    /**
        * Optional actions when the location picking is canceled.
        * Do nothing by default.
        */
    public void cancel() {
    }
    
    PropertyChangeSupport getPropertyChangeSupport(){
        if (propertyChangeSupport == null){
            propertyChangeSupport = new PropertyChangeSupport(this);
        }
        return propertyChangeSupport;
    }

    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        getPropertyChangeSupport().addPropertyChangeListener(propertyName, listener);
    }

    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        getPropertyChangeSupport().removePropertyChangeListener(propertyName, listener);
    }

}
