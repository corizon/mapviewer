/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.render;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.Point2D;

import org.netbeans.api.visual.model.ObjectState;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Point;

/**
 * Renders points
 *
 * @author Timon Veenstra
 */
public class PointRenderer extends GeometryRenderer<Point> {

    private Font font = new Font("Dialog", Font.BOLD, 12);
    private double alpha = (100 - (double) 0 /*
             * transparancy
             */) / 100;
    private static int STROKE_WIDTH = 2; // stroke of the rectangle surrounding the label (pixels)
    private static final int POINT_WIDTH_HEIGHT = 4;

    @Override
    public void render(Point geometry, Geometry boundingBox, Rectangle canvas, GeoTranslator translator, ObjectState state, Graphics2D g) {
        Rectangle clientArea = GeometryRenderer.getViewport(geometry, boundingBox, canvas, translator);
        clientArea.setSize(POINT_WIDTH_HEIGHT, POINT_WIDTH_HEIGHT);
        g.setClip(clientArea);

        com.vividsolutions.jts.geom.Point centroid = geometry.getCentroid();
        final Point2D pointOnMap = translator.geoToPixel(canvas, boundingBox, centroid.getCoordinate());


        if (canvas.contains(pointOnMap)) {
            final int x = (int) (pointOnMap.getX() - canvas.x);
            final int y = (int) (pointOnMap.getY() - canvas.y);

            g.setFont(font);
            g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, new Float(alpha)));
            g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            g.setStroke(new BasicStroke(STROKE_WIDTH, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));

            Rectangle rect = new Rectangle(POINT_WIDTH_HEIGHT, POINT_WIDTH_HEIGHT);
            rect.setLocation(x, y);
            g.fill(rect);
        }
    }
}
