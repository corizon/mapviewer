/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.jxmap.map;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;

/**
 * Layout manager which attempts to use the entire available area from the parent.
 * 
 * @author Timon Veenstra
 */
public class FillLayout implements LayoutManager {

    @Override
    public void addLayoutComponent(final String name, final Component comp) {
    }

    @Override
    public void layoutContainer(final Container parent) {
        for (final Component component : parent.getComponents()) {
            component.setBounds(0, 0, parent.getSize().width, parent.getSize().height);
        }
    }

    @Override
    public Dimension minimumLayoutSize(final Container parent) {
        return new Dimension();
    }

    @Override
    public Dimension preferredLayoutSize(final Container parent) {
        return parent.getSize();
    }

    @Override
    public void removeLayoutComponent(final Component comp) {
    }

}
