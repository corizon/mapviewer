/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.render;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JPanel;


import org.geotools.coverage.grid.GridCoverage2D;
import org.netbeans.api.visual.model.ObjectState;

import eu.limetri.api.geo.Geographical;
import nl.bebr.mapviewer.api.Palette;

/**
 * Panel which displays a GridCoverage2D object
 *
 * <p>Uses the {@link RasterRenderer} to render the content on the
 * JPanel</p>
 *
 * @author Timon Veenstra
 * @author Maciek Dulkiewicz
 */
public class GridCoverage2DPreviewPanel extends JPanel {

    private GridCoverage2D coverage;
    private Palette palette;
    private Geographical<GridCoverage2D> geographical;
    // TODO: busy state either via icon or layered panels with progress handle on top of grid
//    private JLayeredPane layeredPane = new JLayeredPane();
//    private JPanel loadingPanel;
//    private JPanel coveragePanel;    
    private PaletteListener paletteListener = new PaletteListener();
    private GeographicalListener geographicalListener = new GeographicalListener();
    
    // TODO: show legend when palette instance of LegendPalette
    // - extract superclass from nl.cloudfarming.client.geoviewer.jxmap.map.LegendPanel,
    // - move coverage painting to nested panel.

    public GridCoverage2DPreviewPanel(GridCoverage2D coverage, Palette palette) {
        this.coverage = coverage;
        this.palette = palette != null ? palette : defaultPalette();
        this.palette.addPropertyChangeListener(paletteListener);
    }

    public GridCoverage2DPreviewPanel(GridCoverage2D coverage) {
        this(coverage, defaultPalette());
    }
    
    public GridCoverage2DPreviewPanel(Geographical<GridCoverage2D> geographical, Palette palette) {
        this(geographical.getRenderObject(null), palette);
        this.geographical = geographical;
        this.geographical.addPropertyChangeListener(geographicalListener);
    }

    @Override
    public void paint(Graphics g) {
        RasterRenderer rasterRenderer = RasterRendererFactory.getRenderer(coverage);
        rasterRenderer.paint(coverage, getVisibleRect(), (Graphics2D) g, palette, ObjectState.createNormal(), new SimpleGeoTranslator());
        
        if (geographical != null && geographical instanceof Geographical.AsyncRenderObject){
            Geographical.AsyncRenderObject asyncRenderObject = (Geographical.AsyncRenderObject) geographical;
            if (asyncRenderObject.hasStaleRenderObject()) {
                // TODO: show hourglass / progress handle
            }
        }
    }
    
    public void setGridCoverage2D(GridCoverage2D gridCoverage) {
        this.coverage = gridCoverage;
        repaint();
    }
    
    private static Palette defaultPalette() {
        return new Palette(Color.BLACK); // renderer will use hardcoded fallback range and colors instead.
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        // clean up listeners:
        palette.removePropertyChangeListener(paletteListener);
        if (geographical != null) {
            geographical.removePropertyChangeListener(geographicalListener);
        }
    }
    
    private class PaletteListener implements PropertyChangeListener {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
           repaint();
        }
    }
    
    private class GeographicalListener implements PropertyChangeListener {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if (Geographical.PROP_RENDER_OBJECT.equals(evt.getPropertyName())) {
                setGridCoverage2D(geographical.getRenderObject(null));
            }
        }
    }
    
}
