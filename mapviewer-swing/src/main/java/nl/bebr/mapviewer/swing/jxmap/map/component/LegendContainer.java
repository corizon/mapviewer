/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.jxmap.map.component;

import nl.bebr.mapviewer.swing.overlay.AbstractOverlayPainter;
import nl.bebr.mapviewer.swing.overlay.LegendProvider;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTabbedPane;
import org.openide.util.Lookup;

/**
 *
 * @author Frantisek Post
 */
public class LegendContainer extends JTabbedPane {
    
    private final List<AbstractOverlayPainter> painters;
    private PropertyChangeListener propertyChangeListener;
    
    public LegendContainer() {
        painters = new ArrayList(Lookup.getDefault().lookupAll(AbstractOverlayPainter.class));
        initialize();
    }
    
    private void initialize() {
        propertyChangeListener = (PropertyChangeEvent evt) -> {
            if (AbstractOverlayPainter.VISIBLE.equals(evt.getPropertyName())) {
                refreshLegend();
            }
        };
        
        painters.stream().forEach((painter) -> {
            painter.addPropertyChangeListener(propertyChangeListener);
        });
        
        refreshLegend();
    }
    
    private void refreshLegend() {
        removeAll();
        boolean someVisible = false;
         for (final AbstractOverlayPainter painter : painters) {
            if (painter instanceof LegendProvider && painter.isVisible()) {
                someVisible = true;
                LegendProvider legendProvider = (LegendProvider) painter;
                addTab(painter.getName(), legendProvider.getLegend());
            }
        }
         
         if (someVisible) {
             setVisible(true);
         } else {
             setVisible(false);
         }
    }
    
}
