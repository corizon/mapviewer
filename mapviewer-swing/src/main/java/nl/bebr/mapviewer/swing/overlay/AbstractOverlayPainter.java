/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.overlay;

import nl.bebr.mapviewer.swing.JXMapViewer;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import org.jdesktop.swingx.painter.Painter;

/**
 *
 * @author Frantisek Post
 */
public abstract class AbstractOverlayPainter implements Painter<JXMapViewer> {
    
    private PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
    
    public static final String ORDER = "order";
    
    public static final String VISIBLE = "visible";
    
    public static final String NAME = "name";
    
    private long order = 0;
    
    private String name;
    
    protected boolean visible;

    protected boolean hasLegend;
    
    public void setOrder(long order) {
        long oldOrder = this.order;
        this.order = order;
        propertyChangeSupport.firePropertyChange(ORDER, oldOrder, order);
    }

    public long getOrder() {
        return order;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        boolean oldVisible = this.visible;
        this.visible = visible;
        propertyChangeSupport.firePropertyChange(VISIBLE, oldVisible, visible);
    }

    public void setName(String name) {
        String oldName = this.name;
        this.name = name;
        propertyChangeSupport.firePropertyChange(NAME, oldName, name);
    }

    public String getName() {
        return name;
    }
    
    protected abstract void processMapEvent(MouseEvent event);
    
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }
    
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }
    
}
