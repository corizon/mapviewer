/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.jxmap.map;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;


import org.openide.nodes.Node;
import org.openide.util.NbBundle;

import nl.bebr.mapviewer.api.SingleObjectLayer;
import nl.bebr.mapviewer.api.event.GeoEvent;
import nl.bebr.mapviewer.swing.jxmap.map.Bundle;
import nl.bebr.mapviewer.swing.jxmap.map.Bundle;

/**
 * Action to split geometries
 * Needs a {@link SingleObjectLayer} in the lookup of the node passed in the constructor
 * @author johan
 */
@NbBundle.Messages("split_geometry_action_name=Split geometry")
public class SplitGeometryAction extends AbstractAction {
    private final Node node;
    
    public SplitGeometryAction(final Node node) {
        this.node = node;
        putValue(NAME, Bundle.split_geometry_action_name());
        SingleObjectLayer layer = node.getLookup().lookup(SingleObjectLayer.class);
        setEnabled(layer.hasGeometry());
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        GeoEvent.getProducer().triggerEvent(GeoEvent.SPLIT_LAYER, node);
    }
}
