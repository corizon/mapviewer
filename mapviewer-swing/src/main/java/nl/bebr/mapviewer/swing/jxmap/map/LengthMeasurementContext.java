/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.jxmap.map;

import nl.bebr.mapviewer.data.GeoPosition;
import nl.bebr.mapviewer.data.util.ScaleUtil;
import nl.bebr.mapviewer.swing.JXMapViewer;
import nl.bebr.mapviewer.swing.render.DrawingRenderer;
import nl.bebr.mapviewer.swing.render.LineDrawingRenderer;
import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import nl.bebr.mapviewer.swing.jxmap.map.Bundle;
import nl.bebr.mapviewer.swing.jxmap.map.Bundle;
import org.openide.util.NbBundle;

/**
 *
 * @author Frantisek Post
 */
@NbBundle.Messages({"length_measurement_result_format=<html>Complete length is: <b> %s</b></html>", 
                     "length_measurement=Area measurement"})
public class LengthMeasurementContext extends AbstractMeasurementContext<GeoPosition> {

    public LengthMeasurementContext(JXMapViewer mapViewer) {
        super(mapViewer);
    }

    @Override
    public DrawingRenderer getRenderer() {
        return new LineDrawingRenderer(coords);
    }

    @Override
    protected JComponent createResultComponent() {
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        JLabel label = new JLabel();

        double length = 0;
        String[] data;

        if (coords.size() > 1) {
            data = new String[coords.size() - 1];

            for (int i = 1; i < coords.size(); i++) {
                float dist = coords.get(i).distanceTo(coords.get(i - 1));
                length += dist;
                data[i - 1] = ScaleUtil.formatLength(dist);
            }
        } else {
            data = new String[]{};
        }
        JList<String> list = new JList<>(data);

        label.setText(String.format(Bundle.length_measurement_result_format(), ScaleUtil.formatLength(length)));

        if (coords.size() > 10) {
            JScrollPane scrollPane = new JScrollPane(list);
            scrollPane.setPreferredSize(new Dimension(200, 180));
            panel.add(scrollPane, BorderLayout.CENTER);
        } else {
            panel.add(list, BorderLayout.CENTER);
        }
        panel.add(label, BorderLayout.SOUTH);
        label.setBorder(new EmptyBorder(10, 8, 0, 0));
        panel.setBorder(new EmptyBorder(2, 2, 10, 2));

        return panel;
    }

    @Override
    public String getDescription() {
        return Bundle.length_measurement();
    }

    @Override
    protected void addPointImpl(GeoPosition position) {
        coords.add(position);
    }
}
