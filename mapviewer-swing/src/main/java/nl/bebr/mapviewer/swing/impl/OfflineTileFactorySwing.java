/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.impl;

import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;


import nl.bebr.mapviewer.data.AbstractTileFactory;
import nl.bebr.mapviewer.data.Tile;
import nl.bebr.mapviewer.data.TileCache;
import nl.bebr.mapviewer.data.TileFactoryInfo;
import nl.bebr.mapviewer.data.cache.OfflineTileFactory;
import nl.bebr.mapviewer.data.cache.spi.TileComparator;

/**
 * Default implementation of {@link OfflineTileFactory} for Swing
 * 
 * @author Frantisek Post
 *
 */
public class OfflineTileFactorySwing extends OfflineTileFactory<BufferedImage> {

	private final BlockingQueue<Tile<BufferedImage>> tileQueue = new PriorityBlockingQueue<Tile<BufferedImage>>(5, new TileComparator<BufferedImage>());
	private final Map<String, Tile<BufferedImage>> tileMap = new HashMap<>();
	
	
	/**
	 * Constructor
	 * 
	 * @param tileFactoryInfo
	 */
	public OfflineTileFactorySwing(TileFactoryInfo tileFactoryInfo) {
		super(tileFactoryInfo);
	}

	@Override
	protected Tile<BufferedImage> createOfflineTile(int tileX, int tileY, int zoom, String url) {
		return new OfflineTileSwing(tileX, tileY, zoom, url, this);
	}

	@Override
	protected Runnable createTileRunner() {
		return new OfflineTileRunnerSwing(this);
	}

	@Override
	public TileCache<BufferedImage> createTileCache() {
		return new TileCacheSwing();
	}


	@Override
	public Tile<BufferedImage> createTile(int x, int y, int zoom, String url, AbstractTileFactory<BufferedImage, Tile<BufferedImage>> tileFactory) {
		return new OfflineTileSwing(x, y, zoom, url, (OfflineTileFactory<BufferedImage>) tileFactory);
	}

	@Override
	public BlockingQueue<Tile<BufferedImage>> getTileQueue() {
		return tileQueue;
	}

	@Override
	protected Map<String, Tile<BufferedImage>> getTileMap() {
		return tileMap;
	}

}
