/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.impl;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;


import nl.bebr.mapviewer.data.cache.OfflineTileFactory;
import nl.bebr.mapviewer.data.cache.OfflineTileRunner;
import nl.bebr.mapviewer.data.cache.TileCacheInfo;
import nl.bebr.mapviewer.data.cache.TileRepository;
import nl.bebr.mapviewer.data.util.GraphicsUtilities;

/**
 * Default implementation of {@link OfflineTileRunner} for Swing
 * 
 * @author Frantisek Post
 *
 */
public class OfflineTileRunnerSwing extends OfflineTileRunner<BufferedImage>{

	/**
	 * Constructor
	 * 
	 * @param tileFactory
	 */
	public OfflineTileRunnerSwing(OfflineTileFactory<BufferedImage> tileFactory) {
		super(tileFactory);
	}

	@Override
	protected BufferedImage readTileFromRepository(TileCacheInfo info) {
		return getTileRepository().getTile(info);
	}

	@Override
	public BufferedImage loadImage(URI uri) throws MalformedURLException, IOException {
		
		byte[] bimg = cacheInputStream(uri.toURL());
		if (bimg != null) {
			BufferedImage img = GraphicsUtilities.loadCompatibleImage(new ByteArrayInputStream(bimg));
			return img;
		} else {
			return null;
		}
	}

	@Override
	public TileRepository<BufferedImage> getTileRepository() {
		return TileRepositorySwing.getInstance();
	}

}
 