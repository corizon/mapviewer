/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JPopupMenu.Separator;
import javax.swing.border.EtchedBorder;


import nl.bebr.mapviewer.data.TileFactoryInfo;
import nl.bebr.mapviewer.data.common.TileFactoryInfoSelectionEventHandler;

public class MapTypeSelectionPanelSwing extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private static final Icon downIcon = new ImageIcon(ZoomPanel.class.getResource("down14.png"));
	
	private JButton button;
	private JLabel label;
	private TileFactoryInfo[] tileFactoryInfos;
	private TileFactoryInfoSelectionEventHandler eventHandler;
	private TileFactoryInfo activeTileFactoryInfo;
	
	public MapTypeSelectionPanelSwing() {
		super();
		init();
	}
	
	private void init() {
		label = new JLabel();
		button = new JButton(downIcon);
		button.setPreferredSize(new Dimension(20,20));
		add(label);
		add(button);
		
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				showPopup();
			}
			
		});
		
		setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
	}
	
	private void showPopup() {
		JPopupMenu menu = new JPopupMenu();
		
		int counter = 0;
		int itemCount = tileFactoryInfos.length-1;
		
		for (final TileFactoryInfo info : tileFactoryInfos) {
			JMenuItem menuItem = new JMenuItem();
			menuItem.setText(info.getName());
			if (info.getIconUrl() != null) {
				menuItem.setIcon(new ImageIcon(info.getIconUrl()));
			}
			menu.add(menuItem);
			
			boolean active = info.equals(activeTileFactoryInfo);
			if (!active) {
				menuItem.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						selectTileInfo(info);
						setActiveTileFactoryInfo(info);
					}

				});
			} else {
				decorateActiveInfoItem(menuItem);
			}
			
			if (counter++ < itemCount) {
				Separator itemSeparator = new Separator();
				itemSeparator.setOrientation(Separator.HORIZONTAL);
				menu.add(itemSeparator);
			}
		}
		
		menu.show(this, 0, 0);
	}
	
	protected void selectTileInfo(TileFactoryInfo info) {
		if (eventHandler != null) {
			eventHandler.itemSelected(info);
		}
	}
	
	protected void decorateActiveInfoItem(JMenuItem menuItem) {
		Font newFont = menuItem.getFont().deriveFont(Font.ITALIC);
		menuItem.setFont(newFont);
	}

	public void setItems(TileFactoryInfo[] tileFactoryInfos) {
		this.tileFactoryInfos = tileFactoryInfos;
	}
	
	public void setOnSelectionChange(TileFactoryInfoSelectionEventHandler eventHandler) {
		this.eventHandler = eventHandler;
	}
	
	public void setActiveTileFactoryInfo(TileFactoryInfo activeTileFactoryInfo) {
		this.activeTileFactoryInfo = activeTileFactoryInfo;
		label.setText(activeTileFactoryInfo.getName());
	}

}
