/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.render;

import java.awt.image.DirectColorModel;
import java.awt.image.Raster;

import nl.bebr.mapviewer.api.Palette;


/**
 *
 * @author Timon Veenstra
 */
public class PaletteColorModel extends DirectColorModel {

    private static final int RED = 0x00ff0000; //    64-32-16-8-4-2-1
    private static final int GREEN = 0x0000ff00;
    private static final int BLUE = 0x000000ff;
    private static final int ALPHA = 0xff000000;
    private final Palette palette;

    //24 = 00000000 00000000 00000000 00000000
    //16 = 0010000
    //8 =  0001000
    // 00110000 << 0011000
    public PaletteColorModel(Palette palette) {
        super(32,
                RED, // Red
                GREEN, // Green
                BLUE, // Blue
                ALPHA // Alpha
                );
        this.palette = palette;
    }


    @Override
    public boolean isCompatibleRaster(Raster raster) {
        return true;
    }

    @Override
    public int getRGB(Object inData) {
        return palette.getColorForValue(inData).getRGB();
    }

}
