/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.jxmap.layerlist;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Component;
import java.awt.Composite;
import java.awt.Container;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.MultipleGradientPaint;
import java.awt.RadialGradientPaint;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.Area;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;

import javax.swing.AbstractButton;
import javax.swing.ButtonModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.border.AbstractBorder;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.metal.MetalButtonUI;

import sun.awt.AppContext;

/**
 *
 * @author Frantisek Post
 */
public class LayerListFoldedButtonUI extends MetalButtonUI {

    private static final Color BACKGROUND_COLOR = new Color(216, 216, 216);
    private static final Color SELECTED_COLOR = new Color(255, 128, 0);
    
    public static ComponentUI createUI(JComponent c) {
        AppContext appContext = AppContext.getAppContext();
        LayerListFoldedButtonUI metalButtonUI = (LayerListFoldedButtonUI) appContext.get("TestButtonUI");
        if (metalButtonUI == null) {
            metalButtonUI = new LayerListFoldedButtonUI();
            appContext.put("TestButtonUI", metalButtonUI);
        }
        return metalButtonUI;
    }

    @Override
    public void installDefaults(AbstractButton b) {
        super.installDefaults(b); //TODO FP better border
        Border border = new RoundCornerBorder();
        b.setBorder(border);
    }

    @Override
    public void update(Graphics g, JComponent c) {
        g.setColor(BACKGROUND_COLOR);
        g.fillRoundRect(2, 2, c.getWidth() - 4, c.getHeight() - 4, 4, 4);
        paint(g, c);
    }


    @Override
    protected void paintIcon(Graphics g, JComponent c, Rectangle iconRect) {

        Graphics2D g2d = (Graphics2D) g.create();
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        ButtonModel model = ((JButton) c).getModel();

        if (!model.isPressed()) {
            Composite oldComp = g2d.getComposite();
            float alpha = 0.9f;
            Composite alphaComp = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha);
            g2d.setComposite(alphaComp);
            int offset = 3;
            GradientPaint cornerGradient;
            
            if (model.isSelected()) {
                cornerGradient = new GradientPaint(0, 0, SELECTED_COLOR, c.getWidth() / 3, c.getHeight() / 2, BACKGROUND_COLOR);
            } else {
                cornerGradient = new GradientPaint(0, 0, Color.WHITE, c.getWidth() / 3, c.getHeight() / 2, new Color(0.9f, 0.9f, 0.9f, 0.4f));
            }
            g2d.setPaint(cornerGradient);

            int[] x = new int[]{offset, (int) (c.getWidth() / 1.1), (int) (c.getWidth() / 3), offset};
            int[] y = new int[]{offset, offset, (int) (c.getHeight() / 5), (int) (c.getHeight() / 1.7)};
            g2d.fillPolygon(x, y, 4);
            g2d.setComposite(oldComp);
        }

        if (model.isRollover() && !model.isPressed()) {
            Point2D center = new Point2D.Float(c.getWidth() / 2, c.getHeight());
            float radius = c.getHeight() / 2;
            Point2D focus = new Point2D.Float(c.getWidth() / 2, c.getHeight() + 5);
            float[] dist = {0.1f, 0.2f, 1.0f};
            Color[] colors = {Color.WHITE, Color.WHITE, new Color(1f, 1f, 1f, 0.01f)};

            RadialGradientPaint p = new RadialGradientPaint(center, radius, focus, dist, colors, MultipleGradientPaint.CycleMethod.NO_CYCLE);
            g2d.setPaint(p);
            g2d.fillRect(0, (int) (c.getHeight() - radius), c.getWidth() - 1, (int) radius);
        }

        super.paintIcon(g, c, iconRect);
    }
    
    
    private static class RoundCornerBorder extends AbstractBorder {

        static final int RADIUS = 8;
        
        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            Graphics2D g2 = (Graphics2D) g.create();
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            RoundRectangle2D outerRound = new RoundRectangle2D.Float(x, y, width - 1, height - 1, RADIUS, RADIUS);
            RoundRectangle2D innerRound = new RoundRectangle2D.Float(x+1, y+1, width - 3, height - 3, RADIUS-1, RADIUS-1);
            Container parent = c.getParent();
            if (parent != null) {
                g2.setColor(parent.getBackground());
                Area corner = new Area(new Rectangle2D.Float(x, y, width, height));
                corner.subtract(new Area(outerRound));
                g2.fill(corner);
            }
            g2.setColor(Color.WHITE);
            g2.draw(innerRound);
            g2.setColor(Color.GRAY);
            g2.draw(outerRound);
            g2.dispose();
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return new Insets(2, 2, 2, 2);
        }

        @Override
        public Insets getBorderInsets(Component c, Insets insets) {
            insets.left = insets.right = 2;
            insets.top = insets.bottom = 2;
            return insets;
        }
    }
    
}
