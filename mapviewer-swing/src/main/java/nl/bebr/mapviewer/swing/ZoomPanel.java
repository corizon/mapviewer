/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing;

import static nl.bebr.mapviewer.swing.JXMapViewer.MAX_ZOOM;
import static nl.bebr.mapviewer.swing.JXMapViewer.MIN_ZOOM;
import static nl.bebr.mapviewer.swing.JXMapViewer.ZOOM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class ZoomPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private static final int BUTTON_SIZE = 20;

	private JButton buttonZoomPlus;

	private JButton buttonZoomMinus;

	private JSlider sliderZoom;

	private JXMapViewer mapViewer;

	private PropertyChangeListener zoomChangeListener;
	
	private static final Icon plusIcon = new ImageIcon(ZoomPanel.class.getResource("plus.png"));
	private static final Icon minusIcon = new ImageIcon(ZoomPanel.class.getResource("minus.png"));

	public ZoomPanel(JXMapViewer mapViewer) {
		super();
		this.mapViewer = mapViewer;
		init();
	}

	
	private void init() {
		setOpaque(false);

		buttonZoomPlus = new JButton(plusIcon);
		buttonZoomMinus = new JButton(minusIcon);

		buttonZoomPlus
				.setPreferredSize(new Dimension(BUTTON_SIZE, BUTTON_SIZE));
		buttonZoomMinus
				.setPreferredSize(new Dimension(BUTTON_SIZE, BUTTON_SIZE));

		sliderZoom = new JSlider(JSlider.VERTICAL, MIN_ZOOM, MAX_ZOOM,	mapViewer.getZoom());
		setLayout(new BorderLayout());
		setBorder(new EmptyBorder(8, 8, 8, 8));
		sliderZoom.setOpaque(false);
		add(buttonZoomPlus, BorderLayout.NORTH);
		add(sliderZoom, BorderLayout.CENTER);
		add(buttonZoomMinus, BorderLayout.SOUTH);

		buttonZoomMinus.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (sliderZoom.getValue() > MIN_ZOOM) {
					sliderZoom.setValue(sliderZoom.getValue() - 1);
				}
			}

		});

		buttonZoomPlus.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (sliderZoom.getValue() < MAX_ZOOM) {
					sliderZoom.setValue(sliderZoom.getValue() + 1);
				}
			}

		});

		sliderZoom.addChangeListener(new ChangeListener() {

			int oldValue = 0;

			@Override
			public void stateChanged(ChangeEvent e) {
				if (sliderZoom.getValue() != oldValue
						&& !mapViewer.isSetZoomFlag()) {
					mapViewer.setZoom((MAX_ZOOM - sliderZoom.getValue()) + 1);
				}
				oldValue = sliderZoom.getValue();
			}

		});
		
		zoomChangeListener = new PropertyChangeListener() {
			
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				sliderZoom.setValue((MAX_ZOOM-mapViewer.getZoom())+1);
			}
			
		};
	}
	
	public void attach() {
		mapViewer.addPropertyChangeListener(ZOOM, zoomChangeListener);
	}
	
	
	public void detach() {
		mapViewer.removePropertyChangeListener(ZOOM, zoomChangeListener);
	}

}
