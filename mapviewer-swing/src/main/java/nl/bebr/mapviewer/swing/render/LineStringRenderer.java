/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.render;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Point2D;

import org.netbeans.api.visual.model.ObjectState;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.LineString;

/**
 * Render a single line string.
 * 
 * @author johan
 */
public class LineStringRenderer extends GeometryRenderer<LineString> {

    @Override
    public void render(LineString geometry, Geometry boundingBox, Rectangle canvas, GeoTranslator translator, ObjectState state, Graphics2D g) {
        int len = geometry.getCoordinates().length;
        if (len > 1) {
            int[] xs = new int[len];
            int[] ys = new int[len];
            for (int i = 0; i < len; i++) {
                Point2D point2D = translator.geoToPixel(canvas, boundingBox, geometry.getCoordinateN(i));
                xs[i] = (int) point2D.getX();
                ys[i] = (int) point2D.getY();
            }
            g.drawPolyline(xs, ys, len);
        }
    }
}
