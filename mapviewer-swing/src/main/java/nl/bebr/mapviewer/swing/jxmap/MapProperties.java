/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.jxmap;

import java.awt.Component;
import java.awt.Font;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;

/**
 * <p>Class which describes which properties can be set for the Map</p> <p> the
 * different properties which can be set are: Layer list in expanded
 * collapsed</br> Widget actions enabled disabled</br> default layer list node
 * actions enabled disabled</br> </p> 
 * <p>
 * Register your implementation of this class as Service provider of this class via @ServiceProvider(service=MapProperties.class)
 * </p>
 *
 * @author Merijn Zengers <Merijn.Zengers@gmail.nl>
 */
public abstract class MapProperties {

    private static final MapProperties implementation  = Lookup.getDefault().lookup(MapProperties.class);
    
    /**
     * Tries to find the property which determines if the layer list should be
     * expanded or collapsed if no property is set will return false
     *
     * @return
     */
    public static boolean getLayerListExpandedValue() {
        if (implementation != null) {
            return implementation.isLayerListExpanded();
        }
        return false;
    }

    /**
     * Tries to find out if the Layer node remove action should be enabled by
     * looking for {@link ServiceProvider} implementations of this class if no
     * implementation is found returns default true
     *
     * @return
     */
    public static boolean getLayerNodeRemoveActionEnabledValue() {
        if (implementation != null) {
            return implementation.isLayerNodeRemoveActionEnabled();
        }
        return true;
    }

    /**
     * Tries to find out if the Layer node settings action should be enabled by
     * looking for {@link ServiceProvider} implementations of this class if no
     * implementation is found returns default true
     *
     * @return
     */
    public static boolean getLayerNodeSettingsActionEnabledValue() {
        if (implementation != null) {
            return implementation.isLayerNodeSettingsActionEnabled();
        }
        return true;
    }

    /**
     * Tries to find out if the Layer node visibility action should be enabled by
     * looking for {@link ServiceProvider} implementations of this class if no
     * implementation is found returns default true
     *
     * @return
     */
    public static boolean getLayerNodevisibilityActionEnabledValue() {
        if (implementation != null) {
            return implementation.isLayerNodeVisibilityActionEnabled();
        }
        return true;
    }
    
    /**
     * Tries to find out if the actions on the widgets should be enabled by looking for {@link ServiceProvider} implementations of this class if no
     * implementation is found returns default true
     * @return 
     */
    public static boolean getWidgetStateActionsEnabledValue(){
        if (implementation != null) {
            return implementation.widgetStateActionsEnabled();
        }
        return true;
    }
    
    /**
     * Tries to find out if the actions available on the node should be available on the widgets by looking for {@link ServiceProvider} implementations of this class if no
     * implementation is found returns default true
     * @return 
     */
    public static boolean getNodeActionsAvailableOnWidgetValue(){
        if (implementation != null) {
            return implementation.nodeActionsAvailableOnWidget();
        }
        return true;
    } 
    
    /**
     * Adds title to popup menu and puts it at the top
     * 
     * @param menu
     * @param title
     * @return 
     */
        
    public static JPopupMenu addTitleToPopup(JPopupMenu menu, String title) {
        JPopupMenu popupMenu = new JPopupMenu();
        JMenuItem titleItem = new JMenuItem(title);
        Font font = titleItem.getFont();
        titleItem.setFont(font.deriveFont(Font.BOLD).deriveFont(font.getSize2D() + 1.5f)); //bold and bigger
        titleItem.setRolloverEnabled(false);
        titleItem.setFocusable(false);
        popupMenu.add(titleItem);
        popupMenu.add(new JPopupMenu.Separator());

        for (Component c : menu.getComponents()) {
            popupMenu.add(c);
        }
        return popupMenu;
    }
    
    /**
     * use {@link MapProperties#isLayerListExpanded() } static method
     * 
     * Should the layer list be expanded
     * @return 
     */
    public abstract boolean isLayerListExpanded();

    /**
     * * {@link MapProperties#isLayerNodeRemoveActionEnabled() } static method
     * 
     * Should the remove action on the layer node in the layer list be enabled
     * @return 
     */
    public abstract boolean isLayerNodeRemoveActionEnabled();

    /**
     * use {@link MapProperties#isLayerNodeSettingsActionEnabled() } static method
     * 
     * Should the settings action on the layer node in the layer list be enabled
     * @return 
     */
    public abstract boolean isLayerNodeSettingsActionEnabled();

    /**
     * 
     *  use {@link MapProperties#isLayerNodeVisibilityActionEnabled() } static method
     * 
     * Should the visibility action on the layer node in the layer list be enabled
     * @return 
     */
    public abstract boolean isLayerNodeVisibilityActionEnabled();
    
    
    /**
     * 
     * use {@link MapProperties#widgetStateActionsEnabled()  } static method
     * 
     * Should the widget select, hover an object hover actions on a widget be enabled
     * @return 
     */
    public abstract boolean widgetStateActionsEnabled();
    
    /**
     * 
     * use {@link MapProperties#nodeActionsAvailableOnWidget()  } static method
     * 
     * Should the actions available on the node from which the widget is created be available on the widget itself
     * @return 
     */
    public abstract boolean nodeActionsAvailableOnWidget();
    
    
}
