/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.jxmap.layerlist;

import static nl.bebr.mapviewer.swing.jxmap.layerlist.LayerListFoldedButton.BUTTON_WIDTH;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingWorker;
import javax.swing.border.MatteBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import nl.bebr.mapviewer.swing.jxmap.layerlist.Bundle;
import nl.bebr.mapviewer.swing.jxmap.layerlist.Bundle;

import org.jdesktop.swingx.JXPanel;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.NodeListModel;
import org.openide.nodes.Node;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

/**
 *
 * @author Frantisek Post
 */
@NbBundle.Messages({"scroll_up_icon=nl/bebr/mapviewer/swing/jxmap/icons/scroll_up10.png",
    "scroll_down_icon=nl/bebr/mapviewer/swing/jxmap/icons/scroll_down10.png"
})
public class LayerListFoldedPanel extends JXPanel {

    private static final Dimension SCROLL_BUTTON_SIZE = new Dimension(BUTTON_WIDTH, 16);
    private ExplorerManager explorerManager;
    private JPanel panel;
    private JButton buttonUp;
    private JButton buttonDown;
    private JScrollPane scrollPane;
    private Icon scrollUpIcon = ImageUtilities.loadImageIcon(Bundle.scroll_up_icon(), true);
    private Icon scrollDownIcon = ImageUtilities.loadImageIcon(Bundle.scroll_down_icon(), true);
    
    public LayerListFoldedPanel(ExplorerManager explorerManager, NodeListModel model) {
        super();
        this.explorerManager = explorerManager;
        init();
        if (model != null) {
              initListener(model);
        }
    }
    
    private void initListener(NodeListModel model) {
        model.addListDataListener(new ListDataListener() {

            @Override
            public void intervalAdded(ListDataEvent e) {
                refresh();
            }

            @Override
            public void intervalRemoved(ListDataEvent e) {
                refresh();
            }

            @Override
            public void contentsChanged(ListDataEvent e) {
                refresh();
            }
            
        });
        
        explorerManager.addPropertyChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (ExplorerManager.PROP_SELECTED_NODES.equals(evt.getPropertyName())) {
                    refreshSelection();
                }
            }
            
        });
        
    } 

    private void init() {
        panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        setLayout(new BorderLayout());
        scrollPane = new JScrollPane(panel);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        add(scrollPane, BorderLayout.CENTER);
        scrollPane.setBorder(new MatteBorder(1,0,1,0, Color.GRAY));

        buttonUp = new JButton(scrollUpIcon);
        buttonDown = new JButton(scrollDownIcon);
        buttonUp.setPreferredSize(SCROLL_BUTTON_SIZE);
        buttonDown.setPreferredSize(SCROLL_BUTTON_SIZE);
        add(buttonUp, BorderLayout.NORTH);
        add(buttonDown, BorderLayout.SOUTH);

        buttonDown.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                scrollDown();
            }
        });

        buttonUp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                scrollUp();
            }
        });

        scrollPane.getViewport().addChangeListener(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {
                setUpScrollButtons();
            }
        });

        setUpScrollButtons();
    }

    protected LayerListFoldedButton createButtonForNode(Node node) {
        return new LayerListFoldedButton(node, explorerManager);
    }

    //API
    
    public void refresh() {
        panel.removeAll();
        Node rootNode = explorerManager.getRootContext();
        if (rootNode != null) {
            Node[] nodes = rootNode.getChildren().getNodes();

            List<LayerListFoldedButton> buttonList = new ArrayList<>();
            for (Node node : nodes) {
                LayerListFoldedButton comp = createButtonForNode(node);
                buttonList.add(comp);
            }

            Collections.sort(buttonList);
            
            for (LayerListFoldedButton comp: buttonList) {
                if (comp.layerInfo != null) {
                    panel.add(comp);
                }
            }
                
            panel.setPreferredSize(new Dimension(BUTTON_WIDTH, BUTTON_WIDTH*nodes.length));
            panel.revalidate();
            panel.doLayout();
            panel.repaint();
        }
        
        refreshSelection();
    }

    private void scrollDown() {
        final JViewport viewport = scrollPane.getViewport();
        Dimension scrollSize = viewport.getSize();
        Dimension viewportSize = viewport.getViewSize();
        final Rectangle rect = ((JComponent) viewport.getView()).getVisibleRect();

        int spaceLeft = viewportSize.height - (scrollSize.height + rect.y);
        int newPosition = rect.y+Math.min(BUTTON_WIDTH, spaceLeft);
        scroll(rect.y, newPosition);
    }

    private void scrollUp() {
        final JViewport viewport = scrollPane.getViewport();
        final Rectangle rect = ((JComponent) viewport.getView()).getVisibleRect();

        int newPosition = 0;
        if (rect.y % BUTTON_WIDTH != 0) {
            newPosition = rect.y - (rect.y % BUTTON_WIDTH);
        } else if (rect.y > 0) {
            newPosition = rect.y - BUTTON_WIDTH;
        }
        scroll(rect.y, newPosition);

    }

    private void scroll(final int from, final int to) {
        final JViewport viewport = scrollPane.getViewport();
        final Rectangle rect = ((JComponent) viewport.getView()).getVisibleRect();

        SwingWorker swingWorker = new SwingWorker() {
            
            static final int BASE_SLEEP_TIME = 10;
            static final long NANO_TO_MILIS = 1000000l;
            static final int SCROLL_STEP = 4;

            @Override
            protected Object doInBackground() throws Exception {
                JComponent view = (JComponent) viewport.getView();
                long start;
                long stop;
                long sleepTime;
                int steps = Math.abs((to - from) / SCROLL_STEP);
                int step = SCROLL_STEP;
                if (to < from) {
                    step = -SCROLL_STEP;
                }
                for (int i = 0; i < steps; i++) {
                    start = System.nanoTime();
                    rect.y = from + (i * step);
                    view.scrollRectToVisible(rect);
                    stop = System.nanoTime();
                    sleepTime = Math.max(BASE_SLEEP_TIME - ((stop - start) / NANO_TO_MILIS), 0);
                    Thread.sleep(sleepTime); //keep fixed frame rate
                }
                rect.y = to;
                view.scrollRectToVisible(rect);

                return null;
            }
        };
        swingWorker.execute();
    }

    private void setUpScrollButtons() {
        JViewport viewport = scrollPane.getViewport();
        Rectangle rect = ((JComponent) viewport.getView()).getVisibleRect();
        Dimension viewSize = viewport.getViewSize();

        if (rect.y == 0) {
            buttonUp.setEnabled(false);
        } else {
            buttonUp.setEnabled(true);
        }

        if (rect.y + rect.height >= viewSize.height) {
            buttonDown.setEnabled(false);
        } else {
            buttonDown.setEnabled(true);
        }

    }

    void refreshSelection() {
        Node[] selectedNodes = explorerManager.getSelectedNodes();
        
        Component[] components = panel.getComponents();
        for (Component component : components) {
            if (component instanceof LayerListFoldedButton) {
                LayerListFoldedButton button = (LayerListFoldedButton) component;
                Node buttonNode = button.getNode();
                
                for (Node node: selectedNodes) {
                    if (node.equals(buttonNode)) {
                        button.setSelected(true);
                    } else {
                        button.setSelected(false);
                    } 
                }
            }
        }
    }
    
}