/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.render;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.util.List;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Polygon;
import java.awt.BasicStroke;
import java.awt.Stroke;

/**
 *
 * @author johan
 */
public class SplitPolygonDrawingRenderer implements DrawingRenderer {

    private final List<Coordinate> coords;
    private final Polygon source;
    private final List<Polygon> results;

    private static final Color COLOR_SPLIT_SECTION_1 = new Color(0, 255, 127, 204); // dark green
    private static final Color COLOR_SPLIT_SECTION_2 = new Color(127, 255, 0, 204); // light green
    private static final Color COLOR_TO_SPLIT = new Color(255, 0, 0, 127); // red

    private static final float BORDER_WIDTH = 4;
    private static final float SPLIT_LINE_WIDTH = 1;
    private static final Stroke BORDER = new BasicStroke(BORDER_WIDTH);
    private static final Stroke SPLIT_LINE = new BasicStroke(SPLIT_LINE_WIDTH);

    /**
     *
     * @param coords drawn splitline coordinates
     * @param source geometry to split
     * @param results split result geometries
     */
    public SplitPolygonDrawingRenderer(List<Coordinate> coords, Polygon source, List<Polygon> results) {
        this.coords = coords;
        this.source = source;
        this.results = results;
    }

    @Override
    public void render(Graphics2D g, Rectangle canvas, GeoTranslator translator) {

        // show either the borders of the split results when available (shades of green),
        // or the source geometry to split (red):
        if (results.size() == 2) {
            renderGeometry(g, results.get(0), COLOR_SPLIT_SECTION_1, canvas, translator);
            renderGeometry(g, results.get(1), COLOR_SPLIT_SECTION_2, canvas, translator);
        } else {
            renderGeometry(g, source, COLOR_TO_SPLIT, canvas, translator);
        }

        // show the splitline:
        if (coords != null && !coords.isEmpty()) {

            int len = coords.size();
            int xs[] = new int[len];
            int ys[] = new int[len];

            // TODO: calculate bounding box, 
            // no problems yet since the currently used translator doesn't need it.
            for (int i = 0; i < len; i++) {
                Point2D point = translator.geoToPixel(canvas, null, coords.get(i));
                xs[i] = (int) (point.getX() - canvas.x);
                ys[i] = (int) (point.getY() - canvas.y);
            }
            if (len >= 2) {
                renderLine(g, xs, ys);
            }
            renderPoints(g, xs, ys);
        }
    }

    //TODO FP copied methods from PolygonDrawingRenederer
    //some AbstractDrawing Renderer could be created
    private void renderPoints(Graphics2D g, int[] xs, int[] ys) {
        int len = xs.length;

        g.setColor(Color.DARK_GRAY);
        for (int i = 0; i < len - 1; i++) {
            g.fillRect(xs[i] - 1, ys[i] - 1, 3, 3);
        }
        // alt. style for last point:
        g.setColor(Color.YELLOW);
        g.fillRect(xs[len - 1] - 1, ys[len - 1] - 1, 3, 3);
    }

    private void renderLine(Graphics2D g, int[] xs, int[] ys) {
        g.setColor(Color.RED);
        g.setStroke(SPLIT_LINE);
        g.drawPolyline(xs, ys, xs.length);
    }

    private void renderGeometry(Graphics2D g, Geometry geometry, Color color, Rectangle canvas, GeoTranslator translator) {
        int len = geometry.getNumPoints();
        if (len > 2) {
            int xs[] = new int[len];
            int ys[] = new int[len];
            for (int i = 0; i < len; i++) {
                Point2D point2D = translator.geoToPixel(canvas, geometry.getEnvelope(), geometry.getCoordinates()[i]);
                xs[i] = (int) (point2D.getX() - canvas.getBounds().getX());
                ys[i] = (int) (point2D.getY() - canvas.getBounds().getY());
            }

            g.setColor(color);
            g.setStroke(BORDER);
            g.drawPolyline(xs, ys, len);

            // show geometry outline points;
            // make them a little larger than the split line points to show where they coincide:
            g.setColor(new Color(255, 255, 255, 191));
            for (int i = 0; i < len; i++) {
                g.fillRect(xs[i] - 2, ys[i] - 2, 5, 5);
            }
        }
    }

}
