/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.render;

import java.awt.Graphics2D;


import nl.bebr.mapviewer.data.Waypoint;
import nl.bebr.mapviewer.swing.JXMapViewer;

/**
 * A interface that draws waypoints. Implementations of WaypointRenderer can
 * be set on a WayPointPainter to draw waypoints on a JXMapViewer
 * @author joshua.marinacci@sun.com
 */
public interface WaypointRenderer {
    /**
     * paint the specified waypoint on the specified map and graphics context
     * @param g 
     * @param map 
     * @param waypoint 
     * @return 
     */
    public boolean paintWaypoint(Graphics2D g, JXMapViewer map, Waypoint waypoint);
    
}
