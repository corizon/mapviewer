/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.render;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;


import nl.bebr.mapviewer.data.Waypoint;
import nl.bebr.mapviewer.swing.JXMapViewer;

/**
 * This is a standard waypoint renderer. It draws all waypoints as blue
 * circles with crosshairs over the waypoint center
 * @author joshy
 */
public class DefaultWaypointRenderer implements WaypointRenderer {
    BufferedImage img = null;
    
    public DefaultWaypointRenderer() {
        try {
            img = ImageIO.read(getClass().getResource("resources/standard_waypoint.png"));
        } catch (Exception ex) {
            ex.printStackTrace(); 
        }
    }
    
    /**
     * {@inheritDoc}
     * @param g
     * @param map
     * @param waypoint
     * @return
     */
    public boolean paintWaypoint(Graphics2D g, JXMapViewer map, Waypoint waypoint) {
        if(img != null) {
            g.drawImage(img,-img.getWidth()/2,-img.getHeight(),null);
        } else {
            g.setStroke(new BasicStroke(3f));
            g.setColor(Color.BLUE);
            g.drawOval(-10,-10,20,20);
            g.setStroke(new BasicStroke(1f));
            g.drawLine(-10,0,10,0);
            g.drawLine(0,-10,0,10);
        }
        return false;
    }
}
