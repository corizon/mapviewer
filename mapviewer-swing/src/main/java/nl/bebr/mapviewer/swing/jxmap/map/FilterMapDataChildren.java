/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.jxmap.map;

import java.util.ArrayList;
import java.util.List;


import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;

import nl.bebr.mapviewer.api.Layer;
import eu.limetri.api.geo.Geographical;

/**
 * Create Filtered children for map. Accepts nodes which have a {@link Geographical} or a {@link Layer} in the lookup
 * @author Timon Veenstra
 */
public class FilterMapDataChildren extends FilterNode.Children{

    public FilterMapDataChildren(Node or) {
        super(or);
    }
    
    @Override
    protected Node[] createNodes(Node object) {
        List<Node> result = new ArrayList<>();
        for (Node node : super.createNodes(object)) {
            if (accept(node)) {
                result.add(new FilterNode(node, new FilterMapDataChildren(node)));
            }
        }
        return result.toArray(new Node[0]);
    }

    /**
     * only accept child nodes which have a Geographical or Layer in their lookup
     * @param node
     * @return 
     */
    private boolean accept(Node node) {
        if  (node.getLookup().lookup(Geographical.class)!=null){
            return true;
        }
        if (node.getLookup().lookup(Layer.class)!=null){
            return true;
        }
        return false;
    }        
}
