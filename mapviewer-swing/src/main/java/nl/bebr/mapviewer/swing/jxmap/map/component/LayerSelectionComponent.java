/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.jxmap.map.component;

import nl.bebr.mapviewer.swing.overlay.AbstractOverlayPainter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JPopupMenu;
import org.openide.util.Lookup;

/**
 * Component for selecting one or more map overlays to be displayed (weather for example)
 * 
 * @author Frantisek Post
 */
public class LayerSelectionComponent extends JButton {
    
    private final List<AbstractOverlayPainter> painters;
    private JPopupMenu menu;
    private static final Icon layerIcon = new ImageIcon(LayerSelectionComponent.class.getResource("application_cascade.png"));
    
    public LayerSelectionComponent() {
        super(layerIcon);
        
        painters = new ArrayList(Lookup.getDefault().lookupAll(AbstractOverlayPainter.class));
        
        Collections.sort(painters, (AbstractOverlayPainter painter1, AbstractOverlayPainter painter2) -> (int) (painter1.getOrder() - painter2.getOrder()));
        
        setText(null);
        setIconTextGap(0);
        initialize();
    }
    
    private void initialize() {
        menu = new JPopupMenu();
        
        painters.stream().map((painter) -> {
            final JCheckBoxMenuItem item = new JCheckBoxMenuItem(painter.getName());
            item.setSelected(painter.isVisible());
            item.addItemListener((ItemEvent e) -> {
                painter.setVisible(item.isSelected());
            });
            return item;
        }).forEach((item) -> {
            menu.add(item);
        });
        
        addActionListener((ActionEvent e) -> {
            menu.show(LayerSelectionComponent.this, 0, LayerSelectionComponent.this.getHeight());
        });
    }
    
}
