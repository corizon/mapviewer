/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.input;

import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

import javax.swing.SwingUtilities;

import nl.bebr.mapviewer.swing.JXMapViewer;

/**
 * Centers the map on the mouse cursor
 * if left is double-clicked or middle mouse
 * button is pressed.
 * @author Martin Steiger
 * @author joshy
 */
public class CenterMapListener extends MouseAdapter
{
	private JXMapViewer viewer;
        private boolean enabled = true;
	
	/**
	 * @param viewer the jxmapviewer
	 */
	public CenterMapListener(JXMapViewer viewer)
	{
		this.viewer = viewer;
	}

	@Override
	public void mousePressed(MouseEvent evt)
	{
            if (enabled) {
		boolean left = SwingUtilities.isLeftMouseButton(evt);
		boolean middle = SwingUtilities.isMiddleMouseButton(evt);
		boolean doubleClick = (evt.getClickCount() == 2);

		if (middle || (left && doubleClick))
		{
			recenterMap(evt);
		}
            }
	}
	
	private void recenterMap(MouseEvent evt)
	{
		Rectangle bounds = viewer.getViewportBounds();
		double x = bounds.getX() + evt.getX();
		double y = bounds.getY() + evt.getY();
		viewer.setCenter(new Point2D.Double(x, y));
                viewer.setZoom(viewer.getZoom() - 1);
		viewer.repaint();
	}
        
        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }
        
        public boolean isEnabled() {
            return enabled;
        }
}


