/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.render;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.util.List;

import com.vividsolutions.jts.geom.Coordinate;

/**
 *
 * @author johan
 */
//TODO FP harcoded colors. What about set colors in LAF initialisation and read them from UIManager? 
public class PolygonDrawingRenderer implements DrawingRenderer {
    private final List<Coordinate> coords;

    public PolygonDrawingRenderer(List<Coordinate> coords) {
        this.coords = coords;
    }
    
    @Override
    public void render(Graphics2D g, Rectangle canvas, GeoTranslator translator) {
            
            if (coords == null || coords.isEmpty()) return;
            
            int len = coords.size();
            int xs[] = new int[len];
            int ys[] = new int[len];
            
            // TODO: calculate bounding box, 
            // no problems yet since the currently used translator doesn't need it.
            
            for (int i = 0; i < len; i++) {
                Point2D point = translator.geoToPixel(canvas, null, coords.get(i));
                xs[i] = (int) (point.getX() - canvas.x);
                ys[i] = (int) (point.getY() - canvas.y);
            }
            if (len >= 3) {
                renderShape(g, xs, ys);
            }
            if (len >= 2) {
                renderEdges(g, xs, ys);          
            }
            renderPoints(g, xs, ys);
        }
        
        private void renderPoints(Graphics2D g, int[] xs, int[]  ys) {
            int len = xs.length;
            
            g.setColor(Color.DARK_GRAY);
            for (int i = 0; i < len - 1; i++) {
                g.fillRect(xs[i] - 1, ys[i] - 1, 3, 3);
            }
            // alt. style for last point:
            g.setColor(Color.YELLOW);
            g.fillRect(xs[len - 1] - 1, ys[len - 1] - 1, 3, 3);
        }
        
        //shape outline except for connection from last to first point.
        private void renderEdges(Graphics2D g, int[] xs, int[]  ys) {
            g.setColor(Color.RED);
            g.drawPolyline(xs, ys, xs.length);
        }
        
        private void renderShape(Graphics2D g, int[] xs, int[]  ys) {
            g.setColor(new Color(255, 255, 255, 127));
            g.fillPolygon(xs, ys, xs.length);
        }    
        
}
