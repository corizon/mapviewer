/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.jxmap.layerlist;

import nl.bebr.mapviewer.api.Layer;
import nl.bebr.mapviewer.api.LayerInfo;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.border.BevelBorder;

import net.miginfocom.swing.MigLayout;

import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.painter.CompoundPainter;
import org.jdesktop.swingx.painter.MattePainter;
import org.jdesktop.swingx.painter.PinstripePainter;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.NodeListModel;
import org.openide.nodes.Node;
import org.openide.nodes.NodeAdapter;
import org.openide.nodes.NodeListener;
import org.openide.nodes.NodeMemberEvent;

/**
 *
 * @author Timon Veenstra
 */
public class LayerListPanel extends JXPanel implements ExplorerManager.Provider {

    //TODO retrieve colors from a branded color scheme
    //private static final Color COLOR_1 = new Color(191, 148, 6);
    private static final Color COLOR_BASE = new Color(225, 225, 225);
    private static final Color COLOR_SHADOW = new Color(150, 150, 150);
    private static final Color COLOR_HIGH = new Color(210, 210, 210);
    private final OrderableListView listView = new OrderableListView();
    private final ExplorerManager explorerManager;
    
    private JXPanel expandedListPanel;
    private LayerListFoldedPanel foldedListPanel;
    private JPanel mainPanel;
    private CardLayout cardLayout;
    private LayerListHeader header;
    
    static final Dimension FULL_DIMENSION = new Dimension(200, 200);
    static final Dimension HIDDEN_DIMENSION = new Dimension(LayerListFoldedButton.BUTTON_WIDTH+5, 202);

    private NodeListener rootNodeListener;
    private PropertyChangeListener reorderListener;
    private final List<LayerInfo> layerInfoList = new ArrayList<>();
    private final List<AbstractLayerListNode> nodeList = new ArrayList<>();
    private final LayerInfoComparator layerInfoComparator = new LayerInfoComparator();
    private boolean recalculating = false;

    public LayerListPanel(final ExplorerManager explorerManager) {
//        listView.setRootVisible(false);
        this.explorerManager = explorerManager;
                
        listView.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        listView.setPreferredSize(new Dimension(200, 200));
        listView.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        
        initComponents();
        initListeners();
    }
    
    private void initListeners() {
        
        reorderListener = (PropertyChangeEvent evt) -> {
            recalculateOrders();
        };
        
        rootNodeListener = new NodeAdapter() {
            @Override
            public void childrenAdded(NodeMemberEvent nme) {
                Node[] nodes = nme.getDelta();

                for (Node node : nodes) {
                    if (node instanceof AbstractLayerListNode) {
                        AbstractLayerListNode allNode = (AbstractLayerListNode) node;
                        LayerInfo info = allNode.getInfo();
                        if (info != null) {
                            info.addPropertyChangeListener(LayerInfo.PROPERTY_ORDER, reorderListener);
                            layerInfoList.add(info);
                        }
                        nodeList.add(allNode);
                    }
                }
                
                recalculateOrders();
            }

            @Override
            public void childrenRemoved(NodeMemberEvent nme) {
               Node[] nodes = nme.getDelta();

                for (Node node : nodes) {
                    if (node instanceof AbstractLayerListNode) {
                        AbstractLayerListNode allNode = (AbstractLayerListNode) node;
                        LayerInfo info = allNode.getInfo();
                        if (info != null) {
                            info.removePropertyChangeListener(LayerInfo.PROPERTY_ORDER, reorderListener);
                            layerInfoList.remove(info);
                        }
                        nodeList.remove(allNode);
                    }
                }
                
                recalculateOrders();
            }
        };
                
         explorerManager.addPropertyChangeListener((PropertyChangeEvent evt) -> {
             if ( ExplorerManager.PROP_ROOT_CONTEXT.equals(evt.getPropertyName())) {
                 Node oldRootNode = (Node) evt.getOldValue();
                 Node newRootNode = (Node) evt.getNewValue();
                 
                 if (oldRootNode != null) {
                     oldRootNode.removeNodeListener(rootNodeListener);
                 }
                 
                 if (newRootNode != null) {
                     newRootNode.addNodeListener(rootNodeListener);
                 }
             }
        });
    }

    private void recalculateOrders() {
        /*
        run on EDT to prevent deadlocks caused by Swing updates via `foldedListPanel.refresh()` and the layerInfo listeners.
        see agrosense-134
        */
        if (SwingUtilities.isEventDispatchThread()) {
            recalculateOrdersImpl();
        } else {
            SwingUtilities.invokeLater(this::recalculateOrdersImpl);
        }
    }
    
    private void recalculateOrdersImpl() {
        if (recalculating) {
            return;
        }
        Collections.sort(layerInfoList, layerInfoComparator);
        
        recalculating = true;
        int layerCounter = 0;
        for (LayerInfo layerInfo : layerInfoList) {
            layerInfo.setOrder(layerCounter);
            layerCounter += 2; //create gaps between
        }
        recalculating = false;
        foldedListPanel.refresh();
    }
    
    private void initComponents() {
        setOpaque(false);
        expandedListPanel = new JXPanel();
        
        expandedListPanel.setLayout(new MigLayout("wrap 1"));
        setupPainters(expandedListPanel);
        expandedListPanel.setBorder(new BevelBorder(BevelBorder.RAISED, COLOR_HIGH, COLOR_SHADOW));
        expandedListPanel.setDoubleBuffered(true);
        expandedListPanel.add(listView, "dock north");
        expandedListPanel.setAlpha(.8f);
        
        mainPanel = new JPanel();
        mainPanel.setOpaque(false);
        mainPanel.setPreferredSize(FULL_DIMENSION);
        setLayout(new BorderLayout());
        add(mainPanel, BorderLayout.CENTER);
        
        header = new LayerListHeader(this);
        add(header, BorderLayout.NORTH);
        
        //AGROSENSE-1150 - can't find other way. need JList model to listen to changes
        Component view = listView.getViewport().getView();
        NodeListModel model = null;
        if (view instanceof JList) {
            JList list = (JList) view;
            if (list.getModel() instanceof NodeListModel) {
                model = (NodeListModel) list.getModel();
            }
        }
        
        foldedListPanel = new LayerListFoldedPanel(explorerManager, model);
        foldedListPanel.setBorder(new BevelBorder(BevelBorder.RAISED, COLOR_HIGH, COLOR_SHADOW));
        
        cardLayout = new CardLayout();
        mainPanel.setLayout(cardLayout);
        
        mainPanel.add(expandedListPanel, "expanded");
        mainPanel.add(foldedListPanel, "folded");
    }
    
    private void setupPainters(JXPanel panel) {
        //Tom doesnt like the gloss :)
//        GlossPainter gloss = new GlossPainter(new Color(1.0f, 1.0f, 1.0f, 0.2f),
//                GlossPainter.GlossPosition.TOP);
        
        PinstripePainter stripes = new PinstripePainter();
        stripes.setPaint(new Color(1.0f, 1.0f, 1.0f, 0.17f));
        stripes.setSpacing(5.0);
        
        MattePainter matte = new MattePainter(COLOR_BASE);

        panel.setBackgroundPainter(new CompoundPainter(matte,stripes)); 
    }
    
    @Override
    public void addNotify() {
        super.addNotify();

        final ExplorerManager em = ExplorerManager.find(this);
        
        em.addPropertyChangeListener((PropertyChangeEvent evt) -> {
            if (ExplorerManager.PROP_SELECTED_NODES.equals(evt.getPropertyName())) {
                ExplorerManager emm = ExplorerManager.find(listView);
                
                Node[] selectedFilterNodes = em.getSelectedNodes();
                Node[] nodes = getNodes(selectedFilterNodes);
                
                try {
                    listView.fireSelectionChange(false);
                    emm.setSelectedNodes(nodes);
                    listView.fireSelectionChange(true);
                    
                } catch (PropertyVetoException ex) {
                    //ignore
                }
            }
        });

    }

    @Override
    public ExplorerManager getExplorerManager() {
        return explorerManager;
    }

    protected void validateComp() {
        Container parent = mainPanel.getParent();

        if (parent != null) {
            if (parent instanceof JComponent) {
                ((JComponent) parent).revalidate();
            } else {
                parent.invalidate();
            }
            parent.doLayout();
            parent.repaint();
        }
    }
	
    public void expand() {
        cardLayout.show(mainPanel, "expanded");
        header.expand();
        mainPanel.setPreferredSize(FULL_DIMENSION);
        setPreferredSize(FULL_DIMENSION);
        validateComp();
    }

    public void collapse() {
        foldedListPanel.refresh(); 
        mainPanel.setPreferredSize(HIDDEN_DIMENSION);
        setPreferredSize(HIDDEN_DIMENSION);
        cardLayout.show(mainPanel, "folded");
        validateComp();
    }
    
    public void hideList() {
        mainPanel.setVisible(false);
    }
    
    public void showList() {
        mainPanel.setVisible(true);
    }
    
    public boolean isListHidden() {
        return !mainPanel.isVisible();
    }
    
    private static class LayerInfoComparator implements Comparator<LayerInfo> {

        @Override
        public int compare(LayerInfo o1, LayerInfo o2) {
            
            if (o1 != null && o2 != null) {
                if (o1.getOrder() > o2.getOrder()) {
                    return 1;
                } else if (o1.getOrder() < o2.getOrder()) {
                    return -1;
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        }
        
    }
    
    private AbstractLayerListNode[] getNodes(Node[] nodes) {
        List<AbstractLayerListNode> matchingNodesList = new ArrayList<>();
        
        for (AbstractLayerListNode alln: nodeList) {
            Layer layer = getLayer(alln);
            
            for (Node node : nodes) {
                if (sameInstance(layer, getLayer(node))) {
                    matchingNodesList.add(alln);
                }
            }
        }
        
        return matchingNodesList.toArray(new AbstractLayerListNode[matchingNodesList.size()]);
    }
    
    public static boolean compareNodes(Node node1, Node node2) {
        return sameInstance(getLayer(node1), getLayer(node2));
    }
    
    private static Layer getLayer(Node node) {
        return node.getLookup().lookup(Layer.class);
    }
    
    private static boolean sameInstance(Object o1, Object o2) {
        return o1 != null && o1 == o2;
    }

}
