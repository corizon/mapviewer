/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.jxmap.map;


import org.openide.awt.ActionID;
import org.openide.awt.ActionRegistration;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.util.actions.NodeAction;

import nl.bebr.mapviewer.api.LayerListRemovableNode;
import nl.bebr.mapviewer.api.event.GeoEvent;
import nl.bebr.mapviewer.swing.jxmap.map.Bundle;
import nl.bebr.mapviewer.swing.jxmap.map.Bundle;

/**
 * <p>Action that can be enabled by placing {@link LayerListRemovableNode} in
 * the {@link  Utilities#actionsGlobalContext()}</p>
 *
 * <p>The {@link Node} which uses this action must implement {@link LayerListRemovableNode}</P>
 *
 * <p> Add the action to your context menu of your node by returning
 * SystemAction.get(RemoveLayerAction.class) in {@link Node#getActions(boolean)}
 * method </p>
 *
 * @author Timon Veenstra & Merijn Zengers
 */
@ActionID(category = "LayerAction",
id = "nl.bebr.mapviewer.swing.util.RemoveLayerAction")
@ActionRegistration(displayName = "#remove_layer.name")
@NbBundle.Messages({"remove_layer.name=Remove"})
public class RemoveLayerAction extends NodeAction {

    /**
     * Will trigger a {@link GeoEvent#DISCARD_LAYER} with parameter {@link LayerListRemovableNode#getRemovableNode()}
     * for every passed {@link Node}
     *
     * @param activatedNodes
     */
    @Override
    protected void performAction(Node[] activatedNodes) {
        for (Node node : activatedNodes) {
            if (node instanceof LayerListRemovableNode) {
                GeoEvent.getProducer().triggerEvent(GeoEvent.DISCARD_LAYER, ((LayerListRemovableNode) node).getRemovableNode());
            }
        }
    }

    /**
     * <p>Checks if this action can be enabled</p> Can only be enabled id and
     * only if all the passed nodes are of type{@link LayerListRemovableNode}
     *
     * @param activatedNodes
     * @return
     */
    //FIXME: Should only be enabled when LayerListRemovableNode 's are int the lookup. 
    //But The widgets on the map also contain nodes and because the layerlist and the map are one top component these are in the lookup as well.
    //Beacause of that is enabled if one of the activated nodes is a LayerListRemovableNode. 
    //This in the relative comfort of knowing only LayerListNode and Filternode from the map are available activatedNodes 
    //FP: changed to enabled if there is some node implementing LayerListRemovableNode interface
    @Override
    protected boolean enable(Node[] activatedNodes) {
        boolean enabled = false;
        for (Node node : activatedNodes) {
            if (node instanceof LayerListRemovableNode) {
                enabled = true;
                break;
            } 
        }
        return enabled;
    }

    /**
     * Get the name of the action
     */
    @Override
    public String getName() {
        return Bundle.remove_layer_name();
    }

    /**
     * No help is provided
     *
     * @return
     */
    @Override
    public HelpCtx getHelpCtx() {
        return null;
    }

    @Override
    protected boolean asynchronous() {
        return false;
    }
    
    
}
