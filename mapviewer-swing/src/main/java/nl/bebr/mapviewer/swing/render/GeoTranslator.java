/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.render;

import java.awt.Rectangle;
import java.awt.geom.Point2D;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;

import nl.bebr.mapviewer.data.GeoPosition;

/**
 * Implementations of this class are able to translate pixels to geo coordinates an visa versa
 * @author Merijn Zengers
 */
public interface GeoTranslator {
    
    /**
     * Translates geo Coordinate to pixel 
     * @param canvas the canvas on which the coordinate is drawn
     * @param boundingBox the bounding box geometry to scale the coordinate to
     * @param coordinate the coordinate to translate to pixels
     * @return Point2D the calculated pixel
     */
    public Point2D geoToPixel(Rectangle canvas, Geometry boundingBox, Coordinate coordinate);
    
    /**
     * Translates pixels to GeoPosition
     * @param canvas the canvas the pixel is on
     * @param boundingBox the bounding box of the geometry
     * @param pixel the pixel to translate
     * @return the calculated GeoPosition
     */
    public GeoPosition pixelToGeo(Rectangle canvas, Geometry boundingBox, Point2D pixel);

    
}
