/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.jxmap.layerlist;

import nl.bebr.mapviewer.api.LayerInfo;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.ListView;
import org.openide.explorer.view.NodeListModel;
import org.openide.explorer.view.NodeRenderer;
import org.openide.explorer.view.Visualizer;
import org.openide.nodes.Node;

/**
 *
 * @author Frantisek Post
 */
public class OrderableListView extends ListView {

    private static final String SELECTED_MARK = "\u2022";
    private boolean fireChange = true;
    
    @Override
    protected NodeListModel createModel() {
        return new OrderableModel(); 
    }

    @Override
    protected JList createList() {
        JList _list =  super.createList(); 
        _list.setCellRenderer(new SelectedRenderer());
        return _list;
    }
    
    @Override
    protected void selectionChanged(Node[] nodes, ExplorerManager em) throws PropertyVetoException {
        if (fireChange) { // we need to stop it, when selecting. otherwise node manager gets notified and infinite loop start 
            super.selectionChanged(nodes, em); 
        }
    }

    void fireSelectionChange(boolean fire) {
        fireChange = fire;
    }
    
    public class OrderableModel extends NodeListModel {
        
        private PropertyChangeListener layerInfoListener = new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                reorder();
                repaint();
            }
            
        };
        
        private Comparator comparator = new Comparator() {

            @Override
            public int compare(Object o1, Object o2) {

                Node node1 = Visualizer.findNode(o1);
                Node node2 = Visualizer.findNode(o2);
                
                if (node1 instanceof AbstractLayerListNode && node2 instanceof AbstractLayerListNode) {
                    AbstractLayerListNode aln1 = (AbstractLayerListNode) node1;
                    AbstractLayerListNode aln2 = (AbstractLayerListNode) node2;
                    int order1 = aln1.getInfo().getOrder();
                    int order2 = aln2.getInfo().getOrder();
                    
                    if (order1 > order2) {
                        return 1;
                    } else if (order1 < order2) {
                        return -1;
                    } else {
                        return 0;
                    }
                    
                } else {
                    return 0;
                }
            }
            
        };
        
        List<Object> mod = new ArrayList<>();

        public OrderableModel() {
            super();
            addListDataListener(new ListDataListener() {

                @Override
                public void intervalAdded(ListDataEvent e) {
                    registerListener(e.getIndex0(), e.getIndex1());
                    reorder();
                    repaint();
                }

                @Override
                public void intervalRemoved(ListDataEvent e) {
                    unregister();
                    reorder();
                    repaint();
                }

                @Override
                public void contentsChanged(ListDataEvent e) {
                   reorder();
                   repaint();
                }
                
            });
            
        }
        
        private void registerListener(int lower, int upper) {
            for (int i = lower; i <= upper; i++) {
                Object el = OrderableModel.super.getElementAt(i);
                Node node = Visualizer.findNode(el);
                if (node instanceof AbstractLayerListNode) {
                    AbstractLayerListNode alln = (AbstractLayerListNode) node;
                    LayerInfo info = alln.getInfo();
                    info.addPropertyChangeListener(LayerInfo.PROPERTY_ORDER, layerInfoListener);
                }
            }
        }
        
        private void unregister() {
            //unregister all, since I don't know which were removed
            for (Object object : mod) {
                Node node = Visualizer.findNode(object);
                if (node instanceof AbstractLayerListNode) {
                    AbstractLayerListNode alln = (AbstractLayerListNode) node;
                    LayerInfo info = alln.getInfo();
                    info.removePropertyChangeListener(LayerInfo.PROPERTY_ORDER, layerInfoListener);
                }
            }
            
            //register all remaining now
            registerListener(0, getSize()-1);
        }
        
        public void reorder() {
            int size = super.getSize();
            mod.clear();
            for (int i = 0; i < size; i++) {
                mod.add(super.getElementAt(i));
            }
            Collections.sort(mod, comparator);
        }

        @Override
        public Object getElementAt(int i) {
            if (i >= mod.size()) {
                reorder();
            }
            return mod.get(i);
        }

        @Override
        public int getIndex(Object o) {
            return mod.indexOf(o);
        }
        
    }

    private class SelectedRenderer extends NodeRenderer {

        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean sel, boolean cellHasFocus) {
            Component component = super.getListCellRendererComponent(list, value, index, sel, cellHasFocus); //To change body of generated methods, choose Tools | Templates.
            
            if (component instanceof JLabel) {
                
                JLabel label = (JLabel) component;
                if (sel) {
                    label.setText(SELECTED_MARK + " " + label.getText());
                }  else {
                    label.setText("  " + label.getText());
                }
            }
            return component;
        }
        
    }
    
}
