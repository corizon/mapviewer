/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.jxmap.layerlist;

import nl.bebr.mapviewer.api.LayerInfo;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;

/**
 * Abstract node with reference to LayerInfo 
 * 
 * @author Frantisek Post
 */
public class AbstractLayerListNode extends FilterNode {
 
    protected LayerInfo info;
    
    public AbstractLayerListNode(Node original, org.openide.nodes.Children children, LayerInfo info) {
        super(original, children);
        
        if (info == null) {
           info = new LayerInfo();
        }
        this.info = info;
    }

    public LayerInfo getInfo() {
        return info;
    }
    
}
