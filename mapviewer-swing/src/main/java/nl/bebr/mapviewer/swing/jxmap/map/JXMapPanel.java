/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.jxmap.map;

import java.awt.Graphics;
import java.awt.geom.Point2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.prefs.Preferences;
import org.openide.util.NbPreferences;
import nl.bebr.mapviewer.data.GeoPosition;
import nl.bebr.mapviewer.data.TileFactoryInfo;
import nl.bebr.mapviewer.data.tilefactory.OSMTileFactoryInfo;
import nl.bebr.mapviewer.data.util.ConnectionChecker;
import nl.bebr.mapviewer.swing.JXMapViewer;
import nl.bebr.mapviewer.swing.impl.OfflineTileFactorySwing;
import nl.bebr.mapviewer.swing.overlay.CompoundOverlayPainter;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


/**
 * <p>Wrapper around the JXMapViewer with some appropriate defaults</p>
 *
 * Take notice!! The zoom function is of {@link JXMapViewer} is disabled. Instead
 * we implemented a custom zoom to mouse position method. Due to non overridable
 * methods implemented new methods Use {@link JXMapPanel#isZoomToMousePositionEnabled()}
 * to determine zoom enabled and use {@link JXMapPanel#setZoomToMousePositionEnabled(boolean)}
 * to set zoom enabled/disabled
 *
 * @author Timon Veenstra
 */
public class JXMapPanel extends JXMapViewer {

    protected static double DEFAULT_LATITUDE = 52.155174;
    protected static double DEFAULT_LONGITUDE = 5.387206;
    protected static int DEFAULT_ZOOM_LEVEL = 11; // Max, any higher then 11 will default to 1!
    private final ConnectionChecker connectionChecker;
    private PropertyChangeListener connectionChangeListener;
    private static final String KEY_LATITUDE = "jxmap.lat";
    private static final String KEY_LONGITUDE = "jxmap.long";
    private static final String KEY_ZOOM = "jxmap.zoomlevel";
    private CompoundOverlayPainter layerPainter;
    
    public JXMapPanel() {
        setOpaque(true);
        setAddressLocation(new GeoPosition(DEFAULT_LATITUDE, DEFAULT_LONGITUDE));
        setZoom(DEFAULT_ZOOM_LEVEL);
        TileFactoryInfo osmInfo = new OSMTileFactoryInfo();
        setTileFactory(new OfflineTileFactorySwing(osmInfo));
        putClientProperty("print.printable", true);
        
        connectionChecker = ConnectionChecker.getInstance();
        initConnectionChangeListener();
        
        layerPainter = new CompoundOverlayPainter();
        setOverlayPainter(layerPainter);
        
//        JPanel panel = new JPanel();
//        panel.setOpaque(false);
//        panel.setLayout(new MigLayout("rtl","130[]","1[]"));
//        panel.add(new LayerSelectionComponent(), "wrap");
//        add(panel);
        
        addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                layerPainter.processEvent(e);
            }

        });
    }

    private void initConnectionChangeListener() {
        connectionChangeListener = (PropertyChangeEvent evt) -> {
            if (connectionChecker.isOnline()) {
                refreshMap();
            }
        };
    }

    /**
     * force JXMapView to repaint when we are online
     */
    private void refreshMap() {
        Graphics g = getGraphics().create();
        g.setClip(0, 0, getWidth(), getHeight());
        paintComponent(g);
    }

    @Override
    public void addNotify() {
        // Set saved coordinates and zoomlevel
        Preferences preferences = NbPreferences.forModule(JXMapPanel.class);
        Double latitude = preferences.getDouble(KEY_LATITUDE, DEFAULT_LATITUDE);  //TODO preferences names to constants?
        Double longitude = preferences.getDouble(KEY_LONGITUDE, DEFAULT_LONGITUDE);
        int zoomLevel = preferences.getInt(KEY_ZOOM, DEFAULT_ZOOM_LEVEL);
        this.setAddressLocation(new GeoPosition(latitude, longitude));
        this.setZoom(zoomLevel);
        connectionChecker.addPropertyChangeListener(connectionChangeListener);
        super.addNotify();
    }

    @Override
    public void removeNotify() {
        // Save coordinates and zoomlevel.
        Preferences preferences = NbPreferences.forModule(JXMapPanel.class);
        preferences.putDouble(KEY_LATITUDE, this.getCenterPosition().getLatitude());
        preferences.putDouble(KEY_LONGITUDE, this.getCenterPosition().getLongitude());
        preferences.putInt(KEY_ZOOM, this.getZoom());
        connectionChecker.removePropertyChangeListener(connectionChangeListener);
        super.removeNotify();
    }

    /**
     * <p>Get the offset between the stable point and the new center point</p>
     * <p>Offset to the right of the stable point is negative to the left is
     * positive</p>
     *
     * @param stablePoint the point that needs to be stable between zoom actions
     * @param centerPoint the center point of the map
     * @param scaleFactorX the scale factor of x. x < 1 means zoom in
     * @param scaleFactorY the scale factor of y. y < 1 means zoom in
     * @return
     */
    Point2D findOffset(Point2D stablePoint, Point2D centerPoint, double scaleFactorX, double scaleFactorY) {
        double x = (stablePoint.getX() - centerPoint.getX()) * scaleFactorX;
        double y = (stablePoint.getY() - centerPoint.getY()) * scaleFactorY;

        return new Point2D.Double(x, y);
    }

}
