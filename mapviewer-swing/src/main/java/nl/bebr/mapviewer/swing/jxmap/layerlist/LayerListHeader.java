/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.jxmap.layerlist;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.border.Border;
import nl.bebr.mapviewer.swing.jxmap.layerlist.Bundle;
import nl.bebr.mapviewer.swing.jxmap.layerlist.Bundle;

import org.jdesktop.swingx.JXPanel;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

/**
 *
 * @author Frantisek Post
 */
@NbBundle.Messages({"fold_icon=nl/bebr/mapviewer/swing/jxmap/icons/minimize14.png",
    "fold_icon_disabled=nl/bebr/mapviewer/swing/jxmap/icons/minimize_disabled14.png",
    "unfold_icon=nl/bebr/mapviewer/swing/jxmap/icons/maximize14.png",
    "unfold_icon_disabled=nl/bebr/mapviewer/swing/jxmap/icons/maximize_disabled14.png",
    "hide_icon=nl/bebr/mapviewer/swing/jxmap/icons/up14.png",
    "show_icon=nl/bebr/mapviewer/swing/jxmap/icons/down14.png",
    "layer_list_header_fold=Fold",
    "layer_list_header_unfold=Unfold",
    "layer_list_header_hide=Hide",
    "layer_list_header_show=Show"})
public class LayerListHeader extends JXPanel {

    private static final Dimension BUTTON_SIZE = new Dimension(20, 20);
    private static final Dimension HEADER_SIZE = new Dimension(44, 20);
    private static final float ALPHA_HIDDEN = 0.4f;
    private static final float ALPHA_FULL = 0.8f;
    
    private LayerListPanel layerListPanel;
    private JToggleButton foldButton;
    private JToggleButton hideButton;
    private JPanel tabPanel;
    
    private Icon foldIcon = ImageUtilities.loadImageIcon(Bundle.fold_icon(), true);
    private Icon foldIconDisabled = ImageUtilities.loadImageIcon(Bundle.fold_icon_disabled(), true);
    private Icon unfoldIcon = ImageUtilities.loadImageIcon(Bundle.unfold_icon(), true);
    private Icon unfoldIconDisabled = ImageUtilities.loadImageIcon(Bundle.unfold_icon_disabled(), true);
    private Icon hideIcon = ImageUtilities.loadImageIcon(Bundle.hide_icon(), true);
    private Icon showIcon = ImageUtilities.loadImageIcon(Bundle.show_icon(), true);
    private  boolean expanded = false;
    
    private MouseListener mouseListener;

    public LayerListHeader(LayerListPanel layerListPanel) {
        this.layerListPanel = layerListPanel;
        initComponents();
        initListeners();
        String f = Bundle.fold_icon();
    }

    private void initComponents() {
        setOpaque(false);
        setLayout(new BorderLayout());

        //TODO FP miglayout
        tabPanel = new JPanel();
        add(tabPanel, BorderLayout.WEST);
        tabPanel.setLayout(new FlowLayout(FlowLayout.TRAILING, 0, 0));

        foldButton = new JToggleButton();
        hideButton = new JToggleButton();
        
        foldButton.setPreferredSize(BUTTON_SIZE);
        foldButton.setContentAreaFilled(false);
        foldButton.setFocusable(false);
        foldButton.setSelectedIcon(foldIcon);
        foldButton.setIcon(unfoldIcon);
        foldButton.setDisabledIcon(unfoldIconDisabled);
        foldButton.setDisabledSelectedIcon(foldIconDisabled);
        foldButton.setToolTipText(Bundle.layer_list_header_unfold());

        hideButton.setPreferredSize(BUTTON_SIZE);
        hideButton.setContentAreaFilled(false);
        hideButton.setFocusable(false);
        hideButton.setIcon(hideIcon);
        hideButton.setSelectedIcon(showIcon);
        hideButton.setToolTipText(Bundle.layer_list_header_hide());

        tabPanel.add(foldButton);
        tabPanel.add(hideButton);
        tabPanel.setMinimumSize(HEADER_SIZE);
        tabPanel.setPreferredSize(HEADER_SIZE);
        tabPanel.setBorder(new HeaderTabBorder(layerListPanel));
        tabPanel.setOpaque(false);
        
        setInheritAlpha(false);
    }
    
    void expand(){
        foldButton.setSelected(true);
        expanded = true;
        foldButton.setToolTipText(Bundle.layer_list_header_fold());
    }
    
    void collapse(){
        foldButton.setSelected(false);
        expanded = false;
        foldButton.setToolTipText(Bundle.layer_list_header_unfold());
    }
    
    private void initListeners() {
        foldButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (expanded) {
                    layerListPanel.collapse();
                    collapse();
                } else {
                    layerListPanel.expand();
                    expand();
                }
            }
        });

        hideButton.addActionListener(new ActionListener() {
            boolean visible = true;

            @Override
            public void actionPerformed(ActionEvent e) {
                if (visible) {
                    layerListPanel.hideList();
                    foldButton.setEnabled(false);
                    visible = false;
                    hideButton.setToolTipText(Bundle.layer_list_header_show());
                } else {
                    layerListPanel.showList();
                    foldButton.setEnabled(true);
                    visible = true;
                    hideButton.setToolTipText(Bundle.layer_list_header_hide());
                }
            }
        });
        
        setAlpha(0.3f);
        mouseListener = new MouseAdapter() {

            @Override
            public void mouseEntered(MouseEvent e) {
                setAlpha(ALPHA_FULL);
                tabPanel.setOpaque(true);
                repaint();
            }

            @Override
            public void mouseExited(MouseEvent e) {
                setAlpha(ALPHA_HIDDEN);
                tabPanel.setOpaque(false);
                repaint();
            }
            
        };
        
        foldButton.addMouseListener(mouseListener);
        hideButton.addMouseListener(mouseListener);
    }

    private static class HeaderTabBorder implements Border {

        private LayerListPanel layerListPanel;
        private static final Color INNER_COLOR = new Color(210, 210, 210);
        private static final Color SHADE_COLOR = new Color(150, 150, 150);
        private static final Color HIGHLIGHT_COLOR = Color.WHITE;
        
        public HeaderTabBorder(LayerListPanel layerListPanel) {
            this.layerListPanel = layerListPanel;
        }
        
        @Override
        public Insets getBorderInsets(Component arg0) {
            return new Insets(0, 0, 0, 2);
        }

        @Override
        public boolean isBorderOpaque() {
            return true;
        }

        @Override
        public void paintBorder(Component component, Graphics g, int x,
                int y, int width, int height) {

            g.setColor(HIGHLIGHT_COLOR);
            g.drawLine(x, y, x + width - 1, y);
            g.drawLine(x, y, x, y + height - 1);
            g.setColor(SHADE_COLOR);
            g.drawLine(x + width - 1, y, x + width - 1, y + height - 1);

            g.setColor(INNER_COLOR);
            g.drawLine(x + 1, y + 1, x + width - 3, y + 1);
            g.drawLine(x + 1, y + 1, x + 1, y + height - 2);
            g.drawLine(x + width - 2, y + 1, x + width - 2, y + height - 2);
            
            if (layerListPanel.isListHidden()) {
                g.setColor(SHADE_COLOR);
                g.drawLine(x, y+height-1, x + width - 1, y+height-1);
                g.setColor(INNER_COLOR);
                g.drawLine(x+1, y+height-2, x + width - 3, y+height-2);
            }
        }
    }
}
