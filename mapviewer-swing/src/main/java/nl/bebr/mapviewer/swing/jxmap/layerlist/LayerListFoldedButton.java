/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.jxmap.layerlist;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateSequence;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.BeanInfo;
import java.beans.PropertyVetoException;

import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JViewport;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;


import org.openide.explorer.ExplorerManager;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Utilities;

import eu.limetri.api.geo.Geographical;
import nl.bebr.mapviewer.api.LayerInfo;
import nl.bebr.mapviewer.api.event.GeoEvent;
import nl.bebr.mapviewer.data.GeoPosition;
import nl.bebr.mapviewer.swing.MapViewerLookup;
import nl.bebr.mapviewer.swing.JXMapViewer;
import nl.bebr.mapviewer.swing.jxmap.MapProperties;

/**
 *
 * @author Frantisek Post
 */
public class LayerListFoldedButton extends JButton implements Comparable {

    public static final int BUTTON_WIDTH = 48;
    public static final Dimension BUTTON_SIZE = new Dimension(BUTTON_WIDTH, BUTTON_WIDTH);
    public static final Insets BUTTON_MARGIN = new Insets(1, 2, 1, 2);
    private Node node;
    private ExplorerManager explorerManager;
    
    protected LayerInfo layerInfo;
    protected final GeometryFactory geometryFactory = new GeometryFactory();

    public LayerListFoldedButton(Node node, ExplorerManager explorerManager) {
        super();
        this.node = node;
        this.explorerManager = explorerManager;
        
        if (node instanceof AbstractLayerListNode) {
            layerInfo = ((AbstractLayerListNode) node).getInfo();
            setSelected(layerInfo.isSelected());
        }

        init();
    }

    private void init() {
        setBorderPainted(true);
        setIconTextGap(0);
        setVerticalTextPosition(SwingConstants.BOTTOM);
        setHorizontalTextPosition(SwingConstants.CENTER);
        setFocusable(false);
        setMargin(BUTTON_MARGIN);
        setMinimumSize(BUTTON_SIZE);
        setMaximumSize(BUTTON_SIZE);
        setRolloverEnabled(true);
        Font font = getFont();
        setFont(font.deriveFont(font.getSize2D() - 1f)); //smaller
        if (node != null) {
            setText(node.getDisplayName());
            setToolTipText(node.getDisplayName());
            setIcon(new ImageIcon(node.getIcon(BeanInfo.ICON_COLOR_32x32)));
        }
        updateUI();

        addMouseListener(new MouseAdapter() {
            private JPopupMenu popup;
            
            @Override
            public void mouseReleased(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    doPopup();
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    doPopup();
                }
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                if (!isPopupShowing() && selectNode()) {
                    // trigger goto action 
                    Geographical geographical = node.getLookup().lookup(Geographical.class);
                    boolean isOnMap = isGeographicalOnMap(geographical);
                    if (geographical != null && !isOnMap){
                        GeoEvent.getProducer().triggerEvent(GeoEvent.REQUEST_FOCUS, geographical);
                    }
                }
            }
            
            private boolean selectNode() {
                try {
                    explorerManager.setSelectedNodes(new Node[]{node});
                } catch (PropertyVetoException ex) {
                    Exceptions.printStackTrace(ex);
                    return false;
                }
                return true;
            }
            
            private boolean isPopupShowing() {
                return popup != null && popup.isShowing();
            }

            private void doPopup() {
                selectNode();
                Action[] actions = node.getActions(true);
                popup = Utilities.actionsToPopup(actions, LayerListFoldedButton.this);
                popup = MapProperties.addTitleToPopup(popup, node.getDisplayName());
                popup.show(LayerListFoldedButton.this, getWidth(), 0);
            }
        });

    }

    private Geometry createScreenGeometry(GeometryFactory geometryFactory) {
        JXMapViewer mapViewer = MapViewerLookup.getInstance().lookup(JXMapViewer.class);
        GeoPosition[] positions = mapViewer.getScreenCoordinates();
        
        Coordinate[] coords = new Coordinate[positions.length];
        for (int i = 0; i < positions.length; i++) {
            GeoPosition geoPosition = positions[i];
            coords[i] = new Coordinate(geoPosition.getLongitude(), geoPosition.getLatitude());
        }
        
        CoordinateSequence coordSequence = new CoordinateArraySequence(coords);
        LinearRing ring = new LinearRing(coordSequence, geometryFactory);
        return new Polygon(ring, null, geometryFactory);
    }
    
    private boolean isGeographicalOnMap(Geographical geographical) {
        Geometry envelope = geographical.getBoundingBox();
        Geometry screen = createScreenGeometry(geometryFactory);
        return envelope != null ? envelope.intersects(screen) : false;
    }
    
    @Override
    public void updateUI() {
        setUI(LayerListFoldedButtonUI.createUI(this));
    }

    @Override
    public int compareTo(Object o) {
        int result = 0;
        if (o instanceof LayerListFoldedButton) {
           LayerListFoldedButton that = (LayerListFoldedButton) o; 
           
           if (this.layerInfo == null || that.layerInfo == null) {
               return 0;
           }
           
           long thatOrder = that.layerInfo.getOrder();
           long thisOrder = this.layerInfo.getOrder();
           if (thatOrder > thisOrder) {
               result = -1;
           } else if (thatOrder < thisOrder) {
               result = 1;
           }
        } 
        
        return result;
    }

    Node getNode() {
        return node;
    }

    @Override
    public void setSelected(boolean b) {
        super.setSelected(b); 

        if (b && getParent() != null) {
            JViewport viewport = (JViewport) SwingUtilities.getAncestorOfClass(JViewport.class, this);
            if (viewport != null) {
                Point buttonLocation = getLocation();
                Rectangle rect = ((JComponent) viewport.getView()).getVisibleRect();

                if (buttonLocation.y < rect.y || (buttonLocation.y + getHeight()) > (rect.y + rect.height)) {
                    scrollRectToVisible(new Rectangle(0, 0, BUTTON_WIDTH, BUTTON_WIDTH));
                }
            }
        }
    }

}
