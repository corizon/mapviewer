/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.input;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

import javax.swing.SwingUtilities;
import javax.swing.event.MouseInputAdapter;

import nl.bebr.mapviewer.swing.JXMapViewer;

/**
 * Used to pan using press and drag mouse gestures
 * @author joshy
 */
public class PanMouseInputListener extends MouseInputAdapter
{
	private Point prev;
	private JXMapViewer viewer;
	private boolean selectingZoomArea = false;
	
	/**
	 * @param viewer the jxmapviewer
	 */
	public PanMouseInputListener(JXMapViewer viewer)
	{
		this.viewer = viewer;
	}

	@Override
	public void mousePressed(MouseEvent evt)
	{
		prev = evt.getPoint();
	}

	@Override
	public void mouseDragged(MouseEvent evt)
	{
		if (!SwingUtilities.isLeftMouseButton(evt))
			return;

		if (evt.isControlDown()) {
			//selecting zoom area
			viewer.markZoomArea(prev, evt.getPoint());
			selectingZoomArea = true;
		} else {
			selectingZoomArea = false;
			Point current = evt.getPoint();
                        
                        if (viewer != null && viewer.getCenter() != null && prev != null) {
                            double x = viewer.getCenter().getX() - (current.x - prev.x);//TODO NPE
                            double y = viewer.getCenter().getY() - (current.y - prev.y);

                            int maxHeight = (int) (viewer.getTileFactory()
                                    .getMapSize(viewer.getZoom()).getHeight() * viewer
                                    .getTileFactory().getTileSize(viewer.getZoom()));
                            if (y > maxHeight) {
                                y = maxHeight;
                            }

                            prev = current;
                            viewer.setCenter(new Point2D.Double(x, y));
                            viewer.repaint();
                            viewer.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
                        }
		}
	}

	@Override
	public void mouseReleased(MouseEvent evt)
	{
		if (!SwingUtilities.isLeftMouseButton(evt))
			return;

		if (evt.isControlDown() || selectingZoomArea) {
			viewer.zoomToArea(prev, evt.getPoint());
		} else {
			prev = null;
			viewer.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}
	}

	@Override
	public void mouseEntered(MouseEvent e)
	{
		SwingUtilities.invokeLater(new Runnable()
		{
			@Override
			public void run()
			{
				viewer.requestFocusInWindow();
			}
		});
	}
}
