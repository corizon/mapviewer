/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.overlay;

import nl.bebr.mapviewer.swing.JXMapViewer;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.jdesktop.swingx.painter.Painter;
import org.openide.util.Lookup;

/**
 *
 * @author Frantisek Post
 */
public class CompoundOverlayPainter implements Painter<JXMapViewer> {
    
    private final List<AbstractOverlayPainter> painters;
    
    private PropertyChangeListener propertyChangeListener;

    public CompoundOverlayPainter() {
        painters = new ArrayList(Lookup.getDefault().lookupAll(AbstractOverlayPainter.class));

        propertyChangeListener = (PropertyChangeEvent evt) -> {
            if (AbstractOverlayPainter.ORDER.equals(evt.getPropertyName())) {
                reorder();
            }
            
            if (AbstractOverlayPainter.ORDER.equals(evt.getPropertyName()) || AbstractOverlayPainter.VISIBLE.equals(evt.getPropertyName())) {
                forceRepaint();
            }
        };
        
        painters.stream().forEach((painter) -> {
            painter.addPropertyChangeListener(propertyChangeListener);
        });
        
        reorder();
    }
    
    private void reorder() {
        Collections.sort(painters, (AbstractOverlayPainter painter1, AbstractOverlayPainter painter2) -> (int) (painter1.getOrder() - painter2.getOrder()));
    }
    
    @Override
    public void paint(Graphics2D g, JXMapViewer viewer, int width, int height) {
        painters.stream().forEach((painter) -> {
            painter.paint(g, viewer, width, height);
        });
    }

    public List<AbstractOverlayPainter> getPainters() {
        return painters;
    }
    
    public void processEvent(MouseEvent mouseEvent) {
        for (AbstractOverlayPainter painter: painters) {
            painter.processMapEvent(mouseEvent);
            if (mouseEvent.isConsumed()) {
                break;
            }
        }
    }
    
    private void forceRepaint() {
        //TODO do repaint
    }
    
}
