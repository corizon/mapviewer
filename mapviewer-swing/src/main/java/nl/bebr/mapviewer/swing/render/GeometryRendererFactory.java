/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.render;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;

/**
 *
 * @author Timon Veenstra
 */
public class GeometryRendererFactory {

    private GeometryRendererFactory(){}
    
    public static GeometryRenderer getRenderer(Object geometry) {
        GeometryRenderer renderer = null;

        if (geometry instanceof Point) {
            renderer = new PointRenderer();
        } 
        else if (geometry instanceof LineString) {
            renderer = new LineStringRenderer();
        } 
        else if (geometry instanceof MultiLineString) {
            renderer = new MultiLineStringRenderer();
        } 
        else if (geometry instanceof Polygon) {
            renderer = new PolygonRenderer();
        } 
        else if (geometry instanceof MultiPolygon) {
            renderer = new MultiPolygonRenderer();
        } 
        else if (geometry instanceof Geometry) {
            renderer = new GeometryRenderer();
        }        
        

        return renderer;
    }
}
