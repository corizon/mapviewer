/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.jxmap.map;


import org.openide.util.NbBundle;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateSequence;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;

import nl.bebr.mapviewer.api.SingleObjectLayer;
import nl.bebr.mapviewer.swing.JXMapViewer;
import nl.bebr.mapviewer.swing.jxmap.map.Bundle;
import nl.bebr.mapviewer.swing.jxmap.map.Bundle;
import nl.bebr.mapviewer.swing.render.DrawingRenderer;
import nl.bebr.mapviewer.swing.render.PolygonDrawingRenderer;

/**
 * Drawing context for polygons
 * @author johan
 */
@NbBundle.Messages({"# {0} - name of the object being edited", "PolygonDrawingContext.description=creating geometry for {0}"})
public class PolygonDrawingContext extends AbstractLineDrawingContext {
    private SingleObjectLayer layer;

    public PolygonDrawingContext(SingleObjectLayer layer, JXMapViewer mapViewer) {
        super(layer, mapViewer);
        this.layer = layer;
    }

    @Override
    public boolean canStart() {
        // no input geometry to validate, we can always start creating a new one:
        return true;
    }
    
    @Override
    public boolean canFinish() {
        // are there enough points to create a polygon?
        if (coords.size()  < 3) {
            return false;
        }
        // check if closing the currently drawn line produces a valid ring:
        return makeRing().isValid();    
    }
    
    /**
     * Create a ring form the currently drawn line by adding a 
     * segment from the last to the first point.
     * 
     * @return 
     */
    private LinearRing makeRing() {
        int len = coords.size() + 1;
        Coordinate[] coordArray = coords.toArray(new Coordinate[len]);
        coordArray[len - 1] = coordArray[0]; // close the ring
        CoordinateSequence coordSequence = new CoordinateArraySequence(coordArray);
        return new LinearRing(coordSequence, geometryFactory);
    }
    
    @Override
    public boolean finish() {
        LinearRing ring = makeRing();
        if (ring.isValid()) {
            // create the polygon
            Polygon polygon = new Polygon(ring, null, geometryFactory);
            layer.setGeometry(polygon);
            // TODO:  save (cookie is in node lookup via dataobject)
            cleanup();
            return true;
        }
        return false;
    }
    
    @Override
    public DrawingRenderer getRenderer() {
        return new PolygonDrawingRenderer(coords);
    }

    @Override
    public String getDescription() {
        return Bundle.PolygonDrawingContext_description(layer.getName());
    }
    
}
