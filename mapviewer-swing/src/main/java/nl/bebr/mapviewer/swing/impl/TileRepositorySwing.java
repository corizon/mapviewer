/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.impl;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.openide.util.Lookup;

import nl.bebr.mapviewer.data.cache.TileRepository;
import nl.bebr.mapviewer.data.cache.TileService;
import nl.bebr.mapviewer.data.cache.WritableTileService;

/**
* Default implementation of {@link TileRepository} for Swing
* 
* @author Frantisek Post
*
*/
public class TileRepositorySwing extends TileRepository<BufferedImage> {

	private static final TileRepositorySwing instance = new TileRepositorySwing();

	private static DefaultTileServiceSwing service;
	
	private TileRepositorySwing() {
	}
	
	
	/**
	 * Gets the instance
	 * 
	 * @return
	 */
	public static TileRepository<BufferedImage> getInstance() {
		return instance;
	}

	
	@SuppressWarnings("rawtypes")
        @Override
	protected List<TileService> findImplementations() {
		Collection<? extends TileService> caches = Lookup.getDefault().lookupAll(TileServiceSwing.class);
		List<TileService> list = new ArrayList<>();
		list.add(getService()); //default first
		list.addAll(caches);
		return list;
	}

	protected WritableTileService<BufferedImage> findWritableImplementation() {
        WritableTileServiceSwing customWritableTileService = Lookup.getDefault().lookup(WritableTileServiceSwing.class); 
        if (customWritableTileService != null) {
        	return customWritableTileService;
        } else {
   			return getService();
        }
	}

	private DefaultTileServiceSwing getService() {
		if (service == null) {
			service = new DefaultTileServiceSwing();
		}
		return service;
	}
	
}
