/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.jxmap.map;

import java.beans.PropertyChangeListener;

import javax.swing.Action;

import nl.bebr.mapviewer.api.Layer;
import nl.bebr.mapviewer.swing.render.DrawingRenderer;


/**
 *
 * @author johan
 */
public interface DrawingContext {
    String PROP_DRAWING_CONTEXT = "drawingContext";

    Layer getLayer();
    
 
    /**
     * Check preconditions before activating the drawing mode.
     * 
     * @return 
     */
    boolean canStart();
    
    /**
     * Check if the points drawn so far can be used to complete the action.
     * e.g. line not self-crossing; a helpline must split another polygon, ...
     * 
     * @return 
     */
    boolean canFinish();

    /**
     * Finish the drawing action.
     * e.g save a new geometry.
     * 
     * @return true if completing the action succeeded
     */
    boolean finish();

    void cancel();

    DrawingRenderer getRenderer();

    /**
     * Create toolbar actions.
     * 
     * @return 
     */
    Action[] getActions();
    
    void addPropertyChangeListener(PropertyChangeListener listener);
    void removePropertyChangeListener(PropertyChangeListener listener);
    
    String getDescription();
    
}
