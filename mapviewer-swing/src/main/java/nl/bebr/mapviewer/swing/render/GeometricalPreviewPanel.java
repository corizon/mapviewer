/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.render;

import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;


import com.vividsolutions.jts.geom.Geometry;

import eu.limetri.api.geo.Geometrical;

/**
 * Default preview panel which shows a preview of a geometrical object
 * 
 * 
 * @author Timon Veenstra <monezz@gmail.com>
 */
public class GeometricalPreviewPanel extends JPanel {

    private final Geometrical geometrical;

    public GeometricalPreviewPanel(Geometrical geometrical) {
        this.geometrical = geometrical;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        //
        // draw the geometry
        //
        Graphics2D gd = (Graphics2D) g.create();
        Geometry geometry = geometrical.getGeometry();

        if (geometry != null && geometry.getCoordinates().length > 1) {
            GeometryRenderer renderer = GeometryRendererFactory.getRenderer(geometry);
            renderer.render(geometry, geometry.getEnvelope(), getVisibleRect(), new SimpleGeoTranslator(), gd);
        }
        gd.dispose();
    }
}
