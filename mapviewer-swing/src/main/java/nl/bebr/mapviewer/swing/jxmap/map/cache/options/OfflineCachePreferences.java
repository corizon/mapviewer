/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.jxmap.map.cache.options;


import org.openide.util.NbPreferences;

import nl.bebr.mapviewer.data.cache.spi.CacheCleaner;

/**
 * 
 * @author Frantisek Post
 */
public enum OfflineCachePreferences {

    CACHE_LIMIT("cacheLimit", CacheCleaner.UNLIMITED_SIZE); //default is unlimited cache
    
    private String propertyName;
    private long defaultProperty;
    
    private OfflineCachePreferences(String propertyName, long defaultProperty) {
        this.propertyName = propertyName;
        this.defaultProperty = defaultProperty;
    }
    
    public void saveValue(long value) {
        NbPreferences.forModule(this.getClass()).putLong(this.getPropertyName(), value);
    }
    
    public long getValue() {
        return NbPreferences.forModule(this.getClass()).getLong(this.getPropertyName(), this.getDefaultProperty());
    }

    public String getPropertyName() {
        return propertyName;
    }

    public long getDefaultProperty() {
        return defaultProperty;
    }
    
}
