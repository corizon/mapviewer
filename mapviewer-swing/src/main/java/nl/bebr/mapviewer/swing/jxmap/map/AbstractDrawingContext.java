/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.jxmap.map;

import nl.bebr.mapviewer.data.GeoPosition;
import nl.bebr.mapviewer.swing.JXMapViewer;
import static nl.bebr.mapviewer.swing.jxmap.map.DrawingContext.PROP_DRAWING_CONTEXT;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import nl.bebr.mapviewer.swing.jxmap.map.Bundle;
import nl.bebr.mapviewer.swing.jxmap.map.Bundle;
import static javax.swing.Action.LARGE_ICON_KEY;
import static javax.swing.Action.SHORT_DESCRIPTION;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

/**
 *
 * @author Frantisek Post
 */
public abstract class AbstractDrawingContext<Coord> implements DrawingContext {
    
    protected final List<Coord> coords = new ArrayList<>();
    private PropertyChangeSupport propertyChangeSupport;
    protected final AbstractLocationPicker locationPicker;
    protected JXMapViewer mapViewer;
    
    public AbstractDrawingContext(JXMapViewer mapViewer) {
        locationPicker = new AbstractLocationPicker(mapViewer, true) {

            @Override
            public boolean select(GeoPosition position) {
                return addPoint(position);
            }
        };
        locationPicker.start();
        this.mapViewer = mapViewer;
    }
    
    protected abstract boolean addPoint(GeoPosition position);
    
    protected void removeLastPoint() {
        if (!coords.isEmpty()) {
            coords.remove(coords.size() - 1);
            pointRemoved();
            getPropertyChangeSupport().firePropertyChange(PROP_DRAWING_CONTEXT, null, null);
        }
    }
    
    protected void removeAllPoints() {
        if (!coords.isEmpty()) {
            coords.clear();
            pointRemoved();
            getPropertyChangeSupport().firePropertyChange(PROP_DRAWING_CONTEXT, null, null);
        }
    }
    
    PropertyChangeSupport getPropertyChangeSupport() {
        if (propertyChangeSupport == null){
            propertyChangeSupport = new PropertyChangeSupport(this);
        }
        return propertyChangeSupport;
    }
    
    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        getPropertyChangeSupport().addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        getPropertyChangeSupport().removePropertyChangeListener(listener);
    }
    
    /*
     * Alow subclasses to perform extra actions after points are added or removed
     * (but before re-rendering), because there might be other objects in the 
     * scene depending on the drawn line.
     */
    protected void pointAdded() {
    }
    
    protected void pointRemoved() {
    }
    
    protected void cleanup() {
        if (locationPicker.isActive()) {
            locationPicker.stop();
        }        
    }

    @Override
    public void cancel() {
        cleanup();
    }
    
    /**
     * Create toolbar actions.
     * 
     * @return 
     */
    @Override
    public Action[] getActions() {
        List<Action> actions = new ArrayList<>();
        // toggle drawing/navigation:
        actions.add(new DrawAction());
        actions.add(new NavigateAction());
        
        actions.add(new RemoveOneAction());
        actions.add(new RemoveAllAction());
        
        return actions.toArray(new Action[0]);
    }
    
    //
    // toolbar actions:
    //

    @NbBundle.Messages({
            "drawing_draw_action_name=Add points",
            "drawing_draw_action_tooltip=Add points",
            "drawing_draw_action_icon=nl/bebr/mapviewer/swing/jxmap/icons/draw_path24.png"})
    private class DrawAction extends AbstractAction {

        public DrawAction() {
            putValue(SHORT_DESCRIPTION, Bundle.drawing_draw_action_tooltip());
            putValue(LARGE_ICON_KEY, ImageUtilities.loadImageIcon(Bundle.drawing_draw_action_icon(), true));
            setEnabled(!locationPicker.isActive());
            locationPicker.addPropertyChangeListener(AbstractLocationPicker.PROP_ACTIVE, new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    setEnabled(!locationPicker.isActive());
                }
            });
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            locationPicker.start();
        }
    }
    
    @NbBundle.Messages({
            "drawing_navigate_action_name=Navigate",
            "drawing_navigate_action_tooltip=Navigate",
            "drawing_navigate_action_icon=nl/bebr/mapviewer/swing/jxmap/icons/map24.png"})
    private class NavigateAction extends AbstractAction {

        public NavigateAction() {
            putValue(SHORT_DESCRIPTION, Bundle.drawing_navigate_action_tooltip());
            putValue(LARGE_ICON_KEY, ImageUtilities.loadImageIcon(Bundle.drawing_navigate_action_icon(), true));
            setEnabled(locationPicker.isActive());
            locationPicker.addPropertyChangeListener(AbstractLocationPicker.PROP_ACTIVE, new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    setEnabled(locationPicker.isActive());
                }
            });
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            locationPicker.stop();
        }
    }

    @NbBundle.Messages({
            "drawing_remove_one_action_name=Remove one",
            "drawing_remove_one_action_tooltip=Remove the last point",
            "drawing_remove_one_action_icon=nl/bebr/mapviewer/swing/jxmap/icons/vector_delete24.png"})
    private class RemoveOneAction extends AbstractAction {

        public RemoveOneAction() {
            putValue(SHORT_DESCRIPTION, Bundle.drawing_remove_one_action_tooltip());
            putValue(LARGE_ICON_KEY, ImageUtilities.loadImageIcon(Bundle.drawing_remove_one_action_icon(), true));
            setEnabled(coords.size() > 0);
            AbstractDrawingContext.this.addPropertyChangeListener(new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    setEnabled(coords.size() > 0);
                }
            });
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            removeLastPoint();
        }
    }
    
    @NbBundle.Messages({
            "drawing_remove_all_action_name=Remove all",
            "drawing_remove_all_action_tooltip=Remove all points",
            "drawing_remove_all_action_icon=nl/bebr/mapviewer/swing/jxmap/icons/delete24.png"})
    private class RemoveAllAction extends AbstractAction {

        public RemoveAllAction() {
            putValue(SHORT_DESCRIPTION, Bundle.drawing_remove_all_action_tooltip());
            putValue(LARGE_ICON_KEY, ImageUtilities.loadImageIcon(Bundle.drawing_remove_all_action_icon(), true));
            setEnabled(coords.size() > 0);
            AbstractDrawingContext.this.addPropertyChangeListener(new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    setEnabled(coords.size() > 0);
                }
            });
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            removeAllPoints();
        }
    }
    
}
