/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.jxmap.map;

import nl.bebr.mapviewer.api.Layer;
import nl.bebr.mapviewer.data.GeoPosition;
import nl.bebr.mapviewer.swing.JXMapViewer;
import static nl.bebr.mapviewer.swing.jxmap.map.DrawingContext.PROP_DRAWING_CONTEXT;
import java.awt.Dialog;
import javax.swing.JComponent;
import nl.bebr.mapviewer.swing.jxmap.map.Bundle;
import nl.bebr.mapviewer.swing.jxmap.map.Bundle;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;

/**
 *
 * @author Frantisek Post
 */
@NbBundle.Messages({"button_continue=Continue", "button_done=Done", "measurement=Measurement"})
public abstract class AbstractMeasurementContext<Coord> extends AbstractDrawingContext<Coord> {

    public AbstractMeasurementContext(JXMapViewer mapViewer) {
        super(mapViewer);
    }
    
    @Override
    protected boolean addPoint(GeoPosition position) {

        if (isValid(position)) {
            addPointImpl(position);
            getPropertyChangeSupport().firePropertyChange(PROP_DRAWING_CONTEXT, null, null);
            return true;
        }

        return false;
    }
    
    protected abstract void addPointImpl(GeoPosition position);

    protected boolean isValid(GeoPosition coord) {
        return true;
    }
    
    @Override
    public Layer getLayer() {
        return null;
    }

    @Override
    public boolean canStart() {
        return true;
    }

    @Override
    public boolean canFinish() {
        return coords.size() > 1;
    }

    protected abstract JComponent createResultComponent();
    
    @Override
    public boolean finish() {
        JComponent component = createResultComponent();
        
        String[] options = new String[] {Bundle.button_continue(), Bundle.button_done()};
        DialogDescriptor dialogDescriptor = new DialogDescriptor(component, Bundle.measurement(), true, options, options[1], DialogDescriptor.BOTTOM_ALIGN, HelpCtx.DEFAULT_HELP, null);
        dialogDescriptor.setClosingOptions(options);
        Dialog dialog = DialogDisplayer.getDefault().createDialog(dialogDescriptor);
        dialog.setVisible(true);
        Object selectedValue = dialogDescriptor.getValue();
        
        boolean canFinish = !options[0].equals(selectedValue);
        if (canFinish) {
            mapViewer.repaint();
            if (locationPicker.isActive()) {
                locationPicker.stop();
            } 
            cleanup();
        }
        
        return canFinish;
    }
    
}
