/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.render;

import java.awt.Point;
import java.awt.Rectangle;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.SwingUtilities;


import org.netbeans.api.visual.widget.Scene;
import org.netbeans.api.visual.widget.Widget;

import eu.limetri.api.geo.Geographical;
import nl.bebr.mapviewer.api.Palette;

/**
 *
 * @author Timon Veenstra <monezz@gmail.com>
 */
public abstract class GeographicalWidget extends Widget {

    private final Geographical geographical;
    Palette palette;
    static final double DISTANCE = 0.00001;

    public GeographicalWidget(Geographical geographical, Palette palette, Scene scene) {
        super(scene);
        this.palette = palette;
        this.geographical = geographical;

        geographical.addPropertyChangeListener(new WidgetRefresher());
        setForeground(this.palette.getColorForState(getState()));
    }

    public Palette getPalette() {
        return palette;
    }

    public void setPalette(Palette palette) {
        this.palette = palette;
        setForeground(this.palette.getColorForState(getState()));
        repaint();
    }

    /**
     * Determines if a widget isHit. A widget is hit when a point lays between
     * the bounds of the widget and: - The localLocation intersects the widgets
     * GeoPostition - the localLocation is within a distance of DISTANCE of
     * GeoPosition
     *
     * @param localLocation the Point which possibly hits the widget
     * @return if the widget is hit
     */
    @Override
    public boolean isHitAt(Point localLocation) {
        if (isVisible() && getBounds().contains(localLocation)) {
            //FIXME check based on envelope
            return true;
        } else {
            return false;
        }
    }
    
    protected Geographical getGeographical() {
        return geographical;
    }
    
    protected Rectangle getSceneViewport() {
        // FIXME: points are still rendered on top of the map window border, though not outside of the map window anymore (AGROSENSE-1479);
        // could be caused by the points render width (extends 4px down and left from its location),
        // or we may need to subtract the map window border insets (may need to go all the way up the component tree to the MapTopComponent)
        return getScene().getView().getBounds();
    }

    private class WidgetRefresher implements PropertyChangeListener {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if (SwingUtilities.isEventDispatchThread()) {
                getScene().getView().repaint();
            } else {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        getScene().getView().repaint();
                    }
                });
            }
        }
    }
}
