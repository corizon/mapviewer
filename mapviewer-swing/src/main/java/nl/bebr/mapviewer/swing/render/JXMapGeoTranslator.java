/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.render;

import java.awt.Rectangle;
import java.awt.geom.Point2D;



import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;

import nl.bebr.mapviewer.data.GeoPosition;
import nl.bebr.mapviewer.swing.JXMapViewer;
import nl.bebr.mapviewer.swing.render.GeoTranslator;


/**
 * JXMap implementation to translate geo to pixel and visa versa
 * @author Merijn Zengers
 */
public class JXMapGeoTranslator implements GeoTranslator{

    private final JXMapViewer mapViewer;

    public JXMapGeoTranslator(JXMapViewer mapViewer) {
        this.mapViewer = mapViewer;
    }
    
    
    /**
     * Translate Coordinate to pixel
     * @param canvas mapviewer is used instead, should be null
     * @param boundingBox mapviewer is used instead, should be null
     * @param coordinate coordinate to translate for current map position
     * @return the translated pixel
     */
    @Override
    public Point2D geoToPixel(Rectangle canvas, Geometry boundingBox, Coordinate coordinate) {
        GeoPosition geopoint = new GeoPosition(coordinate.y, coordinate.x);
        //convert from geo to world bitmap
        Point2D pt = mapViewer.getTileFactory().geoToPixel(geopoint, mapViewer.getZoom());
        //convert from world bitmap to local 
        Rectangle bounds = mapViewer.getViewportBounds();
        return new Point2D.Double(pt.getX() - bounds.getX(),pt.getY() - bounds.getY());
    }

    /**
     * Translate pixel to geoPosition
     * @param canvas mapviewer is used instead, should be null
     * @param boundingBox mapviewer is used instead, should be null
     * @param pixel the pixel to transform
     * @return the translated GeoPosition
     */
    @Override
    public GeoPosition pixelToGeo(Rectangle canvas, Geometry boundingBox, Point2D pixel) {
        Rectangle viewportBounds = mapViewer.getViewportBounds();
        int mapX = (int) (viewportBounds.getX() + pixel.getX());
        int mapY = (int) (viewportBounds.getY() + pixel.getY());
        Point2D newPoint = new java.awt.Point(mapX, mapY);
        GeoPosition geoPosition = mapViewer.getTileFactory().pixelToGeo(newPoint, mapViewer.getZoom());
        return geoPosition;
    }
    
}
