/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.render;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Point2D;




import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;


import nl.bebr.mapviewer.data.GeoPosition;
import nl.bebr.nblib.geotools.GeometryTools;

/**
 * Implementation that transform pixels to geo and visa versa
 * 
 * @author Merijn Zengers
 */
//TODO FP could be static? injected/found via lookup?
public class SimpleGeoTranslator implements GeoTranslator {

    /**
     * Translates a Coordinate to a pixel
     *
     * @param canvas Rectangle to be painted on, usually something like
     * JPanel.getVisibleRectangle()
     * @param boundingBox Bounding box of the entire geometry
     * @param coordinate Coordinate to translate
     * @return the translated point
     */    
    @Override
    public Point2D geoToPixel(Rectangle canvas, Geometry boundingBox, Coordinate coordinate) {
        assert boundingBox.getCoordinates().length >= 4 : "Boundingbox should always have at least 4 coordinates instead of " + boundingBox.getCoordinates().length;
        
        double geoMinX = boundingBox.getCoordinates()[0].x;
        double geoMaxX = boundingBox.getCoordinates()[2].x;

        double geoMinY = boundingBox.getCoordinates()[0].y;
        double geoMaxY = boundingBox.getCoordinates()[2].y;

        double geoWidth = GeometryTools.getDistance(geoMinY, geoMinX, geoMinY, geoMaxX);
        double geoHeight = GeometryTools.getDistance(geoMinY, geoMinX, geoMaxY, geoMinX);

        double geoRatio = geoWidth / geoHeight;
        double canvasRatio = new Integer(canvas.width).doubleValue() / new Integer(canvas.height).doubleValue();

        // if geo ratio is smaller then canvas ration, take y factor as leading factor
        double factor;
        double heightDifference =0.0;
        
        if(geoRatio <= canvasRatio){ // Georatio is smaller then canvasratio
            factor = (double) canvas.height / geoHeight;
        }
        else{ // Georatio is bigger then canvasratio
            factor = (double) canvas.width / geoWidth;
            // Using width factor this means a part of the height of the canvas is not used.
            // Need to calculate how much to use for the y axis conversion
            heightDifference = canvas.height - geoHeight * factor;
        }
        double distanceX = GeometryTools.getDistance(coordinate.y, geoMinX, coordinate.y, coordinate.x);
        int x = new Double(distanceX * factor).intValue();
        // need to substract from height because axes run in different direction
        double distanceY = GeometryTools.getDistance(geoMinY, coordinate.x, coordinate.y, coordinate.x);
        int y = new Double(((double) canvas.height - heightDifference) - (distanceY * factor)).intValue();
        
        return new Point(x, y);
    }

    /**
     * Converts pixel to GeoPosition
     * Does this based on the scale of the canvas and bounding box
     * @param canvas the canvas the pixel is currently on
     * @param boundingBox the bounding box of the geometry to place the pixel in 
     * @param pixel the pixel itself
     * @return The calculated GeoPosition
     */
    @Override
    public GeoPosition pixelToGeo(Rectangle canvas, Geometry boundingBox, Point2D pixel) {
        double geoMinX = boundingBox.getCoordinates()[0].x;
        double geoMaxX = boundingBox.getCoordinates()[2].x;

        double geoWidth = geoMaxX - geoMinX;

        double geoMinY = boundingBox.getCoordinates()[0].y;
        double geoMaxY = boundingBox.getCoordinates()[2].y;
        double geoHeight = geoMaxY - geoMinY;
        
        double geoRatio = geoWidth / geoHeight;
        double canvasRatio = new Double(canvas.width) / new Double(canvas.height);
        
        // if geo ratio is smaller then canvas ration, take y factor as leading factor
        double factor = (geoRatio <= canvasRatio) ? (double) canvas.height / geoHeight : (double) canvas.width / geoWidth;
        
        double mapX = geoMinX + (pixel.getX() / factor); 

        // geoToPixel does y = z-x where z is canvas height
        // use inverse here to calculate: x = -(y-z)
        double mapY = geoMinY + ((pixel.getY()-(double) canvas.height)/-factor);
        
        
        GeoPosition geoPosition = new GeoPosition(mapY, mapX);
        return geoPosition;
    }
    
    
    
}
