/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing;

import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

/**
 *
 * @author Frantisek Post
 */
public class MapViewerLookup {
    
    private InstanceContent instanceContent = new InstanceContent();
    private Lookup lookup = new AbstractLookup(instanceContent);
    
    private static final MapViewerLookup instance = new MapViewerLookup();
    
    private MapViewerLookup(){
    }
    
    public void addObject(Object object) {
        instanceContent.add(object);
    }
    
    public void removeObject(Object object) {
        instanceContent.remove(object);
    }
    
    public <T> T lookup(Class<T> lookupClass) {
        return lookup.lookup(lookupClass);
    }

    public static MapViewerLookup getInstance() {
        return instance;
    }
}
