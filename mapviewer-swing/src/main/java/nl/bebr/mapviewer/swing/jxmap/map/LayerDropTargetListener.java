/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.jxmap.map;

import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


import org.openide.awt.StatusDisplayer;
import org.openide.nodes.Node;
import org.openide.nodes.NodeTransfer;

import nl.bebr.mapviewer.api.Layer;
import nl.bebr.mapviewer.api.LayerDropTarget;

/**
 * DropTargetListener implementation which accepts nodes with 
 * layers in their lookup  and delegates the actual dropping to
 * all registered {@link DropTarget}s
 * 
 * 
 * @author Timon Veenstra
 */
public class LayerDropTargetListener implements DropTargetListener {

    private final List<LayerDropTarget> targets = Collections.synchronizedList(new ArrayList<LayerDropTarget>());
    private static final Logger LOGGER = Logger.getLogger(LayerDropTargetListener.class.getCanonicalName());

    public LayerDropTargetListener() {
    }
    
    public LayerDropTargetListener(LayerDropTarget... droptargets) {
        targets.addAll(Arrays.asList(droptargets));
    }
    
    /**
     * register a new dropTarget which the node dropping gets delegated to
     * 
     * @param dropTarget 
     */
    public void addDropTarget(LayerDropTarget dropTarget){
        targets.add(dropTarget);
    }
    
    public void removeDropTarget(LayerDropTarget dropTarget){
        targets.remove(dropTarget);
    }
   
    @Override
    public void dragEnter(DropTargetDragEvent dtde) {
    }

    @Override
    public void dragExit(DropTargetEvent dtde) {
    }

    @Override
    public void dragOver(DropTargetDragEvent dtde) {
    }

    @Override
    public void dropActionChanged(DropTargetDragEvent dtde) {
    }

    /**
     * Do the actual dropping, drop the node on all registered targets
     * 
     * @param node
     * @return 
     */
    private boolean dropNode(Node node) {
        //TODO verify if we can accept all nodes and just search for 
        // layers. This would allow to drag a root node and add multiple 
        // layers at once. This would also require from refactoring in the 
        // filter nodes
        LOGGER.finest("Incoming dropped node received on the map");
        boolean atLeastOneTargetAccepted = false;
        Layer layer = node.getLookup().lookup(Layer.class);
        if (layer != null) {
            
            StatusDisplayer.getDefault().setStatusText(node.getName());
            for (LayerDropTarget dropTarget:targets){
                boolean accepted = dropTarget.dropNode(node);
                atLeastOneTargetAccepted = atLeastOneTargetAccepted?true:accepted;
            }
        }else{
            LOGGER.log(Level.WARNING, "dropped node {0} did not contain a layer", node.toString());
        }
        return atLeastOneTargetAccepted;
    }

    @Override
    public void drop(DropTargetDropEvent dtde) {
        //
        // handle drop of single nodes
        // (handle single first since multiple also handles single nodes, but not vice versa)
        //
        Node node = NodeTransfer.node(dtde.getTransferable(), NodeTransfer.DND_COPY);
        if (node != null) {
            if (dropNode(node)) {
                dtde.acceptDrop(DnDConstants.ACTION_COPY);
                return;
            }
        }
        //
        // handle drop of multiple nodes
        //
        Node[] nodes = NodeTransfer.nodes(dtde.getTransferable(), NodeTransfer.DND_COPY);
        if (nodes != null) {
//            LOGGER.log(Level.FINEST, "{0} nodes dropped", nodes.length);
            for (Node n : nodes) {
                dropNode(n);
            }
            dtde.acceptDrop(DnDConstants.ACTION_COPY);
            return;
        }
        //
        // unsupported drop, reject
        //
        dtde.rejectDrop();
        dtde.dropComplete(false);
    }
}
