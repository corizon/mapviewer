/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.jxmap.map;

import nl.bebr.mapviewer.swing.jxmap.map.FillLayout;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import nl.bebr.mapviewer.swing.jxmap.SceneSupport;
import org.jdesktop.swingx.JXPanel;
import org.netbeans.api.visual.action.ActionFactory;
import org.netbeans.api.visual.action.PopupMenuProvider;
import org.netbeans.api.visual.action.TwoStateHoverProvider;
import org.netbeans.api.visual.action.WidgetAction;
import org.netbeans.api.visual.widget.LabelWidget;
import org.netbeans.api.visual.widget.LayerWidget;
import org.netbeans.api.visual.widget.Scene;
import org.netbeans.api.visual.widget.Widget;
import org.openide.awt.MouseUtils.PopupMouseAdapter;


/**
 *
 * @author Timon Veenstra
 */
public class SceneDisplayer2 {

    private static final Logger LOGGER = Logger.getAnonymousLogger();
    
    static{
        LOGGER.setLevel(Level.ALL);
//        LOGGER.addHandler(SystemLogController.getInstance());
    }    
    
    static {
        System.setProperty("awt.toolkit", "sun.awt.X11.XToolkit");
    }

    public static void main(String[] args) {
        final JXPanel root = new JXPanel(new BorderLayout());
        final JLayeredPane layeredPane;
        final JXPanel panel = new JXPanel();

        layeredPane = new JLayeredPane();
        layeredPane.setLayout(new FillLayout());
        root.add(layeredPane, BorderLayout.CENTER);

        Scene scene = new Scene();

        LayerWidget layer = new LayerWidget(scene);
        scene.addChild(layer);

        LabelWidget hello1 = createLabel(scene, "Hello", 100, 100);
        layer.addChild(hello1);
        LabelWidget hello2 = createLabel(scene, "NetBeans", 300, 200);
        layer.addChild(hello2);

        scene.getActions().addAction(ActionFactory.createZoomAction());
        scene.getActions().addAction(ActionFactory.createPanAction());

        hello1.getActions().addAction(ActionFactory.createMoveAction());
        hello2.getActions().addAction(ActionFactory.createMoveAction());

        WidgetAction hoverAction = ActionFactory.createHoverAction(new MyHoverProvider());
        scene.getActions().addAction(hoverAction);
        hello1.getActions().addAction(hoverAction);
        hello2.getActions().addAction(hoverAction);

        WidgetAction popupMenuAction = ActionFactory.createPopupMenuAction(new MyPopupProvider());
        hello1.getActions().addAction(popupMenuAction);
        hello2.getActions().addAction(popupMenuAction);

        final JComponent view = scene.createView();
        panel.add(view, BorderLayout.CENTER);



        MouseAdapter ma = new PopupMouseAdapter() {

            @Override
            protected void showPopup(MouseEvent evt) {
                JPopupMenu m = new JPopupMenu("popupje");
                m.add(new JMenuItem("la die da"));
                m.setVisible(true);
                evt.consume();
                m.show(view, evt.getX(), evt.getY());
            }
        };

        view.addMouseListener(ma);


        layeredPane.add(panel, JLayeredPane.DEFAULT_LAYER);
        
        JXPanel top = new JXPanel();
        top.setAlpha(0.5f);
        top.add(new JLabel("im at the top"));
        
        layeredPane.add(top,JLayeredPane.MODAL_LAYER);


        LOGGER.finest("SceneSupport.show(root);");
        SceneSupport.show(root);
    }

    private static LabelWidget createLabel(Scene scene, String text, int x, int y) {
        LabelWidget widget = new LabelWidget(scene, text);
        widget.setFont(scene.getDefaultFont().deriveFont(24.0f));
        widget.setOpaque(true);
        widget.setPreferredLocation(new Point(x, y));
        return widget;
    }

    private static class MyHoverProvider implements TwoStateHoverProvider {

        @Override
        public void unsetHovering(Widget widget) {
            if (widget != null) {
                widget.setBackground(Color.WHITE);
                widget.setForeground(Color.BLACK);
            }
        }

        @Override
        public void setHovering(Widget widget) {
            if (widget != null) {
                widget.setBackground(new Color(52, 124, 150));
                widget.setForeground(Color.WHITE);
            }
        }
    }

    private static class MyPopupProvider implements PopupMenuProvider {

        @Override
        public JPopupMenu getPopupMenu(Widget widget, Point localLocation) {
            JPopupMenu menu = new JPopupMenu();
            menu.add("Open");
            return menu;
        }
    }
}
