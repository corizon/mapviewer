/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.jxmap.render;

import java.awt.Rectangle;
import org.junit.Before;

import nl.bebr.mapviewer.data.TileFactoryInfo;
import nl.bebr.mapviewer.data.cache.OfflineTileFactory;
import nl.bebr.mapviewer.swing.JXMapViewer;
import nl.bebr.mapviewer.swing.impl.OfflineTileFactorySwing;
import static org.mockito.Mockito.*;



/**
 *
 * @author Timon Veenstra <monezz@gmail.com>
 */
public abstract class MapViewerInitializer {
    
    protected JXMapViewer mapViewer;
    public static final int VIEWPORT_X = 8570764;
    public static final int VIEWPORT_Y = 5577054;

    @Before
    public void setUp() {
        // Mapviewer
        mapViewer = mock(JXMapViewer.class);
        Rectangle viewportBounds = new Rectangle(VIEWPORT_X, VIEWPORT_Y, 597, 527);

        // TODO: Mock?!?  Tilefactory creation copied from JXMapPanel. 
        final int max = 19;
        TileFactoryInfo tileFactoryInfo = new TileFactoryInfo(1, max - 8, max,
                256, true, true,
                "https://tile.openstreetmap.org",
                "x", "y", "z") {

            @Override
            public String getTileUrl(int x, int y, int zoom) {
                int calculatedZoom = max - zoom;
                return this.baseURL + "/" + calculatedZoom + "/" + x + "/" + y + ".png";
            }
        };
        when(mapViewer.getTileFactory()).thenReturn(new OfflineTileFactorySwing(tileFactoryInfo));
        when(mapViewer.getViewportBounds()).thenReturn(viewportBounds);
        when(mapViewer.getZoom()).thenReturn(3);
    }    
}
