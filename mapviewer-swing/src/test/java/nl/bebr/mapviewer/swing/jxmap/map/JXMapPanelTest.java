/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.jxmap.map;

import nl.bebr.mapviewer.swing.jxmap.map.JXMapPanel;
import java.awt.geom.Point2D;
import static org.junit.Assert.*;
import org.junit.Test;

import nl.bebr.mapviewer.data.GeoPosition;

/**
 *
 * @author Timon Veenstra
 */
public class JXMapPanelTest {

//    @Test
    public void testZoom() {
        JXMapPanel mapPanel = new JXMapPanel();
        // get defaults
        mapPanel.addNotify();
        // test for default zoom level
        assertEquals(JXMapPanel.DEFAULT_ZOOM_LEVEL, mapPanel.getZoom());
        // set new zoom value
        mapPanel.setZoom(5);
        // remove notify, settings are stored
        mapPanel.removeNotify();
        // change zoom level ( should have no effect)
        mapPanel.setZoom(6);
        // restore settings
        mapPanel.addNotify();
        // zoom should be back to 5
        assertEquals(5, mapPanel.getZoom());
    }

    @Test
    public void testLocation() {
        GeoPosition testPosition1 = new GeoPosition(1.0, 2.0);
        GeoPosition testPosition2 = new GeoPosition(3.0, 4.0);
        JXMapPanel mapPanel = new JXMapPanel();
        // get defaults
        mapPanel.addNotify();
        // test for default position level
        assertGeopositionEquals(new GeoPosition(JXMapPanel.DEFAULT_LATITUDE, JXMapPanel.DEFAULT_LONGITUDE), mapPanel.getCenterPosition());
        // move the map to position 1
        mapPanel.setAddressLocation(testPosition1);
        // store settings
        mapPanel.removeNotify();
        // move the map to position 2
        mapPanel.setAddressLocation(testPosition2);
        // restore settings
        mapPanel.addNotify();
        // position should be on position 1
        assertGeopositionEquals(testPosition1, mapPanel.getCenterPosition());

    }

    //TODO move to MapViewer test
//    @Test
//    public void testZoomToMousePosition() {
//        JXMapPanel mapPanel = mock(JXMapPanel.class);
//        int zoomIn = JXMapPanel.DEFAULT_ZOOM_LEVEL - 1;
//        int zoomOut = JXMapPanel.DEFAULT_ZOOM_LEVEL;
//
//        Point2D mousePosition = new Point2D.Double(75, 25);
//
//        TileFactory tileFactory = mock(TileFactory.class);
//
//        when(mapPanel.getTileFactory()).thenReturn(tileFactory);
//
//        when(tileFactory.geoToPixel(any(GeoPosition.class), anyInt())).thenReturn(mousePosition);
//        when(mapPanel.getZoom()).thenReturn(zoomOut);
//        when(tileFactory.getMapSize(zoomIn)).thenReturn(new Dimension(8192, 8192));
//        when(tileFactory.getMapSize(zoomOut)).thenReturn(new Dimension(4096, 4096));
//
//        when(mapPanel.getViewportBounds()).thenReturn(new Rectangle(0, 0, 200, 150));
//        GeoPosition centerPosition = mock(GeoPosition.class);
//        when(mapPanel.getCenterPosition()).thenReturn(centerPosition);
//        Point2D offset = new Point2D.Double(62.5, 75);
//        final double SCALE = 0.5;
//        when(mapPanel.findOffset(mousePosition, mousePosition, SCALE, SCALE)).thenReturn(offset);
//        doCallRealMethod().when(mapPanel).zoomToMousePosition(zoomIn, mousePosition);
//
//        mapPanel.zoomToMousePosition(zoomIn, mousePosition);
//
//        verify(mapPanel).findOffset(mousePosition, mousePosition, SCALE, SCALE);
//    }

    @Test
    public void testFindOffsetX() {
        JXMapPanel mapPanel = new JXMapPanel();
        Point2D mousePoint1 = new Point2D.Double(25, 75);
        Point2D centerPoint = new Point2D.Double(100, 75);
        double scaleZoomIn = 0.5;
        assertEquals(-37.5, mapPanel.findOffset(mousePoint1, centerPoint, scaleZoomIn, scaleZoomIn).getX(), 0);

        Point2D mousePoint2 = new Point2D.Double(175, 75);
        assertEquals(37.5, mapPanel.findOffset(mousePoint2, centerPoint, scaleZoomIn, scaleZoomIn).getX(), 0);

        double scaleZoomOut = 2;
        
        assertEquals(-150, mapPanel.findOffset(mousePoint1, centerPoint, scaleZoomOut, scaleZoomOut).getX(), 0);
        assertEquals(150, mapPanel.findOffset(mousePoint2, centerPoint, scaleZoomOut, scaleZoomOut).getX(), 0);

    }
    
    @Test
    public void testFindOffsetY() {
        JXMapPanel mapPanel = new JXMapPanel();
        Point2D mousePoint1 = new Point2D.Double(75, 25);
        Point2D centerPoint = new Point2D.Double(75, 100);
        double scaleZoomIn = 0.5;
        assertEquals(-37.5, mapPanel.findOffset(mousePoint1, centerPoint, scaleZoomIn, scaleZoomIn).getY(), 0);

        Point2D mousePoint2 = new Point2D.Double(75, 175);
        assertEquals(37.5, mapPanel.findOffset(mousePoint2, centerPoint, scaleZoomIn, scaleZoomIn).getY(), 0);

        double scaleZoomOut = 2.0;
        
        assertEquals(-150, mapPanel.findOffset(mousePoint1, centerPoint, scaleZoomOut, scaleZoomOut).getY(), 0);
        assertEquals(150, mapPanel.findOffset(mousePoint2, centerPoint, scaleZoomOut, scaleZoomOut).getY(), 0);

    }
    public static final double EPSILON = 0.000001;

    static void assertGeopositionEquals(GeoPosition first, GeoPosition second) {
        assertEquals(first.getLatitude(), second.getLatitude(), EPSILON);
        assertEquals(first.getLongitude(), second.getLongitude(), EPSILON);
    }
}
