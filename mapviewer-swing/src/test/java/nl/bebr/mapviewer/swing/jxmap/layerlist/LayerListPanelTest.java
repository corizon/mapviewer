/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.jxmap.layerlist;

import nl.bebr.mapviewer.swing.jxmap.layerlist.LayerListPanel;
import java.awt.Component;
import java.awt.Dimension;
import static org.junit.Assert.*;
import org.junit.Test;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.ListView;
import org.uispec4j.Panel;
import org.uispec4j.finder.ComponentMatcher;

import org.junit.Ignore;

/**
 *
 * @author Timon Veenstra
 */
@Ignore
public class LayerListPanelTest {

    private final ListViewComponentMatcher listViewMatcher = new ListViewComponentMatcher();

    @Test
    public void testPanel() {
        Panel panel = new Panel(new LayerListPanel(new ExplorerManager()));
        assertTrue(panel.containsComponent(listViewMatcher).isTrue());

        Dimension panelSize = panel.getAwtComponent().getPreferredSize();
        Component listView = panel.findSwingComponent(listViewMatcher);
        Dimension listSize = listView.getPreferredSize();
        assertEquals("width of panel and list should be equal",panelSize.width, listSize.width);
     }

    class ListViewComponentMatcher implements ComponentMatcher {

        @Override
        public boolean matches(Component component) {
            return (component instanceof ListView);
        }
    }
}
