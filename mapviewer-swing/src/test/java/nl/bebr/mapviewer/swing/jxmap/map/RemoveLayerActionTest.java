/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.jxmap.map;

import nl.bebr.mapviewer.swing.jxmap.map.RemoveLayerAction;
import nl.cloudfarming.eventbus.GuiEvent;
import nl.cloudfarming.eventbus.GuiEventBus;
import nl.cloudfarming.eventbus.GuiEventListener;
import static org.junit.Assert.*;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import static org.mockito.Mockito.*;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;

import nl.bebr.mapviewer.api.LayerListRemovableNode;
import nl.bebr.mapviewer.api.event.GeoEvent;
import nl.cloudfarming.eventbus.GuiEventKey;
import org.junit.After;
import org.junit.Before;


/**
 *
 * @author Merijn Zengers
 */
public class RemoveLayerActionTest {

    GuiEventListener<Node> eventListenerMock;
    
    //Test
    public void testFireAction() {
        GuiEventListener<Node> eventListener = new GuiEventListener() {

            @Override
            public void onEvent(GuiEvent ge) {
                System.out.println("onEvent");
            }

            @Override
            public boolean listensTo(GuiEventKey gek) {
                return true;
            }

            @Override
            public String getModuleName() {
                return "mapviewer-swing";
            }
            
        };
        
        GuiEventBus.addListener(eventListener);

        final MockNode mockNode1 = new MockNode();
        final MockNode mockNode2 = new MockNode();

        Node[] activatedNodes = new AbstractNode[]{mockNode1, mockNode2};
        RemoveLayerAction instance = new RemoveLayerAction();
        instance.performAction(activatedNodes);
        
        GuiEventBus.removeListener(eventListener);
    }
    
    /**
     * Test of performAction method, of class RemoveLayerAction.
     */
    //Test
    public void testPerformAction() {
        System.out.println("performAction");

        
        when(eventListenerMock.listensTo(GeoEvent.DISCARD_LAYER)).thenReturn(true);
        when(eventListenerMock.getModuleName()).thenReturn("mapviewer-swing");

        GuiEventBus.addListener(eventListenerMock);

        final MockNode mockNode1 = new MockNode();
        final MockNode mockNode2 = new MockNode();

        Node[] activatedNodes = new AbstractNode[]{mockNode1, mockNode2};
        RemoveLayerAction instance = new RemoveLayerAction();
        instance.performAction(activatedNodes);

        verify(eventListenerMock).onEvent(argThat(new ArgumentMatcher<GuiEvent<Node>>(){

            @Override
            public boolean matches(Object argument) {
                return ((GuiEvent<Node>)argument).getContent().equals(mockNode1);
            }
            
        }));
        verify(eventListenerMock).onEvent(argThat(new ArgumentMatcher<GuiEvent<Node>>(){

            @Override
            public boolean matches(Object argument) {
                return ((GuiEvent<Node>)argument).getContent().equals(mockNode2);
            }
            
        }));
        
        GuiEventBus.removeListener(eventListenerMock);
    }

    /**
     * Test of enable method, of class RemoveLayerAction.
     * FIXME: see comment on enable method
     */
 //   @Test
    public void testEnable() {
        System.out.println("enable");
        MockNode mockNode1 = new MockNode();
        MockNode mockNode2 = new MockNode();

        Node[] activatedNodes = new AbstractNode[]{mockNode1, mockNode2};
        RemoveLayerAction instance = new RemoveLayerAction();
        boolean expResult = true;
        boolean result = instance.enable(activatedNodes);
        assertEquals(expResult, result);

        expResult = false;
        activatedNodes = new AbstractNode[]{mockNode1, mockNode2, new AbstractNode(Children.LEAF)};
        result = instance.enable(activatedNodes);
        assertEquals(expResult, result);
    }

    /**
     * Test of getName method, of class RemoveLayerAction.
     */
    //Test
    public void testGetName() {
        System.out.println("getName");
        RemoveLayerAction instance = new RemoveLayerAction();
        String expResult = Bundle.remove_layer_name();
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getHelpCtx method, of class RemoveLayerAction.
     */
    //Test
    public void testGetHelpCtx() {
        System.out.println("getHelpCtx");
        RemoveLayerAction instance = new RemoveLayerAction();
        HelpCtx expResult = null;
        HelpCtx result = instance.getHelpCtx();
        assertEquals(expResult, result);
    }

    private class MockNode extends AbstractNode implements LayerListRemovableNode {

        public MockNode() {
            super(Children.LEAF);
        }

        @Override
        public Node getRemovableNode() {
            return this;
        }
    }
    
    @Before
    public void setUp() {
        eventListenerMock = mock(GuiEventListener.class);
    }
    
    @After
    public void tearDown() {
        reset(eventListenerMock);
    }
}
