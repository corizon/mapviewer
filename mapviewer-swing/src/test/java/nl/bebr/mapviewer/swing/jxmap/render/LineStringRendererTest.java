/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.jxmap.render;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateSequence;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;

import nl.bebr.mapviewer.swing.render.JXMapGeoTranslator;
import nl.bebr.mapviewer.swing.render.GeoTranslator;
import nl.bebr.mapviewer.swing.render.GeometryRenderer;
import nl.bebr.mapviewer.swing.render.LineStringRenderer;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import static junit.framework.Assert.*;
import org.junit.Test;
import static org.mockito.Mockito.*;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

/**
 * Test LineStringRenderer
 *
 * @author Gerben Feenstra
 */
public class LineStringRendererTest extends MapViewerInitializer {

    // rendered polyline components:
    int[] xPoints;
    int[] yPoints;
    int nPoints;

    /**
     * Test of render method, of class LineStringRenderer.
     */
    //Test 
    public void testRender() {

        // setup line Geometry
        final Coordinate[] coords = new Coordinate[2];
        coords[0] = new Coordinate(3.863702678844967, 51.517975078766305);
        coords[1] = new Coordinate(3.863928677259818, 51.51976716114912);
        CoordinateSequence coordinateSequence = new CoordinateArraySequence(coords);
        LineString  lineString = new LineString(coordinateSequence, new GeometryFactory());

        Graphics2D gd = mock(Graphics2D.class);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                assertEquals(args.length, 3);
                xPoints = (int[])args[0];
                yPoints = (int[])args[1];
                nPoints= (Integer)args[2];
                assertEquals(nPoints, coords.length);
                assertEquals(nPoints, xPoints.length);
                assertEquals(nPoints, yPoints.length);
                return null;
            }
        }).when(gd).drawPolyline(any(int[].class), any(int[].class), anyInt());

        GeoTranslator translator = new JXMapGeoTranslator(mapViewer);
        
        Rectangle canvas = new Rectangle(0 , 0, 11, 134);
        Rectangle result = GeometryRenderer.getViewport(lineString, lineString.getEnvelope(), canvas, translator);

        assertEquals(-2094, result.x);
        assertEquals(872, result.y);
        assertEquals(11, result.width);
        assertEquals(134, result.height);

        new LineStringRenderer().render(lineString, lineString.getEnvelope(), canvas, translator, gd);

        assertEquals(-2094, xPoints[0]);
        assertEquals(1006, yPoints[0]);
        assertEquals(-2083, xPoints[1]);
        assertEquals(872, yPoints[1]);
    }
}
