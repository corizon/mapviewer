/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.jxmap;

import java.awt.Color;
import org.junit.Ignore;
import org.netbeans.api.visual.model.ObjectState;

import nl.bebr.mapviewer.api.Palette;

/**
 *
 * @author Timon Veenstra
 */
@Ignore
public class TestPalette extends Palette {

    public TestPalette() {
        super(Color.BLACK);
    }

    
    
    @Override
    public Color getColorForValue(Object value) {
        return Color.BLACK;
    }

    @Override
    public Color getColorForState(ObjectState state) {
        if (state.isSelected()) {
            return Color.GREEN;
        }
        if (state.isHovered() || state.isObjectHovered()) {
            return Color.RED;
        }
        return Color.BLUE;
    }
}
