/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.render;


import nl.bebr.mapviewer.swing.render.SimpleGeoTranslator;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;

import nl.bebr.mapviewer.data.GeoPosition;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import nl.bebr.nblib.geotools.GeometryTools;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Simple Translator test  
 * @author Merijn Zengers
 */
public class SimpleGeoTranslatorTest {
    
    /**
     * Test of geoToPixel method, of class GeometryRenderer.
     */
    //Test
    public void testGeoToPixel() {
        SimpleGeoTranslator geoTranslator = new SimpleGeoTranslator();
        
        System.out.println("geoToPixel");
        Rectangle canvas = new Rectangle(0, 0, 100, 100);

        Geometry geometry = GeometryTools.wktToGeometry("POLYGON((0.020 0.220, 0.040 0.220, 0.060 0.100,0 0, 0.020 0.220))");
        assertNotNull(geometry);
        assertNotNull(geometry.getCoordinates());
        assertEquals(5, geometry.getCoordinates().length);

        Geometry boundingBox = geometry.getEnvelope();
        Coordinate coordinate = new Coordinate(0.030d, 0.110d);
        // actually 13.5,50,5 but values get truncated to ints
        Point expResult = new Point(13, 50);
        Point2D result = geoTranslator.geoToPixel(canvas, boundingBox, coordinate);
        assertEquals(expResult, result);
        
        // check if all points of the geometry are valid
        Point2D pointA = geoTranslator.geoToPixel(canvas, boundingBox, new Coordinate(0.020d, 0.220d));
       // actually 9.090909091 but values get truncated to ints
        assertEquals(9, pointA.getX(), 0.0);
        assertEquals(0, pointA.getY(), 0);
        Point2D pointB = geoTranslator.geoToPixel(canvas, boundingBox, new Coordinate(0.040d, 0.220d));
        assertTrue(pointB.getX() >= 0);
        assertTrue(pointB.getY() >= 0);
        Point2D pointC = geoTranslator.geoToPixel(canvas, boundingBox, new Coordinate(0.060d, 0.100d));
        assertTrue(pointC.getX() >= 0);
        assertTrue(pointC.getY() >= 0);
        Point2D pointD = geoTranslator.geoToPixel(canvas, boundingBox, new Coordinate(0.0d, 0.0d));
        assertTrue(pointD.getX() >= 0);
        assertTrue(pointD.getY() >= 0);        
    }

    /**
     * Test of geoToPixel method, of class GeometryRenderer. Use a geometry with
     * a non zero base, ie no point 0,0 within the geometry
     */
    //Test
    public void testGeoToPixelWithNonZeroBase() {
        System.out.println("testGeoToPixelWithNonZeroBase");
        SimpleGeoTranslator geoTranslator = new SimpleGeoTranslator();
        Rectangle canvas = new Rectangle(0, 0, 100, 100);

        Geometry geometry = GeometryTools.wktToGeometry("POLYGON((0.120 0.320, 0.140 0.320, 0.160 0.200,0.100 0.100, 0.120 0.320))");
        assertNotNull(geometry);
        assertNotNull(geometry.getCoordinates());
        assertEquals(5, geometry.getCoordinates().length);

        Geometry boundingBox = geometry.getEnvelope();
        Coordinate coordinate = new Coordinate(0.130d, 0.210d);
        // actually 13.5,50,5 but values get truncated to ints
        Point expResult = new Point(13, 50);
        Point2D result = geoTranslator.geoToPixel(canvas, boundingBox, coordinate);
        assertEquals(expResult, result);
        
        // check if all points of the geometry are valid
        Point2D pointA = geoTranslator.geoToPixel(canvas, boundingBox, new Coordinate(0.120d, 0.320d));
        assertTrue(pointA.getX() >= 0);
        assertTrue(pointA.getY() >= 0);
        Point2D pointB = geoTranslator.geoToPixel(canvas, boundingBox, new Coordinate(0.140d, 0.320d));
        assertTrue(pointB.getX() >= 0);
        assertTrue(pointB.getY() >= 0);
        Point2D pointC = geoTranslator.geoToPixel(canvas, boundingBox, new Coordinate(0.160d, 0.200d));
        assertTrue(pointC.getX() >= 0);
        assertTrue(pointC.getY() >= 0);
        Point2D pointD = geoTranslator.geoToPixel(canvas, boundingBox, new Coordinate(0.100d, 0.100d));
        assertTrue(pointD.getX() >= 0);
        assertTrue(pointD.getY() >= 0);        
    }

    /**
     * Test of geoToPixel method, of class GeometryRenderer. Use a geometry with
     * a non zero base, ie no point 0,0 within the geometry
     */
    //Test
    public void testGeoToPixelWithYTransform() {
        System.out.println("testGeoToPixelWithYTransform");
        SimpleGeoTranslator geoTranslator = new SimpleGeoTranslator();
        int canvasHeight = 600;
        int canvaswidth = 1100;

        Rectangle canvas = new Rectangle(0, 0, canvaswidth, canvasHeight);

        Geometry geometry = GeometryTools.wktToGeometry("POLYGON((0.120 0.320, 0.140 0.320, 0.160 0.200,0.100 0.100, 0.120 0.320))");
        assertNotNull(geometry);
        assertNotNull(geometry.getCoordinates());
        assertEquals(5, geometry.getCoordinates().length);

        Geometry boundingBox = geometry.getEnvelope();
        Coordinate coordinate = new Coordinate(0.130d, 0.210d);
        
        //geowidth = 24326.344262344825
        //geoHeight = 24326.344262344825
        //geo ratio should be smaller than canvas ratio so use the y axis
        //factor cavasheight / geoheight : 600 / 24326.344262344825 = 0.02466461846997498
        //Distance from x min to coordinate x = 3339.5624426140525
        //distance from y min to coordinate y = 12163.171271741809
        // multiply the distance x times the factor : 3339.5624426140525 * 0.02466461846997498 = 82.369033504
        //substract the distance y times factor from canvas height : 600 - 12163.171271741809 * 0.02466461846997498 = 300.000021198
        // actually 82.369033504 ,300.000021198 but values get truncated to ints
        Point2D expResult = new Point(82, 300);
        Point2D result = geoTranslator.geoToPixel(canvas, boundingBox, coordinate);
        assertEquals(expResult, result);
        
        // check if all points of the geometry are valid
        Point2D pointA = geoTranslator.geoToPixel(canvas, boundingBox, new Coordinate(0.120d, 0.320d));
        assertTrue(pointA.getX() >= 0);
        assertTrue(pointA.getY() >= 0);
        Point2D pointB = geoTranslator.geoToPixel(canvas, boundingBox, new Coordinate(0.140d, 0.320d));
        assertTrue(pointB.getX() >= 0);
        assertTrue(pointB.getY() >= 0);
        Point2D pointC = geoTranslator.geoToPixel(canvas, boundingBox, new Coordinate(0.160d, 0.200d));
        assertTrue(pointC.getX() >= 0);
        assertTrue(pointC.getY() >= 0);
        Point2D pointD = geoTranslator.geoToPixel(canvas, boundingBox, new Coordinate(0.100d, 0.100d));
        assertTrue(pointD.getX() >= 0);
        assertTrue(pointD.getY() >= 0);
    }

    /**
     * Test of geoToPixel method, of class GeometryRenderer. Use a geometry with
     * a non zero base, ie no point 0,0 within the geometry
     */
    //Test
    public void testGeoToPixelWithXTransform() {
        System.out.println("testGeoToPixelWithXTransform");
        SimpleGeoTranslator geoTranslator = new SimpleGeoTranslator();
        int canvasHeight = 1100;
        int canvaswidth = 200;
        Rectangle canvas = new Rectangle(0, 0, canvaswidth, canvasHeight);

        Geometry geometry = GeometryTools.wktToGeometry("POLYGON((0.120 0.320, 0.140 0.320, 0.160 0.200,0.100 0.100, 0.120 0.320))");
        assertNotNull(geometry);
        assertNotNull(geometry.getCoordinates());
        assertEquals(5, geometry.getCoordinates().length);

        Geometry boundingBox = geometry.getEnvelope();
        Coordinate coordinate = new Coordinate(0.130d, 0.210d);
        
        //geowidth = 6679.1593427398875
        //geoHeight = 24326.344262344825
        //geo ratio should be bigger than canvas ratio so use the y axis
        //factor cavasheight / geoheight : 200 / 6679.1593427398875 = 0.029943888105828168
        //Distance from x min to coordinate x = 3339.5624426140525
        //distance from y min to coordinate y = 12163.171271741809
        //multiply the distance x times the factor : 3339.5624426140525 * 0.029943888105828168 = 99.9994841040614
        //Calculate height difference canvasHeight - geoHeight * factor : 1100 - 24326.344262344825 * 0.029943888105828168 = 371.5746693844915
        //substract the distance y times factor from (canvas height - heightDifference) : (1100 - 371.5746693844915) - 12163.171271741809 * 0.029943888105828168 = 364.212691042
        // actually 99.9994841040614 ,364.212691042 but values get truncated to ints
        Point expResult = new Point(99, 364);
        Point2D result = geoTranslator.geoToPixel(canvas, boundingBox, coordinate);
        assertEquals(expResult, result);

        // check if all points of the geometry are valid
        Point2D pointA = geoTranslator.geoToPixel(canvas, boundingBox, new Coordinate(0.120d, 0.320d));
        assertTrue(pointA.getX() >= 0);
        assertTrue(pointA.getY() >= 0);
        Point2D pointB = geoTranslator.geoToPixel(canvas, boundingBox, new Coordinate(0.140d, 0.320d));
        assertTrue(pointB.getX() >= 0);
        assertTrue(pointB.getY() >= 0);
        Point2D pointC = geoTranslator.geoToPixel(canvas, boundingBox, new Coordinate(0.160d, 0.200d));
        assertTrue(pointC.getX() >= 0);
        assertTrue(pointC.getY() >= 0);
        Point2D pointD = geoTranslator.geoToPixel(canvas, boundingBox, new Coordinate(0.100d, 0.100d));
        assertTrue(pointD.getX() >= 0);
        assertTrue(pointD.getY() >= 0);
    }

    /**
     * Test of geoToPixel method, of class GeometryRenderer. Use a geometry with
     * a non zero base, ie no point 0,0 within the geometry
     */
    //Test
    public void testGeoToPixelWithNegativeGeometryCoordinates() {
        System.out.println("testGeoToPixelWithNegativeGeometryCoordinates");
        SimpleGeoTranslator geoTranslator = new SimpleGeoTranslator();
        int canvasHeight = 600;
        int canvaswidth = 1100;
        Rectangle canvas = new Rectangle(0, 0, canvaswidth, canvasHeight);

        Geometry geometry = GeometryTools.wktToGeometry("POLYGON((-0.120 -0.320, -0.140 -0.320, -0.160 -0.200,-0.100 -0.100, -0.120 -0.320))");
        assertNotNull(geometry);
        assertNotNull(geometry.getCoordinates());
        assertEquals(5, geometry.getCoordinates().length);

        Geometry boundingBox = geometry.getEnvelope();
        Coordinate coordinate = new Coordinate(-0.130d, -0.210d);

        //geowidth = 6679.1593427398875
        //geoHeight = 24326.344262344825
        //geo ratio should be bigger than canvas ratio so use the y axis
        //factor canvaswidth / geowidth : 600 / 24326.344262344825 = 0.02466461846997498
        //Distance from x min to coordinate x = 3339.562442614051
        //distance from y min to coordinate y = 12163.172990603012
        // multiply the distance x times the factor : 3339.562442614051 * 0.02466461846997498 = 82.36903350373328
        //substract the distance y times factor from (canvas height) : (600) - 12163.171271741809 * 0.02466461846997498 = 299.9999788024721
        // actually 82.36903350373328 ,299.9999788024721 but values get truncated to ints
        Point expResult = new Point(82, 299);
        Point2D result = geoTranslator.geoToPixel(canvas, boundingBox, coordinate);
        assertEquals(expResult, result);
        
        // check if all points of the geometry are valid
        Point2D pointA = geoTranslator.geoToPixel(canvas, boundingBox, new Coordinate(-0.120d, -0.320d));
        assertTrue(pointA.getX() >= 0);
        assertTrue(pointA.getY() >= 0);
        Point2D pointB = geoTranslator.geoToPixel(canvas, boundingBox, new Coordinate(-0.140d, -0.320d));
        assertTrue(pointB.getX() >= 0);
        assertTrue(pointB.getY() >= 0);
        Point2D pointC = geoTranslator.geoToPixel(canvas, boundingBox, new Coordinate(-0.160d, -0.200d));
        assertTrue(pointC.getX() >= 0);
        assertTrue(pointC.getY() >= 0);
        Point2D pointD = geoTranslator.geoToPixel(canvas, boundingBox, new Coordinate(-0.100d, -0.100d));
        assertTrue(pointD.getX() >= 0);
        assertTrue(pointD.getY() >= 0);       
    }
    
    //Test
    public void testPixelToGeoScaleDown(){
        SimpleGeoTranslator geoTranslator = new SimpleGeoTranslator();
        Rectangle canvas = new Rectangle(0, 0, 1100, 600);
        
        Geometry geometry = GeometryTools.wktToGeometry("POLYGON((120 320, 140 320, 160 200,100 100, 120 320))");
        assertNotNull(geometry);
        assertNotNull(geometry.getCoordinates());
        assertEquals(5, geometry.getCoordinates().length);
        
        Geometry boundingBox = geometry.getEnvelope();
        Point2D pixel = new Point(550, 300);
        
        GeoPosition position = geoTranslator.pixelToGeo(canvas, boundingBox, pixel);
        
        // b = base or min, 
        // 1 = geo, 
        // 2 = pixel, 
        // f = scaling factor
        // d = delta or canvas widht/height
        //
        // X1 = Xb + (X2 / f) => X1 = 100+(550/2.72) => 301.667
        assertEquals(301.6667, position.getLongitude(), 0.0001);
        // Y1 = Yb + ((y2-yd)/-f) => 100 + ((300-600)/-2.72) => 210.0
        assertEquals(210.0, position.getLatitude(), 0.001);
   }
    
    //Test
    public void testPixelToGeoScaleUp(){
        SimpleGeoTranslator geoTranslator = new SimpleGeoTranslator();
        Rectangle canvas = new Rectangle(0, 0, 1100, 600);
        
        Geometry geometry = GeometryTools.wktToGeometry("POLYGON((1200 3200, 1400 3200, 1600 2000,1000 1000, 1200 3200))");
        assertNotNull(geometry);
        assertNotNull(geometry.getCoordinates());
        assertEquals(5, geometry.getCoordinates().length);
        
        
        Geometry boundingBox = geometry.getEnvelope();
        Point2D pixel = new Point(300, 800);
        GeoPosition position = geoTranslator.pixelToGeo(canvas, boundingBox, pixel);
        // b = base or min, 
        // 1 = geo, 
        // 2 = pixel, 
        // f = scaling factor
        // d = delta or canvas widht/height
        //
        // X1 = Xb + (X2 / f) => X1 = 1000+(300/0.272) => 2100.0
        assertEquals(2100.0, position.getLongitude(), 0.001);
        // Y1 = Yb + ((y2-yd)/-f) => 1000 + ((800-600)/-0.272) => 266.6666
        assertEquals(266.6666, position.getLatitude(), 0.001);
   }
    
    
    
    
}
