/**
 *  Copyright (C) 2008-2022 BEBR. All rights reserved.
 *
 *  AgroSense is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  There are special exceptions to the terms and conditions of the GPLv3 as it
 *  is applied to this software, see the FLOSS License Exception
 *  <http://www.agrosense.eu/foss-exception.html>.
 *
 *  AgroSense is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *  A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  AgroSense. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.bebr.mapviewer.swing.jxmap.render;

import com.vividsolutions.jts.geom.Geometry;

import nl.bebr.mapviewer.swing.render.JXMapGeoTranslator;
import nl.bebr.mapviewer.swing.render.GeoTranslator;
import nl.bebr.mapviewer.swing.render.GeometryRenderer;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import junit.framework.Assert;
import nl.bebr.nblib.geotools.GeometryTools;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 *
 * @author Gerben Feenstra
 */
public class GeometryRendererTest extends MapViewerInitializer {

    /**
     * Test of render method, of class GeometryRenderer.
     */
    //Test
    public void testRender() {
        System.out.println("render");
        Geometry geometry = GeometryTools.wktToGeometry("POLYGON((20 220, 40 220, 60 100, 20 220))");
        assertNotNull(geometry);
        assertNotNull(geometry.getCoordinates());
        assertEquals(4, geometry.getCoordinates().length);
        Graphics2D g = mock(Graphics2D.class);
        GeometryRenderer instance = new GeometryRenderer();
        instance.render(geometry,geometry.getBoundary(),mapViewer.getViewportBounds(), new JXMapGeoTranslator(mapViewer), g);

        verify(g).clip(isA(java.awt.Polygon.class));
        verify(g).fill(isA(java.awt.Polygon.class));
    }
   
    /**
     * Test of getViewport method, of class GeometryRenderer.
     */
    //Test
    public void testGetViewport() {
        System.out.println("getViewport");
        Geometry geometry = GeometryTools.wktToGeometry("MULTILINESTRING((3.863702678844967 51.517975078766305, 3.863928677259818 51.51976716114912))");
        GeoTranslator translator = new JXMapGeoTranslator(mapViewer);
        Rectangle canvas = new Rectangle(0 , 0, 11, 134);
        Rectangle result = GeometryRenderer.getViewport(geometry,geometry.getBoundary(),canvas, translator);

        Assert.assertEquals(-2094, result.x);
        Assert.assertEquals(872, result.y);
        Assert.assertEquals(11, result.width);
        Assert.assertEquals(134, result.height);
    }
    
    
}
